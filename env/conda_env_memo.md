Help with conda env:
https://docs.conda.io/projects/conda/en/latest/user-guide/tasks/manage-environments.html#creating-an-environment-from-an-environment-yml-file

Create conda environement:

```
conda create -n myenv python=3.7 numpy

conda create -n jupyter_george python numpy scipy jupyter plotly pandas networkx matplotlib

conda create -n jupy python=3.7 numpy jupyter plotly biopython pandas scipy matplotlib

conda install -n jupy -c etetoolkit ete3

conda install -c conda-forge -n jupy  py2cytoscape

pip install dna_features_viewer

conda create -n nextflow_cluster  python=3.7 numpy biopython pandas scipy hmmer perl blast

```


Activate a conda env:

`conda activate axe3_local`

Export the current env in a yml file:

`conda env export > conda_environement/genomic_region_identification_local.yml`

Create conda env from a yml file:

`conda env create -f conda_environement/genomic_region_identification_local.yml`


Add bioconda channel:

```

conda config --add channels bioconda

#yconda config --add channels etetoolkity

#conda config --add channels py2cytoscape

```

Install dependency to an env:

`conda install -n axe3_local nextflow`

Load conda on genologin

```

module load system/Anaconda3-5.2.0

module load system/Miniconda3-4.4.10

```

Probleme with perl. version conflict
```
perl: symbol lookup error: /tools/perl5/modules/lib/perl5/x86_64-linux-thread-multi/auto/Cwd/Cwd.so: undefined symbol: Perl_xs_version_bootcheck
```
There is no problem when using default perl on genologin

Removing perl from the conda env, removes blast

so creation of a separate conda blast env
```
 conda remove perl -n nextflow_cluster
 conda create -n nextflow_blast python=3.7 blast

```
