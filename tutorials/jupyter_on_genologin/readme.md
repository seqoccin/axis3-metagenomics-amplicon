# Jupyter_notebook

# Load a conda environment before using jupyter

Python packages that are not part of  the standard library are often usefull in a jupyter notebook. For example panda and plotly packages are very usefull inside a jupyter notebook.

To easily have this packages installed in the environment one solution is to use a conda environment.

See conda doc : https://docs.conda.io/projects/conda/en/latest/user-guide/tasks/manage-environments.html#creating-an-environment-from-an-environment-yml-file

## Creating an environment from an environment.yml file

You can use the conda env recipe that include python packages that can be useful along the use of jupyter notebook.

```bash
conda env create -f env/jupyter_env.yml
conda activate jupy
```

# How to use jupyter notebook on genologin
The avantage of using jupyter notebook directly on genologin is to be able to access easily to any file on genologin rather than downloading it first. However using jupyter notebook on genologin imply to use it on the frontal node (genologin1 or genologin2) which is theoretically forbidden.


Using Jupyter_notebook on genologin requires a small trick to be able to use it remotely. (Instructions are taken from this tuto : https://amber-md.github.io/pytraj/latest/tutorials/remote_jupyter_notebook)

First you need to connect your local computer and genologin with a ssh tunnel.
In your local computer open a terminal and the type.

```bash
ssh -N -f -L localhost:8889:localhost:8889 [YOU ID]@genologin2.toulouse.inra.fr
```
Here you connect the port 8889 of your local computer the port 8889 of genologin2. You can now close this terminal.

In the same or a new terminal, log into genologin2, load the module anaconda to have jupyter notebook in your path and launch jupyter notebook on port 8889.

```bash
ssh [YOU ID]@genologin2.toulouse.inra.fr

module load system/Anaconda3-5.2.0
conda activate jupy
jupyter notebook --no-browser --port=8889
```
