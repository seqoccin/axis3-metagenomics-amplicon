# 1. Assembly database

## Assembly selection

An assembly summary selection is a selection of assemblies from refseq. RefSeq includes a huge number of assemblies and overrepresent few species. Assembly selections are useful as they allow to have a fare representation of species.

It is a tsv file with the following headers `assembly_accession	refseq_category	taxid	species_taxid	organism_name	version_status	assembly_level	genome_rep	ftp_path	excluded_from_refseq	taxonomy`

example:

```tsv
assembly_accession	refseq_category	taxid	species_taxid	organism_name	version_status	assembly_level	genome_rep	ftp_path	excluded_from_refseq	taxonomy
GCF_000010525.1	representative genome	438753	7	Azorhizobium caulinodans ORS 571	latest	Complete Genome	Full	ftp://ftp.ncbi.nlm.nih.gov/genomes/all/GCF/000/010/525/GCF_000010525.1_ASM1052v1		Azorhizobium caulinodans ORS 571 [tax_name];Azorhizobium caulinodans [species];Azorhizobium [genus];Xanthobacteraceae [family];Rhizobiales [order];Alphaproteobacteria [class];Proteobacteria [phylum];Bacteria other [kingdom];Bacteria [superkingdom]
GCF_000009605.1	reference genome	107806	9	Buchnera aphidicola str. APS (Acyrthosiphon pisum)	latest	Complete Genome	Full	ftp://ftp.ncbi.nlm.nih.gov/genomes/all/GCF/000/009/605/GCF_000009605.1_ASM960v1		Buchnera aphidicola str. APS (Acyrthosiphon pisum) [tax_name];Buchnera aphidicola [species];Buchnera [genus];Erwiniaceae [family];Enterobacterales [order];Gammaproteobacteria [class];Proteobacteria [phylum];Bacteria other [kingdom];Bacteria [superkingdom]
GCF_000218545.1	representative genome	593907	11	Cellulomonas gilvus ATCC 13127	latest	Complete Genome	Full	ftp://ftp.ncbi.nlm.nih.gov/genomes/all/GCF/000/218/545/GCF_000218545.1_ASM21854v1		Cellulomonas gilvus ATCC 13127 [tax_name];Cellulomonas gilvus [species];Cellulomonas [genus];Cellulomonadaceae [family];Micrococcales [order];Actinobacteria [class];Actinobacteria [phylum];Bacteria other [kingdom];Bacteria [superkingdom]

```

4 different assembly selections are made by the pipeline and can be found in the folder `assembly_selections/` of the result directory.

### Build an assembly selection

This file is made based on refseq assembly summary:  `ftp://ftp.ncbi.nlm.nih.gov/genomes/refseq/assembly_summary_refseq.txt`
The last column is the taxonomy of the assembly and has been added using the ncbi new tax dump: `ftp://ftp.ncbi.nlm.nih.gov/pub/taxonomy/new_taxdump/new_taxdump.tar.gz`

Let's download these files:

```bash
wget ftp://ftp.ncbi.nlm.nih.gov/genomes/refseq/assembly_summary_refseq.txt
wget ftp://ftp.ncbi.nlm.nih.gov/pub/taxonomy/new_taxdump/new_taxdump.tar.gz

```

We can now format the file `assembly_summary_refseq.txt` to  remove unnecessary columns and to include taxonomy information found in the `new_taxdump.tar.gz`. To do that we use the script `format_assemblies_summary.py`. It takes the taxid of the taxonomic level under study to keep only assemblies that belong to this taxonomic level. For example for Bacteria the taxid is 2.

Here, to have a quicker example let's select assemblies belonging to Archaea. Their taxid is 2157    


```bash
python ../../region_identification_workflow/bin/format_assemblies_summary.py \
                                    assembly_summary_refseq.txt -t 2157 \
                                    -o all_assemblies_with_taxonomy.tsv \
                                    --ncbi_taxdump new_taxdump.tar.gz
```

We have now an assembly selection made of all the assembly of Archaea. We can then create assembly selection.
The script `assembly_selection.py` can make 3 different assembly selections:
* ref_and_rep_assemblies: keep reference and representative assemblies with uniq assembly taxid
* one_assembly_per_sp: keep 1 assembly per species
* rep_ref_and_10_assemblies_per_sp : all reference and representative genomes and up to 10 assemblies per species

Some or all of these selections can be specified as argument of the script (`--assembly_selections`). The script takes also the initial assembly selection with all assemblies.
```    
# 2. Creation of assemblies selection
# 3. file created ref_and_rep_assemblies.tsv one_assembly_per_sp.tsv rep_ref_and_10_assemblies_per_sp.tsv
assembly_selection.py all_assemblies_with_taxonomy.tsv \
                       --assembly_selections ref_and_rep_assemblies one_assembly_per_sp rep_ref_and_10_assemblies_per_sp \
                       -v
```


The scripts creates 3 files : `ref_and_rep_assemblies.tsv`, `one_assembly_per_sp.tsv` and `rep_ref_and_10_assemblies_per_sp.tsv`.

### Visualize assembly selection

We can visualize assembly selection using the tool krona.

To do that we need to format the assembly selection. First we extract assembly accessions from each selection:

```bash
    cut -f1 all_assemblies_with_taxonomy.tsv > all_assemblies.ids
    cut -f1 rep_ref_and_10_assemblies_per_sp.tsv > rep_ref_and_10_assemblies_per_sp.ids
    cut -f1 one_assembly_per_sp.tsv > one_assembly_per_sp.ids
    cut -f1 ref_and_rep_assemblies.tsv > ref_and_rep_assemblies.ids
```


And then format the selection `all_assemblies_with_taxonomy.tsv` with the script `transformed_assembly_summary_bis.py`. This script takes the accessions files previously created and format the selections to include each selection into a uniq file that gonna be digest by a second script `make_krona_xml_bis.py` which will create a xml file that can be read by krona to create the final krona html file.


```
transformed_assembly_summary_bis.py --assemblies_summary_taxonomy all_assemblies_with_taxonomy.tsv \
                                     --dataset_ids all_assemblies.ids \
                                     rep_ref_and_10_assemblies_per_sp.ids \
                                   one_assembly_per_sp.ids \
                                   ref_and_rep_assemblies.ids > formated_assemblies.tsv

make_krona_xml_bis.py formated_assemblies.tsv -v \
                                        --dataset_labels all_assemblies rep_ref_and_10_assemblies_per_sp one_assembly_per_sp ref_and_rep_assemblies\
                                         > assemblies.xml

# 4. Run krona on xml file
ktImportXML assemblies.xml  -o assemblies_all_selections.html
```

# 2. Downloading assemblies

Let's download from scratch the assemblies of a given selection. To do so we use the script `get_ncbi_assemblies.py`.
It takes an assembly selection as argument.

```bash

get_ncbi_assemblies.py ref_and_rep_assemblies.tsv -v
```

It create a directory `ncbi_data` where assemblies have been downloaded.
