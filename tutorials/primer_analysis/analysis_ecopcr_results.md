# Process ecoPCR results

## Overview of ecoPCR result

An ecoPCR result file starts with a bunch of header lines starting with an `#`.
For example for the primers of the 16S v3-v4  we have these headers lines:
```
#@ecopcr-v2
#
# ecoPCR version 1.0.1
# direct  strand oligo1 : ACGGRAGGCAGCAG                   ; oligo2c :              AGGATTAGATACCCTGGTA
# reverse strand oligo2 : TACCAGGGTATCTAATCCT              ; oligo1c :                   CTGCTGCCTYCCGT
# max error count by oligonucleotide : 3
# optimal Tm for primers 1 :  -nan
# optimal Tm for primers 2 : 49.92
# database : ecoPCR_db/16S-23S/
# amplifiat length between [100,600] bp
# output in superkingdom mode
# DB sequences are considered as linear
#

```

Then the file is composed of a line per amplicon found with different information about the amplicon.

Example of a result line:
```
GCF_001402875.1|NZ_C9 |   3724841 |     1079 | species              |     1079 | Blastochloris viridis          |    59282 | Blastochloris                  |    45401 | Hyphomicrobiaceae              |        2 | Bacteria                       | D | ACGGGAGGCAGCAG                   |  0 |  -nan | TACCAGGGTATCTAATCCT              |  0 | 49.92 |   401 | TGGGGAATATTGGACAATGGGCGCAAGCCTGATCCAGCCATGCCGCGTGAGTGATGAAGGCCTTAGGGTTGTAAAGCTCTTTCGACGGGGAAGATAATGACGGTACCCGTAGAAGAAGCCCCGGCTAACTTCGTGCCAGCAGCCGCGGTAATACGAAGGGGGCTAGCGTTGCTCGGATTTACTGGGCGTAAAGCGCACGTAGGCGGGTCGTTAAGTCGGGGGTGAAATCCCGGAGCTCAACTCCGGAACTGCCTTCGATACTGGCGACCTCGAGTCCGGGAGAGGTGAGTGGAACTCCGAGTGTAGAGGTGAAATTCGTAGATATTCGGAAGAACACCAGTGGCGAAGGCGGCTCACTGGCCCGGTACTGACGCTGAGGTGCGAAAGCGTGGGGAGCAAAC |  Blastochloris viridis strain ATCC 19567, complete genome

```

To count the number of amplicon sequences found per primer pairs, we can use a simple bash command `grep  ^'#' -v ecopcr_results/*ecopcr -c`:

```
ecopcr_results/16S-23S_l3000_L7000_e3.ecopcr:73299
ecopcr_results/16Sv3v4_l100_L600_e3.ecopcr:78866
ecopcr_results/full16S_l1000_L2000_e3.ecopcr:72754

```

In the database we have 72383 sequences (`grep -c ^'>' ecoPCR_db/16S-23S.fna`). So we have more amplicons sequences than sequences in the database meaning that there are more than one amplicon per sequences..

It is maybe due to the high mismatches parameter (e=3) used to run ecopcr. And also the sequences of 16S-23S are maybe not that clean and may contained sometime two 16S and a 23S..

It required some investigation...

>> investigation with a jupyter notebook and some genomic plot?


## Compute taxonomic coverage

### Using ecotaxstat tool

Obitool package has a special tool to do that [ecotaxstat](https://pythonhosted.org/OBITools/scripts/ecotaxstat.html#module-ecotaxstat):
ecotaxstat required the database used to launch ecoPCR (`-d`) and a taxid to focus the coverage on a part of the taxonomy. In order to compute the taxonomic coverage over the Bacteria, we will use `-r 2` because 2 is the taxids of the superkingdom Bacteria.

Command line example to analyse the coverage over all Bacteria:

```bash
module purge # clean modules
module load bioinfo/obitools-v1.2.11
db=ecoPCR_db/16S-23S

# 16S-23S
ecotaxstat -d $db  -r 2 ecopcr_results/16S-23S_l3000_L7000_e3.ecopcr > ecopcr_results/16S-23S_l3000_L7000_e3.ecotaxstat

# 16S full
ecotaxstat -d $db ecopcr_results/full16S_l1000_L2000_e3.ecopcr -r 2  > ecopcr_results/full16S_l1000_L2000_e3.ecotaxstat


# 16S v3 v4
ecotaxstat -d $db ecopcr_results/16Sv3v4_l100_L600_e3.ecopcr -r 2  > ecopcr_results/16Sv3v4_l100_L600_e3.ecotaxstat

```
> ecotaxstat can take quite some time because it has to go through the whole database first.


ecotaxstat result is straightforward to read. It consists of 3 columns:
* rank : the different possible ranks (ie: species, genus, phylum...)
* ecopcr: the number of different taxons represented at a given rank in ecopcr result
* db: the number of different taxons represented at a given rank in the database
* percent: the percentage of the taxon represented in ecopcr result compare to the database

Example of result for the the 16S full :
```
rank                	    ecopcr	        db	percent
class               	        74	        75	   98.67
family              	       378	       393	   96.18
genus               	      1596	      1829	   87.26
order               	       167	       170	   98.24
phylum              	        35	        35	  100.00
species             	      4380	      5378	   81.44
species group       	        44	        44	  100.00
species subgroup    	         6	         6	  100.00
subclass            	         2	         2	  100.00
subfamily           	         1	         1	  100.00
subgenus            	         1	         1	  100.00
suborder            	         6	         6	  100.00
subphylum           	         1	         1	  100.00
subspecies          	        81	        85	   95.29
superkingdom        	         1	         1	  100.00
tribe               	         2	         2	  100.00
```

The taxonomic coverage at the species level:
* 16S-23S: 99.70%
* 16S full: 99.73%
* 16S V3-V4: 99.88%

The coverage is high and comparable between the 3 primer pairs.

>> script to plot and analyse the result by taking into account mismatches JUPYTER/PYTHON SCRIPT

## Compute taxonomic resolution

Goal: evaluate how well the targeted sequences are able to discriminate different species.

The main idea here, is to cluster sequences based on their similarity and then to analyse the clusters to identify unambiguously identified species. An unambiguously identified species is a species that is never found in a cluster with other species, meaning that the target sequences of this species is different enough from the other sequences of the other species.  
The **taxonomic resolution** is the percentage of species that are unambiguously identified by the targeted sequences.

### Sequences preprocessing

Computing taxoomic resolution with ecotaxspecificity tool does not required any preprocessing of the ecoPCR result files. But this step is necessary for 'the homemade workflow strategy' and 'the clustering approach'. 

Preprocessing consist of converting ecoPCR results into fasta files. This section also cover the use and creation of different assembly selection to be able to analyse the sequences into different broad of assembly selections. 


To convert ecoPCR files into fasta files, we'll use the script  `primer_design/scripts/from_ecopcr_to_fasta.py`

```
python primer_design/scripts/from_ecopcr_to_fasta.py ecopcr_results/*ecopcr -o amplicon_sequences -v
```

We have the amplicon sequences of the 3 primers.


Another thing we could do is to add different assembly selections into the analysis. We could then have the taxonomic resolution associated with our primers based on different assembly selections.

Our ecoPCR database consist of 16S-23S sequences extracted based on the **rep_ref_and_10_assemblies_per_sp** selection.  
First we need to make a new assembly selection featuring assemblies of the **rep_ref_and_10_assemblies_per_sp** selection that have at least one 16S-23S sequence.  We call this selection **rep_ref_and_10_assemblies_per_sp_with_16S-23S**.

First we make the list of assemblies that have at least one 16S-23S sequence `assemblies_with_16S-23S.list`. And we then used it to subsamble the original assembly selection **rep_ref_and_10_assemblies_per_sp** to create the new one **rep_ref_and_10_assemblies_per_sp_with_16S-23S** using the script `assembly_list_to_assembly_summary_file.py`.



```bash
mkdir assembly_selections
grep ^'>' ecoPCR_db/16S-23S.fna | cut -f2 -d'>' | cut -f1 -d'|' | sort | uniq > assembly_selections/assemblies_with_16S-23S.list

# symb link of the original selection
ln -s /work2/project/seqoccin/metaG/marker_identification/region_identification_workflow/bacteria_investigation/results_refseq_2020-06-09/assembly_selections/rep_ref_and_10_assemblies_per_sp.tsv assembly_selections/

python ../../region_identification_workflow/16S_investigation/assembly_list_to_assembly_selection.py --assembly_summary assembly_selections/rep_ref_and_10_assemblies_per_sp.tsv \
                                                  --assembly_list  assembly_selections/assemblies_with_16S-23S.list \
                                                  --out_assembly_summary  assembly_selections/rep_ref_and_10_assemblies_per_sp_with_16S-23S.tsv \
                                                  -v

```

To create another selection for example based on **one_assembly_per_sp** but with assemblies that have a 16S-23S sequence, we can rerun the script `assembly_list_to_assembly_selection.py` but using as initial assembly selection **one_assembly_per_sp**.

```bash
# symb link of the original selection
ln -s /work2/project/seqoccin/metaG/marker_identification/region_identification_workflow/bacteria_investigation/results_refseq_2020-06-09/assembly_selections/one_assembly_per_sp.tsv assembly_selections/

python ../../region_identification_workflow/16S_investigation/assembly_list_to_assembly_selection.py --assembly_summary assembly_selections/one_assembly_per_sp.tsv \
                                                  --assembly_list  assembly_selections/assemblies_with_16S-23S.list \
                                                  --out_assembly_summary  assembly_selections/one_assembly_per_sp_with_16S-23S.tsv \
                                                  -v
```
Some warnings will be thrown by the script. The script does not found all the assembly from the list into the initial assembly selection. No worries it is normal as we want to subsample assemblies.
Finally we can do the same with the selection **ref_and_rep_assemblies**:

```bash
# symb link of the original selection
ln -s /work2/project/seqoccin/metaG/marker_identification/region_identification_workflow/bacteria_investigation/results_refseq_2020-06-09/assembly_selections/ref_and_rep_assemblies.tsv assembly_selections/

python ../../region_identification_workflow/16S_investigation/assembly_list_to_assembly_selection.py \
                                                  --assembly_summary assembly_selections/ref_and_rep_assemblies.tsv \
                                                  --assembly_list  assembly_selections/assemblies_with_16S-23S.list \
                                                  --out_assembly_summary  assembly_selections/ref_and_rep_assemblies_with_16S-23S.tsv \
                                                  -v
```

The next step is to subsamble our sequences in order to have sequences corresponding to each selection. To do so we use the  script `filter_sequences_based_on_assemblies.py`. The script stores filtered sequences in a sub directory named after the selection. To be consistent, we will move our initial sequences based on the selection **rep_ref_and_10_assemblies_per_sp_with_16S-23S** into a directory of the same name.

```bash
# mv initial seq into rep_ref_and_10_assemblies_per_sp_with_16S-23S sub dir
mkdir amplicon_sequences/rep_ref_and_10_assemblies_per_sp_with_16S-23S
mv amplicon_sequences/* amplicon_sequences/rep_ref_and_10_assemblies_per_sp_with_16S-23S

# subsample sequences

# selection one_assembly_per_sp_with_16S-23S.tsv
python ../../region_identification_workflow/tax_resolution_test/filter_sequences_based_on_assemblies.py \
        --assembly_selection assembly_selections/one_assembly_per_sp_with_16S-23S.tsv \
        -s amplicon_sequences/rep_ref_and_10_assemblies_per_sp_with_16S-23S/* \
        -o amplicon_sequences/ -v

# selection ref_and_rep_assemblies_with_16S-23S.tsv
python ../../region_identification_workflow/tax_resolution_test/filter_sequences_based_on_assemblies.py \
        --assembly_selection assembly_selections/ref_and_rep_assemblies_with_16S-23S.tsv \
        -s amplicon_sequences/rep_ref_and_10_assemblies_per_sp_with_16S-23S/* \
        -o amplicon_sequences/ -v

```

### With ecotaxspecificity tool

The tool [ecotaxspecificity](https://pythonhosted.org/OBITools/scripts/ecotaxspecificity.html) of the obitool package computes the taxonomic resolution.

ecotaxspecificity required the database used to launch ecoPCR (`-d`).  
It also takes the number of differences between two sequences to consider them different (`-e`).
>> Two sequences are considered as different if they have INT or more differences (default: 1).

example of command for 16Sv3v4
```
module load bioinfo/obitools-v1.2.11
ecotaxspecificity -d ecoPCR_db/16S-23S -e 1 --ecopcr ecopcr_results/16Sv3v4_l100_L600_e3.ecopcr > 16Sv3v4_l100_L600_e3.ecotaxspecificity  
```
However this tool takes a lot of time and has a poor memory management (around 5 hours with e=1 and 78879 amplicon sequence to deal with).

### With a home made nextflow pipeline
The workflow can be found in the directory `taxonomic_resolution/workflow/` of the project. 
It has the same principle of ecotaxspecificity but it is muche faster and is more flexibility.

Sequences are clustered based on two possible metrics:
* number of differences (such as ecotaxspecificity)
* percentage of identity

Keep in mind that the length of the sequences has no influence on the clustering when it is the percentage of identity that is used.

#### How does the workflow work? 
For given sequences of a region: 
It first performs a blast all vs all, then by parsing blast results identify pairs of sequences that are similar enough to undergo global alignment. 
By default similar enough means coverage > 90% and identity percentage > 90%. 
Global alignment step allow to compute precise distances between the pairs. It is a very costly step and is performed with the edlib. 
The workflow then group progressively sequences using identity thresholds from 100% down to 90% and count species that are not unambiguously identified. This feature allows to have the taxonomic resolution in function of identity percentage or number of differences. 
Finally the workflow plots taxonomic resolutions of the different regions. 

#### Quick test of the workflow

First we need to convert ecoPCR files into fasta files. To do that we'll use the script   `primer_design/scripts/from_ecopcr_to_fasta.py`. 
```
python primer_design/scripts/from_ecopcr_to_fasta.py ecopcr_results/*ecopcr -o amplicon_sequences -v
```
We have the amplicon sequences of the 3 primers.


To have the workflow it is necessary to have the required tool and package installed. To make this step easy a conda environement has been created and the yaml recipe is in the `env` directory of the workflow  [tax_resolution_workflow.yml](taxonomic_resolution/workflow/env/tax_resolution_workflow.yml).
You can create the environement on your side or let nextflow take care of this part using the profile conda in the nextflow command `-profile conda`. 

The workflow requires two arguments:

* `--seq_dir`: sequence direcorty. In our case we'll use the 

* `--assembly_summary`: assembly selection file with info and taxonomy of each assembly. 


```bash 
module load system/Anaconda3-5.2.0
module load bioinfo/nfcore-Nextflow-v20.01.0
basedir="~/metaG/marker_identification/" # path the root of the git directory. 


nextflow $basedir/taxonomic_resolution/workflow/main.nf -profile conda,cluster \
                                                        --seq_dir amplicon_sequences/one_assembly_per_sp_with_16S-23S/ \
                                                        --assembly_summary assembly_selections/one_assembly_per_sp_with_16S-23S.tsv

nextflow ../../taxonomic_resolution/workflow/main.nf --seq_dir amplicon_sequences/one_assembly_per_sp_with_16S-23S/ --assembly_summary assembly_selections/one_assembly_per_sp_with_16S-23S.tsv -profile conda,cluster
```

### With a clustering approach

Using a  tool (swarm, cd-hit... ) to cluster sequences according their sequences similarity. Clusters are then analyzed to identify unambiguously identified species (or any other rank.. ).


#### Clustering with swarm

Swarm github: https://github.com/torognes/swarm

Swarm fails when ambiguous nucleotides are found in sequences.
We can remove those sequences with the script : `remove_ambiguous_sequences.py`


```bash
# Sequences of selection rep_ref_and_10_assemblies_per_sp_with_16S-23S
python ../../region_identification_workflow/clustering_approaches/remove_ambiguous_sequences.py\
         -f amplicon_sequences/rep_ref_and_10_assemblies_per_sp_with_16S-23S/*\
                 -o amplicon_sequences/rep_ref_and_10_assemblies_per_sp_with_16S-23S/\
                          -v



# Sequences of selection one_assembly_per_sp_with_16S-23S
python ../../region_identification_workflow/clustering_approaches/remove_ambiguous_sequences.py\
                             -f amplicon_sequences/one_assembly_per_sp_with_16S-23S/*\
                              -o amplicon_sequences/one_assembly_per_sp_with_16S-23S/\
                               -v

# Sequences of selection ref_and_rep_assemblies_with_16S-23S
python ../../region_identification_workflow/clustering_approaches/remove_ambiguous_sequences.py         -f amplicon_sequences/ref_and_rep_assemblies_with_16S-23S/*         -o amplicon_sequences/ref_and_rep_assemblies_with_16S-23S/         -v

```
On verbose mode (`-v`) the script gives the count of ambiguous and unambiguous sequences found in each file:
```bash
INFO: amplicon_sequences/one_assembly_per_sp_with_16S-23S/16S-23S_l3000_L7000_e3.fna: sequences 42762
INFO: amplicon_sequences/one_assembly_per_sp_with_16S-23S/16S-23S_l3000_L7000_e3.fna: ambiguous 1927
INFO: amplicon_sequences/one_assembly_per_sp_with_16S-23S/16S-23S_l3000_L7000_e3.fna: unambiguous 40835
INFO: amplicon_sequences/one_assembly_per_sp_with_16S-23S/16Sv3v4_l100_L600_e3.fna: sequences 45825
INFO: amplicon_sequences/one_assembly_per_sp_with_16S-23S/16Sv3v4_l100_L600_e3.fna: ambiguous 92
INFO: amplicon_sequences/one_assembly_per_sp_with_16S-23S/16Sv3v4_l100_L600_e3.fna: unambiguous 45733
INFO: amplicon_sequences/one_assembly_per_sp_with_16S-23S/full16S_l1000_L2000_e3.fna: sequences 42406
INFO: amplicon_sequences/one_assembly_per_sp_with_16S-23S/full16S_l1000_L2000_e3.fna: ambiguous 386
INFO: amplicon_sequences/one_assembly_per_sp_with_16S-23S/full16S_l1000_L2000_e3.fna: unambiguous 42020

```
The script creates in the out directory a file with ambiguous sequences (prefixed with  `ambiguous_seq_`) and and file with unambiguous sequences (prefixed with `no_ambiguous_seq_`).

We can check the correctness of the script with some bash command that count ambiguous seq in the original file:
```
# count ambiguous seq in the original file
grep -E '^[^>].*(S|M|K|R|Y|B|D|H|V|W|N|Z).*' amplicon_sequences/one_assembly_per_sp_with_16S-23S/16S-23S_l3000_L7000_e3.fna -c
# 1927


# count seq  in ambiguous seq file
grep ^'>' amplicon_sequences/one_assembly_per_sp_with_16S-23S/ambiguous_seq_16S-23S_l3000_L7000_e3.fna -c
# 1927
```



A swarm command look like this : `swarm -a 1 file > result.cluster 2> result.log`. Here we have to use `-a 1 ` which tells swarm that each sequence has a abundnace of 1. Otherwise we should dereplicate sequences first and add abundance in the fasta header as explained in swarm github. However our sequences are not real PCR product amplicons and thus all sequences even the identical one correspond to a uniq sequences in genomes.

We can generate sarray command to launch easily swarm on the cluster.

```bash
echo '# swarm sarray commands' > swarm.sarray


for seqdir in amplicon_sequences/*;
    do

    dir_name=$(basename -- "$seqdir")
    result_dir="swarm/$dir_name/"
    mkdir -p $result_dir

    for f in $seqdir/no_ambiguous_seq_*;
        do  
        echo $f
        filename=$(basename -- "$f")
        filesimplename="${filename%.*}"
        echo "GIVE//" $filesimplename
        echo "module load bioinfo/swarm-2.2.2;swarm -a 1  $f >  $result_dir/${filesimplename}.cluster 2> $result_dir/${filesimplename}.log" >>  swarm.sarray

    done
done

```

We can now launch the sarray with `sarray swarm.sarray`.


##### Process swarm results

To process swarm result we will use the script `process_swarm_results.py`.

```bash
python ../../region_identification_workflow/clustering_approaches/process_swarm_results.py -o taxonomic_resolution/swarm/rep_ref_and_10_assemblies_per_sp_with_unambiguous_rrn/clusters_stat.tsv --assembly_selection assembly_selections/rep_ref_and_10_assemblies_per_sp_with_unambiguous_rrn.tsv --results taxonomic_resolution/swarm/rep_ref_and_10_assemblies_per_sp_with_unambiguous_rrn/*cluster --sequences amplicon_sequences/rep_ref_and_10_assemblies_per_sp_with_unambiguous_rrn/* -v

```

#### Clustering with cd-hit

#### Cluster analysis.

