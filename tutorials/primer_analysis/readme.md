# Primer analysis

## Context
We have a set of primer couples targeting different region in a set of genomes.
We want to evaluate and compare taxonomic coverage and resolution of these couples.

## Tutorial parts
The turorial is divided into two parts

1. [launch_ecopcr.md](launch_ecopcr.md): In this part primer couples are collected, ecoPCR databases are made and finally ecoPCR is launched.
2. [analysis_ecopcr_results.md](analysis_ecopcr_results.md): In this part the ecopcr results are analysed. The taxonomic coverage and the taxonomic resolution is assessed.

## Before to start


### Use an srun on genologin

WARNING: On genologin, you need to be in srun to run commands directly in the terminal.  
```
# Basic srun with default RAM and cpu
srun --pty bash

# srun with personalized RAM and cpu
srun --pty --mem 20G -c 4 bash
```
### Add scripts directory to PATH

In order to use the scripts easily along the tutorial, let's include them in the PATH variable.

Go to the root of the git repository and do:
```bash
PATH=$PATH:$PWD/region_identification_workflow/bin
PATH=$PATH:$PWD/primer_design/scripts
PATH=$PATH:$PWD/scripts
```
