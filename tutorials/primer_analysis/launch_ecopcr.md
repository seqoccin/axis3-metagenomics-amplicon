# 1. Primers
Here we will use 16S primers.

We will compare a primer couple targeting the full 16S rRNA gene, another primer couple targeting the variable regions V3 V4 of the 16S rRNA gene and a couple targeting the 16S-23S region.

**Couple targeting the full 16S rRNA gene:**

* Forward primer 27F: `AGRGTTTGATYHTGGCTCAG`
* Reverse primer 1492R: `TACCTTGTTAYGACTT`

> From Cuscó A, Catozzi C, Viñes J, Sanchez A, Francino O. Microbiota profiling with long amplicons using Nanopore sequencing: full-length 16S rRNA gene and the 16S-ITS-23S of the  rrn operon. F1000Res. 2018;7:1755. Published 2018 Nov 6. doi:10.12688/f1000research.16817.2 (https://pubmed.ncbi.nlm.nih.gov/30815250/)

**Couple targeting the full 16S-23S rRNA genes:**
* Forward primer 27F: `AGRGTTTGATYHTGGCTCAG`
* Reverser primer 2241R: `ACCRCCCCAGTHAAACT`

> From Cuscó A, Catozzi C, Viñes J, Sanchez A, Francino O. Microbiota profiling with long amplicons using Nanopore sequencing: full-length 16S rRNA gene and the 16S-ITS-23S of the  rrn operon. F1000Res. 2018;7:1755. Published 2018 Nov 6. doi:10.12688/f1000research.16817.2 (https://pubmed.ncbi.nlm.nih.gov/30815250/)

**Couple targeting the 16S V3-V4:**

* Forward primer: `ACGGRAGGCAGCAG`
* Reverse primer: `TACCAGGGTATCTAATCCT`


# 2. ecoPCR

ecoPCR simulates an in silico PCR. Documentation can be found at https://pythonhosted.org/OBITools/scripts/ecoPCR.html

## 2.1 create an ecoPCR database
ecoPCR needs a formatted database containing the sequences against it will search potential amplicons (argument `-d`). The database needs to be created by the tool obiconvert which is part of the [OBITools package](https://pythonhosted.org/OBITools/scripts/obiconvert.html).

Obiconvert takes a fasta file containing sequences. These sequences are required to have in their header the taxid. On top of that, obiconvert needs the NCBI Taxonomy dump repository.

**What sequences should we put in the database?**

We have different possibility here:
* we can use a database made of genome: in this case ecoPCR will search potential amplicon anywhere in genomes.
* We can use a database made of sequences of the targeted regions. In our case we could use 16S sequences or even sequences of the rrn operon to be able to evaluate the three primers on the same database.



### 2.1.1 create an ecoPCR database made of target sequences

Exemple with the 16S-23S seqeunces:

We will use sequences of 16S-23S that have been extracted by the pipeline 'region_identification'.
Sequence files of regions can be found in the folder `extracted_regions`.  
Path to `16S-23S.fna` file: `/work2/project/seqoccin/metaG/marker_identification/region_identification_workflow/bacteria_investigation/results_refseq_2020-06-09/extracted_regions/16S-23S.fna`

> Note that 16S sequences are stored in the folder `extracted_genes` as the sequences are only made of one gene.

First we will format this fasta file to add the taxid of the sequences into their header.  For that we will use the script `primer_design/scripts/format_sequences.py`

> To know script arguments, you can run the command: `format_sequences.py -h`

We will need an assembly selection that gather information on each assembly. Assembly selections are created by the pipeline and are stored in the folder `assembly_selections`. More info [here](assembly_selections.md).

The sequences we are using have been extracted based on the extended_assemblies_selection of the pipeline which in our case `rep_ref_and_10_assemblies_per_sp`. So we will use this selection to format and add taxid to our sequences header.
>To know which selections have been used along the pipeline execution, you can check the file `workflow_parameters.txt` stored in the result directory of the pipeline: `/work2/project/seqoccin/metaG/marker_identification/region_identification_workflow/bacteria_investigation/results_refseq_2020-06-09/workflow_parameters.txt`

```bash
assembly_selection=/work2/project/seqoccin/metaG/marker_identification/region_identification_workflow/bacteria_investigation/results_refseq_2020-06-09/assembly_selections/rep_ref_and_10_assemblies_per_sp.tsv

sequences=region_identification_workflow/bacteria_investigation/results_refseq_2020-06-09/extracted_regions/16S-23S.fna

output=ecoPCR_db/16S-23S.fna

format_sequences.py $sequences --assembly_selection $assembly_selection -o $output  -v
```
ecoPCR tronc and rename partially sequence ids with annoying symbol that can caused trouble when parsing it. To prevent any issues, the formatting script rename seq ids with a simple pattern `<assembly_accession>|s<sequence number>`. The original header is added at the end of the new one after the taxid.

For example the first sequence of the assembly `GCF_013282625.1`:

```fasta
>GCF_013282625.1|s000 taxid=2739063; GCF_013282625.1|NZ_CP054020.1|332644-337697 16S|23S rna-HQN79_RS01505|rna-HQN79_RS01520
CTAGCAAGATTAAACTGAAGAGTTTGATCCTGGCTCAGAATGAACGCTGGCGGCAGGCCT
AACACATGCAAGTCGGACGGAAACGATAGAGAGCTTGCTCTCTAGGCGTCGAGTGGCGGA
[...]
```
To keep track to this change, a file is created in the output directory, name as follow `<name of the output file>_map.ids`.


obiconvert needs the NCBI taxonomic dump directory. This archive of this directory is downloaded by the pipeline and is stored in the directory  `ncbi_data/<date_of_downloa>24_refseq_summary` located at the root of the pipeline architecture as it can be used multiple time.. It is important to use the same taxonomy files as the pipeline.. The refseq files are named after the date of download of this file and the one used by the pipeline can be found in the file `workflow_parameters.txt`  stored in the pipeline result directory.

In our case we use refseq file downloaded on the 2019-10-23, so the corresponding taxonomic dump is : `/work2/project/seqoccin/metaG/marker_identification/region_identification_workflow/ncbi_data/2020-06-09_refseq_summary/new_taxdump.tar.gz`

It needs to be uncompressed.

```bash
tax_dumb=/work2/project/seqoccin/metaG/marker_identification/region_identification_workflow/ncbi_data/2020-06-09_refseq_summary/new_taxdump.tar.gz
mkdir ncbi_tax_dumb/2020-06-09/
tar xvzf $tax_dumb --directory ncbi_tax_dumb/2020-06-09/
```

Now that we have everything, we can build the ecoPCR database, using obiconvert.

```bash
module load bioinfo/obitools-v1.2.11
obiconvert --fasta ecoPCR_db/16S-23S.fna  --ecopcrdb-output=ecoPCR_db/16S-23S -t ncbi_tax_dumb/2020-06-09/
module purge # obitools module can be a bit invasive...
```
We have now an ecoPCR database made of 16S-23S sequences: `ecoPCR_db/16S-23S`

### 2.1.2 create an ecoPCR database made of assemblies

First, assemblies need to be formatted similarly as target sequences (see above more details) and then to be concatenated to make a database with more than one assembly. Depending on how many assemblies we want in the database, it can be interesting to split assemblies into several chunks to not have to manage a very large file containing all assembly sequences.

Here, to illustrate the process, we will make a database with the assemblies of the selection `ref_and_rep_assemblies`. This selection, based on refseq date back to 2020-06-09, includes 11072 assemblies.

Let's split the assembly selection file into 10 chunks (to have around 1100 assemblies/chunk) and create a list of assembly accession for each chunk.

```bash
nb_chunk=10
mkdir assembly_chunks
tail -n +2 assembly_selections/ref_and_rep_assemblies.tsv | cut -f1 > assembly_selections/ref_and_rep_assemblies_acc_list.txt
# split into 40 chunks more or less 1000 seq per chunk..
split -d --number=l/$nb_chunk assembly_selections/ref_and_rep_assemblies_acc_list.txt assembly_chunks/ref_and_rep_assemblies.acc_list_chunk.
```
To merger and format assemblies we'll use the script `merge_and_format_assembly.py`

It takes as arguments:
* `assembly_list_to_keep`: the list of assembly accession to format and merged.
* `--assembly_root_dir`: the root dir where assemblies are stored, in our case it is `/work2/project/seqoccin/metaG/ncbi_data/assemblies/`
* `--assembly_selection`: which is the tsv file with information on each assembly of the selection. It uses the script to add the taxid of the assembly in the fasta header.
* `-o`: the output file with formatted and merged assemblies.

To test the command, let's format and merge the 50 first assembly of the first chunk `assembly_chunks/ref_and_rep_assemblies.acc_list_chunk.00`
```bash
mkdir test
head -n50 assembly_chunks/ref_and_rep_assemblies.acc_list_chunk.00 > test/chunk_test.acc_list

merge_and_format_assembly.py \
                    test/chunk_test.acc_list \
                    --assembly_root_dir /work2/project/seqoccin/metaG/ncbi_data/assemblies/ \
                    --assembly_selection assembly_selections/ref_and_rep_assemblies.tsv \
                    -o test/test_chunk.fna -v

```

Assemblies sequences have been merged and formatted, we can now launch obiconvert to create the database:

As before we use the `ncbi_tax_dumb/2020-06-09/`.

```bash
module load bioinfo/obitools-v1.2.11;
obiconvert --fasta test/test_chunk.fna --ecopcrdb-output=test/test_chunk -t ncbi_tax_dumb/2020-06-09/

```
After using obitool it can be very useful to run `module purge` to remove it from the environnement.
To have python3 back we can do `module load system/Python-3.7.4`.



Simple bash loop to generate commands to format, merge and build the database for each chunk of assemblies :

WARNING! If these commands are executed using sarray, the python script  `merge_and_format_assembly.py` will no longer be in the PATH variable. It it is then necessary to give the path to the python script when building the commands.
The script `merge_and_format_assembly.py` is located in the dir `primer_design/scripts/` of the git repository. In the following bash script you need to give the correct path (absolute or relative) to the script.


```bash

out_db='ecoPCR_db/assemblies/'
mkdir -p $out_db
echo '# build ecopcr db on assembly chunk' > build_assemblies_ecopcr_db.sarray  
for chunk_file in assembly_chunks/*chunk*;
do
    command=''
    # parsing chunk file:
    # chunk file has to follow this pattern: <assembly_selection_name>.acc_list_chunk.<chunk_id>
    # exemple: ref_and_rep_assemblies.acc_list_chunk.00
    file_name=$(basename -- "$chunk_file")
    db_name="${file_name%.*.*}"
    chunk_id="${chunk_file##*.}"

    formated_seq_fl=$out_db/${db_name}.chunk${chunk_id}.fna

    #command="module load system/Anaconda3-5.2.0;source /usr/local/bioinfo/src/Anaconda/Anaconda3-5.2.0/etc/profile.d/conda.sh;conda activate krona;"

    command=$command"python ../../primer_design/scripts/merge_and_format_assembly.py --assembly_selection assembly_selections/ref_and_rep_assemblies.tsv  --assembly_root_dir /work2/project/seqoccin/metaG/ncbi_data/assemblies/ -o $formated_seq_fl $chunk_file -v;"

    command=$command'module load bioinfo/obitools-v1.2.11;'
    command=$command"obiconvert --fasta $formated_seq_fl --ecopcrdb-output=$out_db/${db_name}.chunk${chunk_id} -t ncbi_tax_dumb/2020-06-09/ --nuc"
    echo $command >> build_assemblies_ecopcr_db.sarray
done
```

We can then launch the sarray with `sarray build_assemblies_ecopcr_db.sarray`



## 2.2 Launch ecoPCR

In the ecoPCR command we can define the maximum number of errors (mismatches) allowed per primer (default: 0) argument `-e`. In our case we will run ecoCPR with e=3. It is quite a high number but we will be able to monitor this parameter when analysing the results. When the primers are not specific enough (small and highly degenerate) a high `e` parameter can lead to a numerous false positive amplicons which can make the result file very heavy.   

In ecoPCR command we can also chose the length range of the amplicons with arguments `-l` and `-L`.
We will use different length for each targets:
* 16S-23S primers: from 3000 to 7000pb
* full 16S rRNA gene primers: from 1000 to 2000pb
* 16S V3-V4 primers: from 100 to 600pb.  

ecoPCR result are written in the standard output, so we need to redirect it to a file.

A generic ecoPCR command could look like this: `ecoPCR -d <formatted db> -e <max mismatches> -l <min length> -L <max length> <forward primer> <reverse primer> > <result file>`


```bash
module purge # clean modules
module load bioinfo/ecopcr-v1.0.1

db=ecoPCR_db/16S-23S
e=3
outdir=ecopcr_results
mkdir $outdir


# command for the 16S-23S
fprimer=AGRGTTTGATYHTGGCTCAG
rprimer=ACCRCCCCAGTHAAACT
l=3000
L=7000

ecoPCR -d $db -e $e -l $l -L $L $fprimer $rprimer > $outdir/16S-23S_l${l}_L${L}_e${e}.ecopcr

# command for the full 16S
fprimer=AGRGTTTGATYHTGGCTCAG
rprimer=TACCTTGTTAYGACTT
l=1000
L=2000

ecoPCR -d $db -e $e -l $l -L $L $fprimer $rprimer > $outdir/full16S_l${l}_L${L}_e${e}.ecopcr

# command for the 16S v3V4
fprimer=ACGGRAGGCAGCAG
rprimer=TACCAGGGTATCTAATCCT
l=100
L=600

ecoPCR -d $db -e $e -l $l -L $L $fprimer $rprimer > $outdir/16Sv3v4_l${l}_L${L}_e${e}.ecopcr

```

<!--
>> ecoPCR over a fragmented database  
>> generate ecoPCR command automatically   -->


Next part of the tutorial: [analysis_ecopcr_results.md](analysis_ecopcr_results.md)
