# Primer design and primer selection tutorials

1. Primer design steps: [primer_deisgn.md](primer_design.md)
2. Primer evaluation and selection: [primer_selection.md](primer_selection.md)


## Add scripts directory to PATH

In order to use the scripts easily along the tutorial, let's include them in the PATH variable.

Go to the root of the git repository and do:
```bash
PATH=$PATH:$PWD/region_identification_workflow/bin
PATH=$PATH:$PWD/primer_design/scripts
PATH=$PATH:$PWD/scripts
```

## Use an srun on genologin

WARNING: On genologin, you need to be in srun to run commands directly in the terminal.  
```
# Basic srun with default RAM and cpu
srun --pty bash

# srun with personalized RAM and cpu
srun --pty --mem 20G -c 4 bash
```
