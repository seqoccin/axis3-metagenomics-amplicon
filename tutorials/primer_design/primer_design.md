

# 1. Primer design

Primers are designed based on a set of sequences that the primer needs to match.

## 1.1 Sequences preprocessing
To illustrate the primer design process, we will use sequences of the regions COG0090-COG0197 and design primer for this region.

We will process separately sequences of COG0090 and COG0197. Sequences of the genes are output of the identification pipeline and can be found in the directory `extracted_genes/` of the main result directory. These sequences have been extracted based on the assembly selection `rep_ref_and_10_assemblies_per_sp.tsv` containing 44108 assemblies. To check which assembly selection has been used to extract sequences you can have a look to the workflow parameter file `~/metaG/marker_identification/region_identification_workflow/bacteria_investigation/results_refseq_2020-06-09/workflow_parameters.txt`


Let's make symbolic link of the sequences files and store them in the directory `sequences/rep_ref_and_10_assemblies_per_sp`

```bash

mkdir -p sequences/rep_ref_and_10_assemblies_per_sp


ln -s ~/metaG/marker_identification/region_identification_workflow/bacteria_investigation/results_refseq_2020-06-09/extracted_genes/COG0090.fna sequences/rep_ref_and_10_assemblies_per_sp/.

ln -s ~/metaG/marker_identification/region_identification_workflow/bacteria_investigation/results_refseq_2020-06-09/extracted_genes/COG0197.fna sequences/rep_ref_and_10_assemblies_per_sp/.

```

Gene sequences have been extracted from an extended selection of refSeq assemblies made of 44108 assemblies. We need to subsample the sequences to be able to align them.
For that we will use the assembly selection `ref_and_rep_assemblies` and the script `filter_sequences_based_on_assemblies.py`. The script creates a new sequence file made of sequences belonging only to assemblies of the given assembly selection.

Please see [guidelines](../assembly_selection/readme.md) on assembly selection.

```bash
ln -s ~/metaG/marker_identification/region_identification_workflow/bacteria_investigation/results_refseq_2020-06-09/assembly_selections/* assembly_selections/

filter_sequences_based_on_assemblies.py \
            --assembly_selection assembly_selections/ref_and_rep_assemblies.tsv \
            -s sequences/rep_ref_and_10_assemblies_per_sp/* \
            -o sequences/
```

The script automatically creates in the folder `sequences` the sub directory `ref_and_rep_assemblies` where filtered sequences are stored.


## 1.2 Alignment

We use the tool clustalo to align the sequences

```
module load bioinfo/clustalo-1.2.4

mkdir -p alignment/ref_and_rep_assemblies

# alignment of COG0090.fna
clustalo -i sequences/ref_and_rep_assemblies/COG0090.fna  -o alignment/ref_and_rep_assemblies/COG0090.fna --threads=4

# alignment of COG0197.fna
clustalo -i sequences/ref_and_rep_assemblies/COG0197.fna  -o alignment/ref_and_rep_assemblies/COG0197.fna --threads=4

```



## 1.3 Degeprime

Degeprime design degenerate primer on each positions of a multi alignment. More information can be found here: https://github.com/andand/DEGEPRIME.git

Degeprime can be installed by cloning the git tool repository:

```bash
git clone https://github.com/andand/DEGEPRIME.git
```

It will create a folder `DEGEPRIME` with degeprime scripts.


First, the alignment file has to be prepared by the script `TrimAlignment.pl`:

```bash
perl DEGEPRIME/TrimAlignment.pl -i alignment/ref_and_rep_assemblies/COG0090.fna -min 0.9 -o alignment/ref_and_rep_assemblies/trimmed_COG0090.fna

perl DEGEPRIME/TrimAlignment.pl -i alignment/ref_and_rep_assemblies/COG0197.fna -min 0.9 -o alignment/ref_and_rep_assemblies/trimmed_COG0197.fna
  ```


Then, the output of `TrimAlignment.pl` will be processed by the script `DegePrime.pl` to design degenerate primer.
The script takes the maximum degeneracies allowed (`-d`)  and the length of the desired primer (`-l`).

For example to design primer on COG0090 with a degeneracy max of 256 and a length of 20, we would run the following command.

```bash
mkdir degeprime_result/ref_and_rep_assemblies/COG0090/
perl DEGEPRIME/DegePrime.pl -i alignment/ref_and_rep_assemblies/trimmed_COG0090.fna -d 256 -l 20 -o degeprime_result/ref_and_rep_assemblies/COG0090/d256_l20.tsv
```
Degerpime designs a primer at each position of the alignment. The result file contains a line for each position of the alignment which contain information on the primer designed at this position.

Example:
```
Pos	TotalSeq	UniqueMers	Entropy	PrimerDeg	PrimerMatching	PrimerSeq
158	11023	3139	9.74447090598207	256	2179	MNCRSATCGARTAYGACCCS
159	11023	2852	9.61411698766755	256	2431	KCRYMTSGARTACGAYCCSA
160	11023	1939	8.74208443947192	192	3212	CRBMTYGARTACGAYCCSAA
161	11023	1840	8.63766785356035	216	4125	VBATYGARTACGAYCCVAAC
162	11023	1729	8.43047667578963	256	5661	NATYGARTAYGAYCCNAACC
163	11023	982	7.35830952192805	256	7498	MTNGARTAYGAYCCNAACCG
164	11023	1046	7.68303520983677	256	7441	TNGARTAYGAYCCNAACCGY
165	11023	1403	8.24305656157902	256	5914	YGARTAYGAYCCNAACCGYW
166	11023	997	7.54168542575446	256	7335	GARTAYGAYCCNAACCGYWS
167	11023	1637	8.59085293748222	256	5072	ARTACGAYCCNAACCGYWCN
168	11023	1827	8.77457805796638	256	4828	RTACGAYCCNAACCGYWCNG
169	11023	1380	8.15231711331586	256	6205	TAYGAYCCNAACCGYWCNGC
170	11023	2367	9.41443051773703	256	3706	AYGAYCCSAACCGYWCSGCN
171	11024	3144	10.0528910590607	256	2598	YGAYCCSAACCGYACSGCNY
172	11024	2857	9.80723321643518	256	2243	GAYCCSAACCGYWCSGCSMR
```

* `Pos`: Position of the (first base in the) window in the trimmed_align_file (starting at 0).
* `TotalSeq`: Number of sequences that span this window (including sequences with internal gaps in this region).
* `UniqueMers`: Number of unique oligomer sequences (without in/dels) in the window.
* `Entropy`: Entropy of the window, calculated as -Σ Pi log2(Pi), where Pi is the frequency of oligomer i.
* `PrimerDeg`: Degeneracy of the selected primer.
* `PrimerMatching`: Number of sequences that match the selected primer.
* `PrimerSeq`: Sequence of the selected degenerate primer.

We can analyse the result with the jupyter notebook `analyse_degeprime_result.ipynb` to plot Entropy and PrimerMatching along the positions of the alignement.

When looking at the entropy along the positions of the alignment, we can clearly see that the entropy get down around the position 580 (lowest entropy of 5.4 at position 587). Around this position, sequences seem more conserved and thus this area is most suitable for universal primers.

![COG0090_d256_l20_Entropy](images/COG0090_d256_l20_Entropy.png)


Indeed, we the primer matching the most sequences around this location: position 572, matching 9.836k sequences 89.2%.

![COG0090_d256_l20_PrimerMartching](images/COG0090_d256_l20_PrimerMartching.png)

Now, we can apply the same method on COG0197 to design primer:

```bash
mkdir degeprime_result/ref_and_rep_assemblies/COG0197/
perl DEGEPRIME/DegePrime.pl -i alignment/ref_and_rep_assemblies/trimmed_COG0197.fna -d 256 -l 20 -o degeprime_result/ref_and_rep_assemblies/COG0197/d256_l20.tsv
```



![COG0197_d256_l20_Entropy](images/COG0197_d256_l20_Entropy.png)


![COG0197_d256_l20_PrimerMartching](images/COG0197_d256_l20_PrimerMartching.png)

Entropy and Primer matching of COG0197 are clearly less optimal than COG0090. The smallest entropy computed is  6.6 and At most
primers match 66.9% of the sequences.


## 1.4 Automatisation with a bash script

So far we only have design primer with a length of 20nt and degeneracy of 256. But we could try to design primer with different parameters and check if they are improving primer quality.
We can design primer with a degeneracy comprise in `12 24 64 162 256 1024`
and a lengths of from 17nt to 22nt.
The combination of the degeneracy and lengths makes 36 degeprime executions per gene (6 degeneracy * 6 length).  


This bash for loops create an sarray file `degeprime_multiple_params.sarray` with all the 36 degeprime commands for both COG0090 and COG0197.
It uses the trimmed alignment already created and stored in `alignment/ref_and_rep_assemblies/`.
The sarray starts with a bunch of # lines to recall which parameters are used.


```bash

s_array_file='degeprime_multiple_params.sarray'

COG_list='COG0090 COG0197'
result_dir='degeprime_result/ref_and_rep_assemblies/'
aln_dir='alignment/ref_and_rep_assemblies/'

degeneracies='12 24 64 162 256 1024'
lengths='17 18 19 20 21 22'

echo '#' $COG_list > $s_array_file
echo '# degeneracies' $degeneracies >> $s_array_file
echo '# lengths' $lengths >> $s_array_file
echo '# result:' $result_dir >> $s_array_file

for cog in $COG_list
do
  trimmed_alignment="$trimmed_aln_dir/trimmed_${cog}.fna"
  for d in $degeneracies
  do
    for l in $lengths
    do
      output_fl=$result_dir/${cog}_d${d}_l${l}.tsv

      echo "perl DEGEPRIME/DegePrime.pl -i $trimmed_alignment -d $d -l $l -o $output_fl" >> $s_array_file
    done
  done

done

echo sarray -J degeprime -o slurm_array_out/%j_%x.out $s_array_file

```

The sarray can be launch with the command: `sarray degeprime_multiple_params.sarray`

Next part of the tutorial: [primer_selection.md](primer_selection.md)
