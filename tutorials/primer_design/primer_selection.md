
This is the second part of the tutorial. Part 1 can be found here [primer_design.md](primer_design.md)

# 2. Primer selection
## 2.1 Preliminary primer selection

Degeprime design a primer at each position of the alignment. On top of that, we have launched degeprime with a combination of parameters resulting into a long list of possible primers.
We then need to filter, sort and select the most interesting of these primers.
For that we will use the python script `select_degeprime_primers.py`.

We can check script parameters with: `select_degeprime_primers.py -h`
```
Filter and select degeprime primers

optional arguments:
  -h, --help            show this help message and exit
  --target_gene TARGET_GENE
  --degeprime_result_dir DEGEPRIME_RESULT_DIR
  -o OUTPUT, --output OUTPUT
  --min_tm MIN_TM       Keep only primers with a tm >= tm_min (default: 48)
  --min_length MIN_LENGTH
                        Keep only primers with a length >= min_length
                        (default: 17)
  --max_length MAX_LENGTH
                        Keep only primers with a length <= max_length
                        (default: 25)
  --degeneracy_max DEGENERACY_MAX
                        Keep only primers with a degeneracy <= degeneracy_max
                        (default: 2048)
  --n_best_primers N_BEST_PRIMERS
                        Select the n best primers based on Primer Matching:
                        Number of sequences that match the primer. (default:
                        100)
  --tm_calculator {Tm_Wallace,Tm_NN,Tm_GC}
                        3 possible methods to compute melting temperature as
                        explain here: https://biopython.org/DIST/docs/api/Bio.
                        SeqUtils.MeltingTemp-module.html (default: Tm_Wallace)
  -v, --verbose         increase output verbosity (default: False)

```

It requires the name of the gene/cog for which we have designed primers (`--target_gene`) and the directory where degeprime result have been stored (`--degeprime_result_dir`).


We can filter out primers by specifying a minimal melting temperature (`--min_tm`), a maximum degeneracy (`--degeneracy_max`) and the min  and max length of desired primers (`--max_length`, `--min_length`).  

Finally the script sorts primers based on their primer matching number provided by degeprime (which is the number of sequences that match the primer) and select the n best primers (`--n_best_primers`).  



```bash
mkdir degeprime_result/selected_primers

select_degeprime_primers.py  --target_gene COG0197 --degeprime_result_dir degeprime_result/ref_and_rep_assemblies/COG0197/ -v -o degeprime_result/selected_primers/COG0197.tsv

select_degeprime_primers.py  --target_gene COG0090 --degeprime_result_dir degeprime_result/ref_and_rep_assemblies/COG0090/ -v -o degeprime_result/selected_primers/COG0090.tsv

cog=COG0197
select_degeprime_primers.py  --target_gene ${cog} --degeprime_result_dir degeprime_result/ref_and_rep_assemblies/${cog}/ -v -o degeprime_result/selected_primers/${cog}.tsv --min_length 18 --degeneracy_max 256 --n_best_primers 10
```
At this step, we can also manually delete some primers manually if we judge that they are not good enough and do not match sequence requirement (GC clamp, possible primer dimer etc.)


## 2.2 Primer in silico test

### 2.2.1 Strategy

We have a list of selected primers for each genes, so now we can test in silico these primers with the tool ecoPCR.

We are going to test all possible combination of primers couple. With a list of 10 selected primers for each genes, we end up with 100 (10 * 10) possible pairs to test.

We will test the primers on two types of sequences databases:
1. a database made of genome: in this case ecoPCR will search potential amplicon anywhere in genomes.
2. a database made of sequences of the targeted regions. In our case we will use sequences of COG0090-COG0197 identified by the region identification pipeline.

The idea behind testing primers between two database is to identified easily unspecific amplicons (amplicon that are not expected). In one hand, we have expected amplicons coming from database of the target region and on the other hand amplicon coming from all over assemblies. To identify unspecific amplicons we simply subtract target region's amplicons from assemblies' amplicons.  


### 2.2.2 Assemblies' database

The ecoPCR database of assemblies, that we are going to use can be found here: /work2/project/seqoccin/metaG//marker_identification/primer_design/ecoPCR_db/2020-06-09_refseq/ref_and_rep_assemblies/

It corresponds to the assemblies of the selection ref_and_rep_assemblies of refseq of 2020-06-09.

Guideline on how to build it can be found here: [launch_ecopcr.md](tutorials/primer_analysis/launch_ecopcr.md) ### 2.1.2 create an ecoPCR database made of assemblies  


### 2.2.3 Target sequences' database
The ecoPCR database of target sequences will be made by sequences of the region COG0090-COG0197.

Sequences of COG0090-COG0197 can be found in the folder `extracted_regions` of the result directory of the region identification pipeline.

In our exemple: `region_identification_workflow/bacteria_investigation/results_refseq_2020-06-09/extracted_regions/COG0090-COG0197.fna`

In the same way as individual genes, region sequences need first to be filtered to match the assembly selection we are using (see [Sequences preprocessing](#sequences-preprocessing) section).

We are using assembly selection `ref_and_rep_assemblies` and sequences have been extracted based on assembly selection `rep_ref_and_10_assemblies_per_sp`.

Let's filter sequences:
```bash
ln -s /work2/project/seqoccin/metaG/marker_identification/region_identification_workflow/bacteria_investigation/results_refseq_2020-06-09/extracted_regions/COG0090-COG0197.fna .


filter_sequences_based_on_assemblies.py \
            --assembly_selection assembly_selections/ref_and_rep_assemblies.tsv \
            -s COG0090-COG0197.fna \
            -o region_sequences -v
```

Filtered sequences are written here: `target_sequences_db/ref_and_rep_assemblies/COG0090-COG0197.fna`

We need to format the sequences to be then processed by the tool obiconvert. (For more detail please see primer analysis guideline TODO )

```bash
  sequences=region_sequences/ref_and_rep_assemblies/COG0090-COG0197.fna
  output=ecoPCR_db/COG0090-COG0197.fna
  assembly_selection=assembly_selections/ref_and_rep_assemblies.tsv
  format_sequences.py $sequences --assembly_selection $assembly_selection -o $output  -v
```

We can now launch obiconvert to create the database. It needs the ncbi tax_dump that we can found here (`../../region_identification_workflow/ncbi_data/2020-06-09_refseq_summary/taxdumb/`)

```bash
module load bioinfo/obitools-v1.2.11;
tax_dump=../../region_identification_workflow/ncbi_data/2020-06-09_refseq_summary/taxdumb/
obiconvert --fasta ecoPCR_db/COG0090-COG0197.fna --ecopcrdb-output=ecoPCR_db/COG0090-COG0197 -t ncbi_tax_dumb/2020-06-09/ --nuc

```

It creates the db in dir `ecoPCR_db/`.

### 2.2.4 Make primer pair and generate ecopcr commands

We will generate ecoPCR commands for each possible pair of primers.

For that we will use the script `ecopcr_sarray_degeprime_primers.py`.
It takes the two lists of primers for the two flanking genes, check in which relative position the genes are in genomes and reverse_complement primers if needed.
Finally it creates folders for each of the primer pair and prints pit ecoPCR commands for each of the pairs. When the expected ecopcr has been already computed the script skips the corresponding ecopcr command.


The scripts required several arguments:

* Region info file (`--regions_info`):

It is a file created by the identification pipeline which gather information on each region. The information useful here is the organization of the region. In deed we need to know in which order and on which relative strand we expect the two flanking genes. So in our case the organisation of COG0090-COG0197 is coded as `COG0090>-COG0197>`  which means that the two genes are found on the same strand and that COG0090 is before COG0197. In this case primer of COG0197 needs to be reversed complement to be able to amplify the region along the primer of COG0090.
This file is named `selected_pairs_stat.tsv` and is stored in result directory of the region identification pipeline.

Path of the file `/work2/project/seqoccin/metaG/marker_identification/region_identification_workflow/bacteria_investigation/results_refseq_2020-06-09/selected_pairs_stat.tsv`

* directory of the lists of selected primers (`--primer_list_dir`):

Directory where the lists of selected primers that we have created with the script `select_degeprime_primers.py`
Path: `degeprime_result/selected_primers/`

* Regions we want to analyse (`--regions`):

In our case it will be `COG0090-COG0197`


An other possible parameter is the path to ecoPCR tool, by default ecoPCR is expected to be in the PATH but we can also specify the path to a custom version. Here we will specify the command `module load bioinfo/ecopcr-v1.0.1` to charge ecoPCR in the PATH before each ecoPCR command.

The script generate

The script can also take ecopcr parameters:

* Path to the ecopcr databases: for assemblies (`--ecopcr_assembly_dir`) and/or for target regions (`--ecopcr_region_db_dir`).

* Min length (`-l`) and max length (`-L`) of the amplicons

* Mismatches max (`-e`): Maximum number of errors (mismatches) allowed per primer in ecoPCR simulation (default: 0)


We can add the flag `--write_individual_command` to write individual ecoPCR command in a file in each primer couple directory.


To have more details on script argument you can run `ecopcr_sarray_degeprime_primers.py -h`.

Let's combine primers and generate ecoPCR commands.


```bash
region_info=/work2/project/seqoccin/metaG/marker_identification/region_identification_workflow/bacteria_investigation/results_refseq_2020-06-09/selected_pairs_stat.tsv
ecopcr_assembly_dir=/work2/project/seqoccin/metaG//marker_identification/primer_design/ecoPCR_db/2020-06-09_refseq/ref_and_rep_assemblies/
ecopcr_sarray_degeprime_primers.py --regions 'COG0090-COG0197' \
                                                     --regions_info $region_info \
                                                     --primer_list_dir degeprime_result/selected_primers/ \
                                                     --ecopcr_region_db_dir ecoPCR_db/ \
                                                     --ecopcr_assembly_dir $ecopcr_assembly_dir \
                                                     --ecopcr_tool_path 'module load bioinfo/ecopcr-v1.0.1;ecoPCR' \
                                                     --write_individual_command \
                                                     -e 2 \
                                                     -v > ecopcr_commands.sarray
```
The script has written the command in `ecopcr_commands.sarray`.

And it has also created a directory `primers/COG0090-COG0197` containing a sub directory for each combination of primers. So as we had 10 primer for each flanking genes we have know 100 subdirectories.

```
$ ls primers/COG0090-COG0197/
COG0090_0-COG0197_0  COG0090_1-COG0197_2  COG0090_2-COG0197_4  COG0090_3-COG0197_6 ...
```
In each directory we have a json file with information on the primer pair.
For example `primers/COG0090-COG0197/COG0090_0-COG0197_0/fCOG0090_0-rCOG0197_0.json`:

```json
{
  "forward_primer": {
    "primer_id": "fCOG0090_0",
    "seq": "GAACCCNRWNGAYCAYCC",
    "target": "COG0090",
    "degeneracy": "256",
    "PrimerMatching": "10070",
    "tm_min": "52.0",
    "tm_max": "62.0",
    "source": "COG0090_d256_l18.tsv",
    "comment": "PrimerMatching:10070;Pos:574"
  },
  "reverse_primer": {
    "primer_id": "rCOG0197_0",
    "seq": "RCCYTTRCCNKWRCCCAT",
    "target": "COG0197",
    "degeneracy": "256",
    "PrimerMatching": "7801",
    "tm_min": "50.0",
    "tm_max": "62.0",
    "source": "COG0197_d256_l18.tsv",
    "comment": "PrimerMatching:7801;Pos:234"
  }
}
```
With the argument `--write_individual_command` individual script to run ecoPCR has also been created in primer couple directory.

For example : `primers/COG0090-COG0197/COG0090_0-COG0197_0/ecopcr_COG0090-COG0197/fCOG0090_0-rCOG0197_0_l500_L8000_e0.sh`

You can then execute the sarray with `sarray ecopcr_commands.sarray`

It will run the ecoPCR command and output result in each primer couple directory.


## 2.3 ecoPCR results analysis

### 2.3.1 Identify expected and unexpected amplicons
We have ecoPCR results on assembly and on specific target region. What we would like is to identify unexpected amplicons that are identified in assembly but are not part of the target region amplicons (True Postive), meaning that they are potential unspecific amplicons (False Positive).


To do this comparison we use the script `compare_ecopcr_region_and_assembly_results.py`. It takes as arguments:
* `--primer_dirs`: primer directories where ecoPCR results have stored and the core name. In our case it is all subdirectory of `primers/COG0090-COG0197/`
* `--ecopcr_db_assembly_name`: the core name of the assembly ecopcr db. In our case it is `ref_and_rep_assemblies`
* `--ecopcr_db_region_dir`: directory where assembly ecopcr db is stored.
* `-a`: the assembly selection we want to use. If we want analyse a sub selection of assemblies (for example only genomes of a mock) we can provide the corresponding sub selection and only sequences/amplicons from this selection will be analysed and reported and anyother amplicons are ignored. To keep it simple here we will use selection `ref_and_rep_assemblies` so none of the assemblies will be ignored.

Let's first analyse the primer couple: `COG0090_0-COG0197_0`

```bash
compare_ecopcr_region_and_assembly_results.py --primer_dirs primers/COG0090-COG0197/COG0090_0-COG0197_0/ --ecopcr_db_assembly_name ref_and_rep_assemblies --ecopcr_db_region_dir ecoPCR_db/  -a assembly_selections/ref_and_rep_assemblies.tsv

```

The script print out this line :
```
"couple":"COG0090-COG0197","assembly_amplicon":17423,"TP":10055,"FP":7367,"unexpected_duplication":1,"missing_amplicon":0
```

ecoPCR has found 17423 potential amplicons in assembly database among which 10055 were also identified in target sequences.

`unexpected_duplication:1`  means that the same amplicon has been found twice in a genome while it was only expected to be found once. This can happen when the target region is duplicated in the genome but only one copy was identified and then expected.

The script also output the file: `amplicon_stats/ref_and_rep_assemblies/expected_unexpected_amplicon_stats.tsv`
This tsv file gathers info on expected and unexpected amplicons and the number of mismatches. It can be used to plot easily this information.

<!---

[TODO !! ]
JUPY
Gerer la possibilité de préciser la taille

-->


### 2.3.2 Generate stat file

Let's generate a tsv file with primer couple info.
The script  `generate_primers_info_file.py` takes the primer_directory to parse its json file and the ecopcr stat file generated by the script `compare_ecopcr_region_and_assembly_results.py`.
```
generate_primers_info_file.py --primer_dirs primers/COG0090-COG0197/* --ecopcr_stat_fl amplicon_stats/ref_and_rep_assemblies/expected_unexpected_amplicon_stats.tsv -o primer_couple_stat.tsv

```

First lines of the file `primer_couple_stat.tsv`:
```tsv
region	primer_couple	fprimer_name	fprimer_seq	fprimer_length	fprimer_degeneracy	rprimer_name	rprimer_seq	rprimer_length	rprimer_degeneracy	sp_coverage_mismatch0	sp_unspecific_match_mismatch0	sp_coverage_mismatch1	sp_unspecific_match_mismatch1	sp_coverage_mismatch2	sp_unspecific_match_mismatch2
COG0090-COG0197	COG0090_0-COG0197_0	fCOG0090_0	GAACCCNRWNGAYCAYCC	18	256	rCOG0197_0	RCCYTTRCCNKWRCCCAT	18	256	7286	36	2138	724	597	4285
COG0090-COG0197	COG0090_0-COG0197_1	fCOG0090_0	GAACCCNRWNGAYCAYCC	18	256	rCOG0197_1	CYTTRCCNKWACCCATNC	18	256	7141	98	2434	658	875	4900
COG0090-COG0197	COG0090_0-COG0197_2	fCOG0090_0	GAACCCNRWNGAYCAYCC	18	256	rCOG0197_2	CCYTTRCCNKWACCCATNC	19	256	7185	31	2443	331	749	2755
COG0090-COG0197	COG0090_0-COG0197_3	fCOG0090_0	GAACCCNRWNGAYCAYCC	18	256	rCOG0197_3	CCYTTRCCNKWACCCATN	18	256	7185	31	2443	341	479	4070
COG0090-COG0197	COG0090_0-COG0197_4	fCOG0090_0	GAACCCNRWNGAYCAYCC	18	256	rCOG0197_4	ACGNGCNGMYTCRATYTG	18	256	7083	39	2362	76	679	3375
```

Let's make it interactive table inside a jupyter notebook.
Let's open and execute the notebook: `primer_couple_stat_analysis.ipynb`. (Help in jupyter notebook on genologin [jupyter_on_genologin/readme.md](../jupyter_on_genologin/readme.md))
