e=2
region_list='COG0052-COG0264 COG0097-COG0200 COG0080-COG0081 COG0200-COG0201 COG0090-COG0197'
concat_sarray_command=ecopcr_command_e${e}.sarray

python ecopcr_primers_validation.py --region_list "${region_list}" \
                                    --primers_dir 'degeprime_results/bash_pipeline/selected_primers/'\
                                    --ecopcr_db_dir ecoPCR/ecopcr_db/ \
                                    --regions_info ../nextflow_result_2/pairs_stats_collected.tsv \
                                    -e $e --outdir 'degeprime_results/bash_pipeline/ecoPCR' > $concat_sarray_command

echo sarray -J ecopcr_command_e${e}_all -o ecopcr_command_e2/%j.out $concat_sarray_command

# for region in $region_list
# do
#   python ecopcr_primers_validation.py --region_list $region \
#                                       --primers_dir 'degeprime_results/bash_pipeline/selected_primers/'\
#                                       --ecopcr_db_dir ecoPCR/ecopcr_db/ \
#                                       --regions_info ../nextflow_result_2/pairs_stats_collected.tsv \
#                                       -e $e --outdir 'degeprime_results/bash_pipeline/ecoPCR' > ecopcr_command_e${e}_${region}.sarray
#
#   echo sarray -J ecopcr_command_e${e}_${region} -o slurm_array_out/%x.out ecopcr_command_e${e}_${region}.sarray
#   cat ecopcr_command_e${e}_${region}.sarray >> $concat_sarray_command
# done
