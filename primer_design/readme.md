# Design primer

## Degeprime


Get the perl script:



```bash
git clone https://github.com/andand/DEGEPRIME.git
```

Use conda env 'primer': `conda activate primer`

Use of the nextflow script:

First Attempt failed because of memory

Second attempt succeed after 1d 1h 33m 43s
```bash
#!/bin/bash
#SBATCH -J nextflow_run
#SBATCH --mail-type=BEGIN,END,FAIL
#SBATCH --mem=1G
#SBATCH -c 1

module load bioinfo/Nextflow-v19.04.0
module load system/Anaconda3-5.2.0

result_dir="nextflow_stat/"
nextflow run design_primer.nf  -profile cluster -with-report $result_dir/report.html -with-trace $result_dir/trace -with-timeline $result_dir/timeline.html -with-dag $result_dir/flowchart.html

```

Give 24GB/ alignment but only used around 8GB. --> Clutalo seems to use 2GB/cpu



### Filtred sequence

Run primer design on a smaller set of sequences

Use of python script filter_sequence.py

```bash
# get list of assembly to keep

selected_assemblies='../nextflow_result_2/selected_assemblies.tsv'
cut $selected_assemblies -f1 | tail --line=+2 > assembly_list.txt

# Keep sequence that belong only to assembly from the list
python filter_sequence.py sequences/COG0200.fna --assembly_list_to_keep assembly_list.txt -o sequences/filtered_COG0200.fna

```
#### Align only reference genome
```bash
grep 'reference genome' ../nextflow_result_2/selected_assemblies.tsv > primer
grep 'reference genome' ../nextflow_result_2/selected_assemblies.tsv | cut -f1 > reference_genome_accession.txt
```

### Build taxonomy file

Degeprime can process a taxonomy file to add tax info the primer design processed

```bash
python create_taxonomy_file.py sequences/filtered_COG0200.fna ../nextflow_result_2/all_assemblies_with_taxonomy.tsv
```


### PALtoNAL approach

* Translate our fna genes with `translate_fna.py` script.
* Align protein sequences :
* Use the tool pal2nal.pl `pal2nal.pl toy_example/toy_example_prot.aln toy_example/sample_of_filtered_COG0292.fna -output fasta`

## ecoPrimers
Install obitools

```bash
conda create -n old_python python=2.7 gcc

conda activate old_python

python2 get-obitools.py

```

Install is not working on genologin
But obitool is part of the module!
```bash
module load bioinfo/obitools-v1.2.11
```
Try to build ecoPCR db: https://pythonhosted.org/OBITools/scripts/obiconvert.html

```bash
tar xzvf ../../ncbi_data/2019-10-24_refseq_summary/new_taxdump.tar.gz
obiconvert --fasta seq_test.fna --ecopcrdb-output=DB -t tax_dump_dir/

```
I had to add taxid to the header of fasta format
And to add -t tax_dump_dir to command otherwise it fails with exception:
`Exception: Taxonomy error for GCF_001116325.1|NZ_CMHT01000001.1|20901-21341: taxonomy is missing`


Installing ecoPrimers: https://git.metabarcoding.org/obitools/ecoprimers/wikis/home
```bash
wget https://git.metabarcoding.org/obitools/ecoprimers/uploads/40f0fe1896a15ca9ad29835f93893464/ecoPrimers.tar.gz
tar xzvf ecoPrimers.tar.gz
cd ecoprimers/src/
make
```

it seems to works :

```bash
ecoprimers/src/ecoPrimers -d DB -e 10
```


Modification of `filter_sequence.py` to let it add taxid to header when arg `--add_taxid` is set

```bash
python filter_sequence.py region_extracted/COG0097-COG0201.fna \
      --assemblies_with_taxonomy all_assemblies_with_taxonomy.tsv \
      --assembly_list_to_keep accession_list/reference_genome_accession.txt \
      --add_taxid \
      -o filt_withtaxid_COG0097-COG0201.fna
```

command line example : $
```bash
python filter_sequence.py region_extracted/COG0097-COG0201.fna --assemblies_with_taxonomy all_assemblies_with_taxonomy.tsv --assembly_list_to_keep accession_list/ref_and_rep_genome_accession.txt --add_taxid -o filter_sequences/COG0097-COG0201_rep_ref.fna

obiconvert --fasta filter_sequences/COG0097-COG0201_rep_ref.fna --ecopcrdb-output=ecoPrimers/formatted_db/COG0097-COG0201_rep_ref  -t tax_dump/
```



## primer prospector

`conda create primerprospector -n primerprospector`

```
conda activate primerprospector

ln -s /work2/project/seqoccin/metaG/marker_identification/primer_design/primer_design_results/reference_genomes/degeprime_results/aln_mafft_filtered_COG0052_degeprime_d18_l14.tsv result_ref/

ln -s /work2/project/seqoccin/metaG/marker_identification/primer_design/primer_design_results/reference_genomes/degeprime_results/aln_mafft_filtered_COG0264_degeprime_d24_l14.tsv result_ref/

```

Got an error :
```
Traceback (most recent call last):
  File "/home/jmainguy/anaconda3/envs/primerprospector/bin/analyze_primers.py", line 168, in <module>
    main()
  File "/home/jmainguy/anaconda3/envs/primerprospector/bin/analyze_primers.py", line 163, in main
    tp_gap, non_tp_gap)
  File "/home/jmainguy/anaconda3/envs/primerprospector/lib/python2.7/site-packages/primerprospector/analyze_primers.py", line 947, in analyze_primers
    non_tp_mm=non_tp_mm, tp_gap=tp_gap, non_tp_gap=non_tp_gap)
  File "/home/jmainguy/anaconda3/envs/primerprospector/lib/python2.7/site-packages/primerprospector/analyze_primers.py", line 894, in generate_hits_file_and_histogram
    write_primer_histogram(hist_data, graph_filepath)
  File "/home/jmainguy/anaconda3/envs/primerprospector/lib/python2.7/site-packages/primerprospector/analyze_primers.py", line 233, in write_primer_histogram
    'Non 3\' mismatches', y_axis_size=y_axis_max)
  File "/home/jmainguy/anaconda3/envs/primerprospector/lib/python2.7/site-packages/primerprospector/analyze_primers.py", line 179, in primer_hit_histogram
    yticks(range(0, y_axis_size+2, y_tick_step), size=7)
TypeError: range() integer end argument expected, got numpy.float64.

```

Modify the original script `analyze_primers.py` by adding a `int()` line 586 to the value returned by the function get_yaxis_max:

```python
def get_yaxis_max(all_data_sets,
                  max_bin=5,
                  bin_size=1):
    """ Returns largest single bin in list of lists

    The purpose of this function is to find the largest single bin so that
    all subplots in a graph can use this maximum value for the y-axis size.

    all_data_sets: list of lists containing primer hit data (3', non 3'
     mismatches, 3' and non 3' gaps, weighted score).
    max_bin: Upper limit that values are capped at for creating bins.
    bin_size: step size for bins.  Module is currently written to handle
     whole numbers, if fractional values are used, must be careful to
     ensure that all possible bin values are covered to avoid strange results.
    """

    counts_all_max = []

    for data_set in all_data_sets:
        # Create bins
        counts, bins, rects = hist(data_set, bins=arange(0, max_bin, bin_size))
        counts_all_max.append(max(counts))

    return int(max(counts_all_max)) # Add int here to prevent the script to crash  
```

## Try our method on rrn operon
Is our sequences bad or our method?


### with ecoPrimers
Add dump taxid to header :  `primer_design/add_dump_taxid_to_headers.py`

```bash
obiconvert --fasta ecoPrimers/sequences_with_dump_taxid/operons_100_common_species.fa --ecopcrdb-output=ecoPrimers/formatted_db/operons_100_common_species  -t tax_dump/
ecoprimers/src/ecoPrimers -d formatted_db/operons_100_common_species -e 10

```

Found something:
It found 184 primers that respect quorum conditions
But fail with `Erreur du bus error

```bash
obiconvert --fasta ecoPrimers/sequences_with_dump_taxid/COG0200-COG0201_common_species.fa --ecopcrdb-output=ecoPrimers/formatted_db/COG0200-COG0201_common_species -t tax_dump/


```

### With Degeprime

Need an alignment..

Our strategy :
* Tronc sequences to obtain 16S and 23S
* align them
* and launch degeprime on the alignments


Adapt the nextflow to make something simplier and add possibility to skip seq prep

```bash
sbatch launche_nextflow_rrn_method_eval.sbatch
Submitted batch job 8622548
```

Result dir set to : `"rrn_primer_comparison/degeprime_results/"`

nextflow command in the sbatch :

```bash
module load system/Anaconda3-5.2.0
source /usr/local/bioinfo/src/Anaconda/Anaconda3-5.2.0/etc/profile.d/conda.sh
conda activate nextflow
result_dir="rrn_primer_comparison/degeprime_results/"
nextflow run degeprime_design_workflow.nf  -profile cluster \
                               -with-report $result_dir/report.html \
                               -with-trace $result_dir/trace \
                               -with-timeline $result_dir/timeline.html \
                               -resume \
                               --files "rrn_primer_comparison/*fna" \
                               --seq_preparation false \
                               --result_dir $result_dir

```

length and degeneracy used : `degeneracies = Channel.from(12, 24, 64, 162, 256)` `primer_lengths  = Channel.from(17,18,19)`



## ecoPCR: test our primer on all sequences / genomes

### Over all rep and ref genomes (5508)

Test quality of our primers and if it match where it is suppose to and nowhere else.
Need to build db of genomes

Format assembly sequences to have taxid in their header and also the name of the assembly in addition to the name of the molecule

```bash
# Test on 9 accesisons
python merge_and_format_assembly.py --assembly_root_dir '../ncbi_data/refseq_assemblies/'  --assemblies_with_taxonomy 'all_assemblies_with_taxonomy.tsv' accession_list/head10_reference_genome_accession.txt

python merge_and_format_assembly.py --assembly_root_dir '../ncbi_data/refseq_assemblies/'  --assemblies_with_taxonomy 'all_assemblies_with_taxonomy.tsv' accession_list/ref_rep_genomes.txt -o ecoprimer_db/ref_rep_genomes_merged.fna

```
Build ecopcr db
```
obiconvert --fasta ecoprimer_db/ref_rep_genomes_merged.fna --ecopcrdb-output=ecoprimer_db/ref_rep_genomes_merged -t tax_dump/

```

`ref_rep_genomes_merged.fna` is a 22G file. obiconvert tell us that it takes 9 hours to build the db
Use a sbatch `build_ecopcr_db.sbatch` to do build instead of a srun.

seff of the job :
```
Job ID: 8610282
Cluster: genobull
User/Group: jmainguy/BIOINFO
State: COMPLETED (exit code 0)
Nodes: 1
Cores per node: 2
CPU Utilized: 07:21:00
CPU Efficiency: 49.98% of 14:42:26 core-walltime
Job Wall-clock time: 07:21:13
Memory Utilized: 1.73 GB
Memory Efficiency: 10.83% of 16.00 GB
```

I gave too much memory and 1 cpu would have been enough.


Install ecoPCR
```
wget https://git.metabarcoding.org/obitools/ecopcr/uploads/aa3828c196570ea156ce6d4baac22b10/ecopcr-1.0.1.tar.gz
tar -zxvf ecopcr-*.tar.gz
cd ecopcr/src
make
```

Try to launch ecoPCR on the rep and ref genomes and with the primer of the publi gyrB
```bash
ecopcr/src/ecoPCR -d ecopcr_db/ref_rep_genomes_merged \
                  -e 3 \
                  -l 50 \
                  -L 500 \
                  MGNCCNGSNATGTAYATHGG CNCCRTGNARDCCDCCNGA \
                  > search_gyrB_test.ecopcr

```

Try ecoPCR on primers from COG0200 and COG0097.

`degeprime_results/degeprime_results/aln_mafft_filtered_COG0097_degeprime_d256_l18.tsv`:
```tsv
421	5494	673	7.49295792600525	256	3889	NGARCCNTAYAARGGYAA
422	5494	508	6.7084388175822	256	4520	GARCCNTAYAARGGNAAR
423	5494	508	6.70843881758219	256	4520	ARCCNTAYAARGGNAARG
424	5494	500	6.7042230805129	256	4520	RCCNTAYAARGGNAARGG
425	5494	581	6.87024773890707	256	4391	CCNTAYAARGGYAARGGN
426	5494	754	7.62465311752633	256	3754	CNTAYAARGGYAARGGYR
427	5494	727	7.60440135871935	256	3768	NTAYAARGGYAARGGYRT
```

Select one in position 422 : `GARCCNTAYAARGGNAAR`

Reverse complement of this primer with (http://arep.med.harvard.edu/labgc/adnan/projects/Utilities/revcomp.html): `YTTNCCYTTRTANGGYTC`


degeprime_results/degeprime_results/aln_mafft_filtered_COG0200_degeprime_d256_l18.tsv

```tsv
124	5490	888	8.06851618661393	256	3638	GNTTYGARGGYGGNCARA
125	5490	1016	8.29000217810982	256	3384	NTTYGARGGYGGNCARAT
126	5490	749	7.27217565099953	256	3904	TWYGARGGNGGNCARATG
127	5490	770	7.30035645445983	256	3934	WYGARGGNGGNCARATGC
128	5490	747	7.13177872006572	256	4023	YGARGGNGGNCARAYGCC
129	5490	923	7.69012016738886	256	3935	GARGGNGGNCARATGCCN
130	5490	1255	8.59273649911495	256	2839	ARGGYGGNCARATGCCNY
131	5490	1268	8.60981468430181	256	2836	RGGYGGNCARATGCCNYT
132	5490	1427	8.93868135794698	256	2203	GGYGGYCARATGCCNYTN
133	5490	1826	9.55582505603277	216	1446	GYGGBCAGATGCCKHTBY
134	5490	1937	9.66221585045343	216	1305	YGGYCARATGCCDHTBCA
135	5490	1882	9.64638183622436	243	1317	GGBCAGATGCCBHTBCAV
136	5490	2121	9.92159169399046	192	997	GSCAGATGCCSMTBYAYM
```

Select primer at position 128 :  `YGARGGNGGNCARAYGCC`

COG0200-COG0097 stat:


| cog_pair        | nb represented genomes | different_pair_organisations                                                                      | min post len filtering | max_post_len_filtering | mean post len filtering | median post len filtering | std post len_filtering | nb alternative site |
|-----------------|------------------------|------------------------------------------------------------------------------------|------------------------|------------------------|-------------------------|---------------------------|------------------------|---------------------|
| COG0097-COG0200 | 5445                   | COG0097>-COG0200>:5435 COG0097>-<COG0200:4 COG0200>-COG0097>:3 <COG0097-COG0200>:5 | 1752                   | 9258                   | 2220.228840125392       | 2127.0                    | 453.1673161760394      | 4                   |

Expected Size is around 2220nt


The Tm of the primers are not optimal : (https://tmcalculator.neb.com/#!/main)

Anyway let's try ecoPCR :

```bash

ecopcr/src/ecoPCR -d ecopcr_db/ref_rep_genomes_merged -e 3 -l 1500 -L 3000 YGARGGNGGNCARAYGCC YTTNCCYTTRTANGGYTC > search_COG0200-COG0097_test.ecopcr

```

```
Job ID: 8642501
Cluster: genobull
User/Group: jmainguy/BIOINFO
State: COMPLETED (exit code 0)
Cores: 1
CPU Utilized: 00:19:23
CPU Efficiency: 99.83% of 00:19:25 core-walltime
Job Wall-clock time: 00:19:25
Memory Utilized: 169.48 MB
Memory Efficiency: 4.14% of 4.00 GB
```

Both gyrB and COG0200-0097 took around 20 minutes
Does not use any memory

Coverage with ecotaxstat
```bash
module load bioinfo/obitools-v1.2.11
ecotaxstat -d ecopcr_db/ref_rep_genomes_merged search_gyrB_test.ecopcr -r 2
```

### Over all refseq but only on the region:

Build db of the region

```
python filter_sequence.py --add_taxid --assemblies_with_taxonomy all_assemblies_with_taxonomy.tsv --taxon_name 'Bacteria [superkingdom]' -o ecoPCR/COG0097-COG0200.fna region_extracted/COG0097-COG0200.fna

module load bioinfo/obitools-v1.2.11
obiconvert --fasta ecoPCR/ecopcr_db/COG0097-COG0200.fna --ecopcrdb-output=ecoPCR/ecoprimer_db/COG0097-COG0200 -t tax_dump/
```



### over the 16 and gyrB

```bash
ecoPCR/ecopcr/src/ecoPCR -d ecoPCR/ecopcr_db/ref_rep_genomes_merged -e 0 -l 100 -L 500 'ACGGRAGGCAGCA#G#' 'TACCAGGGTATCTAATCC#T#' > ecoPCR/search_16v3v4_e0.ecopcr ; module load bioinfo/obitools-v1.2.11;ecotaxstat -d ecoPCR/ecopcr_db/ref_rep_genomes_merged ecoPCR/ecoPCR/search_16v3v4_e0.ecopcr -r 2 > ecoPCR/search_16v3v4_e0.ecotaxstat;ecotaxspecificity -d ecoPCR/ecopcr_db/ref_rep_genomes_merged -e 5 --ecopcr ecoPCR/search_16v3v4_e0.ecopcr > ecoPCR/search_16v3v4_e0.ecotaxspecificity
```

# Degeneracy choice:

a possible degeneracy, i.e. 1, 2, 3, 4, 6, 8, 9, 12, and so forth (or more generally a number > 0 that can be expressed as 2^i * 3^j, where i and j are integers or 0).

```python
>>> deg = set()
>>> for n in range(10):
...   for k in range(10):
...     deg.add(2**n * 3**k)
...
>>> sorted(deg)
1, 2, 3, 4, 6, 8, 9, 12, 16, 18, 24, 27, 32, 36, 48, 54, 64, 72, 81, 96, 108, 128, 144, 162, 192, 216, 243, 256, 288, 324, 384, 432, 486, 512, 576, 648, 729, 768, 864, 972, 1152, 1296, 1458, 1536, 1728, 1944, 2187, 2304, 2592, 2916, 3456, 3888, 4374, 4608, 5184, 5832, 6561, 6912, 7776, 8748, 10368, 11664, 13122, 13824, 15552, 17496, 19683, 20736, 23328, 26244, 31104, 34992, 39366, 41472, 46656, 52488, 62208, 69984, 78732, 93312, 104976, 124416, 139968, 157464, 186624, 209952, 279936, 314928, 373248, 419904, 559872, 629856, 839808, 1119744, 1259712, 1679616, 2519424, 3359232, 5038848, 10077696
```
Shouldwe allow N in the degeneracy? would mean that the number should be included in 2^i * 3^j * 4^k

# Design à la mano

Use of jalview to see the aln

## COG0097
From po 380 of the trimmed aln to po 402 aln is quite conserved
380 - 402
CCNGAVCCNTAYAARGG{Y/N}AARGG

22 nt
deg of 4 * 3 * 4 * 2 * 2 * 2 * 2  = 768
or with the N instead of the Y =  1536

Bit too long:
* remove last 3nt  CCNGAVCCNTAYAARGG{Y/N}AA
* remove first 3nt GAVCCNTAYAARGG{Y/N}AARGG

# ecoPCR pipeline

No used of netxflow: it is too simple and no generic enough. Use of ppython instead
* write a a file with primers from different softwares or mannually design with fields : seq/target/source of teh primer / degenerancy / comment
* specify ecoPCR params
* check if the pairs of primers have already been checked with this set of parameter
* if yes stop here
* if not: launch ecoPCR in a one line command from python

## Build ecoPCR db for region

Use python script that launch sbatch for the 83 region
```bash
python build_ecoPCR_db.py region_extracted/ --assemblies_info all_assemblies_with_taxonomy.tsv -o ecoPCR/ecopcr_db/ -t tax_dump/
```

## Primers:
Get Best primers for a cog
```bash
grep ".*" aln_*_filtered_COG0052*.tsv | sort -k6 -rn | head
```

Select five best primers across degeprime results
```
grep ".*" aln_*_filtered_COG0052*l18.tsv | sed 's/:/'\\t'/g' | sort -k7 -rn | awk -F$'\t' '{print $7,$8,"COG0052",$6,$1,$2,"PrimerMatching:"$7";Pos:"$2}' OFS=$'\t' | head -n100 | sort -u -k1,2 | sort -nrk 1 | cut -f2-7 |head -n5
```

Bash script that does the job : `primer_design/primers/generate_primer_file_from_degeprime_results.sh`


## Python script that prepare ecoPCR and ecotaxstat and check if that has been already launched
```bash
e=2
python ecopcr_primers_validation.py --region_list 'COG0052-COG0264 COG0097-COG0200 COG0080-COG0081 COG0200-COG0201 COG0090-COG0197' \
                                    --primers_dir ecoPCR/primers/ \
                                    --ecopcr_db_dir ecoPCR/ecopcr_db/ \
                                    --regions_info ../nextflow_result_2/pairs_stats_collected.tsv \
                                    -e $e > ecopcr_command_e${e}.sarray
```

sarray command on the sarray file
`sarray -J ecopcr_e2 -o %x.out ecopcr_command_e2.sarray`


`grep ^'species  ' ecoPCR/COG0*-COG0*/COG0*-COG0*/*.ecotaxstat | sort -rnk 4 | head`


`sarray -J ecopcr_e5 -o %x.out ecopcr_command_e5.sarray`

## bash pipeline on degeprime

sarray -J ecopcr_command_e0_COG0052-COG0264 -o slurm_array_out/%x.out ecopcr_command_e0_COG0052-COG0264.sarray
sarray -J ecopcr_command_e0_COG0097-COG0200 -o slurm_array_out/%x.out ecopcr_command_e0_COG0097-COG0200.sarray
sarray -J ecopcr_command_e0_COG0080-COG0081 -o slurm_array_out/%x.out ecopcr_command_e0_COG0080-COG0081.sarray
sarray -J ecopcr_command_e0_COG0200-COG0201 -o slurm_array_out/%x.out ecopcr_command_e0_COG0200-COG0201.sarray
sarray -J ecopcr_command_e0_COG0090-COG0197 -o slurm_array_out/%x.out ecopcr_command_e0_COG0090-COG0197.sarray


divide file that are too big :
done manualy
```bash
$ head ecopcr_command_e0_COG0090-COG0197.sarray -n 1000 > ecopcr_command_e0_COG0090-COG0197.sarray.part1
$ head ecopcr_command_e0_COG0090-COG0197.sarray -n 2000 | head -n 1000 > ecopcr_command_e0_COG0090-COG0197.sarray.part2
$ head ecopcr_command_e0_COG0090-COG0197.sarray -n 2000 | tail -n 1000 > ecopcr_command_e0_COG0090-COG0197.sarray.part2
$ head ecopcr_command_e0_COG0090-COG0197.sarray -n 3000 | tail -n 1000 > ecopcr_command_e0_COG0090-COG0197.sarray.part3
$ cat ecopcr_command_e0_COG0090-COG0197.sarray | tail -n 1080 > ecopcr_command_e0_COG0090-COG0197.sarray.part5
$ wc -l ecopcr_command_e0_COG0090-COG0197.sarray.*
   1000 ecopcr_command_e0_COG0090-COG0197.sarray.part1
   1000 ecopcr_command_e0_COG0090-COG0197.sarray.part2
   1000 ecopcr_command_e0_COG0090-COG0197.sarray.part3
   1080 ecopcr_command_e0_COG0090-COG0197.sarray.part5
   4080 total
$ cat ecopcr_command_e0_COG0090-COG0197.sarray.* > trash
$ diff trash ecopcr_command_e0_COG0090-COG0197.sarray
```
## ecotaxspecificity

```bash
module load bioinfo/obitools-v1.2.11
ecotaxspecificity -d ecoPCR/ecopcr_db/COG0052-COG0264 -e 5 ecoPCR/ecopcr_db/COG0052-COG0264.fna > ecotaxspecificity/result.txt
```
With all refSeq take more than 200 days

Try with rep and ref
Filter sequences of the region :
```
python filter_sequence.py region_extracted/COG0052-COG0264.fna  --add_taxid --assemblies_with_taxonomy all_assemblies_with_taxonomy.tsv  -o region_filtered/refrep_COG0052-COG0264.fna --assembly_list_to_keep accession_list/ref_rep_genomes.txt

ecotaxspecificity -d ecoPCR/ecopcr_db/ref_rep_genomes_merged -e 5 region_filtered/refrep_COG0052-COG0264.fna > ecotaxspecificity/result.txt

```
Use of sarray : `sarray -J ecospec_e2 -o %x.out --mem 8G ecospecif.sarray`


## Several command :
```
grep ^'species  ' degeprime_results/bash_pipeline/ecoPCR/COG0*-COG0*/COG0*-COG0*/*.ecotaxstat |   awk -F$'/' '{print $4,$5,$6,$7,$8,$9}' OFS=$'\t' | sort -rnk 6 | sort -u -k1,1
#number of different genome
cut -f1 -d'|' search_16v3v4_e0.ecopcr | grep '#' -v | sort | uniq -c | wc -l

ecopcr/src/ecoPCR -d ecopcr_db/ref_rep_genomes_merged -e 0 -l 100 -L 500 MGNCCNGSNATGTAYATHGG CNCCRTGNARDCCDCCNGA > search_gyrB_e0.ecopcr

ecotaxstat -d ecopcr_db/ref_rep_genomes_merged search_16v3v4_e0.ecopcr -r 2 > search_16v3v4_e0.ecotaxstat
```

## Post process of ecoPCR result
What version of degenerate primers are used an din which proportion..

```
cat AllRefseqRegion_fCOG0097_9-rCOG0200_1_l500_L8000_e0.ecopcr  | grep -v ^'#' | cut -d'|' -f15 | sort | uniq -c | wc -l

```

# Dimer check
With script
conda install -c bioconda viennarna -n primerprospector

check_primer_barcode_dimers.py is not working with viennarna >=2.0

Need an older version
conda install -n pprospector viennarna=1.8.5

check_primer_barcode_dimers.py script has many problem that should be fixed
Not all possible combination of primer are checked but only the version degenerate against it self ..
It is possible and would be easy to modify the script in order to let it do what we want


check_primer_barcode_dimers.py -p ACTCGTAGACGATTGACGGACT -P CAGGACGATTAACGATTAC -b no_barcode_file.txt -e dna_DM.par

conda script files :
/home/jmainguy/.conda/envs/primerprospector/lib/python2.7/site-packages/primerprospector

copy script to modify `check_primer_barcode_dimers.py`, rename it `check_primer_dimers.py` and create symb link of the copy in the orginal folder of conda python modules: `/home/jmainguy/.conda/envs/primerprospector/lib/python2.7/site-packages/primerprospector/`

cp the executable in the bin dir of conda `/home/jmainguy/.conda/envs/primerprospector/bin/check_primer_barcode_dimers.py` to create a new executable with the name `check_primer_dimers.py` in the folder `primer_design/primer_prospector/bin_modified/`

## Small manual benchmark :
Is it really faster to call rnaFold with several sequences instead of one by one?

One by one :

```
time python bin_modified/check_primer_dimers.py -p CGGARGRGACGATTRCTCGTANACGA -P GACCCTRWSGTCCTNTCCTCRGA -b no_barcode_file.txt -e dna_DM.par
  This is a modified version of check_primer_barcode_dimers.py
  ('CGGARGRGACGATTRCTCGTANACGA', 32)
  ('GACCCTRWSGTCCTNTCCTCRGA', 64)
  ('Couple to test ', 4656)
  real	1m29.191s
  user	1m12.341s
  sys	0m15.303s

```

In one call of RNA fold:
modification of the script give a list of the sequences to test instead of a list containing  one seq as it is done in PrimerProspector
```
time python bin_modified/check_primer_dimers.py -p CGGARGRGACGATTRCTCGTANACGA -P GACCCTRWSGTCCTNTCCTCRGA -b no_barcode_file.txt -e dna_DM.par

  real	0m6.755s
  user	0m6.054s
  sys	0m0.344s

```

No doubt that one call is clearly faster  


Test on primers of COG200-COG097```
('GANCCNTAYAARGSNAARGG', 1024)
('GGNNKYTGNCCNCCYTC', 2048)
# primer1:GANCCNTAYAARGSNAARGG;degeneracy:1024
# primer2:GGNNKYTGNCCNCCYTC;degeneracy:2048
# number of couples to test 4 720 128

real	81m13.028s
user	69m9.730s
sys	6m36.542s```


Using a linker has an influence
On top testing both primer in 5'=== 3'-------5'=== 3'  
it is necessary to reverse one of the primer to tets all possibility
 5'=== 3' ---- 3'=== 5'
 These idea is wrong as sequences to fold need to respect the orientation of the molecule
 both 5' of two sequences would not paired  together:
  5'===> 3'
    ||||
  5'===> 3' --> This is not possible

  5'===> 3'
    ||||
  3'<=== 5'


Trying RNAfold on a python terminal
 ```python
 from cogent.app.vienna_package import RNAfold


r = RNAfold(WorkingDir='test/')
annealing_temp=55.0
r.Parameters['-P'].on('dna_DM.par')

r.Parameters['-T'].on(annealing_temp)

 ```

unsuccessful try to prevent .ps to be created by hacking the cogent object

```python
from cogent.app.parameters import Parameter, FlagParameter, ValuedParameter
r.Parameters['-noPS'] = FlagParameter(Prefix='-',Name='-noPS')
r.Parameters['-noPS'].on()
```

Use of a unique name
Give a file / json . with primer info that the script process
Check inter / intra folding

Help on dimers
--> https://www.researchgate.net/post/What_is_the_interpretation_of_the_Tm_and_DG_of_an_amplicon_secondary_structure_in_qPCR
Vocab:
* intra primer binding (hair pin loops)
* homo & hetero inter primer binding (primer dimers)



# Alternative to ecoPCR:
emboss primer search
http://emboss.toulouse.inra.fr/cgi-bin/emboss/help/primersearch
Has a web implementation http://emboss.toulouse.inra.fr/cgi-bin/emboss/primersearch


# Obitools dev
Goal is to parallelize ecotaxspecificity !
Changed input to let it accept chunck of sequences

Use of simple nextflow script `ecotaxspecificity_parall.nf` to manage the splitting of the input file in chunks and successive call of the modified script
```
conda activate nextflow
nextflow ecotaxspecificity_parall.nf -resume
```
Check if resolution obtained by our approach is identical with obitool approach

```
module load bioinfo/obitools-v1.2.11
ecotaxspecificity -d ../ecoPCR/ecopcr_db/ref_rep_genomes_merged -e 10 search_gyrB_e_HEAD1000.ecopcr > search_gyrB_e_HEAD1000_e10.ecotaxspecificity
```

It is identical !

Launch nextflow on real long read data :
```
conda activate nextflow
nextflow ecotaxspecificity_parall.nf --sequence_file ../COG0097_9-COG0200_1/refseqRepRef_fCOG0097_9-rCOG0200_1_l500_L8000_e0.ecopcr -with-report -profile cluster -resume
```

Divided in 1000 chunks so 1000 slurm job.
Around 11817 sequences per chunks and took 21 minutes to complete
and use a full cpu

The folowing process handling graph and resolution computing `process_raw_ecotax_result` took more than 6G of memomry and 30 minutes to complete.

It takes to much memory ! The input file and ouput file are not so big.

Small bug in the script: does not manage correctly distance of 0 between sequences.
Improve by a if statement that prevent computing taxid not ok  when there is no new edges./


nextflow on real data
error in ecotaxspecificity modified script error 137 or 147
grep 'Execution is retried' slurm-10027253.out | cut -d'-' -f2- | sort | uniq -c
watch tail -n50 slurm-10027253.out
