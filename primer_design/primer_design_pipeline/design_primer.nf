params.files =  "sequences/*"
params.cpus = 2

multifna = Channel.fromPath(params.files)

tuple_name_seq = multifna.map { it -> [it.getBaseName(), it] }
params.assembly_list = 'accession_list/reference_genomes.txt'
result_dir = 'primer_design_results/' + file(params.assembly_list).getSimpleName()

process seq_preparation {
  label 'condaEnv'
  publishDir "${result_dir}/filtered_sequnces/", mode:'copy'

  input:
    tuple val(name), file(seq) from tuple_name_seq
  output:
  tuple val(name), file("filtered_$seq"), file("taxonomy_${name}.tsv") into seqfna_to_align
  tuple val(name), file("filtered_${name}.faa"), file("taxonomy_${name}.tsv") into seqfaa_to_align
  file "filtered_$seq" into fna_seq


  """
  python $PWD/filter_sequence.py $seq --assembly_list_to_keep $PWD/${params.assembly_list} -o filtered_$seq

  python $PWD/create_taxonomy_file.py filtered_$seq $PWD/all_assemblies_with_taxonomy.tsv -o taxonomy_${name}.tsv

  #python $PWD/translate_fna.py filtered_$seq -o filtered_${name}.faa
  python $PWD/get_faa_from_fna.py filtered_$seq -o filtered_${name}.faa
  """
}

fna_seqs = fna_seq.collect()

seqfna_to_align.into { seqfna_to_align; seqfna_to_back_translate}

seq_to_align = seqfna_to_align.mix(seqfaa_to_align)

seq_to_align.into { seq_to_align_clustalo;seq_to_align_mafft }

process clustalo {
  label 'condaEnv'
  cpus params.cpus
  publishDir "${result_dir}/clustalo_aln/", mode:'copy'

  input:
    tuple val(name), file(seq), file(taxo) from seq_to_align_clustalo
  output:
    tuple val(name), file("aln_clustalo_$seq"), file(taxo) into alignment_tuple_clustalo

  """

  clustalo -i $seq -o "aln_clustalo_$seq" --threads=${params.cpus}

  """
}

process mafft {
  label 'condaEnv'
  cpus params.cpus
  publishDir "${result_dir}/mafft_aln/", mode:'copy'

  input:
    tuple val(name), file(seq), file(taxo) from seq_to_align_mafft
  output:
    tuple val(name), file("aln_mafft_$seq"), file(taxo) into alignment_tuple_mafft

  """
  mafft --auto --thread $params.cpus $seq > aln_mafft_$seq
  #mafft --maxiterate 1000 --globalpair --thread $params.cpus $seq >  aln_mafft_$seq
  """
}

alignments = alignment_tuple_mafft.mix(alignment_tuple_clustalo)

alignment_branched = alignments.branch {
                                              faa: it[1].getExtension() ==~ /faa/
                                              fna: it[1].getExtension() ==~ /fna/
                                          }


process pal2nal {
  label 'condaEnv'
  publishDir "${result_dir}/aln_back_translated/", mode:'copy'
  input:
    tuple val(name), file(aln_prot), file(taxo) from alignment_branched.faa
    file "fna_seq/*" from fna_seqs
  output:
    tuple val(name), file("*_prot_to_nucl.fna"), file(taxo) into alignment_back_translated

  """
    aln_prot=$aln_prot
    aln_prot_to_nucl="\${aln_prot%.faa}_prot_to_nucl.fna"
    echo \$aln_prot_to_nucl
    pal2nal.pl $aln_prot fna_seq/filtered_${name}.fna -output fasta > \$aln_prot_to_nucl
  """

}

alignment_for_primers = alignment_back_translated.mix(alignment_branched.fna)

degeneracies = Channel.from( 12,18,24)
primer_lengths  = Channel.from( 14,16,18)

process degeprime {
  publishDir "${result_dir}/degeprime_results/", mode:'copy', pattern: "*_degeprime*.tsv"
  publishDir "${result_dir}/trimmed_alignment/", pattern: "trimmed*"

  input:
    set val(name), file(alignment), file(taxonomy_file) from alignment_for_primers
    each d from degeneracies
    each l from primer_lengths
  output:
    file "*_degeprime_d*.tsv" into out
    file "trimmed*" into trimmed_aln

  """
  taxlevel=1
  alignment=$alignment
  output_fl=\${alignment%.*}_degeprime_d${d}_l${l}.tsv
  perl $PWD/DEGEPRIME/TrimAlignment.pl -i $alignment -min 0.9 -o trimmed_$alignment
  perl $PWD/DEGEPRIME/DegePrime.pl -i trimmed_$alignment -d $d -l $l -o \$output_fl #-taxfile $taxonomy_file  -taxlevel \$taxlevel
  """
}
