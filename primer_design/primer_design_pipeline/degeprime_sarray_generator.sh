
s_array_file='degeprime_multiple_params.sarray'

COG_list='COG0052 COG0264 COG0097 COG0090 COG0197 COG0201 COG0094 COG0200 COG0093 COG0080 COG0081'
result_dir='degeprime_results/bash_pipeline/primers/'
trimmed_aln_dir='degeprime_results/bash_pipeline/trimmed_alignment/'
degeneracies='12 24 64 162 256 1024 2048'
lengths='14 15 16 17 18 19 20 21'

echo '#' $COG_list > $s_array_file
echo '# degeneracies' $degeneracies>> $s_array_file
echo '# lengths' $lengths>> $s_array_file
echo '# result:' $result_dir>> $s_array_file

for cog in $COG_list
do
  alignment="degeprime_results/mafft_aln/aln_mafft_filtered_${cog}.fna"
  echo $alignment
  for d in $degeneracies
  do
    for l in $lengths
    do
      alignment_name=`basename $alignment`
      output_fl=$result_dir/${alignment_name%.*}_degeprime_d${d}_l${l}.tsv


      echo "perl DEGEPRIME/TrimAlignment.pl -i $alignment -min 0.9 -o $trimmed_aln_dir/trimmed_$alignment_name;perl DEGEPRIME/DegePrime.pl -i $trimmed_aln_dir/trimmed_$alignment_name -d $d -l $l -o $output_fl" >> $s_array_file
    done
  done

done

echo sarray -J degeprime -o slurm_array_out/%j_%x.out $s_array_file
