params.files =  "sequences/*"
params.cpus = 2

multifna = Channel.fromPath(params.files)

tuple_name_seq = multifna.map { it -> [it.getBaseName(), it] }


degeneracies = Channel.from(12, 24, 64, 162, 256)
primer_lengths  = Channel.from(17,18,19)


params.assembly_list = 'accession_list/reference_genomes.txt'

params.result_dir = 'degeprime_results/' + file(params.assembly_list).getSimpleName()
result_dir = params.result_dir
params.seq_preparation = true

if (params.seq_preparation != true) {
  println 'No seq_preparation. input sequences are directly aligned'
  tuple_name_seq.into {tuple_name_seq; seq_to_align_with_no_prep }
}

//
process seq_preparation {
  label 'condaEnv'
  publishDir "${result_dir}/filtered_sequnces/", mode:'copy'

  when:
   params.seq_preparation == true

  input:
    tuple val(name), file(seq) from tuple_name_seq
  output:
    tuple val(name), file("filtered_$seq") into seq_to_align_with_prep
  //tuple val(name), file("filtered_${name}.faa"), file("taxonomy_${name}.tsv") into seqfaa_to_align
  //file "filtered_$seq" into fna_seq


  """
  python $PWD/filter_sequence.py $seq --assembly_list_to_keep $PWD/${params.assembly_list} -o filtered_$seq
  """
  // #python $PWD/create_taxonomy_file.py filtered_$seq $PWD/all_assemblies_with_taxonomy.tsv -o taxonomy_${name}.tsv
  //
  // #python $PWD/translate_fna.py filtered_$seq -o filtered_${name}.faa
  // #python $PWD/get_faa_from_fna.py filtered_$seq -o filtered_${name}.faa
  // """
}
if (params.seq_preparation != true) {
  seq_to_align_with_no_prep.set{seq_to_align}
}
else {
  seq_to_align_with_prep.set{seq_to_align}
}

seq_to_align.into { seq_to_align_clustalo;seq_to_align_mafft }

process clustalo {
  label 'condaEnv'
  cpus params.cpus
  publishDir "${result_dir}/clustalo_aln/", mode:'copy'

  input:
    tuple val(name), file(seq) from seq_to_align_clustalo
  output:
    tuple val(name), file("aln_clustalo_$seq") into alignment_tuple_clustalo

  """

  clustalo -i $seq -o "aln_clustalo_$seq" --threads=${params.cpus}

  """
}

process mafft {
  label 'condaEnv'
  cpus params.cpus
  publishDir "${result_dir}/mafft_aln/", mode:'copy'

  input:
    tuple val(name), file(seq) from seq_to_align_mafft
  output:
    tuple val(name), file("aln_mafft_$seq") into alignment_tuple_mafft

  """
  mafft --auto --thread $params.cpus $seq > aln_mafft_$seq
  #mafft --maxiterate 1000 --globalpair --thread $params.cpus $seq >  aln_mafft_$seq
  """
}

alignment_for_primers = alignment_tuple_mafft.mix(alignment_tuple_clustalo)


process degeprime {
  publishDir "${result_dir}/degeprime_results/", mode:'copy', pattern: "*_degeprime*.tsv"
  publishDir "${result_dir}/trimmed_alignment/", pattern: "trimmed*"

  input:
    set val(name), file(alignment) from alignment_for_primers
    each d from degeneracies
    each l from primer_lengths
  output:
    file "*_degeprime_d*.tsv" into out
    file "trimmed*" into trimmed_aln

  """
  #taxlevel=1
  alignment=$alignment
  output_fl=\${alignment%.*}_degeprime_d${d}_l${l}.tsv
  perl $PWD/DEGEPRIME/TrimAlignment.pl -i $alignment -min 0.9 -o trimmed_$alignment
  perl $PWD/DEGEPRIME/DegePrime.pl -i trimmed_$alignment -d $d -l $l -o \$output_fl #-taxfile \$taxonomy_file -taxlevel \$taxlevel
  """
}
