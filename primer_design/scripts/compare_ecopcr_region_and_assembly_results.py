#!/usr/bin/env python3

"""
Description

:Example:
python template.py -v
"""

# Metadata
__author__ = 'Mainguy Jean - Plateforme bioinformatique Toulouse'
__copyright__ = 'Copyright (C) 2020 INRAE'
__license__ = 'GNU General Public License'
__version__ = '0.1'
__email__ = 'support.bioinfo.genotoul@inra.fr'
__status__ = 'dev'


from argparse import ArgumentParser, ArgumentDefaultsHelpFormatter
import logging
from collections import defaultdict
import csv
import os
import json
import numpy as np
from Bio import SeqIO
import json


def decoder(filename):
    """Load a json file and return the corresponding structure."""
    with open(filename, 'r', encoding='utf8') as file:
        return json.load(file)

def list_of_dict_to_tsv(list_of_dict, output_file, mode='w', writeheader=True):
    """Encode a list of dictionnary into a tsv file.

    The mode of writing can be specified.
    By default the mode is set to "w"

    """
    if len(list_of_dict) == 0:
        raise ValueError('The given list is empty: we cant write it in a file')

    with open(output_file, mode, newline='') as csvfile:
        writer = csv.DictWriter(csvfile, fieldnames=list(list_of_dict[0].keys()), delimiter='\t')
        if mode == 'w' and writeheader is True:
            writer.writeheader()
        for dict_info in list_of_dict:
            writer.writerow(dict_info)

def rm_duplicated_amplicons_from_overlapping_sequences(seq_header_to_amplicons):
    """
    Remove duplicated amplicons caused by overlapping target sequences.
    Sometime target sequences can be overlapping:

    assembly : -------------------------------
    target1:      |========================|
    target2:    |==========================|
    amplicon:         |~~~~~~~~~~~~~~~~|

    In this case the same amplicon is found in the two target so it is identified twice while only retrieved one in assembly.


    """
    logging.info('Remove duplicated amplicons caused by overlapping target sequences.')
    amplicon_remove_count = 0
    # group seq header by molecul
    header_by_mol = defaultdict(list)
    for seq_header in seq_header_to_amplicons.keys():
        # seq header example : GCF_007197995.1|NZ_VKKH01000001.1|13699-14829
        # mol id is GCF_007197995.1|NZ_VKKH01000001.1
        mol = '|'.join(seq_header.split('|')[:2])
        header_by_mol[mol].append(seq_header)

    # Identify overlapping seq
    # seq header example : GCF_007197995.1|NZ_VKKH01000001.1|13699-14829
    headers_group = [(mol, headers) for headers in header_by_mol.values() if len(headers) > 1 ]
    for mol, headers in headers_group:
        overlapping_seqs = []
        for i, seq_header1 in enumerate(headers[:-1]):
            start1, end1 = (int(po) for po in seq_header1.split('|')[2].split('-'))

            for seq_header2 in headers[i+1:]:

                start2, end2 = (int(po) for po in seq_header2.split('|')[2].split('-'))

                if start1 <= start2 <= end1:
                    overlapping_po = start2, min(end1, end2)
                    overlapping_seqs.append((seq_header1, seq_header2))

                elif start2 <= start1 <= end2:
                    overlapping_po = start1, min(end1, end2)
                    overlapping_seqs.append((seq_header1, seq_header2))

        # if overlapping_seqs:
        #     logging.info(f'{len(overlapping_seqs)} overlaping pair of sequences')

        # remove duplicate amplicons in overlapping seq if any
        for seq_header1, seq_header2 in overlapping_seqs:

            amplicon_to_rm_small = []

            start1, end1 = (int(po) for po in seq_header1.split('|')[2].split('-'))
            length1 = end1 - start1 + 1
            start2, end2 = (int(po) for po in seq_header2.split('|')[2].split('-'))
            length2 = end2 - start2 + 1

            small_seq, big_seq = (seq_header1, seq_header2) if length1 < length2 else  (seq_header2, seq_header1)

            amplicons_small = seq_header_to_amplicons[small_seq]
            amplicons_big = seq_header_to_amplicons[big_seq]

            amplicons_seqs_big = [amplicon_splitted_l[20] for amplicon_splitted_l in amplicons_big ]

            for amplicon_small in amplicons_small:
                amplicon_seq_small = amplicon_small[20]

                if amplicon_seq_small in amplicons_seqs_big:
                    amplicon_to_rm_small.append(amplicon_small)

            # if amplicon_to_rm_small:
            #     logging.info(f'{len(amplicon_to_rm_small)} duplicated amplicons between {seq_header1}, {seq_header2}')

            for amplicon_to_rm in amplicon_to_rm_small:
                seq_header_to_amplicons[small_seq].remove(amplicon_to_rm)
                amplicon_remove_count += 1

    logging.info(f'{amplicon_remove_count} duplicated amplicons caused by overlapping target sequences have been removed')




def get_ecopcr_comparable_amplicon_info(splitted_l):
    assembly = splitted_l[0].split('|')[0]
    try:
        f_primer = splitted_l[13]
        r_primer = splitted_l[16]
        f_mismatch = splitted_l[14]
        r_mismatch = splitted_l[17]
        mismatch = max(int(f_mismatch), int(r_mismatch))

        length = int(splitted_l[19])
        sequence = splitted_l[20]
    except IndexError:
        logging.warning(splitted_l)

    info = {'assembly': assembly,
            "f_primer":f_primer,
            "r_primer": r_primer,
            "f_mismatch": f_mismatch,
            "r_mismatch": r_mismatch,
            "mismatch": mismatch,
            "length": length,
            "sequence": sequence,
            }
    return info

"""
Not used fct
def identify_contained_sequences(ecopcr_result):
    # sequences that are contained in a bigger one and that would create duplicate amplicon..
    contig_assembly_to_position = defaultdict(list)
    with open(ecopcr_result, encoding="latin-1") as fl:
        for l in fl:
            if l.startswith('#'):
                continue
        seq_header = [e.strip() for e in l.split(' | ')][-1]
        assembly, seqid, position = seq_header.split('|')
        contig_assembly_to_position[(assembly, seqid)].append(seq_header)

    contained_seq_header = [] # sequences that are contained in a bigger one
    for seq_headers in contig_assembly_to_position.values():

        for i, seq_header in enumerate(seq_headers[:-1]):
            position = seq_header.split('|')[-1]
            for seq_header_next in enumerate(seq_headers[i+1:]):
                position_next = seq_header_next.split('|')[-1]

"""


def is_circular_sub_sequence(sequence, sub_sequence):
    assert sub_sequence not in sequence # subsequence is not included in the linear sequence
    assert len(sequence) >= len(sub_sequence)

    for i in range(len(sub_sequence)):
        if sequence.endswith(sub_sequence[:i]) and sequence.startswith(sub_sequence[i:]):
            #logging.info('sub_sequence is a circular subsequence of the sequence')
            #logging.info(f'sub_sequence: {sub_sequence[:i]}||{sub_sequence[:i]}')
            #logging.info(sequence)
            #logging.info(f'{sub_sequence[i:]}{"_"*(len(sequence) -len(sub_sequence))}{sub_sequence[:i]}')
            return True

    return False

def parse_ecopcr_result(ecopcr_result):
    amplicons = []
    logging.info(f'Parse ecopcr file {ecopcr_result}')
    with open(ecopcr_result, encoding="latin-1") as fl:
        for l in fl:
            if l.startswith('#'):
                continue
            splitted_l = [e.strip() for e in l.split(' | ')]
            amplicons.append(splitted_l)

    return amplicons


def clean_ecopcr_region_amplicons(ecopcr_result, sequence_db, assembly_infos,  mismatch_max=2):
    seq_header_to_amplicons = defaultdict(list)
    all_amplicons =  parse_ecopcr_result(ecopcr_result)
    clean_amplicons = []

    for splitted_l in all_amplicons:
        # from GCF_007197995.1|NZ_VKKH01000001.1|13699-14829 WP_143833847.1|WP_143833848.1
        # I want to keep only GCF_007197995.1|NZ_VKKH01000001.1|13699-14829
        seq_part_header  = splitted_l[-1].split(' ')[0]

        seq_header_to_amplicons[seq_part_header].append(splitted_l)

    rm_duplicated_amplicons_from_overlapping_sequences(seq_header_to_amplicons)

    circular_amplicons_count = 0
    sequence_with_smaller_amplicons = 0
    circular_amplicons = []
    smaller_amplicons = []
    # Remove circular amplicon
    with open(sequence_db) as fl_r:
        for record in SeqIO.parse(fl_r, "fasta"):
            # From GCF_002564005.1|s000 taxid=372484; GCF_002564005.1|NZ_PDJG01000001.1|2090040-2091292 WP_098455138.1|WP_098455139.1
            # I want to keep GCF_002564005.1|NZ_PDJG01000001.1|2090040-2091292
            seq_part_header = record.description.split(' ')[2]

            assembly = seq_part_header.split('|')[0]
            if assembly not in assembly_infos:
                continue

            amplicons = seq_header_to_amplicons[seq_part_header]
            if len(amplicons) == 0:
                continue

            amplicons_without_circular = []
            for amplicon_splitted_l in amplicons:
                amplicon_seq = amplicon_splitted_l[20]

                if amplicon_seq not in record.seq and is_circular_sub_sequence(record.seq, amplicon_seq):
                    logging.info(f'{seq_part_header} has a circular amplicon ')
                    circular_amplicons_count += 1
                    circular_amplicons.append(amplicon_splitted_l)
                else:
                    amplicons_without_circular.append(amplicon_splitted_l)
            if len(amplicons_without_circular) == 0:
                logging.warning(f'{seq_part_header}. Only circular amplicon {len(amplicons)}  ')

            elif len(amplicons_without_circular) > 1:
                amplicons_without_circular.sort(key = lambda s: len(s[20]), reverse=True) # sort on length of the amplicon...

                if len(amplicons_without_circular[0][20]) == len(amplicons_without_circular[1][20]):
                    logging.critical('More than one amplicon with the biggest size')


                smaller_amplicons += amplicons_without_circular[1:]
                sequence_with_smaller_amplicons += 1

                clean_amplicons.append(amplicons_without_circular[0])

            elif len(amplicons_without_circular) == 1 :
                clean_amplicons.append(amplicons_without_circular[0])

    logging.info(f'{ecopcr_result} --> {circular_amplicons_count} amplicons have been removed because of circularity')
    logging.info(f'{ecopcr_result} --> {len(smaller_amplicons)} amplicons have been removed in {sequence_with_smaller_amplicons} sequences because it exists a bigger amplicon')

    circular_amplicon_rm_per_mismatch = {mismatch: {'lengths': [], 'assemblies': []}
                                             for mismatch in range(mismatch_max+1)}

    for amplicon in circular_amplicons:
        amplicon_info = get_ecopcr_comparable_amplicon_info(amplicon)
        circular_amplicon_rm_per_mismatch[amplicon_info['mismatch']]['lengths'].append(amplicon_info['length'])
        circular_amplicon_rm_per_mismatch[amplicon_info['mismatch']]['assemblies'].append(amplicon_info['assembly'])

    smaller_amplicon_rm_per_mismatch = {mismatch: {'lengths': [], 'assemblies': []}
                                         for mismatch in range(mismatch_max+1)}
    for amplicon in smaller_amplicons:
        amplicon_info = get_ecopcr_comparable_amplicon_info(amplicon)
        smaller_amplicon_rm_per_mismatch[amplicon_info['mismatch']]['lengths'].append(amplicon_info['length'])
        smaller_amplicon_rm_per_mismatch[amplicon_info['mismatch']]['assemblies'].append(amplicon_info['assembly'])

    return clean_amplicons, circular_amplicon_rm_per_mismatch, smaller_amplicon_rm_per_mismatch


    # amplicons_without_circular = [amplicon for amplicon in all_amplicons if amplicon not in circular_amplicons]

    # Remove false circular amplicon



def compare_ecopcr_files(
        region_amplicons_lines, ecopcr_assemblies, assembly_list, mismatch_max=2, amplicon_writers=None, length_max=None):
    # fprimers_to_amplicons = defaultdict(list)
    # rprimers_to_amplicons = defaultdict(list)


    result_per_mismatch_fp = {mismatch: {'lengths': [], 'assemblies': []}
                              for mismatch in range(mismatch_max+1)}
    result_per_mismatch_tp = {mismatch: {'lengths': [], 'assemblies': []}
                              for mismatch in range(mismatch_max+1)}
    expected_amplicon_by_assembly = {}
    found_line_index_by_assembly = defaultdict(list)
    tp_assembly_count = 0
    fp_assembly_count = 0
    unexpected_duplicated_count = 0
    region_amplicon_count = 0

    for splitted_l in region_amplicons_lines:

        #splitted_l = [e.strip() for e in l.split(' | ')]
        assembly = splitted_l[0].split('|')[0]


        # keep only line of the genome of interest
        if assembly not in assembly_list:
            logging.info(
                'amplicon is from an assembly that is not part of the given assembly selection. This amplicon is ignored')
            continue
        # result_per_mismatch[mismatch]['lengths'].append(length)
        # result_per_mismatch[mismatch]['assemblies'].append(assembly)
        amplicon = get_ecopcr_comparable_amplicon_info(splitted_l)

        if length_max and amplicon['length'] > length_max:
            logging.info(f'The amplicon has a length > {length_max}. It is ignored..' )
            continue

        if amplicon['mismatch'] > mismatch_max:
            warning = f'An amplicon has {mismatch} mismatches while the maximum number of mismatches take into acount is {mismatch_max}. '
            warning += 'This amplicon is ignored.'
            logging.info(warning)
            continue

        if assembly not in expected_amplicon_by_assembly:
            expected_amplicon_by_assembly[assembly] = {}

        amplicon_key = tuple(sorted(amplicon.items()))
        if amplicon_key not in expected_amplicon_by_assembly[assembly] :
            expected_amplicon_by_assembly[assembly][amplicon_key] = {"count":0, "found":0}

        expected_amplicon_by_assembly[assembly][amplicon_key]['count'] += 1
        region_amplicon_count += 1

    amplicon_count = 0

    for assembly_f in ecopcr_assemblies:
        expected_lines = []
        unexpected_lines = []
        unexpected_duplicate_lines = []
        #fp_out_fl = open(''.join(assembly_f.split('.')[:-1]) + "false_positive.ecopcr", 'w')
        logging.info(f'Parse ecopcr file {assembly_f}')

        with open(assembly_f, encoding="latin-1") as fl:
            for l in fl:
                if l.startswith('#'):
                    continue

                splitted_l = [e.strip() for e in l.split(' | ')]
                assembly = splitted_l[0].split('|')[0]

                # keep only line of the genome of interest
                if assembly not in assembly_list:
                    logging.info(
                        'amplicon is from an assembly that is not part of the given assembly selection. This ammplicon is ignored')
                    continue

                amplicon = get_ecopcr_comparable_amplicon_info(splitted_l)

                mismatch = amplicon['mismatch']
                length = amplicon['length']

                if amplicon['mismatch'] > mismatch_max:
                    warning = f'An amplicon has {mismatch} mismatches while the maximum number of mismatches take into acount is {mismatch_max}. '
                    warning += 'This amplicon is ignored.'
                    logging.info(warning)
                    continue
                if length_max and amplicon['length'] > length_max:

                    logging.info(f'The amplicon has a length > {length_max}. It is ignored..' )
                    continue
                amplicon_count += 1
                amplicon_tuple = tuple(sorted(amplicon.items()))
                if assembly in expected_amplicon_by_assembly and amplicon_tuple in expected_amplicon_by_assembly[assembly]:
                    amplicon_dict = expected_amplicon_by_assembly[assembly][amplicon_tuple]
                    amplicon_dict['found'] += 1

                    if amplicon_dict['found'] > amplicon_dict['count']:
                        # An expected amplicon is found multiple time in genomes sequences.
                        # It happens when the target region is duplicated but only one region has been identified has target.
                        # The unexpected amplicon are going to be written in a file to keep track of this event.
                        warning = f'Unexpected duplicated amplicon has been found in {assembly}'
                        # warning += f': {amplicon_tuple}'
                        warning +=(f'The amplicon was expected to be found exactly {amplicon_dict["count"]} times in genome sequences but has been found {amplicon_dict["found"] } times already')
                        logging.info(warning)

                        unexpected_duplicated_count += 1
                        unexpected_duplicate_lines.append(l)

                    else:
                        expected_lines.append(l)
                        result_per_mismatch_tp[mismatch]['assemblies'].append(assembly)
                        result_per_mismatch_tp[mismatch]['lengths'].append(length)
                        tp_assembly_count += 1

                else:
                    unexpected_lines.append(l)
                    result_per_mismatch_fp[mismatch]['assemblies'].append(assembly)
                    result_per_mismatch_fp[mismatch]['lengths'].append(length)
                    fp_assembly_count += 1

        if amplicon_writers:
            amplicon_writers['TP'].write(''.join(expected_lines))
            amplicon_writers['FP'].write(''.join(unexpected_lines))
            amplicon_writers['unexpected_duplication'].write(''.join(unexpected_duplicate_lines))


    missing_region_amplicons = 0
    for assembly, amplicons in expected_amplicon_by_assembly.items():
        for amplicon, count_dict in amplicons.items():
            if count_dict['count'] > count_dict['found']:
                logging.critical(f'Region amplicon has not been found in assembly genomes : {assembly}')
                logging.critical(f'The amplicon was expected to be found {count_dict["count"]} times but was found {count_dict["found"]}: {amplicon}')
                missing_region_amplicons += 1

    if missing_region_amplicons != 0:
        logging.critical(f'Found {missing_region_amplicons} missing amplicons')

    couple = ecopcr_assemblies[0].split('/')[1]
    logging.info(f'RESULTS: Assembly amplicon {amplicon_count}: True positive {tp_assembly_count} and False Positive {fp_assembly_count} and unexpected amplicon duplication {unexpected_duplicated_count}')
    print(f'"couple":"{couple}","assembly_amplicon":{amplicon_count},"TP":{tp_assembly_count},"FP":{fp_assembly_count},"unexpected_duplication":{unexpected_duplicated_count},"missing_amplicon":{missing_region_amplicons}')
    if region_amplicon_count != tp_assembly_count:
        logging.critical(
            f'Number of True positive found in assembly {tp_assembly_count} is different of the number of amplicon from the region ecopcr result.. ({region_amplicon_count})  ')



    return result_per_mismatch_tp, result_per_mismatch_fp


def get_ecopcr_files(primer_dirs, assembly_db_name, ecopcr_db_region_dir):
    ecopcr_result_tuples = []
    for couple_dir in primer_dirs:
        couple_dir = couple_dir[:-1] if couple_dir.endswith('/') else couple_dir
        logging.info(f'Get ecoPCR results for {couple_dir}')
        try:
            info_file = [f for f in os.listdir(couple_dir) if f.endswith(
                '.json') and f.startswith('f')][0]
        except IndexError:
            logging.critical(f'No json info file found in primer couple dir {couple_dir}. {couple_dir} is not processed')
            continue
        json_info = decoder(os.path.join(couple_dir, info_file))
        p1_f = json_info['forward_primer']
        p2_r = json_info['reverse_primer']
        target_region_f = p1_f['target']
        target_region_r = p2_r['target']
        if target_region_f != target_region_r:
            region = '-'.join(sorted([target_region_f, target_region_r]))
        else:
            region = target_region_f
        logging.info(f'Region of couple dir {couple_dir} is {region}')

        ecopcr_region_dir = os.path.join(couple_dir, f'ecopcr_{region}')
        ecopcr_region_results = [os.path.join(ecopcr_region_dir, f) for f in os.listdir(
            ecopcr_region_dir) if f.endswith('.ecopcr')]
        if len(ecopcr_region_results) > 1:
            logging.critical(f'More than one ecopcr result file for region have been found: {ecopcr_region_results}')
            continue
        elif len(ecopcr_region_results) == 0:
            logging.critical(f'No ecopcr result file for region have been found in ecopcr_region_dir {ecopcr_region_dir}')
            continue
        else:
            ecopcr_region_result = ecopcr_region_results[0]
            ecopcr_region_result_name = os.path.basename(ecopcr_region_results[0])

        logging.info(f'ecopcr_region_result_name {ecopcr_region_result_name}')
        ecopcr_assembly_dir = os.path.join(couple_dir, f'ecopcr_{assembly_db_name}')
        ecopcr_assembly_results = [os.path.join(ecopcr_assembly_dir, f) for f in os.listdir(
            ecopcr_assembly_dir) if f.endswith(ecopcr_region_result_name)]

        if len(ecopcr_assembly_results) == 0:
            logging.critical(f'No ecopcr result file for assembly have been found in ecopcr_assembly_dir {ecopcr_assembly_dir}')
            continue
        logging.info(f'Region ecopcr {ecopcr_region_result}')
        logging.info(f'{len(ecopcr_assembly_results)} assembly ecopcr files in {ecopcr_assembly_dir} ')



        region_db_seq = os.path.join(ecopcr_db_region_dir, region+'.fna')
        if not os.path.isfile(region_db_seq):
            logging.critical(f'{region_db_seq} does not exist. EcoPCR region sequence db for {region} was not found in {ecopcr_db_region_dir}. ')
            continue
        logging.info(f'{region_db_seq} exists. EcoPCR region sequence db for {region} was found in {ecopcr_db_region_dir}. ')


        primer_couple = os.path.basename(couple_dir)
        ecopcr_result_tuples.append((region, ecopcr_region_result, ecopcr_assembly_results, region_db_seq, primer_couple))

    return ecopcr_result_tuples

def generate_info_line(assemblies_at_m, assemblies, lengths,assembly_to_species, assembly_to_genus, assembly_to_family):
    count = len(assemblies_at_m)
    assemblies_m_specific = set(assemblies_at_m) - set(assemblies)

    species = {assembly_to_species[a] for a in assemblies}
    species_at_m = {assembly_to_species[a] for a in assemblies_at_m }
    species_m_specific = species_at_m - species

    genus = {assembly_to_genus[a] for a in assemblies}
    genus_at_m = {assembly_to_genus[a] for a in assemblies_at_m }
    genus_m_specific = genus_at_m - genus

    family = {assembly_to_family[a] for a in assemblies}
    family_at_m = {assembly_to_family[a] for a in assemblies_at_m }
    family_m_specific = genus_at_m - genus

    assemblies_count = len(assemblies_m_specific)
    species_count = len(species_m_specific)
    genus_count = len(genus_m_specific)
    family_count = len(family_m_specific)


    median_length = None if not lengths else np.median(lengths)


    line_info = {"count" : count,
        "assemblies_covered" : assemblies_count,
        'sp_covered': species_count,
        'genus_covered': genus_count,
        'family_covered': family_count,
        "lengths": median_length,}

    return line_info

def fill_amplicon_details_dict(details, amplicon_type, assemblies, lengths, mismatch):
    for a, l in zip(assemblies, lengths):
        if a not in details:
            details[a] = {
                          'TP':defaultdict(list),
                          'FP':defaultdict(list)
                          }
        details[a][amplicon_type][mismatch].append(l)


def parse_arguments():
    """Parse script arguments."""
    parser = ArgumentParser(description="...",
                            formatter_class=ArgumentDefaultsHelpFormatter)
    parser.add_argument("-v", "--verbose", help="increase output verbosity",
                        action="store_true")
    parser.add_argument("--write_details", help="write details on amplicon... ecopcr files f expected unexpected amplicons and a json file for each primer couples. In some case, files may be big..",
                        action="store_true")
    parser.add_argument("--write_amplicons", help="write details on amplicon... ecopcr files f expected unexpected amplicons and a json file for each primer couples. In some case, files may be big..",
                        action="store_true")
    parser.add_argument("--primer_dirs", nargs='+',
                        help="directories of each primer couples where ecoppcr results are stored",
                        required=True)
    parser.add_argument("--ecopcr_db_assembly_name", help="Name of the ecopcr assembly database. ", required=True)
    parser.add_argument("--ecopcr_db_region_dir", help="Directory where ecopcr databases of region sequences are stored", required=True)
    parser.add_argument("-a", "--assembly_selection", help="",
                        required=True)
    parser.add_argument("-e", "--mismatch_max", help="max mismatch to retrieve and analysed in ecopcr result file",
                        default=2, type=int, required=False)
    parser.add_argument("-o", "--outdir", help="output dir", type=str, default='amplicon_stats/')
    parser.add_argument("--length_tresholds_file", help="File with max length per region to applied a maximum length threshold to amplicons", default=None)

    args = parser.parse_args()
    return args


def main():

    args = parse_arguments()

    if args.verbose:
        logging.basicConfig(format="%(levelname)s: %(message)s", level=logging.DEBUG)
        logging.info('Mode verbose ON')

    else:
        logging.basicConfig(format="%(levelname)s: %(message)s")

    np.seterr('raise')

    mismatch_max = args.mismatch_max
    root_dir = args.outdir

    write_details = args.write_details

    write_amplicons = args.write_amplicons

    primer_dirs = args.primer_dirs
    ecopcr_db_assembly_name = args.ecopcr_db_assembly_name
    ecopcr_db_region_dir = args.ecopcr_db_region_dir


    assembly_summary = args.assembly_selection
    assembly_selection_name = os.path.basename(assembly_summary).split('.')[0]
    #expected_amplicons_dir = f'expected_amplicons/{assembly_selection_name}'

    outdir = os.path.join(root_dir,assembly_selection_name )
    os.makedirs(outdir, exist_ok=True)

    output_file = os.path.join(outdir,  'expected_unexpected_amplicon_stats.tsv')

    length_treshold_by_region_fl = args.length_tresholds_file
    if length_treshold_by_region_fl:
        with open(length_treshold_by_region_fl) as fl:
            length_treshold_by_region =  {l.split(':')[0]:int(l.rstrip().split(':')[1]) for l in fl}
    else:
        length_treshold_by_region = None


    lines_info = []
    with open(assembly_summary) as fl:
        reader = csv.DictReader(fl, delimiter='\t')
        assembly_infos = {d['assembly_accession']: d for d in reader}

    # Example of taxonomy field:
    # Campylobacter coli [tax_name];Campylobacter coli [species];Campylobacter [genus];Campylobacteraceae [family];Campylobacterales [order];Epsilonproteobacteria [class];Proteobacteria [phylum];Bacteria other

    assembly_to_species = {a: info['taxonomy'].split(';')[1] for a, info in assembly_infos.items()}
    assembly_to_genus = {a: info['taxonomy'].split(';')[2] for a, info in assembly_infos.items()}
    assembly_to_family = {a: info['taxonomy'].split(';')[3] for a, info in assembly_infos.items()}

    total_sp = len({info['species_taxid'] for info in assembly_infos.values()})
    logging.info(f'{total_sp} species found in assembly summary {assembly_summary}')

    ecopcr_region_assembly_tuples = get_ecopcr_files(primer_dirs, ecopcr_db_assembly_name, ecopcr_db_region_dir)



    for region, ecopcr_region, ecopcr_assemblies, region_db_seq, primer_couple in ecopcr_region_assembly_tuples:
        logging.info(f"ecopcr_region result {ecopcr_region}" )
        logging.info(f"ecopcr_assembly result found {len(ecopcr_assemblies)} first one {ecopcr_assemblies[0]}" )

        ecopcr_assemblies_name_set = {'_'.join(os.path.basename(f).split('_')[1:]) for f in ecopcr_assemblies}
        if len(ecopcr_assemblies_name_set) != 1:
            logging.critical(f'Assembly db is not coherent... more than one suffix name {ecopcr_assemblies_name_set}')
            raise ValueError(f'Assembly db is not coherent... more than one suffix name {ecopcr_assemblies_name_set}')

        logging.info(f"ecopcr_assembly result name {ecopcr_assemblies_name_set}" )
        ecopcr_assemblies_name = ecopcr_assemblies_name_set.pop()

        outdir_couple = os.path.join(outdir, primer_couple)


        if write_amplicons:
            os.makedirs(outdir_couple, exist_ok=True)
            expected_amplicon_fl = os.path.join(outdir_couple, f'expected_amplicon.ecopcr')
            unexpected_amplicon_fl = os.path.join(outdir_couple, f'unexpected_amplicon.ecopcr')
            unexpected_duplicate_amplicon_fl = os.path.join(outdir_couple, f'unexpected_duplicate_amplicon.ecopcr')

            amplicon_writers = {
                'TP' : open(expected_amplicon_fl, 'w'),
                'FP' : open(unexpected_amplicon_fl, 'w'),
                'unexpected_duplication' : open(unexpected_duplicate_amplicon_fl, 'w'),
            }
        else:
            amplicon_writers = False

        if length_treshold_by_region:
            try:
                length_max = length_treshold_by_region[region]
            except KeyError:
                logging.critical(f'{region} was not found in {length_treshold_by_region_fl}. So length max was not defined' )
                raise  KeyError(f'{region} was not found in {length_treshold_by_region_fl}. So length max was not defined')
        else:
            length_max = None # No filter are applied on length.. all amplicon will be kept

        clean_region_amplicons, circular_amplicon_rm_per_mismatch, smaller_amplicon_rm_per_mismatch = clean_ecopcr_region_amplicons(ecopcr_region, region_db_seq, assembly_infos, mismatch_max)

        # input()
        result_per_mismatch_tp, result_per_mismatch_fp = compare_ecopcr_files(
            clean_region_amplicons, ecopcr_assemblies,
            assembly_infos, mismatch_max,
            amplicon_writers=amplicon_writers, length_max=length_max)

        assemblies = []
        lengths = []

        assemblies_tp = []
        lengths_tp= []

        assemblies_fp = []
        lengths_fp= []

        assemblies_rm_small = []
        lengths_rm_small= []

        lengths_rm_circ = []
        assemblies_rm_circ = []

        details_by_assembly = {}

        print(primer_couple)
        for m in range(mismatch_max + 1):
            general_info = {'region':region,
                            'primer_couple':primer_couple,
                            'ecopcr_parameters': os.path.basename(ecopcr_region).split('.')[0],
                            'ecopcr_assembly_db': ecopcr_assemblies_name,
                            'ecopcr_region_db': ecopcr_assemblies_name,
                            'mismatch': m,
                            'total_sp': total_sp,
                            'assembly_summary': assembly_summary,
                            }

            assemblies_tp_m = result_per_mismatch_tp[m]['assemblies']
            lengths_tp += result_per_mismatch_tp[m]['lengths']

            if len(assemblies_tp_m) == 0:
                logging.warning(f'NO TP retrieve with {m} mismatch..')


            line_info = generate_info_line(assemblies_tp_m,assemblies_tp, lengths_tp, assembly_to_species, assembly_to_genus, assembly_to_family)
            line_info['type'] = 'TP'
            line_info.update(general_info)
            lines_info.append(line_info)

            assemblies_tp += result_per_mismatch_tp[m]['assemblies']



            assemblies_fp_m = result_per_mismatch_fp[m]['assemblies']

            lengths_fp += result_per_mismatch_fp[m]['lengths']

            if len(assemblies_fp_m) == 0:
                logging.warning(f'NO FP retrieve with {m} mismatch..')

            line_info = generate_info_line(assemblies_fp_m,assemblies_fp, lengths_fp, assembly_to_species, assembly_to_genus, assembly_to_family)
            line_info['type'] = 'FP'
            line_info.update(general_info)
            lines_info.append(line_info)

            assemblies_fp += result_per_mismatch_fp[m]['assemblies']


            assemblies_m = result_per_mismatch_tp[m]['assemblies'] + result_per_mismatch_fp[m]['assemblies']



            lengths += result_per_mismatch_tp[m]['lengths']
            lengths += result_per_mismatch_fp[m]['lengths']

            line_info = generate_info_line(assemblies_m, assemblies, lengths, assembly_to_species, assembly_to_genus, assembly_to_family)
            line_info['type'] = 'ALL'
            line_info.update(general_info)
            lines_info.append(line_info)
            assemblies += result_per_mismatch_tp[m]['assemblies']
            assemblies += result_per_mismatch_fp[m]['assemblies']

            assemblies_rm_circ_m = circular_amplicon_rm_per_mismatch[m]['assemblies']


            lengths_rm_circ += circular_amplicon_rm_per_mismatch[m]['lengths']

            line_info = generate_info_line(assemblies_rm_circ_m, assemblies_rm_circ,lengths_rm_circ, assembly_to_species, assembly_to_genus, assembly_to_family)
            line_info['type'] = 'rm_circular'
            line_info.update(general_info)
            lines_info.append(line_info)

            assemblies_rm_circ += circular_amplicon_rm_per_mismatch[m]['assemblies']

            assemblies_rm_small_m = smaller_amplicon_rm_per_mismatch[m]['assemblies']


            lengths_rm_small += smaller_amplicon_rm_per_mismatch[m]['lengths']

            line_info = generate_info_line(assemblies_rm_small_m, assemblies_rm_small, lengths_rm_small, assembly_to_species, assembly_to_genus, assembly_to_family)
            line_info['type'] = 'rm_smaller'
            line_info.update(general_info)
            lines_info.append(line_info)

            assemblies_rm_small += smaller_amplicon_rm_per_mismatch[m]['assemblies']

            if write_details:
                fill_amplicon_details_dict(details_by_assembly, 'TP', result_per_mismatch_tp[m]['assemblies'], result_per_mismatch_tp[m]['lengths'], m)
                fill_amplicon_details_dict(details_by_assembly, 'FP', result_per_mismatch_fp[m]['assemblies'], result_per_mismatch_fp[m]['lengths'], m)

        if write_details:
            """Encode a python structure in a json file."""
            os.makedirs(outdir_couple, exist_ok=True)
            filename = os.path.join(outdir_couple, f'details.json')
            with open(filename, 'w', encoding='utf8') as fl:
                json.dump(details_by_assembly, fl, indent=4)

    if lines_info:
        logging.info(f'Write {output_file}')
        logging.info(f'File has {len(lines_info)} lines')
        list_of_dict_to_tsv(lines_info, output_file)

    if write_amplicons:
        for writer in amplicon_writers.values():
            writer.close()


if __name__ == '__main__':
    main()
