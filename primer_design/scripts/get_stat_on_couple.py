import analyse_primers
import generate_primers_info_file
import os
from argparse import ArgumentParser, ArgumentDefaultsHelpFormatter
import sys
import csv


def parse_arguments():
    parser = ArgumentParser(description="...",
                            formatter_class=ArgumentDefaultsHelpFormatter)
    parser.add_argument("couple_dir", type=str,
                        help='')
    args = parser.parse_args()
    return args


def main():
    args = parse_arguments()
    couple_dir = args.couple_dir
    region = os.path.basename(couple_dir)
    recompute_all = True

    dict_info = generate_primers_info_file.get_info_on_primer_couple(
        region, couple_dir, recompute_all, get_binding_info=False)
    # dict_info['region'] = region

    # main_dict_infos.append(dict_info)
    # file_name = f'primer_summary_info'
    # if cov_min:
    #     file_name += f'_covsup{cov_min}'
    # if nb_best_element_to_select:
    #     file_name += f'_first_{nb_best_element_to_select}'
    # analyse_primers.list_of_dict_to_tsv(main_dict_infos, file_name + '.tsv')


if __name__ == '__main__':
    main()
