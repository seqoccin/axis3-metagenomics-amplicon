#!/usr/bin/env python3

"""
Filter fasta sequences.

Filtering can be based upon an assembly accession list or a taxon name.
"""

# Metadata
__author__ = 'Mainguy Jean - Plateforme bioinformatique Toulouse'
__copyright__ = 'Copyright (C) 2020 INRA'
__license__ = 'GNU General Public License'
__version__ = '0.1'
__email__ = 'support.bioinfo.genotoul@inra.fr'
__status__ = 'dev'


import os
import logging
from argparse import ArgumentParser, ArgumentDefaultsHelpFormatter
import sys
import csv
from Bio import SeqIO
import re
from collections import defaultdict


def tsv_to_dict_of_dicts(file, key_field):
    """
    Take a tsv with header and parse into dict of dict
    Uses
    * the specified field as key
    * the line turned into a dict as value .
    """
    dict_of_list_of_dict = defaultdict(list)
    with open(file) as csvfile:
        reader = csv.DictReader(csvfile, delimiter='\t')
        for l in reader:
            dict_of_list_of_dict[l[key_field]].append(l)
        return dict(dict_of_list_of_dict)


def parse_arguments():
    """Parse script arguments."""
    parser = ArgumentParser(description="...",
                            formatter_class=ArgumentDefaultsHelpFormatter)

    parser.add_argument("sequence_file", type=str,
                        help="")
    parser.add_argument("--assemblies_with_taxonomy", type=str,
                        help="", default=None)
    parser.add_argument("--taxon_name", type=str,
                        help="filter based on a taxon name. ie 'Firmicutes [phylum]' ", default=None)
    parser.add_argument("--assembly_list_to_keep", type=str, default=None,
                        help="Keep only sequences belonging to an assembly of this list. A file with assembly accession separated by new lines.")
    parser.add_argument("--add_taxid", action='store_true',  default=False,
                        help="The taxid of the sequence is added to the header of the sequence. Required when building an ecoPCR database.")
    parser.add_argument("-o", "--output", type=str, default=None,
                        help="Name of the output file. Default depends on ")
    parser.add_argument("-v", "--verbose", help="increase output verbosity",
                        action="store_true")
    args = parser.parse_args()
    return args


def main():

    args = parse_arguments()

    if args.verbose:
        logging.basicConfig(format="%(levelname)s: %(message)s", level=logging.DEBUG)
        logging.info('Mode verbose ON')

    else:
        logging.basicConfig(format="%(levelname)s: %(message)s")

    args = parse_arguments()
    sequence_file = args.sequence_file
    assemblies_with_taxonomy = args.assemblies_with_taxonomy
    taxon_name = args.taxon_name

    if args.output:
        filter_seq_file = args.output
    elif taxon_name:
        filter_seq_file = f"{taxon_name.split(' ')[0]}_" + os.path.basename(sequence_file)
        logging.ingo('')
    else:
        filter_seq_file = f"filtered_" + os.path.basename(sequence_file)

    logging.info(f'Output file sequence is {filter_seq_file}')
    capture_assemby_name = re.compile("^([A-Z]{3}_\d+.\d+)")

    assemblies_id_mapper_fl = '.'.join(filter_seq_file.split('.')[:-1]) + '_map.ids'

    assembly_to_keep = set()
    if args.assembly_list_to_keep:
        with open(args.assembly_list_to_keep) as fl:
            assembly_to_keep = {a.rstrip() for a in fl}

    # Parse assembly_taxonomy_file
    if args.taxon_name:
        taxon_assembly = []
        with open(assemblies_with_taxonomy) as fl:
            reader = csv.DictReader(fl, delimiter='\t')
            for d in reader:
                tax = d['taxonomy'].rstrip().split(';')
                if taxon_name in tax:
                    assembly_to_keep.add(d['assembly_accession'].rstrip())

    if args.add_taxid:
        with open(assemblies_with_taxonomy) as csvfile:
            reader = csv.DictReader(csvfile, delimiter='\t')
            assembly_to_taxid = {d['assembly_accession']: d['species_taxid']
                                 for d in reader if d['assembly_accession'] in assembly_to_keep}

    logging.info(f'{len(assembly_to_taxid)} assemblies are part ')
    assembly_to_new_seq_id = defaultdict(dict)
    removed_seq_count = 0
    with open(sequence_file) as fl, open(filter_seq_file, 'w') as out:
        for seq in SeqIO.parse(fl, "fasta"):
            assembly_name = capture_assemby_name.search(seq.id).group(1)

            if assembly_name in assembly_to_keep:
                if args.add_taxid:
                    original_id = seq.id
                    i = len(assembly_to_new_seq_id[assembly_name])
                    new_id = f'{assembly_name}|s{i:03d}'
                    assembly_to_new_seq_id[assembly_name][new_id] = original_id
                    seq.id = new_id
                    taxid = assembly_to_taxid[assembly_name]
                    seq_extra_info = seq.description
                    seq.description = f'taxid={taxid}; {seq_extra_info}'
                    if len(seq.id) > 20:
                        logging.critical('Containing header length has failed: {seq.id}')

                SeqIO.write(seq, out, "fasta")
            else:
                removed_seq_count += 1

    if assembly_to_new_seq_id:
        with open(assemblies_id_mapper_fl, 'w') as map_id_wrt:
            for list_ids_dict in assembly_to_new_seq_id.values():
                for new_id, original_id in list_ids_dict.items():
                    map_id_wrt.write(f'{new_id}\t{original_id}\n')

    logging.info(f'{removed_seq_count} sequences have been removed because are not part of the selected assemblies')


if __name__ == '__main__':
    main()
