#!/usr/bin/env python3

"""
Description

:Example:
python template.py -v
"""

# Metadata
__author__ = 'Mainguy Jean - Plateforme bioinformatique Toulouse'
__copyright__ = 'Copyright (C) 2019 INRA'
__license__ = 'GNU General Public License'
__version__ = '0.1'
__email__ = 'support.bioinfo.genotoul@inra.fr'
__status__ = 'dev'


from Bio import Seq
from itertools import product
from Bio.SeqUtils import MeltingTemp as mt
from Bio.Alphabet import IUPAC
import gzip
import os
import logging
from argparse import ArgumentParser, ArgumentDefaultsHelpFormatter
import sys
import csv
import json
from collections import defaultdict

import ecopcr_sarray

def parse_arguments():
    """Parse script arguments."""
    parser = ArgumentParser(description="...",
                            formatter_class=ArgumentDefaultsHelpFormatter)
    parser.add_argument(
        "--primers_dir", help="Main directory where sub primer directories are.", default='./')
    parser.add_argument(
        "--ecopcr_region_db_dir", help="Directory where ecopcr database are.",)
    parser.add_argument(
        "--ecopcr_db_assembly_files", nargs='+', help="Files of ecopcr assembly database. ")
    parser.add_argument('-e',
                        "--max_mismatches", help="Maximum number of errors (mismatches) allowed per primer in ecoPCR simulation", default=0)
    parser.add_argument(
        "--n_sarray_command", help="Number of sarray command. commands are concatenated to not overload slurm...", default=100)
    parser.add_argument("--ecopcr_tool_path",
                        help="Path to the executable of ecoPCR. Default assumes ecoPCR is in PATH or current dir. ",
                        default='ecoPCR')
    parser.add_argument("--run_ecotaxstat", help="Create ecotaxstat sarray command", default=False,
                        action="store_true")
    parser.add_argument("--write_individual_command", help="Write command on each primer dir to be able to rerun individual command", default=False,
                        action="store_true")
    parser.add_argument("--adjust_e_with_primer_quality", help="Lower mismatch max in ecoPCR command when primer quality is to low to prevent targeting to many amplicon.",
                        action="store_true", default=False)
    parser.add_argument("-v", "--verbose", help="increase output verbosity",
                        action="store_true")

    args = parser.parse_args()
    return args


def main():

    args = parse_arguments()

    if args.verbose:
        logging.basicConfig(format="%(levelname)s: %(message)s", level=logging.DEBUG)
        logging.info('Mode verbose ON')

    else:
        logging.basicConfig(format="%(levelname)s: %(message)s")

    primers_dir = args.primers_dir
    ecopcr_region_db_dir = args.ecopcr_region_db_dir
    e = args.max_mismatches
    ecopcr_tool_path = args.ecopcr_tool_path
    ecopcr_db_assembly_files = args.ecopcr_db_assembly_files
    logging.info(f"ecopcr_db_assembly_files  {ecopcr_db_assembly_files}")
    run_ecotaxstat = args.run_ecotaxstat
    adjust_e_with_primer_quality = args.adjust_e_with_primer_quality
    write_individual_command = args.write_individual_command

    min_amplicon_size, max_amplicon_size = 100, 600
    nb_job_per_sarray = args.n_sarray_command
    logging.info(f'ecoPCR identify amplicons with a size from {min_amplicon_size} to {max_amplicon_size}.')

    ecopcr_commands_all = []
    ecotaxstat_commands_all = []
    ecopcr_command_per_assembly_db = defaultdict(list)
    ecotaxstat_command_per_assembly_db = defaultdict(list)
    region_primer_dir = [os.path.join(primers_dir, couple)
                         for couple in os.listdir(primers_dir) if os.path.isdir(os.path.join(primers_dir, couple))]

    # print(os.listdir(primers_dir))

    for couple_dir in region_primer_dir:
        logging.info(f'Generate ecoPCR sarray command for {os.path.basename(couple_dir)}')
        try:
            info_file = [f for f in os.listdir(couple_dir) if f.endswith(
                '.json') and f.startswith('f')][0]
        except IndexError:
            logging.critical(f'No json info file found in primer couple dir {couple_dir}. {couple_dir} is not processed')
            continue
        json_info = decoder(os.path.join(couple_dir, info_file))
        p1_f = json_info['forward_primer']
        p2_r = json_info['reverse_primer']
        target_region_f = p1_f['target']
        target_region_r = p2_r['target']
        if target_region_f != target_region_r:
            region = '-'.join(sorted([target_region_f, target_region_r]))
        else:
            region = target_region_f
        logging.info(f'Region of couple dir {couple_dir} is {region}')

        if ecopcr_region_db_dir:

            region_db = os.path.join(ecopcr_region_db_dir, region)
            logging.info(f'ecopcr on region db {region_db}')
            ecopcr_commands, ecotaxstat_commands = ecopcr_sarray.manage_primer_couple(p1_f, p2_r,
                                                                        couple_dir, min_amplicon_size,
                                                                        max_amplicon_size,
                                                                        region_db, e,
                                                                        adjust_e_with_primer_quality, ecopcr_tool_path, write_individual_command)

            ecopcr_commands_all += ecopcr_commands
            ecotaxstat_commands_all += ecotaxstat_commands

        if ecopcr_db_assembly_files:
            logging.info(f'ecopcr on assembly db.. made of {len(ecopcr_db_assembly_files)} files')
            for db_fl in ecopcr_db_assembly_files:

                assembly_db_basename = '.'.join(db_fl.split('.')[:-1])
                logging.info(f'assembly db {db_fl}, assembly_db_basename {assembly_db_basename}')

                ecopcr_commands, ecotaxstat_commands = ecopcr_sarray.manage_primer_couple(p1_f, p2_r,
                                                                            couple_dir, min_amplicon_size,
                                                                            max_amplicon_size,
                                                                            assembly_db_basename, e,
                                                                            adjust_e_with_primer_quality, ecopcr_tool_path, write_individual_command)

                ecopcr_command_per_assembly_db[assembly_db_basename] += ecopcr_commands
                ecotaxstat_command_per_assembly_db[assembly_db_basename] += ecotaxstat_commands

    for ecopcr_commands in ecopcr_command_per_assembly_db.values():
        ecopcr_commands_all += ecopcr_commands

    for commands in ecotaxstat_command_per_assembly_db.values():
        ecotaxstat_commands_all += commands

    ecopcr_sarray.merge_sarray_command(ecopcr_commands_all, ecotaxstat_commands_all,
                                                   nb_job_per_sarray, run_ecotaxstat)


if __name__ == '__main__':
    main()
