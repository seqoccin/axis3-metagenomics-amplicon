#!/usr/bin/env python3

"""
Convert ecopcr files to fasta files
"""

# Metadata
__author__ = 'Mainguy Jean - Plateforme bioinformatique Toulouse'
__copyright__ = 'Copyright (C) 2020 INRAE'
__license__ = 'GNU General Public License'
__version__ = '0.1'
__email__ = 'support.bioinfo.genotoul@inra.fr'
__status__ = 'dev'


from argparse import ArgumentParser, ArgumentDefaultsHelpFormatter
import logging
import re
import csv
from collections import defaultdict
import os
from Bio.Seq import Seq


def get_seq_from_ecopcr_file(ecopcrfile, add_primer_sequences):
    """Get amplicon sequences from an ecopcr file."""
    assembly_counter = defaultdict(int)
    with open(ecopcrfile, encoding="latin-1") as fl:
        for i, l in enumerate(fl):
            if l.startswith('#'):
                continue

            splitted_l = [e.strip() for e in l.split(' | ')]

            if len(splitted_l) < 20:
                logging.critical(f'ecopcr line {i} ({l}) in file {ecopcrfile} is incorrect')
                raise IndexError(f'ecopcr line {i} ({l}) in file {ecopcrfile} is incorrect')

            species = splitted_l[5]
            assembly = splitted_l[0].split('|')[0]
            assembly_counter[assembly] += 1

            f_primer = splitted_l[13]
            r_primer = splitted_l[16]

            if add_primer_sequences:
                r_primer_seq = Seq(r_primer)
                sequence = f_primer + splitted_l[20] + str(r_primer_seq.reverse_complement())

            else:
                sequence = splitted_l[20]

            header = f'{assembly}|seq{assembly_counter[assembly]}'

            yield header, sequence


def write_ecopcr_file_seq_to_fasta(ecopcrfile, output_file, add_primer_sequences):
    """write fasta sequence from an ecopcr file."""
    logging.info(f'writting seq in {output_file}')
    with open(output_file, 'w') as fl:
        for header, seq in get_seq_from_ecopcr_file(ecopcrfile, add_primer_sequences):
            fl.write(f'>{header}\n')
            fl.write(seq+'\n')


def parse_arguments():
    """Parse script arguments."""
    parser = ArgumentParser(description="...",
                            formatter_class=ArgumentDefaultsHelpFormatter)

    parser.add_argument("ecopcr_files", type=str, nargs='+')

    parser.add_argument('--add_primer_sequences',
                        help="Add primer sequences to amplicon sequences", action="store_true",)

    parser.add_argument("-o", '--output_dir', help="", type=str, default='./')
    # parser.add_argument("--mock_compo_file", help="", default='mock_bacterial_composition.tsv',
    #                     action="store_true")

    parser.add_argument("-v", "--verbose", help="increase output verbosity",
                        action="store_true")

    args = parser.parse_args()
    return args


def main():

    args = parse_arguments()

    if args.verbose:
        logging.basicConfig(format="%(levelname)s: %(message)s", level=logging.DEBUG)
        logging.info('Mode verbose ON')

    else:
        logging.basicConfig(format="%(levelname)s: %(message)s")

    ecopcrfiles = [f for f in args.ecopcr_files if f.endswith('ecopcr')]
    logging.info(f'{len(ecopcrfiles)} ecopcrfiles to concert ')

    output_dir = args.output_dir
    os.makedirs(output_dir, exist_ok=True)

    add_primer_sequences = args.add_primer_sequences

    for ecopcrfile in ecopcrfiles:
        logging.info(f'Processing {ecopcrfile}')

        output_file = os.path.join(output_dir, ''.join(
            os.path.basename(ecopcrfile).split('.')[:-1])+'.fna')
        write_ecopcr_file_seq_to_fasta(ecopcrfile, output_file,
                                       add_primer_sequences=add_primer_sequences)


if __name__ == '__main__':
    main()
