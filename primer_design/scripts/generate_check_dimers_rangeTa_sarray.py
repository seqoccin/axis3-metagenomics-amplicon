#!/usr/bin/env python
# coding: utf-8

import os
import csv
import json

# refseqRepRef_fCOG0200_1-rCOG0201_1_l500_L8000_e0.ecotaxstat


def create_sarray_command_with_ta(couple_dir, ta):

    info_file = [f for f in os.listdir(couple_dir) if f.endswith('.json')][0]
    dict_info = decoder(os.path.join(couple_dir, info_file))
    # print(dict_info)
    primer1 = dict_info["forward_primer"]["seq"]
    p1_name = dict_info["forward_primer"]["primer_id"]
    primer2 = dict_info["reverse_primer"]["seq"]
    p2_name = dict_info["reverse_primer"]["primer_id"]
    pre_command = 'module load system/Anaconda3-5.2.0;source /usr/local/bioinfo/src/Anaconda/Anaconda3-5.2.0/etc/profile.d/conda.sh;conda activate check_primer_dimers;'
    output_dir = os.path.join(couple_dir, 'check_binding_by_ta', str(ta))
    os.makedirs(output_dir, exist_ok=True)
    command = 'python check_primer_dimers.py' + f' -p {primer1} -P {primer2}' +   \
        f' --primer_name1 {p1_name} --primer_name2 {p2_name}' +  \
        f' -o {output_dir} -e primer_prospector/dna_DM.par -t {ta}'

    print(pre_command + command)


def decoder(filename):
    """Load a json file and return the corresponding structure."""
    with open(filename, 'r', encoding='utf8') as file:
        return json.load(file)


def main():
    ta_min = 48
    ta_max = 67
    range_ta = range(ta_min, ta_max+1)
    #regions_dirs = [os.path.join(main_result_dir, region) for region in os.listdir(main_result_dir)]

    # regions = 'COG0097-COG0200 COG0200-COG0201 COG0052-COG0264'
    couple_dirs = ["degeprime_results/bash_pipeline/ecoPCR/COG0097-COG0200/COG0097_9-COG0200_1"]
    for ta in range_ta:
        # print(region)
        for couple_dir in couple_dirs:
            # print(couple_dir)
            create_sarray_command_with_ta(couple_dir, ta)


if __name__ == '__main__':
    main()
