import gzip
import os
import logging
from argparse import ArgumentParser, ArgumentDefaultsHelpFormatter
import sys
import csv
from Bio import SeqIO
import re


def parse_arguments():
    parser = ArgumentParser(description="...",
                            formatter_class=ArgumentDefaultsHelpFormatter)

    parser.add_argument("fna_sequence", type=str,
                        help="")

    parser.add_argument("--assemblies_path", type=str, default='/work2/project/seqoccin/metaG/ncbi_data/refseq_assemblies/',
                        help="")
    parser.add_argument("-o", "--output", type=str, default=None,
                        help="")
    args = parser.parse_args()
    return args


def main():
    args = parse_arguments()
    fna_seq_fl = args.fna_sequence
    assemblies_path = args.assemblies_path
    if args.output:
        out_faa_file = args.output
    else:
        out_faa_file = os.path.splitext(fna_seq_fl)[0] + '.faa'

    with open(fna_seq_fl) as fl:

        for seq in SeqIO.parse(fl, "fasta"):
            assembly = seq.id.split('|')[0]
            prot_id = seq.description.split(' ')[-1]
            print(prot_id)
            assembly_dir = os.path.join(assemblies_path, assembly)
            faa_file = [os.path.join(assembly_dir, f)
                        for f in os.listdir(assembly_dir) if f.endswith('.faa.gz')][0]

            print("zcat " + faa_file + " | perl -ne 'if(/^>(\S+)/){$c=grep{/^$1$/}qw("+prot_id +
                  ")}print if $c' " + '| sed "s/\(^>.*\)/>' + seq.description + '/" >> ' + out_faa_file)
            os.system("zcat " + faa_file + " | perl -ne 'if(/^>(\S+)/){$c=grep{/^$1$/}qw("+prot_id +
                      ")}print if $c' " + '| sed "s/\(^>.*\)/>' + seq.description + '/" >> ' + out_faa_file)


if __name__ == '__main__':
    main()
