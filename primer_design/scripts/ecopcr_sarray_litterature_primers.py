from Bio import Seq
from itertools import product
from Bio.SeqUtils import MeltingTemp as mt
from Bio.Alphabet import IUPAC
import gzip
import os
import logging
from argparse import ArgumentParser, ArgumentDefaultsHelpFormatter
import sys
import csv
import json
import ecopcr_primers_validation


d = '.'
all_refseq = False
run_ecotaxstat = True
e = 2
adjust_e_with_primer_quality = False


ecopcr_commands_all = []
ecotaxstat_commands_all = []
short_regions = [('gyrB', 'ecoPCR/gyrB/'), ('16S_v3v4', 'ecoPCR/16S_v3v4/'),
                 ('rpoB', 'ecoPCR/rpoB/'), ('rpoB_proteoB', 'ecoPCR/rpoB_proteoB/')]
min_amplicon_size, max_amplicon_size = 100, 500
for region, couple_dir in short_regions:
    info_file = [f for f in os.listdir(couple_dir) if f.endswith('.json') and f.startswith('f')][0]
    json_info = ecopcr_primers_validation.decoder(os.path.join(couple_dir, info_file))
    p1_f = json_info['forward_primer']
    p2_r = json_info['reverse_primer']
    ecopcr_commands, ecotaxstat_commands = ecopcr_primers_validation.manage_primer_couple(p1_f, p2_r, couple_dir, min_amplicon_size,
                                                                                          max_amplicon_size, d, e,  all_refseq, adjust_e_with_primer_quality)
    ecopcr_commands_all += ecopcr_commands
    ecotaxstat_commands_all += ecotaxstat_commands

long_regions = [('rrn', 'ecoPCR/rrn/'), ('16Sfull', 'ecoPCR/16Sfull/')]
min_amplicon_size, max_amplicon_size = 500, 8000
for region, couple_dir in long_regions:
    info_file = [f for f in os.listdir(couple_dir) if f.endswith('.json') and f.startswith('f')][0]
    json_info = ecopcr_primers_validation.decoder(os.path.join(couple_dir, info_file))
    p1_f = json_info['forward_primer']
    p2_r = json_info['reverse_primer']

    ecopcr_commands, ecotaxstat_commands = ecopcr_primers_validation.manage_primer_couple(p1_f, p2_r, couple_dir, min_amplicon_size,
                                                                                          max_amplicon_size, d, e,  all_refseq, adjust_e_with_primer_quality)
    ecopcr_commands_all += ecopcr_commands
    ecotaxstat_commands_all += ecotaxstat_commands

nb_job_per_sarray = 10
ecopcr_primers_validation.merge_sarray_command(ecopcr_commands_all, ecotaxstat_commands_all,
                                               nb_job_per_sarray, run_ecotaxstat)
