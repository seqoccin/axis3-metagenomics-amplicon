#!/usr/bin/env python3

"""
Description

:Example:
python template.py -v
"""

# Metadata
__author__ = 'Mainguy Jean - Plateforme bioinformatique Toulouse'
__copyright__ = 'Copyright (C) 2020 INRAe'
__license__ = 'GNU General Public License'
__version__ = '0.1'
__email__ = 'support.bioinfo.genotoul@inra.fr'
__status__ = 'dev'


import os
import csv
import json
from collections import defaultdict
from argparse import ArgumentParser, ArgumentDefaultsHelpFormatter, FileType
import logging


def decoder(filename):
    """Load a json file and return the corresponding structure."""
    with open(filename, 'r', encoding='utf8') as file:
        return json.load(file)




def generate_info_line(primer_info, ecopcr_stat_by_mismatch ):
    mismatches = [0,1,2]
    # HEADERS
    # region	fprimer_name	fprimer_seq	fprimer_length	fprimer_degeneracy	rprimer_name	rprimer_seq	rprimer_length	rprimer_degeneracy	sp_coverage_mismatch0	sp_coverage_mismatch1	sp_coverage_mismatch2	sp_unspecific_match_mismatch0	sp_unspecific_match_mismatch1	sp_unspecific_match_mismatch2 median_length	fprimer_tm_range	r_tmrange_m	tm_window
    line = {}


    line.update(ecopcr_stat_by_mismatch)
    return line


def parse_primer_dir(couple_dir):

    couple_dir = couple_dir[:-1] if couple_dir.endswith('/') else couple_dir
    logging.info(f'Parse primer couple dir  {couple_dir}')
    try:
        info_file = [f for f in os.listdir(couple_dir) if f.endswith(
            '.json') and f.startswith('f')][0]
    except IndexError:
        logging.critical(f'No json info file found in primer couple dir {couple_dir}. {couple_dir} is not processed')
        return

    info = decoder(os.path.join(couple_dir, info_file))
    target_region_f = info['forward_primer']['target']
    target_region_r = info['reverse_primer']['target']
    if target_region_f != target_region_r:
        region = '-'.join(sorted([target_region_f, target_region_r]))
    else:
        region = target_region_f

    logging.info(f'Region of couple dir {couple_dir} is {region}')

    # Depend on the json file but may not contain tm info and degeneracy. In this case it would be necessary to compute those info here.
    filtered_info = {}
    filtered_info['region'] = region
    filtered_info['primer_couple'] = os.path.basename(couple_dir)


    filtered_info["fprimer_name"] = info['forward_primer']['primer_id']
    filtered_info["fprimer_seq"] = info['forward_primer']['seq']
    filtered_info["fprimer_length"] = len(info['forward_primer']['seq'])
    filtered_info["fprimer_degeneracy"] = info['forward_primer']['degeneracy']

    filtered_info["rprimer_name"] = info['reverse_primer']['primer_id']
    filtered_info["rprimer_seq"] = info['reverse_primer']['seq']
    filtered_info["rprimer_length"] = len(info['reverse_primer']['seq'])
    filtered_info["rprimer_degeneracy"] = info['reverse_primer']['degeneracy']

    return filtered_info


def parse_ecopcr_stat(ecopcr_stat_fl):
    couple_ecopcr_stat = defaultdict(dict)
    with open(ecopcr_stat_fl) as csvfile:
        reader = csv.DictReader(csvfile, delimiter='\t')
        for d in reader:
            m = d['mismatch']
            if d['type'] == 'TP':
                couple_ecopcr_stat[d['primer_couple']][f'sp_coverage_mismatch{m}'] = d['sp_covered']
            if d['type'] == 'FP':
                couple_ecopcr_stat[d['primer_couple']][f'sp_unspecific_match_mismatch{m}'] = d['sp_covered']

    return dict(couple_ecopcr_stat)


def parse_arguments():
    parser = ArgumentParser(description="...",
                            formatter_class=ArgumentDefaultsHelpFormatter)

    parser.add_argument("--primer_dirs", nargs='+',
                        help="")

    parser.add_argument("--ecopcr_stat_fl",
                        help="")

    parser.add_argument("-o", "--output_file", default='primers_info.tsv',
                        help="")
    args = parser.parse_args()
    return args


def main():
    args = parse_arguments()

    primer_dirs = args.primer_dirs
    ecopcr_stat_fl = args.ecopcr_stat_fl
    output_file =   args.output_file
    region_infos = {}
    main_dict_infos = []

    couples_stat_by_mismatches = parse_ecopcr_stat(ecopcr_stat_fl)

    stat_lines = []

    for primer_dir in primer_dirs:
        primer_info = parse_primer_dir(primer_dir)
        primer_couple = primer_info['primer_couple']
        ecopcr_coulpe_stat = couples_stat_by_mismatches[primer_couple]
        primer_info.update(ecopcr_coulpe_stat)
        stat_lines.append(primer_info)


    with open(output_file, "w", newline='') as csvfile:
        writer = csv.DictWriter(csvfile, fieldnames=list(stat_lines[0].keys()), delimiter='\t')
        writer.writeheader()
        for line in stat_lines:
            writer.writerow(line)


if __name__ == '__main__':
    main()
