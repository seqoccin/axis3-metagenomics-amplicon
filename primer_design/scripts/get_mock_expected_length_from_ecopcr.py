#!/usr/bin/env python3increase output verbosity

"""
Description

:Example:
python template.py -v
"""

# Metadata
__author__ = 'Mainguy Jean - Plateforme bioinformatique Toulouse'
__copyright__ = 'Copyright (C) 2019 INRA'
__license__ = 'GNU General Public License'
__version__ = '0.1'
__email__ = 'support.bioinfo.genotoul@inra.fr'
__status__ = 'dev'


from argparse import ArgumentParser, ArgumentDefaultsHelpFormatter
import logging


def parse_arguments():
    """Parse script arguments."""
    parser = ArgumentParser(description="...",
                            formatter_class=ArgumentDefaultsHelpFormatter)

    parser.add_argument("ecopcr_file", help="", type=str)
    parser.add_argument("--mock_compo_file", help="", default='mock_bacterial_composition.tsv',
                        action="store_true")

    parser.add_argument("-v", "--verbose", help="increase output verbosity",
                        action="store_true")

    args = parser.parse_args()
    return args


def main():

    args = parse_arguments()

    if args.verbose:
        logging.basicConfig(format="%(levelname)s: %(message)s", level=logging.DEBUG)
        logging.info('Mode verbose ON')

    else:
        logging.basicConfig(format="%(levelname)s: %(message)s")

    ecopcrfile = args.ecopcr_file

    with open(args.mock_compo_file) as fl:
        mock_species = [l.split('\t')[0].strip() for l in fl]

    # print(f'Assembly_id\tspecies\tamplicon_length')

    with open(ecopcrfile, encoding="latin-1") as fl:
        for l in fl:
            if l.startswith('#'):
                continue

            splitted_l = [e.strip() for e in l.split(' | ')]
            species = splitted_l[5]
            f_primer_len = len(splitted_l[13])
            r_primer_len = len(splitted_l[16])
            length = int(splitted_l[19]) + r_primer_len + f_primer_len
            genome_id = splitted_l[0].split('|')[0]

            if species in mock_species:
                print(f'{species}\t{genome_id}\t{length}')


if __name__ == '__main__':
    main()
