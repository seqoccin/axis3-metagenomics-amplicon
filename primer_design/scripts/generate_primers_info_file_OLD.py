#!/usr/bin/env python
# coding: utf-8


import os
import csv
import json
from collections import defaultdict
from Bio import Seq
from itertools import product
from Bio.SeqUtils import MeltingTemp as mt
import gzip
import os
from argparse import ArgumentParser, ArgumentDefaultsHelpFormatter, FileType
import sys
import csv
from Bio import SeqIO
import pandas as pd
#import itables.interactive
import melting
import logging

# standard imports
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt

import analyse_primers
import concurrent.futures
import time


def tsv_to_dict_of_dicts(file, key_field):
    """
    Take a tsv with header and parse into dict of dict
    Uses
    * the specified field as key
    * the line turned into a dict as value .
    """
    dict_of_list_of_dict = defaultdict(list)
    with open(file) as csvfile:
        reader = csv.DictReader(csvfile, delimiter='\t')
        for l in reader:
            dict_of_list_of_dict[l[key_field]].append(l)
        return dict(dict_of_list_of_dict)


def tsv_to_list_of_dict(file):
    """Load a tsv file into a list of dictionnary"""
    with open(file) as csvfile:
        reader = csv.DictReader(csvfile, delimiter='\t')
        return list(reader)


def encoder(filename, f):
    """Encode a python structure in a json file."""
    with open(filename, 'w', encoding='utf8') as file:
        json.dump(f, file, indent=2)


def decoder(filename):
    """Load a json file and return the corresponding structure."""
    with open(filename, 'r', encoding='utf8') as file:
        return json.load(file)


def get_info_on_primer_couple(region, couple_dir, mock_species, recompute_all=True, get_binding_info=True):
    repref_sp = 5378
    repref_genomes = 5508
    #couple_dir = 'degeprime_results/bash_pipeline/ecoPCR/COG0097-COG0200/COG0097_1-COG0200_1'
    json_file_info = 'primer_info.json'
    json_file_path = os.path.join(couple_dir, json_file_info)
    if os.path.isfile(json_file_path) and not recompute_all:
        dict_info = decoder(json_file_path)
        return dict_info

    dict_info = {'region': region}

    json_info = analyse_primers.get_info_couple_primers(couple_dir)

    # print(dict_info)
    fprimer = json_info["forward_primer"]["seq"]
    fp_name = json_info["forward_primer"]["primer_id"]
    rprimer = json_info["reverse_primer"]["seq"]
    rp_name = json_info["reverse_primer"]["primer_id"]

    dict_info['fp_name'] = fp_name
    dict_info['fprimer'] = fprimer.upper()
    dict_info['len_f'] = len(fprimer)
    dict_info['f_deg'] = len(analyse_primers.extend_ambiguous_dna(fprimer))
    dict_info['rp_name'] = rp_name
    dict_info['rprimer'] = rprimer.upper()
    dict_info['len_r'] = len(rprimer)
    dict_info['r_deg'] = len(analyse_primers.extend_ambiguous_dna(rprimer))

    # # ECOTAXSTAT
    # tax_cov_sp_dict = analyse_primers.get_species_stat_from_ecotax(json_info['ecotaxstat_file'])
    # dict_info['tax_cov_sp_prct'] = tax_cov_sp_dict['prct']
    # dict_info['tax_cov_sp_count'] = tax_cov_sp_dict['count']
    #
    # if json_info['ecotaxstat_file_all_refseq']:
    #     tax_cov_sp_dict = analyse_primers.get_species_stat_from_ecotax(
    #         json_info['ecotaxstat_file_all_refseq'])
    #
    #     dict_info['tax_cov_sp_prct_all_seq'] = tax_cov_sp_dict['prct']
    #     dict_info['tax_cov_sp_count_all_seq'] = tax_cov_sp_dict['count']
    # else:
    #     dict_info['tax_cov_sp_prct_all_seq'] = None
    #     dict_info['tax_cov_sp_count_all_seq'] = None

    # ECOTAXSPECIFICITY
    for diff in []:  # [1, 2, 5, 10, 20]:
        ecotaxspe_file = json_info[f'ecotaxspe_file_diff{diff}']
        #print(diff, ecotaxspe_file)
        if ecotaxspe_file:
            tax_cov_sp_dict = analyse_primers.get_species_stat_from_ecotax(
                ecotaxspe_file)
        else:
            tax_cov_sp_dict = None

        if tax_cov_sp_dict:
            dict_info[f'resol_e{diff}_sp_prct'] = tax_cov_sp_dict['prct']
            dict_info[f'resol_e{diff}_sp_count'] = tax_cov_sp_dict['count']
        else:
            dict_info[f'resol_e{diff}_sp_prct'] = None
            dict_info[f'resol_e{diff}_sp_count'] = None

    ecopcr_dict = analyse_primers.parse_ecopcr_result(json_info['ecopcr_file'])
    #
    # mock_sp_caught = [sp for sp in mock_species if sp in ecopcr_dict['species']]
    #
    # dict_info['amplicon_per_genome'] = len(
    #     ecopcr_dict['fprimers_used'])/len(set(ecopcr_dict['genomes']))
    #
    # dict_info['single_copy_amplicon'] = len([genome for genome in set(
    #     ecopcr_dict['genomes']) if ecopcr_dict['genomes'].count(genome) == 1])
    #
    # dict_info['single_copy_prct'] = round(
    #     (dict_info['single_copy_amplicon'] / len(set(ecopcr_dict['genomes']))) * 100, 2)
    # dict_info['mock_species'] = len(mock_sp_caught)
    # dict_info['missing_mock_sp'] = '; '.join(
    #     [sp for sp in mock_species if sp not in mock_sp_caught])

    ecopcr_mismatched = json_info['c']
    mismatch_max = 4
    stat_by_mismatch = analyse_primers.get_mismatch_stat_from_ecopcrfile(
        ecopcr_mismatched, mismatch_max)
    for e in range(mismatch_max+1):
        ecopcr_stat = stat_by_mismatch[e]
        if ecopcr_stat:
            # repref_sp = 5378
            # repref_genomes = 5508
            # stat_by_mismatch[mismatch] = {'single_copy_genomes': nb_single_copy_genome, 'genome_covered': len(
            #     genomes), 'sp_covered': nb_sp_covered, "amplicon_count": mismatch_amplicon_count[mismatch]}
            dict_info[f'tax_cov_sp_prct{e}'] = round(100*len(ecopcr_stat['sp_covered']) / repref_sp, 2)
            dict_info[f'tax_cov_sp_count{e}'] = len(ecopcr_stat['sp_covered'])
            dict_info[f'single_copy_amplicon_e{e}'] = ecopcr_stat['nb_single_copy_genomes']
            dict_info[f'single_copy_prct_e{e}'] = round(100*ecopcr_stat["nb_single_copy_genomes"] / ecopcr_stat['nb_genome_covered'], 2)

            mock_sp_caught = [sp for sp in mock_species if sp in ecopcr_stat['sp_covered']]

            dict_info[f'mock_species_e{e}'] = len(mock_sp_caught)
            dict_info[f'missing_mock_spe{e}'] = '; '.join(
                [sp for sp in mock_species if sp not in mock_sp_caught])

        else:
            dict_info[f'tax_cov_sp_prct{e}'] = None
            dict_info[f'tax_cov_sp_count{e}'] = None
            dict_info[f'single_copy_amplicon_e{e}'] = None
            dict_info[f'single_copy_prct_e{e}'] = None
            dict_info[f'mock_species_e{e}'] = None
            dict_info[f'missing_mock_spe{e}'] = None

    dict_info['fprimers_used'] = len(ecopcr_dict['fprimers_to_amplicons'])
    dict_info['rprimers_used'] = len(ecopcr_dict['rprimers_to_amplicons'])
    dict_info['len_median'] = np.median(ecopcr_dict['lengths'])

    dict_info['len_mean'] = np.mean(ecopcr_dict['lengths'])
    if ecopcr_dict['lengths']:
        dict_info['len_min'] = min(ecopcr_dict['lengths'])
        dict_info['len_max'] = max(ecopcr_dict['lengths'])

        fprimers_used_deg = analyse_primers.get_degenerate_consensus_seq(
            ecopcr_dict['fprimers_used'])
        rprimers_used_deg = analyse_primers.get_degenerate_consensus_seq(
            ecopcr_dict['rprimers_used'])
        dict_info['f_deg_diff'] = fprimers_used_deg != fprimer
        dict_info['r_deg_diff'] = rprimers_used_deg != rprimer
        dict_info['f_tmrange'] = analyse_primers.get_tm_range(
            ecopcr_dict['fprimers_used'], fct=analyse_primers.get_tms)
        dict_info['r_tmrange'] = analyse_primers.get_tm_range(
            ecopcr_dict['rprimers_used'], fct=analyse_primers.get_tms)

        min_f, max_f = analyse_primers.get_tm_range(
            ecopcr_dict['fprimers_used'], fct=analyse_primers.get_tms_from_melting)
        min_r, max_r = analyse_primers.get_tm_range(
            ecopcr_dict['rprimers_used'], fct=analyse_primers.get_tms_from_melting)

        dict_info['f_tmrange_m'] = (min_f, max_f)
        dict_info['r_tmrange_m'] = (min_r, max_r)
        dict_info['tm_window'] = max([max_f, max_r]) - min([min_f, min_r]) + 1
    else:
        dict_info['len_min'] = None
        dict_info['len_max'] = None
        dict_info['f_deg_diff'] = None
        dict_info['r_deg_diff'] = None
        dict_info['f_tmrange'] = None
        dict_info['r_tmrange'] = None
        dict_info['f_tmrange_m'] = None
        dict_info['r_tmrange_m'] = None
        dict_info['tm_window'] = None

    binding_result_name = 'inter_primer_binding_result.tsv'
    binding_result_fl = os.path.join(couple_dir, binding_result_name)

    #tms_fprimers = get_tms(ecopcr_dict['fprimers_used'])
    #tms_rprimers = get_tms(ecopcr_dict['rprimers_used'])

    """
    dict_info['fprimers_min_tm'] = min(tms_fprimers)

    dict_info['rprimers_min_tm'] = min(tms_rprimers)

    dict_info['fprimers_max_tm'] = max(tms_fprimers)
    dict_info['rprimers_max_tm'] = max(tms_rprimers)
    """

    if get_binding_info:

        if not os.path.isfile(binding_result_fl):
            # No binding info:/

            dict_info.update({"fprimer_binds": None, "rprimer_binds": None, "problematic_fprimers": None,
                              "problematic_rprimers": None, "seq_impacted_by_binding": None})
        else:
            fprimer_binds, rprimer_binds = analyse_primers.get_binding_results(binding_result_fl)

            problematic_fprimers = set(ecopcr_dict['fprimers_used']) & set(fprimer_binds)
            problematic_rprimers = set(ecopcr_dict['rprimers_used']) & set(rprimer_binds)

            dict_info['fprimer_binds'] = len(set(fprimer_binds))
            dict_info['rprimer_binds'] = len(set(rprimer_binds))
            dict_info['problematic_fprimers'] = len(problematic_fprimers)
            dict_info['problematic_rprimers'] = len(problematic_rprimers)

            seq_impacted_f_primer_binding = [
                amplicon for p in problematic_fprimers for amplicon in ecopcr_dict['fprimers_to_amplicons'][p]]
            seq_impacted_r_primer_binding = [
                amplicon for p in problematic_rprimers for amplicon in ecopcr_dict['rprimers_to_amplicons'][p]]

            dict_info['seq_impacted_by_binding'] = len(
                set(seq_impacted_f_primer_binding + seq_impacted_r_primer_binding))

    encoder(json_file_path, dict_info)

    return dict_info


def parse_arguments():
    parser = ArgumentParser(description="...",
                            formatter_class=ArgumentDefaultsHelpFormatter)

    parser.add_argument("--region_list", type=str, default=None,
                        help="")

    args = parser.parse_args()
    return args


def main():
    args = parse_arguments()
    regions = args.region_list
    if regions:
        regions = regions.split(' ')
    else:
        regions = []
    start = time.perf_counter()
    main_result_dir = 'degeprime_results/bash_pipeline/ecoPCR/'
    # regions = 'COG0097-COG0200 COG0200-COG0201 COG0052-COG0264'
    # regions = 'COG0052-COG0264 COG0097-COG0200 COG0080-COG0081 COG0200-COG0201 COG0090-COG0197'
    # regions = 'COG0090-COG0197'
    region_infos = {}
    main_dict_infos = []

    recompute_all = True
    cov_min = 60  # 60  # 60

    nb_best_element_to_select = None

    with open('mock_bacterial_composition.tsv') as fl:
        mock_species = [l.split('\t')[0].strip() for l in fl]

    for region in regions:
        print(region)
        region_dir = os.path.join(main_result_dir, region)
        sorted_couples = analyse_primers.get_best_couples_by_taxcov(region_dir)
        if nb_best_element_to_select:
            selected_couples = sorted_couples[:nb_best_element_to_select]
        else:
            selected_couples = sorted_couples

        couple_dirs = [couple_dir for couple_dir,
                       prct in selected_couples if float(prct) >= cov_min]
        dict_infos = []

        with concurrent.futures.ProcessPoolExecutor() as executor:
            conc_result = [executor.submit(get_info_on_primer_couple, region,
                                           couple_dir, mock_species, recompute_all,
                                           get_binding_info=False) for couple_dir in couple_dirs]
            dict_infos = [f.result() for f in concurrent.futures.as_completed(conc_result)]

        # for couple_dir in couple_dirs:
        #     dict_info = get_info_on_primer_couple(region,
        #                                           couple_dir, recompute_all=recompute_all, get_binding_info=False)
        #
        #     dict_infos.append(dict_info)
        analyse_primers.list_of_dict_to_tsv(dict_infos, f'{region}_primer_info.tsv')
        main_dict_infos += dict_infos
        #region_infos[region] = dict_infos
        # break
    # #
    # region = 'gyrB'
    # couple_dir = 'ecoPCR/gyrB/'

    finish = time.perf_counter()
    print(f'Finished in {round(finish-start, 2)} second(s)')

    additional_couples = [('gyrB', 'ecoPCR/gyrB/'), ('16S_v3v4', 'ecoPCR/16S_v3v4/'),
                          ('rrn', 'ecoPCR/rrn/'), ('16Sfull', 'ecoPCR/16Sfull/'), ('rpoB', 'ecoPCR/rpoB/'), ('rpoB_proteoB', 'ecoPCR/rpoB_proteoB/')]

    for region, couple_dir in additional_couples:
        dict_info = get_info_on_primer_couple(
            region, couple_dir, mock_species, recompute_all,  get_binding_info=False)
        # dict_info['region'] = region
        main_dict_infos.append(dict_info)

    # region = '16S_v3v4'
    # couple_dir = 'ecoPCR/16S_v3v4/'
    # dict_info = get_info_on_primer_couple(couple_dir, get_binding_info=False)
    # dict_info['region'] = region
    # main_dict_infos.append(dict_info)
    # for d in main_dict_infos:
    #     for k in d:
    #         print(k)

    file_name = f'stat_primers/primer_summary_info_{"_".join(regions)}'
    if cov_min:
        file_name += f'_covsup{cov_min}'
    if nb_best_element_to_select:
        file_name += f'_first_{nb_best_element_to_select}'

    analyse_primers.list_of_dict_to_tsv(main_dict_infos, file_name + '.tsv')
    print(file_name)


if __name__ == '__main__':
    main()
