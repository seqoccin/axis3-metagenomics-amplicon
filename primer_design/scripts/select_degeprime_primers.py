#!/usr/bin/env python3

"""
Filter and select degeprime primers

:Example:
usage: select_degeprime_primers.py --target_gene TARGET_GENE \
                                   --degeprime_result_dir DEGEPRIME_RESULT_DIR \
                                   -o OUTPUT --min_tm 48 \
                                   --n_best_primers 100 \
                                   --tm_calculator Tm_Wallace \
                                   -v

"""

# Metadata
__author__ = 'Mainguy Jean - Plateforme bioinformatique Toulouse'
__copyright__ = 'Copyright (C) 2020 INRAE'
__license__ = 'GNU General Public License'
__version__ = '0.1'
__email__ = 'support.bioinfo.genotoul@inra.fr'
__status__ = 'dev'


from argparse import ArgumentParser, ArgumentDefaultsHelpFormatter
import logging
from Bio.SeqUtils import MeltingTemp as mt
from Bio import SeqIO
import os
import csv
from Bio import Seq
from itertools import product

def parse_degeprime_result(file):
    degeprime_header = ['Pos', 'TotalSeq', 'UniqueMers', 'Entropy', 'PrimerDeg', 'PrimerMatching', 'PrimerSeq']

    with open(file) as csvfile:
        primers = list(csv.DictReader(csvfile, delimiter='\t'))

    if not primers or sorted(primers[0].keys()) != sorted(degeprime_header):
        raise KeyError('File does not seem to be a valid degeprime result file')

    return primers



def extend_ambiguous_dna(deg_seq):
    """return list of all possible sequences given an ambiguous DNA input"""
    d = Seq.IUPAC.IUPACData.ambiguous_dna_values
    r = []
    for i in product(*[d[j] for j in deg_seq]):
        r.append("".join(i))
    return r


def get_Tm_intervall_from_seqs(seqs, method = 'Tm_Wallace'):
    # https://biopython.org/DIST/docs/api/Bio.SeqUtils.MeltingTemp-module.html
    tm_list = []
    for seq in seqs:
        if method == 'Tm_Wallace':
            tm_list.append(mt.Tm_Wallace(Seq.Seq(seq)))
        elif method == 'Tm_NN':
            tm_list.append(mt.Tm_NN(Seq.Seq(seq)))
        elif method == 'Tm_GC':
            tm_list.append(mt.Tm_GC(Seq.Seq(seq)))
        else:
            raise ValueError(f'{method} is an unknown method to copute TM.')

    return min(tm_list), max(tm_list)

def get_primer_Tm_intervall(primer_seq, method = 'Tm_Wallace'):
    return get_Tm_intervall_from_seqs(extend_ambiguous_dna(primer_seq), method)

def parse_arguments():
    """Parse script arguments."""
    parser = ArgumentParser(description="Filter and select degeprime primers",
                            formatter_class=ArgumentDefaultsHelpFormatter)
    parser.add_argument("--target_gene",  required=True)
    parser.add_argument("--degeprime_result_dir",  default='./')
    parser.add_argument("-o", "--output", default='selected_primers.tsv')
    parser.add_argument("--min_tm", type=int, default=48,
                        help="Keep only primers with a tm >= tm_min")
    parser.add_argument("--min_length", type=int, default=17,
                        help="Keep only primers with a length >= min_length")
    parser.add_argument("--max_length", type=int, default=25,
                        help="Keep only primers with a length <= max_length")
    parser.add_argument("--degeneracy_max", type=int, default=2048,
                        help="Keep only primers with a degeneracy <= degeneracy_max")
    parser.add_argument("--n_best_primers", type=int, default=100,
                        help="Select the n best primers based on Primer Matching: Number of sequences that match the primer.")
    parser.add_argument('--tm_calculator', choices=['Tm_Wallace', 'Tm_NN', 'Tm_GC'], default='Tm_Wallace', help='3 possible methods to compute melting temperature as explain here: https://biopython.org/DIST/docs/api/Bio.SeqUtils.MeltingTemp-module.html')
    parser.add_argument("-v", "--verbose", help="increase output verbosity",
                        action="store_true")

    args = parser.parse_args()
    return args


def main():

    args = parse_arguments()

    if args.verbose:
        logging.basicConfig(format="%(levelname)s: %(message)s", level=logging.DEBUG)
        logging.info('Mode verbose ON')

    else:
        logging.basicConfig(format="%(levelname)s: %(message)s")

    degeprime_result_dir = args.degeprime_result_dir
    target_gene = args.target_gene
    min_tm = args.min_tm
    tm_calculator = args.tm_calculator
    n_best_primers = args.n_best_primers
    output_file = args.output
    degeneracy_max =  args.degeneracy_max
    min_length = args.min_length
    max_length = args.max_length



    already_treated_seq  = []
    primer_infos = []
    rejected_primers = []


    # Gather primer and remove redundancy
    for file_name in os.listdir(degeprime_result_dir):
        file = os.path.join(degeprime_result_dir, file_name)
        logging.info(f'Process primer of file {file}')
        try:
            raw_primers = parse_degeprime_result(file)
        except KeyError:
            logging.warning(f'{file} does not seems to be a degeprime result file. This file is ignored.')
            continue

        primers = [ p for p in raw_primers if p['PrimerSeq'] not in already_treated_seq]

        already_treated_seq += [p['PrimerSeq'] for p in primers]
        logging.info(f'{len(already_treated_seq)} non redundant primers')
        # filter primer based on tm min and shape primer dict
        for p in primers:
            d = {}
            d['seq'] = p['PrimerSeq']
            d['target'] = target_gene
            d['degeneracy'] = p['PrimerDeg']
            d['tm_min'], d['tm_max'] = get_primer_Tm_intervall(p['PrimerSeq'], method=tm_calculator)
            d['source'] = file_name
            d['comment'] = f"PrimerMatching:{p['PrimerMatching']};Pos:{p['Pos']}"
            d['PrimerMatching'] = int(p['PrimerMatching'])

            # Filter primers
            if d['tm_min'] >= min_tm and int(d['degeneracy']) <= degeneracy_max and min_length <= len(d['seq']) <= max_length:
                primer_infos.append(d)
            else:
                rejected_primers.append(d)

    logging.info(f'{len(rejected_primers)} primers have been rejected because of their Tm, length or degeneracy')
    logging.info(f'{len(primer_infos)} selected primers')

    sorted_primers = sorted(primer_infos, key=lambda k: k['PrimerMatching'], reverse=True)

     # select n best primers
    selected_primers = [p for i, p in enumerate(sorted_primers) if i < n_best_primers]

    # Add primer id to primer dicts
    for i, d in enumerate(selected_primers):
        d['primer_id'] = f'f{target_gene}_{i}'

    # Write selected primers into a file
    headers = ['primer_id', 'seq',  'target', 'degeneracy',
    'PrimerMatching', 'tm_min', 'tm_max', 'source', 'comment']

    logging.info(f'Writing selected primer in {output_file}')
    with open(output_file, 'w', newline='') as out_fh:
        writer = csv.DictWriter(out_fh, fieldnames=headers, delimiter='\t')
        writer.writeheader()
        for dict_info in selected_primers:
            writer.writerow(dict_info)


if __name__ == '__main__':
    main()
