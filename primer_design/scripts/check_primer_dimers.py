#!/usr/bin/env python

from __future__ import division
from os import rename, remove, path
from os.path import isfile
from string import upper

from cogent.app.vienna_package import RNAfold
from cogent.seqsim.sequence_generators import SequenceGenerator, IUPAC_DNA
from cogent import DNA


import itertools
from argparse import ArgumentParser, ArgumentDefaultsHelpFormatter

"""This a mofify version of the script check_primer_barcode_dimers.py of PrimerProspector 1.0.1"""

# __author__ = "William A. Walters"
# __copyright__ = "Copyright 2010, The PrimerProspector project"
# __credits__ = ["William A. Walters", "Greg Caporaso", "Rob Knight"]
# __license__ = "GPL"
# __version__ = "1.0.1-release"
# __maintainer__ = "William A. Walters"
# __email__ = "william.a.walters@colorado.edu"
# __status__ = "Release"

""" This module contains functions for finding the folding energy of
barcode-primer/primer combinations, creating a text file of these combinations
whose energy falls below a particular threshold, and generation of graphical
files displaying the secondary structure of these flagged sequences"""


def expand_degeneracies(raw_primer):
    """ Returns all non-degenerate versions of a given primer sequence """

    primers = SequenceGenerator(template=raw_primer, alphabet=IUPAC_DNA)
    expanded_primers = []
    for primer in primers:
        expanded_primers.append(primer)

    return expanded_primers


def is_inter_binding(mfe_string, seq):
    """
    exemple of mfe format string
    ((((..((((...................))))..)))).........

    """
    # Is it only sequence that is tested to check intra binding?
    # Is there no connector symbol in the seq
    if '-' not in seq:
        return True
    # There is two seq connected by a connector seq made of '-'
    # Is it an inter binding or intra binding?
    primer1, primer2 = seq.split('-'*seq.count('-'))
    mfe_p1 = mfe_string[:len(primer1)]
    mfe_p2 = mfe_string[seq.rindex(primer2):]

    # assert mfe_p1 + '.'*seq.count('-') + mfe_p2 == mfe_string

    if mfe_p1.count('(') != mfe_p1.count(')') and mfe_p2.count('(') != mfe_p2.count(')'):
        return True
    else:
        return False  # can be due to intra binding or no binding

    # if '(' not in mfe_string:
        # Not binidng found there


def launch_RNAfold(seqs, output_dir,
                   annealing_temp, energy_parameters,
                   score_threshold,  fl_dimer, fl_all):
    # print(seq)

    r = RNAfold(WorkingDir=output_dir)
    r.Parameters['-T'].on(annealing_temp)
    r.Parameters['-P'].on(energy_parameters)

    # print(seqs.keys())
    # print('outdir', output_dir)
    result = r(list(seqs.keys()))

    while 1:
        try:
            seq = next(result['StdOut']).strip().replace('U', 'T')

            fold_result = next(result['StdOut'])

        except StopIteration:
            return
        mfe_string, energy_str = fold_result.strip().rsplit(' (')
        energy = float(energy_str.replace(')', '').replace(' ', ''))
        if energy < score_threshold or fl_all:
            # print('FROM FCT', seq, energy)
            # print seqs[seq]

            id1, id2 = seqs[seq][0]
            line = '\t'.join([seq, mfe_string, id1, id2, str(energy)])

            if fl_all:
                fl_all.write(line + '\n')

            if energy < score_threshold and is_inter_binding(mfe_string, seq):
                fl_dimer.write(line + '\n')


def check_primers(primer1,
                  primer2,
                  energy_parameters,
                  annealing_temp,
                  score_threshold,
                  output_dir='.',
                  p1_name='p1',
                  p2_name='p2'):
    """Main function for checking primers for secondary structure.

    primer1: primer associated with barcodes (can be forward or reverse primer).
     This primer is listed in 5' to 3' direction.
    primer2: second primer. This primer is listed in 5' to 3' direction.
    energy_parameters: filepath containing DNA parameters file.
    output_dir: output directory.
    annealing_temp: Annealing temperature-this should be the lowest
     temperature during the PCR process.  In degrees Celcius.
    score_threshold: Value at or below which barcode/primer combinations will
     be flagged for potential secondary structure.  Gibbs energy in kcal/mol.
    """

    primer1 = upper(primer1)
    primer2 = upper(primer2)

    f_primers = sorted(expand_degeneracies(primer1))
    r_primers = sorted(expand_degeneracies(primer2))

    print(primer1, len(f_primers))
    print(primer2, len(r_primers))

    # give unique id to each version of degenerate primer
    f_primers_id_tuples = [(p1_name+'_v' + str(i), p) for i, p in enumerate(f_primers)]
    r_primers_id_tuples = [(p2_name+'_v' + str(i), p) for i, p in enumerate(r_primers)]

    connector_seq = "----------"

    write_all_result = False

    # Test the forward and reverse primers against each other, as well as the
    # forward against forward, reverse against reverse.

    # Use of combinations_with_replacement to test all possible pair of primers
    # https://docs.python.org/2.7/library/itertools.html#itertools.combinations_with_replacement
    # exemple
    # >>> list(itertools.combinations_with_replacement(['a', 'b', "c"], r=2))
    # [('a', 'a'), ('a', 'b'), ('a', 'c'), ('b', 'b'), ('b', 'c'), ('c', 'c')]

    id_primer_iter = itertools.combinations_with_replacement(
        f_primers_id_tuples + r_primers_id_tuples, r=2)

    i = 0
    total_seq = len(f_primers) + len(r_primers)
    nb_total_pair = (total_seq * (total_seq - 1))/2 + total_seq

    header = '# primer1:' + primer1 + '|name:'+p1_name + '|degeneracy:'+str(len(f_primers)) + '\n'
    header += '# primer2:' + primer2 + '|name:' + \
        p2_name + '|degeneracy:'+str(len(r_primers)) + '\n'

    column_name = '\t'.join(["sequence_tested", 'mfe_folding',
                             "primer1_version", "primer2_version", "energy"])+'\n'

    header_inter = header + '# number of couples of sequences tested ' + \
        str(int(nb_total_pair)) + '\n' + column_name
    header_intra = header + '# number of sequences tested ' + \
        str(len(f_primers)+len(r_primers)) + '\n' + column_name
    print header

    if write_all_result:
        fl_all = open(path.join(output_dir, 'all_inter_primer_binding_result_ta' +
                                str(int(annealing_temp))+'.tsv'), 'w')
        fl_all.write(header_inter)

        fl_all_intra = open(
            path.join(output_dir, 'all_intra_primer_binding_result_ta'+str(int(annealing_temp))+'.tsv'), 'w')
        fl_all_intra.write(header_intra)
    else:
        fl_all = None
        fl_all_intra = None

    fl_dimer = open(path.join(output_dir, 'inter_primer_binding_result_ta' +
                              str(int(annealing_temp))+'.tsv'), 'w')
    fl_intra = open(path.join(output_dir, 'intra_primer_binding_result_ta' +
                              str(int(annealing_temp))+'.tsv'), 'w')
    fl_dimer.write(header_inter)
    fl_intra.write(header_intra)

    seqs = {}
    seq_chunck = 100000

    seqs = {}
    for (id1, primer1), (id2, primer2) in id_primer_iter:

        seq = primer1 + connector_seq + primer2
        if seq in seqs:
            seqs[seq].append((id1, id2))
        else:
            seqs[seq] = [(id1, id2)]

        # Write all result:
        i += 1
        if i % seq_chunck == 0:
            launch_RNAfold(seqs, output_dir, annealing_temp,
                           energy_parameters, score_threshold, fl_dimer, fl_all)
            seqs = {}
            print i, round(i/float(nb_total_pair)*100, 2),  '%'

    launch_RNAfold(seqs, output_dir, annealing_temp,
                   energy_parameters, score_threshold, fl_dimer, fl_all)
    print i, round(i/float(nb_total_pair)*100, 2),  '%'

    for id, primer in f_primers_id_tuples + r_primers_id_tuples:
        launch_RNAfold({primer: [(id, 'None')]}, output_dir, annealing_temp,
                       energy_parameters, score_threshold, fl_intra, fl_all_intra)

    fl_dimer.close()
    fl_intra.close()


def parse_arguments():
    parser = ArgumentParser(description="...",
                            formatter_class=ArgumentDefaultsHelpFormatter)

    parser.add_argument('-p', '--primer1', type=str, required=True,
                        help="Primer, written in 5' to 3', that is linked to barcodes" +
                        " tested.  If linker sequence is present between primer and " +
                        "barcode, include it with this sequences.")

    parser.add_argument('-P', '--primer2', type=str, required=True,
                        help="Second primer, written in 5' to 3' orientation.  This " +
                        "primer by default is not associated with any barcodes."),
    parser.add_argument('-e', '--energy_parameters', type=str, required=True,
                        help='Specify energy parameters file for predicting secondary ' +
                        'structures.  A DNA parameters file, dna_DM.par, is found in the ' +
                        'DNA_parameters folder of Primer Prospector, and should be pointed ' +
                        'to with this parameter.  If an incorrect file is used, the ' +
                        'Vienna software will use default parameters, which are for RNA ' +
                        'folding, and could give misleading results.  The provided DNA ' +
                        "parameters file is a modified form of the DNA parameters from " +
                        " David Mathews' RNAstructure program.")

    parser.add_argument('-s', '--score_threshold', type=float,
                        help='Specify a score threshold for the Gibbs energy calculation, ' +
                        'below which a barcode/primer combination is flagged for potential ' +
                        'secondary structure.', default=-10.0)
    parser.add_argument('-t', '--annealing_temp', type=float,
                        help='Specify an annealing temperature in degrees Celsius.', default=50)
    parser.add_argument("--primer_name1", type=str, default="p1",
                        help='Specify output directory for barcode/primer secondary structure' +
                        ' summary and graphs. [default: %default]')
    parser.add_argument("--primer_name2", type=str, default="p2",
                        help='Specify output directory for barcode/primer secondary structure' +
                        ' summary and graphs. [default: %default]')
    parser.add_argument("-o", "--output_dir", type=str, default=".",
                        help='Specify output directory for barcode/primer secondary structure' +
                        ' summary and graphs. [default: %default]')
    args = parser.parse_args()
    return args


def main():

    print('This is a modified version of check_primer_barcode_dimers.py')

    args = parse_arguments()
    print(args)
    primer1 = args.primer1
    primer2 = args.primer2
    output_dir = args.output_dir
    annealing_temp = args.annealing_temp
    score_threshold = args.score_threshold
    energy_parameters = args.energy_parameters
    p1_name = args.primer_name1
    p2_name = args.primer_name2

    #
    if not isfile(energy_parameters):
        raise ValueError, ('Specified path %s ' % energy_parameters +
                           'for DNA energy parameters not a file.')

    # Create output directory if it does not exist
    # create_dir(output_dir)

    check_primers(primer1, primer2, energy_parameters,
                  annealing_temp, score_threshold, output_dir, p1_name, p2_name)


if __name__ == "__main__":
    main()
