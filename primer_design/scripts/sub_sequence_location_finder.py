#!/usr/bin/env python3

"""
Description

:Example:
python template.py -v
"""

# Metadata
__author__ = 'Mainguy Jean - Plateforme bioinformatique Toulouse'
__copyright__ = 'Copyright (C) 2019 INRA'
__license__ = 'GNU General Public License'
__version__ = '0.1'
__email__ = 'support.bioinfo.genotoul@inra.fr'
__status__ = 'dev'


from argparse import ArgumentParser, ArgumentDefaultsHelpFormatter
import os
import csv
import logging
from Bio import SeqIO
import gzip
from Bio.SeqFeature import SeqFeature, FeatureLocation
import re
# from Bio.Seq import Seq


def findAll(a_str, sub, start=0, positions=[]):
    start = a_str.find(sub, start)
    
    if start == -1:
        return positions

    positions.append(start)
    start += 1 
    return findAll(a_str, sub, start, positions)

def list_of_dict_to_tsv(list_of_dict, output_file, mode='w', writeheader=True):
    """Encode a list of dictionnary into a tsv file.

    The mode of writing can be specified.
    By default the mode is set to "w"

    """
    if len(list_of_dict) == 0:
        raise ValueError('The given list is empty: we cant write it in a file')

    with open(output_file, mode, newline='') as csvfile:
        writer = csv.DictWriter(csvfile, fieldnames=list(list_of_dict[0].keys()), delimiter='\t')
        if mode == 'w' and writeheader is True:
            writer.writeheader()
        for dict_info in list_of_dict:
            writer.writerow(dict_info)


def parse_arguments():
    """Parse script arguments."""
    parser = ArgumentParser(description="...",
                            formatter_class=ArgumentDefaultsHelpFormatter)

    parser.add_argument("--sub_sequence_files", type=str, required=True)
    
    parser.add_argument('--main_sequence', help="genomic sequences where the sub sequence have been extracted", required=True)
    
    parser.add_argument("-o", '--output', help="", type=str, default=1)
    # parser.add_argument("--mock_compo_file", help="", default='mock_bacterial_composition.tsv',
    #                     action="store_true")

    parser.add_argument("-v", "--verbose", help="increase output verbosity",
                        action="store_true")

    args = parser.parse_args()
    return args
    
    
def main():

    args = parse_arguments()

    if args.verbose:
        logging.basicConfig(format="%(levelname)s: %(message)s", level=logging.DEBUG)
        logging.info('Mode verbose ON')

    else:
        logging.basicConfig(format="%(levelname)s: %(message)s")
   
   
   
    output = args.output
    
    subsequences_fl = args.sub_sequence_files
    
    main_sequences_file = args.main_sequence


    sub_seq_record_dict = SeqIO.index(subsequences_fl, "fasta")
        
    proper_open = gzip.open if main_sequences_file.endswith('.gz') else open

    capture_assemby_acc = re.compile("([A-Z]{3}_\d+.\d+)")

    assembly_acc = capture_assemby_acc.search(os.path.basename(main_sequences_file)).group(1)

    logging.info(f'Assembly accession {assembly_acc}')
    print(assembly_acc)
    with proper_open(main_sequences_file, 'rt') as handle:
        for record in SeqIO.parse(handle, "fasta"):
            seqid = record.id
            
            for sub_id, sub_seq in sub_seq_record_dict.items():
                
                starts = findAll(str(record.seq), str(sub_seq.seq), start=0, positions=[])
                locations = [f'+{s+1}..{s+len(sub_seq.seq)}' for s in starts]
                
                starts_minus = findAll(str(record.seq), str(sub_seq.seq.reverse_complement() ), start=0, positions=[])
                locations += [f'-{s+1}..{s+len(sub_seq.seq)}' for s in starts_minus]
                print(sub_id, locations)

if __name__ == '__main__':
    main()
