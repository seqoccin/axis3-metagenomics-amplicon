#!/usr/bin/env python3

"""
Format fasta sequences.

Formatting consists of adding taxid to the header in order to be process by the tool obiconvert to create an ecoPCR database.

ecoPCR tronc and rename partially sequence ids with annoying symbol that can caused trouble when parsing it.
To prevent any issues, sequences ids are renamed with a simple pattern `<assembly_accession>|s<sequence number>`.
The original header is added at the end of the new one after the taxid.
To keep track to this change a file is created by the script.
It is stored the same directory as the formated sequences and is named using the following pattern <formatted_sequence_file_name>_map.ids
"""

# Metadata
__author__ = 'Mainguy Jean - Plateforme bioinformatique Toulouse'
__copyright__ = 'Copyright (C) 2020 INRA'
__license__ = 'GNU General Public License'
__version__ = '0.1'
__email__ = 'support.bioinfo.genotoul@inra.fr'
__status__ = 'dev'


import os
import logging
from argparse import ArgumentParser, ArgumentDefaultsHelpFormatter
import sys
import csv
from Bio import SeqIO
from collections import defaultdict


def tsv_to_dict_of_dicts(file, key_field):
    """
    Take a tsv with header and parse into dict of dict
    Uses
    * the specified field as key
    * the line turned into a dict as value .
    """
    dict_of_list_of_dict = defaultdict(list)
    with open(file) as csvfile:
        reader = csv.DictReader(csvfile, delimiter='\t')
        for l in reader:
            dict_of_list_of_dict[l[key_field]].append(l)
        return dict(dict_of_list_of_dict)


def parse_arguments():
    """Parse script arguments."""
    parser = ArgumentParser(description="...",
                            formatter_class=ArgumentDefaultsHelpFormatter)

    parser.add_argument("sequence_file", type=str,
                        help="Sequence file in fasta format to format.")
    parser.add_argument("--assembly_selection", type=str,
                        help="Assembly selection with taxonomy information.", required=True)
    parser.add_argument("-o", "--output", type=str, default=None,
                        help="Name of the output file. By default the prefix 'formatted_' is added to the name of the input file and the file is created in the current directory. ")
    parser.add_argument("-v", "--verbose", help="increase output verbosity",
                        action="store_true")
    args = parser.parse_args()
    return args


def main():

    args = parse_arguments()

    if args.verbose:
        logging.basicConfig(format="%(levelname)s: %(message)s", level=logging.DEBUG)
        logging.info('Mode verbose ON')

    else:
        logging.basicConfig(format="%(levelname)s: %(message)s")

    args = parse_arguments()
    sequence_file = args.sequence_file
    assembly_selection = args.assembly_selection

    if args.output:
        formated_seq_file = args.output
    else:
        formated_seq_file = f"./formatted_" + os.path.basename(sequence_file)

    logging.info(f'Output file sequence is {formated_seq_file}')

    output_dir = os.path.dirname(formated_seq_file)

    os.makedirs(output_dir, exist_ok=True)

    assemblies_id_mapper_fl = os.path.join(output_dir, '.'.join(
        os.path.basename(formated_seq_file).split('.')[:-1]) + '_map.ids')

    # Parse assembly_taxonomy_file
    with open(assembly_selection) as csvfile:
        reader = csv.DictReader(csvfile, delimiter='\t')
        assembly_to_taxid = {d['assembly_accession']: d['species_taxid']
                             for d in reader}

    assembly_to_new_seq_id = defaultdict(dict)

    with open(sequence_file) as fl, open(formated_seq_file, 'w') as formated_outfl:
        for seq in SeqIO.parse(fl, "fasta"):
            assembly_name = seq.id.split('|')[0]

            if assembly_name not in assembly_to_taxid:
                raise KeyError(
                    'Assembly accession {assembly_name} is not found in the given assembly selection {assembly_selection}.')

            original_id = seq.id
            i = len(assembly_to_new_seq_id[assembly_name])
            new_id = f'{assembly_name}|s{i:03d}'
            assembly_to_new_seq_id[assembly_name][new_id] = original_id
            seq.id = new_id
            taxid = assembly_to_taxid[assembly_name]
            seq_extra_info = seq.description
            seq.description = f'taxid={taxid}; {seq_extra_info}'
            if len(seq.id) > 20:
                logging.critical('Containing header length has failed: {seq.id}')

            SeqIO.write(seq, formated_outfl, "fasta")

    with open(assemblies_id_mapper_fl, 'w') as map_id_wrt:
        for list_ids_dict in assembly_to_new_seq_id.values():
            for new_id, original_id in list_ids_dict.items():
                map_id_wrt.write(f'{new_id}\t{original_id}\n')


if __name__ == '__main__':
    main()
