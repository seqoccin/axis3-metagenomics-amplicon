#!/usr/bin/env python3

"""
Functions to generate sarray command of ecoPCR.

:Example:

"""

# Metadata
__author__ = 'Mainguy Jean - Plateforme bioinformatique Toulouse'
__copyright__ = 'Copyright (C) 2020 INRAe'
__license__ = 'GNU General Public License'
__version__ = '0.1'
__email__ = 'support.bioinfo.genotoul@inra.fr'
__status__ = 'dev'


from Bio import Seq
from itertools import product
from Bio.SeqUtils import MeltingTemp as mt
from Bio.Alphabet import IUPAC
import gzip
import os
import logging
from argparse import ArgumentParser, ArgumentDefaultsHelpFormatter
import sys
import csv
from Bio import SeqIO
import re
import time
import json




def split(a, n):
    """
    Split a list a into n chunks of of approximately equal length.
    from https://stackoverflow.com/questions/2130016/splitting-a-list-into-n-parts-of-approximately-equal-length
    """
    #
    k, m = divmod(len(a), n)
    return (a[i * k + min(i, m):(i + 1) * k + min(i + 1, m)] for i in range(n))


def merge_sarray_command(ecopcr_commands, ecotaxstat_commands, nb_job_per_sarray, run_ecotaxstat):
    """
    Merge sarray command to have sarray file with a defined number of line (nb_job_per_sarray).
    """

    ecopcr_chunks = split(ecopcr_commands, nb_job_per_sarray)
    ecotaxstat_chunks = split(ecotaxstat_commands, nb_job_per_sarray)
    # assert len(ecopcr_chunks) == len(ecotaxstat_chunks)

    for ecopcr_chunk, ecotaxstat_chunk in zip(ecopcr_chunks, ecotaxstat_chunks):
        assert len(ecopcr_chunk) == len(ecotaxstat_chunk)
        logging.info(f'{len(ecopcr_chunk)} ecopcr command per sarray line.. ')
        if len(ecopcr_chunk) == 0:
            continue

        ecopcr_joined = ';'.join(ecopcr_chunk)
        if run_ecotaxstat:
            ecotaxstat_joined = ';'.join(ecotaxstat_chunk)
            print(f'{ecopcr_joined};{ecotaxstat_joined}')
        else:
            print(ecopcr_joined)


def encoder(filename, f):
    """Encode a python structure in a json file."""
    with open(filename, 'w', encoding='utf8') as file:
        json.dump(f, file, indent=2)


def decoder(filename):
    """Load a json file and return the corresponding structure."""
    with open(filename, 'r', encoding='utf8') as file:
        return json.load(file)


def extend_ambiguous_dna(deg_seq):
    """return list of all possible sequences given an ambiguous DNA input"""
    d = Seq.IUPAC.IUPACData.ambiguous_dna_values
    r = []
    deg_seq = deg_seq.upper()
    for i in product(*[d[j] for j in deg_seq]):
        r.append("".join(i))
    return r


def get_degeneracy(deg_seq):
    """Get sequence degeneracy."""
    return len(extend_ambiguous_dna(deg_seq))

def write_command_in_fl(command, file):
    """Write command in file."""
    with open(file, 'w') as fl:
        fl.write(command)



def is_an_ecopcr_db(db):
    """Check if the provided db look like an ecoPCR db."""
    if all([os.path.isfile(f'{db}.{ext}') for ext in ['ndx', 'rdx', 'tdx', 'adx']]):
        return True
    else:
        return False


def manage_primer_couple(p_f, p_r, couple_dir, l, L, db, e, adjust_e_with_primer_quality=True, ecopcr_tool_path='ecoPCR', write_cmd=True):
    """
    Build ecoPCR commands for a primer couple.
    """

    f_and_r_name = '-'.join([p_f['primer_id'], p_r['primer_id']])

    if not is_an_ecopcr_db(db):
        logging.critical(f'ecoPCR db {db} does not exist or some files are missing.')
        raise ValueError(f'ecoPCR db {db} does not exist or some files are missing.')

    if not os.path.isdir(couple_dir):
        logging.critical(f'Primer couple dir {couple_dir} does not exists.')
        raise ValueError(f'Primer couple dir {couple_dir} does not exists.')

    # add # at the last 2 base in 3' to prevent any mismatch there
    seq_f = p_f['seq'][:-1] + '#' + p_f['seq'][-1:] + '#'
    seq_r = p_r['seq'][:-1] + '#' + p_r['seq'][-1:] + '#'

    # get upper sequences :
    seq_f = seq_f.upper()
    seq_r = seq_r.upper()

    # Check primers coherence
    couple_dict = {'forward_primer': p_f, 'reverse_primer': p_r}
    couple_dict_fl = os.path.join(couple_dir, f'{f_and_r_name}.json')
    if os.path.isfile(couple_dict_fl):
        past_couple_dict = decoder(couple_dict_fl)
        # print(past_couple_dict)
        if past_couple_dict['forward_primer']['seq'] == couple_dict['forward_primer']['seq'] and past_couple_dict['reverse_primer']['seq'] == couple_dict['reverse_primer']['seq']:
            logging.info('Primers sequences are identical..')
        else:
            logging.critical(f'PRIMERS SEQUENCES ARE NOT IDENTICAL IN {couple_dict_fl}')
            return False
    else:
        encoder(couple_dict_fl, couple_dict)

    # does not use high e when primer is too bad
    if adjust_e_with_primer_quality and int(e) > 1:

        if min(len(p_f['seq']), len(p_r['seq'])) <= 17 and max(get_degeneracy(p_f['seq']), get_degeneracy(p_r['seq'])) > 2000:
            logging.warning(f"Reset ecopcr max mismatch (e) to 1 because primer are not good enough len {min(len(p_f['seq']), len(p_r['seq']))} <= 17 or deg {max(get_degeneracy(p_f['seq']), get_degeneracy(p_r['seq']))} > 2000")
            e = 1
        else:
            logging.info(f"No reseting ecopcr mismatch. Primer are good enough: min len {min(len(p_f['seq']), len(p_r['seq']))} > 17 or max deg {max(get_degeneracy(p_f['seq']), get_degeneracy(p_r['seq']))} <= 2000")

    db_simple_name = os.path.basename(db).split('.')[0]
    if '.' in db:
        db_extension = os.path.basename(db).split('.')[-1] + '_'  # chunk number
    else:
        logging.info(f'No extension in ecopcr_db {db}')
        db_extension = ''
    ecopcr_output_dir = os.path.join(couple_dir, f'ecopcr_{db_simple_name}')
    os.makedirs(ecopcr_output_dir, exist_ok=True)
    ecopcr_output = os.path.join(couple_dir, f'ecopcr_{db_simple_name}', f'{db_extension}{f_and_r_name}_l{l}_L{L}_e{e}.ecopcr')

    ecopcr_command = f'{ecopcr_tool_path} -d {db} -e {e} -l {l} -L {L} {seq_f} {seq_r} > {ecopcr_output}'

    slurm_output = os.path.join(couple_dir, f'ecopcr_{db_simple_name}', f'{db_extension}{f_and_r_name}_l{l}_L{L}_e{e}_slurm%x.out')
    ecotaxstat_result = os.path.join(couple_dir, f'ecopcr_{db_simple_name}', f'{db_extension}{f_and_r_name}_l{l}_L{L}_e{e}.ecotaxstat')
    ecostat_command = f'module load bioinfo/obitools-v1.2.11;ecotaxstat -d {db} {ecopcr_output} -r 2 > {ecotaxstat_result}'
    ecopcr_and_ecostat_command = ecopcr_command + ';' + ecostat_command

    sbatch_command = f'sbatch -J ecoPCR --error="{slurm_output}" --output="{slurm_output}" --wrap "{ecopcr_command};{ecostat_command}"'

    if write_cmd:
        write_command_in_fl(ecopcr_and_ecostat_command, ecopcr_output.split('.')[0] + '.sh')
        write_command_in_fl(sbatch_command, ecopcr_output.split('.')[0] + '.sbatch')

    ecopcr_commands = []
    ecotaxstat_commands = []
    if not os.path.isfile(ecopcr_output):
        ecopcr_commands.append(ecopcr_command)
        ecotaxstat_commands.append(ecostat_command)
    else:
        logging.info(f'NO COMPUTATION.. {ecopcr_output} exist')

    return ecopcr_commands, ecotaxstat_commands
