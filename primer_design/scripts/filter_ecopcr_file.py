#!/usr/bin/env python3

"""
Description

:Example:
python template.py -v
"""

# Metadata
__author__ = 'Mainguy Jean - Plateforme bioinformatique Toulouse'
__copyright__ = 'Copyright (C) 2020 INRAE'
__license__ = 'GNU General Public License'
__version__ = '0.1'
__email__ = 'support.bioinfo.genotoul@inra.fr'
__status__ = 'dev'


from argparse import ArgumentParser, ArgumentDefaultsHelpFormatter
import logging
from collections import defaultdict
import csv


def list_of_dict_to_tsv(list_of_dict, output_file, mode='w', writeheader=True):
    """Encode a list of dictionnary into a tsv file.

    The mode of writing can be specified.
    By default the mode is set to "w"

    """
    if len(list_of_dict) == 0:
        raise ValueError('The given list is empty: we cant write it in a file')

    with open(output_file, mode, newline='') as csvfile:
        writer = csv.DictWriter(csvfile, fieldnames=list(list_of_dict[0].keys()), delimiter='\t')
        if mode == 'w' and writeheader is True:
            writer.writeheader()
        for dict_info in list_of_dict:
            writer.writerow(dict_info)


def filter_ecopcr_file(ecopcrfile, assembly_list, mismatch_max=2):
    # fprimers_to_amplicons = defaultdict(list)
    # rprimers_to_amplicons = defaultdict(list)

    # i = 0
    mismatch_param_str = '# max error count by oligonucleotide : '
                
    with open(ecopcrfile, encoding="latin-1") as fl:
        for l in fl:
            if l.startswith('#'):
                if l.startswith(mismatch_param_str):
                    earlier_mismatch = int(l.split(':')[-1].strip())
                    if mismatch_max > earlier_mismatch:
                        raise ValueError(f'ecoPCR mismatch parameter ({earlier_mismatch}) is bigger than mismatch max {mismatch_max}')
                yield l
                continue

            splitted_l = [e.strip() for e in l.split(' | ')]
            assembly = splitted_l[0].split('|')[0]

            # keep only line of the genome of interest
            if assembly not in assembly_list:
                logging.info(
                    'amplicon is from an assembly that is not part of the given assembly selection. This ammplicon is ignored')
                continue
            # i += 1

            f_mismatch = splitted_l[14]
            r_mismatch = splitted_l[17]
            mismatch = max(int(f_mismatch), int(r_mismatch))
            if mismatch > mismatch_max:
                warning = f'An amplicon has {mismatch} mismatches while the maximum number of mismatches take into account is {mismatch_max}. '
                warning += 'This amplicon is ignored.'
                logging.info(warning)
                continue

            #length = int(splitted_l[19])
            yield l



def parse_arguments():
    """Parse script arguments."""
    parser = ArgumentParser(description="...",
                            formatter_class=ArgumentDefaultsHelpFormatter)
    parser.add_argument("-v", "--verbose", help="increase output verbosity",
                        action="store_true")
    parser.add_argument("-f", "--ecopcr_file", help="",
                        required=True)
    parser.add_argument("-a", "--assembly_selection", help="",
                        required=True)
    parser.add_argument("-e", "--mismatch_max", help="max mismatch to retrieve and analysed in ecopcr result file",
                        default=2, type=int, required=False)
    parser.add_argument("-o", "--output", help="output file",
                        default=0, type=str)
    args = parser.parse_args()
    return args


def main():

    args = parse_arguments()

    if args.verbose:
        logging.basicConfig(format="%(levelname)s: %(message)s", level=logging.DEBUG)
        logging.info('Mode verbose ON')

    else:
        logging.basicConfig(format="%(levelname)s: %(message)s")

    ecopcr_file = args.ecopcr_file
    mismatch_max = args.mismatch_max
    output_file = args.output

    assembly_summary = args.assembly_selection

    lines_info = []
    with open(assembly_summary) as fl:
        reader = csv.DictReader(fl, delimiter='\t')
        assembly_infos = {d['assembly_accession']: d for d in reader}


    total_sp = len({info['species_taxid'] for info in assembly_infos.values()})
    logging.info(f'{total_sp} species found in assembly summary {assembly_summary}')

    with open(output_file, "w") as fl:
        for filter_line in filter_ecopcr_file(ecopcr_file, assembly_infos, mismatch_max):
            fl.write(filter_line)
            


if __name__ == '__main__':
    main()
