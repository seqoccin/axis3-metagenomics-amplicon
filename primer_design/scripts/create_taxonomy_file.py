import gzip
import os
import logging
from argparse import ArgumentParser, ArgumentDefaultsHelpFormatter
import sys
import csv
from Bio import SeqIO
import re
from collections import defaultdict

module_path = os.path.abspath("scripts/")
sys.path.append(module_path)
module_path = os.path.abspath("../scripts/")
sys.path.append(module_path)

#import tools

#import ncbi_related


def parse_arguments():
    parser = ArgumentParser(description="...",
                            formatter_class=ArgumentDefaultsHelpFormatter)

    parser.add_argument("sequence_file", type=str,
                        help="")
    parser.add_argument("assemblies_info_with_taxonomy", type=str,
                        help="", default=None)
    parser.add_argument("-o", "--output", type=str, default=None,
                        help="")
    args = parser.parse_args()
    return args


def main():
    args = parse_arguments()
    sequence_file = args.sequence_file
    assemblies_info_with_taxonomy = args.assemblies_info_with_taxonomy
    if args.output:
        tax_file = args.output
    else:
        tax_file = f"taxonomy_file_" + os.path.basename(sequence_file).split('.')[0] + '.tsv'

    assembly_to_ids = defaultdict(list)
    nb_seq = 0
    with open(sequence_file) as fl:
        for seq in SeqIO.parse(fl, "fasta"):
            assembly_name = seq.id.split('|')[0]
            assembly_to_ids[assembly_name].append(seq.id)
            nb_seq += 1
    id_processed = 0
    # Parse assembly_taxonomy_file
    with open(assemblies_info_with_taxonomy) as fl, open(tax_file, 'w') as out:
        reader = csv.DictReader(fl, delimiter='\t')
        for d in reader:
            if d["assembly_accession"] in assembly_to_ids:
                # reverse taxonomy
                tax = ';'.join(d['taxonomy'].rstrip().split(';')[::-1])
                for id in assembly_to_ids[d["assembly_accession"]]:
                    out.write(f'{id}\t{tax}\n')
                    id_processed += 1

    if id_processed != nb_seq:
        logging.critical(f'nb seq with tax info ({id_processed})is different than nb seq initial {nb_seq}')


if __name__ == '__main__':
    main()
