#!/usr/bin/env python
# coding: utf-8


import os
import csv
import json
from collections import defaultdict
from Bio import Seq
from itertools import product
from Bio.SeqUtils import MeltingTemp as mt
import gzip
import os
from argparse import ArgumentParser, ArgumentDefaultsHelpFormatter, FileType
import sys
import csv
from Bio import SeqIO
import pandas as pd
#import itables.interactive
import melting
import logging

# standard imports
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt


def get_tms(seqs, fct=mt.Tm_Wallace):
    # Tm_GC
    # Tm_Wallace
    return [fct(Seq.Seq(seq)) for seq in seqs]


def get_tms_from_melting(seqs):
    return [melting.temp(seq) for seq in seqs]


def get_degenerate_consensus_seq(seqs):
    """return an ambiguous DNA seq from a list of possible sequences """
    ambig_to_unambig = Seq.IUPAC.IUPACData.ambiguous_dna_values
    unambig_to_ambig = {tuple(sorted(unambigs)): ambig for ambig,
                        unambigs in ambig_to_unambig.items()}

    positions = [defaultdict(int) for _ in range(len(seqs[0]))]
    for s in seqs:
        for l, position in zip(s, positions):
            position[l] += 1
    positions_tuple = [tuple(sorted(''.join(po.keys()))) for po in positions]
    degenerate_seq = ''.join([unambig_to_ambig[t] for t in positions_tuple])
    return degenerate_seq


def extend_ambiguous_dna(deg_seq):
    """return list of all possible sequences given an ambiguous DNA input"""
    d = Seq.IUPAC.IUPACData.ambiguous_dna_values
    r = []
    deg_seq = deg_seq.upper()
    for i in product(*[d[j] for j in deg_seq]):
        r.append("".join(i))
    return r


def get_tm_range(seqs, fct=get_tms):
    # https://biopython.org/DIST/docs/api/Bio.SeqUtils.MeltingTemp-module.html
    tm_list = fct(seqs)
    return round(min(tm_list), 2), round(max(tm_list), 2)


def get_species_prct_from_ecotax(file):
    """Load a tsv file into a list of dictionnary"""
    main_dict = {}
    with open(file) as fl:

        for l in fl:
            l_splitted = [e.strip() for e in l.split('\t')]
            if l_splitted[0] == 'species':
                return float(l_splitted[-1])


def get_species_stat_from_ecotax(file):
    """
    ecotaxstat
    ecotaxspecificity

    """
    main_dict = {}
    with open(file) as fl:

        for l in fl:
            l_splitted = [e.strip() for e in l.split('\t')]
            if l_splitted[0] == 'species':
                return {'count': int(l_splitted[1]), 'total': int(l_splitted[2]), 'prct': float(l_splitted[3])}


def get_best_couples_by_taxcov(region_dir):
    couple_sp_prct = {}

    primer_couples = os.listdir(region_dir)
    primers_couple_dirs = [os.path.join(region_dir, couple) for couple in primer_couples]

    for couple in primer_couples:
        couple_dir = os.path.join(region_dir, couple)
        ecotax_files = [f for f in os.listdir(couple_dir) if f.startswith(
            'refseqRepRef') and f.endswith('_l500_L8000_e0.ecotaxstat')]
        if len(ecotax_files) > 1:
            print('MORE THAN ONE ECOTAX')
            print(ecotax_files)

        ecotax_file_name = ecotax_files[0]
        ecotax_file = os.path.join(region_dir, couple, ecotax_file_name)
        species_prct = get_species_prct_from_ecotax(ecotax_file)

        couple_sp_prct[couple_dir] = species_prct
        sorted_couples = sorted(couple_sp_prct.items(), key=lambda kv: kv[1], reverse=True)
    return sorted_couples


def tsv_to_list_of_dict(file):
    """Load a tsv file into a list of dictionnary"""
    with open(file) as csvfile:
        reader = csv.DictReader(filter(lambda row: row[0] != '#', csvfile), delimiter='\t')
        return list(reader)


def create_sarray_command(couple_dir):

    info_file = [f for f in os.listdir(couple_dir) if f.endswith('.json')][0]
    dict_info = decoder(os.path.join(couple_dir, info_file))
    # print(dict_info)
    primer1 = dict_info["forward_primer"]["seq"]
    p1_name = dict_info["forward_primer"]["primer_id"]
    primer2 = dict_info["reverse_primer"]["seq"]
    p2_name = dict_info["reverse_primer"]["primer_id"]
    pre_command = 'module load system/Anaconda3-5.2.0;source /usr/local/bioinfo/src/Anaconda/Anaconda3-5.2.0/etc/profile.d/conda.sh;conda activate check_primer_dimers;'

    command = 'python check_primer_dimers.py' + f' -p {primer1} -P {primer2}' + f' --primer_name1 {p1_name} --primer_name2 {p2_name}' + f' -o {couple_dir} -e primer_prospector/dna_DM.par'

    print(pre_command + command)


def decoder(filename):
    """Load a json file and return the corresponding structure."""
    with open(filename, 'r', encoding='utf8') as file:
        return json.load(file)


def list_of_dict_to_tsv(list_of_dict, output_file, mode='w', writeheader=True):
    """Encode a list of dictionnary into a tsv file

    The mode of writing can be specified.
    By default the mode is set to "w"

    """
    if len(list_of_dict) == 0:
        raise ValueError('The given list is empty: we cant write it in a file')

    with open(output_file, mode, newline='') as csvfile:
        writer = csv.DictWriter(csvfile, fieldnames=list(list_of_dict[0].keys()), delimiter='\t')
        if mode == 'w' and writeheader == True:
            writer.writeheader()
        for dict_info in list_of_dict:
            writer.writerow(dict_info)


def parse_ecopcr_result(ecopcrfile):
    fprimers_to_amplicons = defaultdict(list)
    rprimers_to_amplicons = defaultdict(list)

    fprimers_used = []
    rprimers_used = []
    i = 0
    with open(ecopcrfile, encoding="latin-1") as fl:
        lengths = []
        species_list = []
        genomes = []
        for l in fl:
            if l.startswith('#'):
                continue
            i += 1

            splitted_l = [e.strip() for e in l.split(' | ')]
            f_primer_used = splitted_l[13]
            r_primer_used = splitted_l[16]
            length = int(splitted_l[19])
            species = splitted_l[5]
            seq_taxid = splitted_l[1]
            genome_id = splitted_l[0].split('|')[0]

            amplicon_id = f'amplicon_{i}'

            fprimers_to_amplicons[f_primer_used].append(amplicon_id)
            fprimers_used.append(f_primer_used)

            rprimers_to_amplicons[r_primer_used].append(amplicon_id)
            rprimers_used.append(r_primer_used)
            lengths.append(length)
            species_list.append(species)
            genomes.append(genome_id)

    ecopcr_dict = {'fprimers_used': fprimers_used,
                   'rprimers_used': rprimers_used,
                   'fprimers_to_amplicons': fprimers_to_amplicons,
                   "rprimers_to_amplicons": rprimers_to_amplicons,
                   'lengths': lengths,
                   'species': species_list,
                   'genomes': genomes
                   }
    return ecopcr_dict


def get_mismatch_stat_from_ecopcrfile(ecopcr_file, mismatch_max=2):
    genomes_by_mismatch = {e: defaultdict(int) for e in range(mismatch_max + 1)}
    species_by_mismatch = {e: set() for e in range(mismatch_max + 1)}
    mismatch_amplicon_count = defaultdict(int)
    mismatches = []
    with open(ecopcr_file, encoding="latin-1") as fl:
        for l in fl:
            if l.startswith('#'):
                # print(l)
                continue
            splitted_l = [e.strip() for e in l.split(' | ')]
            # a = [print(i, e) for i, e in enumerate(splitted_l)]
            # print(splitted_l)
            species = splitted_l[5]
            length = int(splitted_l[19])
            genome_id = splitted_l[0].split('|')[0]
            mismatch_p1 = int(splitted_l[14])
            mismatch_p2 = int(splitted_l[17])
            mismatch = max(mismatch_p1, mismatch_p2)
            if mismatch > mismatch_max:
                continue

            mismatch_amplicon_count[mismatch] += 1
            for e in range(mismatch, mismatch_max + 1):
                genomes_by_mismatch[e][genome_id] += 1

            species_by_mismatch[mismatch].add(species)

    species_set = set()
    # print(genomes_by_mismatch)
    stat_by_mismatch = {}
    for mismatch in range(mismatch_max+1):

        genomes = genomes_by_mismatch[mismatch]

        species_set |= species_by_mismatch[mismatch]

        nb_single_copy_genome = len([g for g, count in genomes.items() if count == 1])
        nb_sp_covered = len(species)
        if mismatch_amplicon_count[mismatch]:
            stat_by_mismatch[mismatch] = {'nb_single_copy_genomes': nb_single_copy_genome, 'nb_genome_covered': len(
                genomes), 'sp_covered': list(species_set), "amplicon_count": mismatch_amplicon_count[mismatch]}
        else:
            stat_by_mismatch[mismatch] = None

        # print('mismatch', mismatch, mismatch_amplicon_count[mismatch])
        # print(f'  --> genome single copy {nb_single_copy_genome} {round((nb_single_copy_genome/len(genomes))*100,2)}%')
        # print(f'  --> species covered {nb_sp_covered} {round((nb_sp_covered/total_sp)*100,2)}%')
        # #print(mismatches.count(mismatch))
    return stat_by_mismatch


def get_binding_results(binding_result_fl):
    binding_results = tsv_to_list_of_dict(binding_result_fl)
    fprimer_binds = []
    rprimer_binds = []

    for bind in binding_results:
        primer1_bind, primer2_bind = bind['sequence_tested'].split('----------')
        if bind['primer1_version'][0] == 'f':
            fprimer_binds.append(primer1_bind)
        else:
            rprimer_binds.append(primer1_bind)

        if bind['primer2_version'][0] == 'f':
            fprimer_binds.append(primer2_bind)
        else:
            rprimer_binds.append(primer2_bind)

    return fprimer_binds, rprimer_binds


def get_info_couple_primers(couple_dir):
    # print(os.listdir(couple_dir))
    info_file = [f for f in os.listdir(couple_dir) if f.endswith('.json') and f.startswith('f')][0]
    json_info = decoder(os.path.join(couple_dir, info_file))
    fprimer = json_info["forward_primer"]["seq"]
    fp_name = json_info["forward_primer"]["primer_id"]
    rprimer = json_info["reverse_primer"]["seq"]
    rp_name = json_info["reverse_primer"]["primer_id"]
    # print(os.listdir(couple_dir))
    # print(f'refseqRepRef_{fp_name}-{rp_name}')
    # for f in os.listdir(couple_dir):
    #     print(f)
    #     if f.startswith(f'refseqRepRef_{fp_name}-{rp_name}') and f.endswith(f'e0.ecopcr'):
    #         print('MATCH PATTERN')
    # print([f for f in os.listdir(couple_dir) if f.startswith(f'refseqRepRef_{fp_name}-{rp_name}') and f.endswith(f'e0.ecopcr')])
    ecotaxstat_name = f'refseqRepRef_{fp_name}-{rp_name}_l500_L8000_e0.ecotaxstat'
    ecotaxstat_name = [f for f in os.listdir(couple_dir) if f.startswith(f'refseqRepRef_{fp_name}-{rp_name}') and f.endswith(f'e0.ecotaxstat')][0]
    ecotaxstat_file = os.path.join(couple_dir, ecotaxstat_name)
    json_info['ecotaxstat_file'] = ecotaxstat_file

    try:
        ecotaxstat_name = [f for f in os.listdir(couple_dir) if f.startswith(f'AllRefseqRegion_{fp_name}-{rp_name}') and f.endswith(f'e0.ecotaxstat')][0]
        ecotaxstat_file = os.path.join(couple_dir, ecotaxstat_name)
        json_info['ecotaxstat_file_all_refseq'] = ecotaxstat_file
    except IndexError:
        json_info['ecotaxstat_file_all_refseq'] = None

    for diff in [1, 2, 5, 10, 20]:
        try:
            ecotaxspe_name = [f for f in os.listdir(couple_dir) if f.startswith(f'refseqRepRef_') and f.endswith(f'e0_diff{diff}.ecotaxspecificity')][0]
            ecotaxspe_file = os.path.join(couple_dir, ecotaxspe_name)
            json_info[f'ecotaxspe_file_diff{diff}'] = ecotaxspe_file
        except IndexError:
            logging.info('NO refseqRepRef_*e0_diff{diff}.ecotaxspecificity file in.. ' + couple_dir)
            json_info[f'ecotaxspe_file_diff{diff}'] = None

    ecopcr_name = [f for f in os.listdir(couple_dir) if f.startswith(f'refseqRepRef_{fp_name}-{rp_name}') and f.endswith(f'e0.ecopcr')][0]

    ecopcr_file = os.path.join(couple_dir, ecopcr_name)
    json_info['ecopcr_file'] = ecopcr_file
    files_in_dir = os.listdir(couple_dir)

    ecopcr_file_with_mismatched = ecopcr_file
    for e in range(5):
        try:
            ecopcr_file_with_mismatched = [os.path.join(couple_dir, f) for f in os.listdir(couple_dir) if f.startswith(f'refseqRepRef_{fp_name}-{rp_name}') and f.endswith(f'e{e}.ecopcr')][0]
        except IndexError:
            pass
    json_info['ecopcr_file_with_mismatched'] = ecopcr_file_with_mismatched
    return json_info
