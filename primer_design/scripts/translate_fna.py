import gzip
import os
import logging
from argparse import ArgumentParser, ArgumentDefaultsHelpFormatter
import sys
import csv
from Bio import SeqIO
import re


def parse_arguments():
    parser = ArgumentParser(description="...",
                            formatter_class=ArgumentDefaultsHelpFormatter)

    parser.add_argument("fna_sequence", type=str,
                        help="")
    parser.add_argument("-o", "--output", type=str, default=None,
                        help="")
    args = parser.parse_args()
    return args


def main():
    args = parse_arguments()
    fna_seq_fl = args.fna_sequence
    if args.output:
        translated_seq_fl = args.output
    else:
        translated_seq_fl = os.path.splitext(fna_seq_fl)[0] + '.faa'

    with open(fna_seq_fl) as fl, open(translated_seq_fl, 'w') as out:

        for seq in SeqIO.parse(fl, "fasta"):
            seq_prot = seq.seq.translate(table=11)
            print(type(seq_prot))
            print(seq_prot)
            print(type(seq.seq))
            seq.seq = seq_prot
            # # print(assembly_name)
            #
            # if assembly_name in assembly_to_keep:
            SeqIO.write(seq, out, "fasta")


if __name__ == '__main__':
    main()
