
import os


def main():

    path = '/home/jmainguy/marker_identification/rrn_database/filter_fasta_files/'
    files = ['operons_100_common_species.fa',
             'COG0200-COG0201_common_species.fa', "COG0052-COG0264_common_species.fa"]
    outdir = 'ecoPrimers/sequences_with_dump_taxid'
    for file in files:
        fl_path = os.path.join(path, file)
        fl_out = os.path.join(outdir, file)
        with open(fl_path) as fl, open(fl_out, 'w') as out:
            for l in fl:
                if l.startswith('>'):
                    split_header = l.rstrip().split(' ')
                    try:
                        header_with_taxid = split_header[0] + \
                            " taxid=2; " + ' '.join(split_header[1:]) + '\n'
                    except:
                        header_with_taxid = split_header[0] + " taxid=2;\n"
                    l = header_with_taxid
                    print(l)
                out.write(l)


if __name__ == '__main__':
    main()
