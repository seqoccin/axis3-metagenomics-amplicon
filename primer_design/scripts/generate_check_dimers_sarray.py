#!/usr/bin/env python
# coding: utf-8

import os
import csv 
import json


def get_species_prct_from_ecotax(file):
    """Load a tsv file into a list of dictionnary"""
    main_dict = {}
    with open(file) as fl:
        
        for l in fl: 
            l_splitted = [e.strip() for e in l.split('\t')]
            if l_splitted[0] == 'species':
                return float(l_splitted[-1]) 
                
def tsv_to_dict_of_dicts(file, key_field):
    """
    Take a tsv with header and parse into dict of dict
    Uses
    * the specified field as key
    * the line turned into a dict as value .
    """
    dict_of_list_of_dict = defaultdict(list)
    with open(file) as csvfile:
        reader = csv.DictReader(csvfile, delimiter='\t')
        for l in reader:
            dict_of_list_of_dict[l[key_field]].append(l)
        return dict(dict_of_list_of_dict)


def get_best_couple(region, main_result_dir):
    couple_sp_prct = {}
    region_dir = os.path.join(main_result_dir, region) 
    primer_couples = os.listdir(region_dir) 
    primers_couple_dirs = [os.path.join(region_dir, couple)  for couple in primer_couples]
   
    
    for couple in primer_couples:
        couple_dir = os.path.join(region_dir, couple)
        ecotax_files = [f for f in os.listdir(couple_dir) if f.startswith('refseqRepRef') and f.endswith('_l500_L8000_e0.ecotaxstat')]
        if len(ecotax_files) > 1: 
            print('MORE THAN ONE ECOTAX')
            print(ecotax_files) 
        
        
        ecotax_file_name = ecotax_files[0] 
        ecotax_file = os.path.join(region_dir, couple, ecotax_file_name)
        species_prct = get_species_prct_from_ecotax(ecotax_file)
        
        couple_sp_prct[couple_dir] = species_prct
        sorted_couples = sorted(couple_sp_prct.items(), key=lambda kv: kv[1], reverse=True)
    return sorted_couples
        
        #refseqRepRef_fCOG0200_1-rCOG0201_1_l500_L8000_e0.ecotaxstat 
def create_sarray_command(couple_dir):

    info_file = [f for f in os.listdir(couple_dir) if f.endswith('.json')][0]
    dict_info = decoder(os.path.join(couple_dir, info_file))
    #print(dict_info)
    primer1 = dict_info["forward_primer"]["seq"]
    p1_name = dict_info["forward_primer"]["primer_id"]
    primer2 = dict_info["reverse_primer"]["seq"]
    p2_name = dict_info["reverse_primer"]["primer_id"]
    pre_command = 'module load system/Anaconda3-5.2.0;source /usr/local/bioinfo/src/Anaconda/Anaconda3-5.2.0/etc/profile.d/conda.sh;conda activate check_primer_dimers;'

    command = 'python check_primer_dimers.py' +     f' -p {primer1} -P {primer2}' +     f' --primer_name1 {p1_name} --primer_name2 {p2_name}' +     f' -o {couple_dir} -e primer_prospector/dna_DM.par'

    print(pre_command + command)    

def decoder(filename):
    """Load a json file and return the corresponding structure."""
    with open(filename, 'r', encoding='utf8') as file:
        return json.load(file)

    
main_result_dir = 'degeprime_results/bash_pipeline/ecoPCR/'

#regions_dirs = [os.path.join(main_result_dir, region) for region in os.listdir(main_result_dir)]

regions = 'COG0097-COG0200 COG0200-COG0201 COG0052-COG0264'

for region in regions.split(' '):
    #print(region)
    sorted_couples = get_best_couple(region, main_result_dir)
    selected_couples = sorted_couples[:15]
    for couple_dir, prct in selected_couples: 
        #print(couple_dir)
        create_sarray_command(couple_dir)



