#!/usr/bin/env python3

"""
Description

:Example:
python template.py -v
"""

# Metadata
__author__ = 'Mainguy Jean - Plateforme bioinformatique Toulouse'
__copyright__ = 'Copyright (C) 2020 INRAE'
__license__ = 'GNU General Public License'
__version__ = '0.1'
__email__ = 'support.bioinfo.genotoul@inra.fr'
__status__ = 'dev'


from argparse import ArgumentParser, ArgumentDefaultsHelpFormatter
import logging
from collections import defaultdict
import csv


def list_of_dict_to_tsv(list_of_dict, output_file, mode='w', writeheader=True):
    """Encode a list of dictionnary into a tsv file.

    The mode of writing can be specified.
    By default the mode is set to "w"

    """
    if len(list_of_dict) == 0:
        raise ValueError('The given list is empty: we cant write it in a file')

    with open(output_file, mode, newline='') as csvfile:
        writer = csv.DictWriter(csvfile, fieldnames=list(list_of_dict[0].keys()), delimiter='\t')
        if mode == 'w' and writeheader is True:
            writer.writeheader()
        for dict_info in list_of_dict:
            writer.writerow(dict_info)


def parse_ecopcr_file(ecopcrfile, assembly_list, mismatch_max=2):
    # fprimers_to_amplicons = defaultdict(list)
    # rprimers_to_amplicons = defaultdict(list)
    result_per_mismatch = {mismatch: {'lengths': [], 'assemblies': []}
                           for mismatch in range(mismatch_max+1)}
    # i = 0
    with open(ecopcrfile, encoding="latin-1") as fl:
        for l in fl:
            if l.startswith('#'):
                continue

            splitted_l = [e.strip() for e in l.split(' | ')]
            assembly = splitted_l[0].split('|')[0]

            # keep only line of the genome of interest
            if assembly not in assembly_list:
                logging.info(
                    'amplicon is from an assembly that is not part of the given assembly selection. This ammplicon is ignored')
                continue
            # i += 1

            f_mismatch = splitted_l[14]
            r_mismatch = splitted_l[17]
            mismatch = max(int(f_mismatch), int(r_mismatch))
            if mismatch > mismatch_max:
                warning = f'An amplicon has {mismatch} mismatches while the maximum number of mismatches take into acount is {mismatch_max}. '
                warning += 'This amplicon is ignored.'
                logging.info(warning)
                continue

            length = int(splitted_l[19])

            result_per_mismatch[mismatch]['lengths'].append(length)
            result_per_mismatch[mismatch]['assemblies'].append(assembly)

    return result_per_mismatch


def parse_arguments():
    """Parse script arguments."""
    parser = ArgumentParser(description="...",
                            formatter_class=ArgumentDefaultsHelpFormatter)
    parser.add_argument("-v", "--verbose", help="increase output verbosity",
                        action="store_true")
    parser.add_argument("-f", "--ecopcr_files", nargs='+', help="",
                        required=True)
    parser.add_argument("-a", "--assembly_selection", help="",
                        required=True)
    parser.add_argument("-e", "--mismatch_max", help="max mismatch to retrieve and analysed in ecopcr result file",
                        default=2, type=int, required=False)
    parser.add_argument("-o", "--output", help="output file",
                        default=0, type=str)
    args = parser.parse_args()
    return args


def main():

    args = parse_arguments()

    if args.verbose:
        logging.basicConfig(format="%(levelname)s: %(message)s", level=logging.DEBUG)
        logging.info('Mode verbose ON')

    else:
        logging.basicConfig(format="%(levelname)s: %(message)s")

    ecopcr_files = args.ecopcr_files
    mismatch_max = args.mismatch_max
    output_file = args.output

    assembly_summary = args.assembly_selection

    lines_info = []
    with open(assembly_summary) as fl:
        reader = csv.DictReader(fl, delimiter='\t')
        assembly_infos = {d['assembly_accession']: d for d in reader}

    # Example of taxonomy field:
    # Campylobacter coli [tax_name];Campylobacter coli [species];Campylobacter [genus];Campylobacteraceae [family];Campylobacterales [order];Epsilonproteobacteria [class];Proteobacteria [phylum];Bacteria other

    assembly_to_species = {a: info['taxonomy'].split(';')[1] for a, info in assembly_infos.items()}
    assembly_to_genus = {a: info['taxonomy'].split(';')[2] for a, info in assembly_infos.items()}
    assembly_to_family = {a: info['taxonomy'].split(';')[3] for a, info in assembly_infos.items()}

    total_sp = len({info['species_taxid'] for info in assembly_infos.values()})
    logging.info(f'{total_sp} species found in assembly summary {assembly_summary}')

    for ecopcr_file in ecopcr_files:
        ecopcr_dict = parse_ecopcr_file(ecopcr_file, assembly_infos, mismatch_max)
        assemblies = []
        for m in range(mismatch_max + 1):

            assemblies += ecopcr_dict[m]['assemblies']
            if len(assemblies) == 0:
                logging.warning(f'Nothing retrieve with {m} mismatch..')
            # print(assembly_infos[assemblies[m]])

            species_count = len({assembly_to_species[a] for a in assemblies})
            genus_count = len({assembly_to_genus[a] for a in assemblies})
            family_count = len({assembly_to_family[a] for a in assemblies})
            # print(species_count)
            line_info = {'ecopcr_file': ecopcr_file,
                         'mismatch': m,
                         'sp_covered': species_count,
                         'total_sp': total_sp,
                         'assembly_summary': assembly_summary, }
            lines_info.append(line_info)

    list_of_dict_to_tsv(lines_info, output_file)


if __name__ == '__main__':
    main()
