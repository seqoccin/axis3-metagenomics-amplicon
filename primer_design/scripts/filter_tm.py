from Bio import Seq
from itertools import product
from Bio.SeqUtils import MeltingTemp as mt
import gzip
import os
import logging
from argparse import ArgumentParser, ArgumentDefaultsHelpFormatter, FileType
import sys
import csv
from Bio import SeqIO
import re
import time
import json


def encoder(filename, f):
    """Encode a python structure in a json file."""
    with open(filename, 'w', encoding='utf8') as file:
        json.dump(f, file, indent=2)


def decoder(filename):
    """Load a json file and return the corresponding structure."""
    with open(filename, 'r', encoding='utf8') as file:
        return json.load(file)


def tsv_to_list_of_dict(file):
    """Load a tsv file into a list of dictionnary"""
    with open(file) as csvfile:
        reader = csv.DictReader(csvfile, delimiter='\t')
        return list(reader)


def list_of_dict_to_tsv(list_of_dict, output_file, mode='w', writeheader=True):
    """Encode a list of dictionnary into a tsv file

    The mode of writing can be specified.
    By default the mode is set to "w"

    """
    if len(list_of_dict) == 0:
        raise ValueError('The given list is empty: we cant write it in a file')

    with open(output_file, mode, newline='') as csvfile:
        writer = csv.DictWriter(csvfile, fieldnames=list(list_of_dict[0].keys()), delimiter='\t')
        if mode == 'w' and writeheader == True:
            writer.writeheader()
        for dict_info in list_of_dict:
            writer.writerow(dict_info)


def extend_ambiguous_dna(deg_seq):
    """return list of all possible sequences given an ambiguous DNA input"""
    d = Seq.IUPAC.IUPACData.ambiguous_dna_values
    r = []
    for i in product(*[d[j] for j in deg_seq]):
        r.append("".join(i))
    return r


def get_Tm_intervall_from_seqs(seqs):
    # https://biopython.org/DIST/docs/api/Bio.SeqUtils.MeltingTemp-module.html
    tm_list = []
    for seq in seqs:
        # tm_list.append(mt.Tm_NN(Seq.Seq(seq)))
        # tm_list.append(mt.Tm_GC(Seq.Seq(seq)))
        tm_list.append(mt.Tm_Wallace(Seq.Seq(seq)))
    return min(tm_list), max(tm_list)


def get_primer_Tm_intervall(primer_seq):
    return get_Tm_intervall_from_seqs(extend_ambiguous_dna(primer_seq))


def parse_arguments():
    parser = ArgumentParser(description="...",
                            formatter_class=ArgumentDefaultsHelpFormatter)

    parser.add_argument('primers', type=str, help='')
    parser.add_argument("--min_tm", type=int, default=48,
                        help="")
    parser.add_argument("--nb_primer_max", type=int, default=10,
                        help="Select 10 first primer that validate tm threshold")

    args = parser.parse_args()
    return args


def main():
    args = parse_arguments()
    min_tm = args.min_tm
    primers_fl = args.primers
    nb_primer_max = args.nb_primer_max
    primers = tsv_to_list_of_dict(primers_fl)

    accepeted_primers = []
    rejected_primers = []
    id = 0
    for d in primers:
        d['tm_min'], d['tm_max'] = get_primer_Tm_intervall(d['seq'])
        if d['tm_min'] >= min_tm and id < nb_primer_max:
            id += 1
            d['primer_id'] = f'f{d["target"]}_{id}'
            accepeted_primers.append(d)
        else:
            rejected_primers.append(d)

    if rejected_primers:
        list_of_dict_to_tsv(rejected_primers, primers_fl+'.rejected')
    if accepeted_primers:
        list_of_dict_to_tsv(accepeted_primers, primers_fl)
    else:
        logging.warning(f'NO PRIMER FOR {primers_fl} WITH A TM >= {min_tm}')
    # primers2 = [d for d in primers1 if d['tm_intervall'][0] >= min_tm]
    #
    # for p1, p2 in product(*[primers1, primers2]):
    #     print(p1)
    #     print(p2)
    # print(get_primer_Tm_intervall(primer_seq))


if __name__ == '__main__':
    main()
