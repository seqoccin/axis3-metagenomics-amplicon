# mock composition

Listeria monocytogenes    12%
Pseudomonas aeruginosa    12%
Bacillus subtilis    12%
Escherichia coli    12%
Salmonella enterica    12%
Lactobacillus fermentum    12%
Enterococcus faecalis    12%
Staphylococcus aureus    12%
Saccharomyces cerevisiae    2%
Cryptococcus neoformans    2%


# Manage non bacterial genomes
## Select genomes
Use of the result of region_identification_workflow on fungi to select genomes of Saccharomyces cerevisiae and Cryptococcus neoformans.

Take assembly selection of fungi from region_identification_workflow results: `../../region_identification_workflow/fungi_investigation/result_fungiTEST/assembly_selections/`

build custom assembly seelection file with the bets genome of Saccharomyces cerevisiae and Cryptococcus neoformans
```
head -n1 ../../region_identification_workflow/fungi_investigation/result_fungiTEST/assembly_selections/ref_and_rep_assemblies.tsv | head -n1 > non_bacterial_mock_genomes/assembly_selection.tsv

grep Saccharomyces ../../region_identification_workflow/fungi_investigation/result_fungiTEST/assembly_selections/ref_and_rep_assemblies.tsv | head -n1 >> non_bacterial_mock_genomes/assembly_selection.tsv

grep Cryptococcus ../../region_identification_workflow/fungi_investigation/result_fungiTEST/assembly_selections/ref_and_rep_assemblies.tsv | head -n1 >> non_bacterial_mock_genomes/assembly_selection.tsv
```

# Buil ecopcr db

Build a list of assembly to keep
and then use script to format the sequences and build db:

```bash
python ../../scripts/merge_and_format_assembly.py assembly_of_interest.txt --assemblies_with_taxonomy assembly_selection.tsv --assembly_root_dir ~/metaG/ncbi_data/assemblies/ -o non_bacterial_mock_genomes.fna


module load bioinfo/obitools-v1.2.11
obiconvert --fasta ecopcr_db/non_bacterial_mock_genomes.fna --ecopcrdb-output=non_bacterial_mock_genomes -t ../../tax_dump/ --nuc

```

#Run ecopcr


```bash
python ../../scripts/ecopcr_sarray_selected_primers.py --ecopcr_db_assembly_files ecopcr_db/non_bacterial_mock_genomes.fna --primers_dir ../primer_couples/ -e 2 -v

```

Look at result `grep ^'#' -vc ../primer_couples/*/ecopcr_non_bacterial_mock_genomes/*e2.ecopcr `

# Old line not used anymore
## Download

`ln -s ~/metaG/marker_identification/region_identification_workflow/ncbi_data/2020-06-03_refseq_summary/ . `

## Saccharomyces cerevisiae

`grep Saccharomyces 2020-06-03_refseq_summary/`

Line used: `GCF_000146045.2	PRJNA128			reference genome	559292	4932	Saccharomyces cerevisiae S288C	strain=S288C		latest	Complete Genome	Major	Full	2014/12/17	R64Saccharomyces Genome Database	GCA_000146045.2	different	ftp://ftp.ncbi.nlm.nih.gov/genomes/all/GCF/000/146/045/GCF_000146045.2_R64`

`wget ftp://ftp.ncbi.nlm.nih.gov/genomes/all/GCF/000/146/045/GCF_000146045.2_R64/GCF_000146045.2_R64_genomic.fna.gz`


## Cryptococcus neoformans

`grep Cryptococcus  2020-06-03_refseq_summary/assembly_summary_refseq.txt`

line used: `GCF_000091045.1	PRJNA10698	SAMN03081418		representative genome	214684	5207	Cryptococcus neoformans var. neoformans JEC21	strain=JEC21		latest	Chromosome	Major	Full2005/01/07	ASM9104v1	TIGR	GCA_000091045.1	identical	ftp://ftp.ncbi.nlm.nih.gov/genomes/all/GCF/000/091/045/GCF_000091045.1_ASM9104v1`

`wget ftp://ftp.ncbi.nlm.nih.gov/genomes/all/GCF/000/091/045/GCF_000091045.1_ASM9104v1/GCF_000091045.1_ASM9104v1_genomic.fna.gz`
