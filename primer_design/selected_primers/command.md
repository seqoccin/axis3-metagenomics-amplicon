
We have sequences of the selected regions in a dir : `region_sequences/`

We build ecopcr database. Use the script build_ecopcr_db.sh that generate command to run to build ecoPCR db on each sequence.

```bash
bash build_ecopcr_db.sh
```
example of script to build db :
```
python ../scripts/filter_sequence.py --add_taxid --assemblies_with_taxonomy rep_ref_and_10_assemblies_per_sp.tsv --taxon_name 'Bacteria [superkingdom]' -o ecopcr_db/16S.fna region_sequences/16S.fna
module load bioinfo/obitools-v1.2.11
obiconvert --fasta ecopcr_db/16S.fna --ecopcrdb-output=ecopcr_db/16S -t ../tax_dump/
```


Generate sarray command to run ecoPCR:
The script does not generate command when ecopcr file already exists...

```bash
python ../../scripts/ecopcr_sarray_selected_primers.py --ecopcr_db_assembly_files ecopcr_db/non_bacterial_mock_genomes.fna --primers_dir ../primer_couples/ -e 2 -v --ecopcr_tool_path ../../ecoPCR/ecopcr/src/ecoPCR > ecopcr.sarray

bash ecopcr.sarray
```

## Build ecopcr db for all assembly.
### make assembly list
Startegy split assembly db into chunks
```bash
tail -n +2 rep_ref_and_10_assemblies_per_sp.tsv | cut -f1 > rep_ref_and_10_assemblies_per_sp_acc_list.txt
# split into 40 chunks more or less 1000 seq per chunk..
split -d --number=l/40 rep_ref_and_10_assemblies_per_sp_acc_list.txt rep_ref_and_10_assemblies_per_sp.acc_list_chunk.

```

### Build a db for each chunck.
Genereic command to build a db
```

module load bioinfo/obitools-v1.2.11

obiconvert --fasta ecoprimer_db/ref_rep_genomes_merged.fna --ecopcrdb-output=ecoprimer_db/ref_rep_genomes_merged -t tax_dump/ --nuc

```
command in a loop in assembly_ecocpcr_db directory
```
command=''
for chunk_file in assembly_accession_chunk_dir/*chunk*;
do
    #echo $chunk_file
    file_name=$(basename -- "$chunk_file")
    db_name="${file_name%.*.*}"
    chunk_id="${chunk_file##*.}"
    formated_seq_fl=${db_name}.chunk${chunk_id}.fna
    command="module load system/Anaconda3-5.2.0;source /usr/local/bioinfo/src/Anaconda/Anaconda3-5.2.0/etc/profile.d/conda.sh;conda activate krona;"
    command=$command"python ../../scripts/merge_and_format_assembly.py --assemblies_with_taxonomy ../rep_ref_and_10_assemblies_per_sp.tsv --assembly_root_dir ../../../region_identification_workflow/ncbi_data/assemblies/ -o $formated_seq_fl $chunk_file -v;"

    command=$command'module load bioinfo/obitools-v1.2.11;'
    command=$command"obiconvert --fasta $formated_seq_fl --ecopcrdb-output=${db_name}.chunk${chunk_id} -t ../../tax_dump/ --nuc"
    echo $command
done
```

Then launch ecoPCR with selected primers on each of this chunk
Finally merge ecoPCR result for these chunks..

Script has been updated to generate sarray for asssembly db
```
usage: ecopcr_sarray_selected_primers.py [-h] [--primers_dir PRIMERS_DIR]
                                         [--ecopcr_region_db_dir ECOPCR_REGION_DB_DIR]
                                         [--ecopcr_db_assembly_files ECOPCR_DB_ASSEMBLY_FILES [ECOPCR_DB_ASSEMBLY_FILES ...]]
                                         [-e MAX_MISMATCHES]
                                         [--ecopcr_tool_path ECOPCR_TOOL_PATH]
                                         [-v]

...

optional arguments:
  -h, --help            show this help message and exit
  --primers_dir PRIMERS_DIR
                        Main directory where sub primer directories are.
                        (default: ./)
  --ecopcr_region_db_dir ECOPCR_REGION_DB_DIR
                        Directory where ecopcr database are. (default: None)
  --ecopcr_db_assembly_files ECOPCR_DB_ASSEMBLY_FILES [ECOPCR_DB_ASSEMBLY_FILES ...]
                        Files of ecopcr assembly database. (default: None)
  -e MAX_MISMATCHES, --max_mismatches MAX_MISMATCHES
                        Maximum number of errors (mismatches) allowed per
                        primer in ecoPCR simulation (default: 0)
  --ecopcr_tool_path ECOPCR_TOOL_PATH
                        Path to the executable of ecoPCR. Default assumes
                        ecoPCR is in PATH or current dir. (default: ecoPCR)
  -v, --verbose         increase output verbosity (default: False)

```
Generate ecopcr command on region and on assembly db:
```
python ../scripts/ecopcr_sarray_selected_primers.py --primers_dir primer_couples/ --ecopcr_region_db_dir region_ecopcr_db/ -e 2 --ecopcr_tool_path ../ecoPCR/ecopcr/src/ecoPCR --ecopcr_db_assembly_files assembly_ecocpcr_db/*fna -v > ecopcr.sarray


```


Compare region and assembly results : use script `compare_ecopcr_region_and_assembly_results.py` to generate a tsv.

```bash

python ../scripts/compare_ecopcr_region_and_assembly_results.py  --primer_dirs primer_couples/* --ecopcr_db_assembly_name rep_ref_and_10_assemblies_per_sp -a assembly_selections/rep_ref_and_10_assemblies_per_sp_with_16S.tsv -e 2 --ecopcr_db_region_dir region_ecopcr_db/

```

POssibility to write details (can be a bit big.. ) of the lengths and assemblies in order to plot it...
relevant for mock analysis:


From ecopcr TP result to fna
```
python ../../region_identification_workflow/tax_resolution_test/from_ecopcr_to_fasta.py -o expected_amplicons_sequences/rep_ref_and_10_assemblies_per_sp_with_16S/ expected_amplicons/rep_ref_and_10_assemblies_per_sp_with_16S/* -v
```


# Analyse mock performance

Create a selection of assembly with genomes of the mock !
Mock composition:
```
cat ../mock_bacterial_composition.tsv
Listeria monocytogenes	12%
Pseudomonas aeruginosa	12%
Bacillus subtilis	12%
Escherichia coli	12%
Salmonella enterica	12%
Lactobacillus fermentum	12%
Enterococcus faecalis	12%
Staphylococcus aureus	12%
```

Creation of the file manually by greping the name of the species in `assembly_selections/rep_ref_and_10_assemblies_per_sp_with_16S.tsv`
Check that first assembly is a representative or reference one..

```
head -n1 assembly_selections/rep_ref_and_10_assemblies_per_sp_with_16S.tsv > assembly_selections/mock_one_assembly_per_sp.tsv

grep 'Listeria monocytogenes' assembly_selections/rep_ref_and_10_assemblies_per_sp_with_16S.tsv | head -n1 >> assembly_selections/mock_one_assembly_per_sp.tsv
grep 'Pseudomonas aeruginosa' assembly_selections/rep_ref_and_10_assemblies_per_sp_with_16S.tsv | head -n1 >> assembly_selections/mock_one_assembly_per_sp.tsv
grep 'Bacillus subtilis' assembly_selections/rep_ref_and_10_assemblies_per_sp_with_16S.tsv | head -n1 >> assembly_selections/mock_one_assembly_per_sp.tsv
grep 'Escherichia coli' assembly_selections/rep_ref_and_10_assemblies_per_sp_with_16S.tsv | head -n1 >> assembly_selections/mock_one_assembly_per_sp.tsv
grep 'Salmonella enterica' assembly_selections/rep_ref_and_10_assemblies_per_sp_with_16S.tsv | head -n1 >> assembly_selections/mock_one_assembly_per_sp.tsv
grep 'Lactobacillus fermentum' assembly_selections/rep_ref_and_10_assemblies_per_sp_with_16S.tsv | head -n1 >> assembly_selections/mock_one_assembly_per_sp.tsv
grep 'Enterococcus faecalis' assembly_selections/rep_ref_and_10_assemblies_per_sp_with_16S.tsv | head -n1 >> assembly_selections/mock_one_assembly_per_sp.tsv
grep 'Staphylococcus aureus' assembly_selections/rep_ref_and_10_assemblies_per_sp_with_16S.tsv | head -n1 >> assembly_selections/mock_one_assembly_per_sp.tsv
```

make a krona with the selection just for fun...
```
python ../../region_identification_workflow/bin/transformed_assembly_summary.py --assemblies_summary_taxonomy assembly_selections/mock_one_assembly_per_sp.tsv -v > mock_tax_formatted.tsv

python ../../region_identification_workflow/bin/make_krona_xml.py --name mock -v mock_tax_formatted.tsv > mock_tax_formatted.xml
PERL5LIB=""
ktImportXML mock_tax_formatted.xml -o mock_tax_formatted.html

```
Compare ecopcr result by taking into account only mock genomes.
```
python ../scripts/compare_ecopcr_region_and_assembly_results.py  --primer_dirs primer_couples/* --ecopcr_db_assembly_name rep_ref_and_10_assemblies_per_sp -a assembly_selections/mock_one_assembly_per_sp.tsv -e 2 --ecopcr_db_region_dir region_ecopcr_db/

```


## Anlayse of 16S-23S and 16Sv3v4 on top of the other regions

```
assembly_selection=assembly_selections/rep_ref_and_10_assemblies_per_sp.tsv

sequences=region_sequences/16S-23S.fna

output=region_ecopcr_db/16S-23S.fna

python primer_design/scripts/format_sequences.py $sequences --assembly_selection $assembly_selection -o $output  -v

module load bioinfo/obitools-v1.2.11
obiconvert --fasta region_ecopcr_db/16S-23S.fna --ecopcrdb-output=output=region_ecopcr_db/16S-23S -t ../tax_dump/tax_dump_refseq2019-10-23/

# with regular length 500 to 8000
python ../scripts/ecopcr_sarray_selected_primers.py --primers_dir primer_couples/ --ecopcr_region_db_dir region_ecopcr_db/ -e 2 --ecopcr_tool_path ../ecoPCR/ecopcr/src/ecoPCR --ecopcr_db_assembly_files assembly_ecocpcr_db/*fna -v  > ecopcr.sarray


# add to change min max amplicon directly in the script fro v3v4 100 to 600pb
python ../scripts/ecopcr_sarray_selected_primers.py --primers_dir primers_short_amplicon --ecopcr_region_db_dir region_ecopcr_db/ -e 2 --ecopcr_tool_path ../ecoPCR/ecopcr/src/ecoPCR --ecopcr_db_assembly_files assembly_ecocpcr_db/*fna -v  > ecopcr.sarray

sarray  ecopcr.sarray





python ../scripts/compare_ecopcr_region_and_assembly_results.py  --primer_dirs primer_couples/* --ecopcr_db_assembly_name rep_ref_and_10_assemblies_per_sp -a assembly_selections/rep_ref_and_10_assemblies_per_sp.tsv -e 2 --ecopcr_db_region_dir region_ecopcr_db/ --length_tresholds_file max_length_by_region.txt -o amplicon_stat_all/ --write_amplicons



for d in amplicon_stat_all/rep_ref_and_10_assemblies_per_sp/*;
do
    region=`basename $d`
    python ../scripts/from_ecopcr_to_fasta.py $d/expected_amplicon.ecopcr -o amplicon_sequences -v

    mv  amplicon_sequences/expected_amplicon.fna  amplicon_sequences/$region.fna
done

# enlever les sequences degenerate de 16S-23S
# creer selection of 16S-23S with no degenerate seq
# filter les sequences avec cette selections
# Lancer swarm sur ces sequences
# plot results
```


 subsamble sequence
 ```
 python ../../region_identification_workflow/tax_resolution_test/filter_sequences_based_on_assemblies.py --assembly_selection assembly_selections/one_assembly_per_sp_with_16S.tsv -s amplicon_sequences/rep_ref_and_10_assemblies_per_sp/* -o amplicon_sequences/ -v

 ```
