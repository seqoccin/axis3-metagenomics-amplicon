#!/bin/bash
#SBATCH -J buildEcoPCRdb
#SBATCH --mail-type=BEGIN,END,FAIL
#SBATCH --mem=6G
#SBATCH -c 1

# module load system/singularity-3.0.1
assembly_file=rep_ref_and_10_assemblies_per_sp.tsv

for region_file_path in region_sequences/*
do
  region_file=$(basename -- "$region_file_path")
  region="${region_file%.*}"
  command="python ../scripts/filter_sequence.py --add_taxid --assemblies_with_taxonomy $assembly_file --taxon_name 'Bacteria [superkingdom]' -o ecopcr_db/${region_file} $region_file_path;"
 
  command=$command"module load bioinfo/obitools-v1.2.11;"
  command=$command"obiconvert --fasta ecopcr_db/$region_file --ecopcrdb-output=ecopcr_db/$region -t ../tax_dump/"
  echo $command
done



