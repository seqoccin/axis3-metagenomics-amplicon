from Bio import SeqIO
from Bio.SeqRecord import SeqRecord
import os
import csv


def main():
    operon_seq_fl = '/home/jmainguy/marker_identification/rrn_database/filter_fasta_files/operons_100_common_species.fa'
    file_16S = 'operons_100_common_species_16S_extracted.fna'
    file_23S = 'operons_100_common_species_23S_extracted.fna'
    size_23S = 2905  # size in coli https://biocyc.org/gene?orgid=ECOLI&id=RRSA-RRNA
    size_16S = 1542  # size in coli https://biocyc.org/gene?orgid=ECOLI&id=RRLA-RRNA
    with open(operon_seq_fl) as fl, open(file_16S, 'w') as out16, open(file_23S, 'w') as out23:
        for seq in SeqIO.parse(fl, "fasta"):
            gene16s = SeqRecord(seq.seq[:1542],
                                id=seq.id + '[:1542]',
                                name=seq.name,
                                description="16S")

            gene23s = SeqRecord(seq.seq[-2905:],
                                id=seq.id + '[:-2905]',
                                name=seq.name,
                                description="23S")

            SeqIO.write(gene16s, out16, "fasta")
            SeqIO.write(gene23s, out23, "fasta")


if __name__ == '__main__':
    main()
