# Axis3: Metagenomics amplicon

Identification and evaluation of new target regions for metagenomics amplicon long reads


## `region_identification_workflow`

Workflow implemented in nextflow to identify potential new target regions and extract them from refseq genomes.


## `taxonomic_resolution`

Evaluation of the taxonomic resolution of potential target regions.

## `primer_design`

Design and in silico evaluation of new designed primers to amplify teh identified regions


## `turorials`

Step by step guidelines on how to use scripts and method developped in this repository.
