#!/usr/bin/env nextflow


/*
 * Define helpMessage
 */

 def helpMessage() {

   log.info"""

   Usage:

     The typical command for running the pipeline is as follows:

       nextflow run [...]

     Mandatory arguments:
       --tax_level

     Other options:
       --outdir                      The output directory where the results will be saved.
       --help                        Show this message and exit.


     Options:
     [...]
   """.stripIndent()
 }

 // Show help message.
 params.help = false
if (params.help){
  helpMessage()
  exit 0
}



// Initial parameters
params.cog_single_copy_prct = 95
params.cog_prct = 95
params.region_genome_prct_threshold = 95
params.tax_level = 2


// directory where result files are going to be pubblished
params.result_dir = "results"
result_dir = file(params.result_dir)




params.primary_assemblies_selection = 'ref_and_rep_assemblies'
params.extended_assemblies_selection = 'ref_and_rep_assemblies'

params.rrna_genes_to_fetch = '16S 23S 5S 16S-23S'


// Assembly selection possible values:

possible_assembly_selections = ["ref_and_rep_assemblies",
                                "one_assembly_per_sp",
                                "rep_ref_and_10_assemblies_per_sp",
                                "all_assemblies"]


//Only region with a length smaller than this length cutoff will be take into account
params.length_cutoff = 10000

// directories where downloaded files are going to be stored
// and check to not be downloaded multiple time
params.eggNOG_data_dir = "$baseDir/eggNOG_data"
params.ncbi_genomes_dir = "$baseDir/ncbi_data"

// Should the lastest version of assembly files be downloaded..
params.fetch_lastest_assembly_versions = false
fetch_lastest_assembly_versions  = params.fetch_lastest_assembly_versions
println "fetch_lastest_assembly_versions $fetch_lastest_assembly_versions"

// Check if paths are absolute or relatif. If relatif make them absolute..
eggNOG_data_dir = "${params.eggNOG_data_dir}" =~ /^\// ? params.eggNOG_data_dir : "$PWD/${params.eggNOG_data_dir}"
println "eggNOG_data_dir: from ${params.eggNOG_data_dir} to ${eggNOG_data_dir}"

ncbi_genomes_dir =  "${params.ncbi_genomes_dir}" =~ /^\// ? params.ncbi_genomes_dir : "$PWD/${params.ncbi_genomes_dir}"
println "ncbi_genomes_dir: from ${params.ncbi_genomes_dir} to ${ncbi_genomes_dir}"


// RefSeq genomes
refseq_ftp_bact_url = 'ftp://ftp.ncbi.nlm.nih.gov/genomes/refseq/assembly_summary_refseq.txt'
ncbi_taxdump = 'ftp://ftp.ncbi.nlm.nih.gov/pub/taxonomy/new_taxdump/new_taxdump.tar.gz'

params.refseq_path = false
if (params.refseq_path) {
    println 'Assembly summary and new_taxdump are taken locally'
    summary_refseq = file("${params.refseq_path}/assembly_summary_refseq.txt")
    ncbi_taxdump  = file("${params.refseq_path}/new_taxdump.tar.gz")
    refseq_date_dir = file(params.refseq_path).getBaseName()

}
else {
    println 'Assembly summary and new_taxdump are downloaded'
    refseq_date_dir = new java.util.Date().format("yyyy-MM-dd") + '_refseq_summary'
    summary_refseq =  Channel.fromPath(file(refseq_ftp_bact_url)).collectFile(storeDir: "${ncbi_genomes_dir}/$refseq_date_dir/")
    ncbi_taxdump = Channel.fromPath(file(ncbi_taxdump)).collectFile(storeDir: "${ncbi_genomes_dir}/$refseq_date_dir/").collect()
}


// Number of assemblies to process by chunk.
params.assemblies_chunk_size = 200

// Run the workflow on a test sample
// the result dir is changed according
params.test = false

parameter_summary = ['tax_level':params.tax_level,
                    "cog_single_copy_prct":params.cog_single_copy_prct,
                    "cog_prct":params.cog_prct,
                    "region_genome_prct_threshold":params.region_genome_prct_threshold,
                    "ncbi_genomes_dir":params.ncbi_genomes_dir,
                    "refseq_date_dir":refseq_date_dir,
                    "length_cutoff":  params.length_cutoff,
                    "primary_assemblies_selection":params.primary_assemblies_selection,
                    "extended_assemblies_selection": params.extended_assemblies_selection,
                    "rrna_genes_to_fetch":params.rrna_genes_to_fetch,
                    "is_this_a_test_run":params.test,]

parameter_string = ""
parameter_summary.each { key, value ->
                parameter_string += "$key:$value\n"}

/*
CHECK PARAMETERS
*/
prc_parameter = ["cog_single_copy_prct":params.cog_single_copy_prct,
                 "cog_prct":params.cog_prct,
                 "region_genome_prct_threshold":params.region_genome_prct_threshold,]

prc_parameter.each { param, value  ->
  if (!(value instanceof Integer) || (value < 0 ||  value > 100))  {
    exit 1, "parameter $param should be an integer ranging from 0 to 100.. (Value provided $value)"
  }
}

positive_int_parameter = ['tax_level':params.tax_level,
                          "length_cutoff":  params.length_cutoff,]

positive_int_parameter.each { param, value  ->
  if (!(value instanceof Integer) || (value < 0 ))  {
    exit 1, "parameter $param should be a positive integer. (Value provided $value)"
  }
}

assembly_selection_parameter = ["primary_assemblies_selection":params.primary_assemblies_selection,
                            "extended_assemblies_selection": params.extended_assemblies_selection,]

assembly_selection_parameter.each { param, value  ->
  if (!(possible_assembly_selections.contains(value)))  {
    exit 1, "parameter $param should be one of these selections: ${possible_assembly_selections}. (Value provided $value)"
  }
}



println "result directory: ${result_dir}"

if (file(result_dir).isDirectory()) {
  if (file("$result_dir/workflow_parameters.txt").exists()){

    if (file("$result_dir/workflow_parameters.txt").text == parameter_string) {
      println "Parameters are identical to the ones found in the workflow_parameters.txt file of the result dir"
    }
    else {
      error_message = ""
      // error_message += "result directory: ${result_dir}\n"
      error_message += "\nParameters found in the workflow_parameters.txt of the result dir "
      error_message +=  "are not identical to the ones currently provided to this workflow execution\n"
      error_message += "Please change the result directory or the parameters.\n"
      error_message +=  "\nParameters found in the result directory:\n"
      error_message +=  file("$result_dir/workflow_parameters.txt").text
      error_message +=  "\nCurrent parameters:\n"
      error_message +=  "$parameter_string\n"
      exit 1, error_message
    }

  }
  else {
    warning_message = ""
    warning_message += "WARNING\n"
    warning_message +=  ">>The specified result direcory already exists\n"
    warning_message +=  ">>and no \'workflow_parameters.txt\' file has been found\n"
    println warning_message
  }
}
else
{
  if (file(result_dir).exists()) {
    error_message = ""
    // error_message += "result directory: ${result_dir}\n"
    error_message += "\nThe specified result direcory already exists and it is a file\n"
    error_message +="Please change the result direcory..\n"
    exit 1, error_message
  }

}

file(result_dir).mkdir()
file("$result_dir/workflow_parameters.txt").text = parameter_string

println "\nPARAMETERS"
println parameter_string
// Input channels
tax_level_channel = Channel.value(params.tax_level)
single_copy_treshold_channel =  Channel.value(params.cog_single_copy_prct)
cog_prct_channel =  Channel.value(params.cog_prct)
region_prct_threshold_channel =  Channel.value(params.region_genome_prct_threshold)
rrna_genes_to_fetch = Channel.value(params.rrna_genes_to_fetch)

// MULTI TAX LEVELS
params.multi_tax_levels = false
if (params.multi_tax_levels) {
    multi_tax_levels_file = Channel.fromPath(params.multi_tax_levels)
}
else {
    multi_tax_levels_file = Channel.value(params.multi_tax_levels)
}


/**
 The python script executed by this process:
  - it takes the taxon level for which we want to find genomic regions
  - It checks if the corresponding member file is in the check dir
  - If not, it downloads the eggnog members file.
  - The file is parsed and OGs that fit the threshold are identified.
  - An OG is selected if it is found in single copy in more than a specified percentage of the total genomes.
This percentage threshold is defined by the variable params.cog_single_copy_prct.


When the flag --get_COG_gene_name is added, the script downloads for each OGs
the extended members file corresponding. This information is not used by nextflow but is
a valuable information to analyse the results as a gene name is more informative than
a COG ID and use it to get the most used gene name for each COG

An eggnog member file contains general information about orthologs groups at a specific taxon level.
For each group are specified:
  - the name of the OG
  - proteins included in the group
  - genomes that contain proteins belonging to the group
*/
process parse_and_identify_universal_cog {

  publishDir "${result_dir}/cogs_identification/", pattern: "identified_cogs.tsv", mode:'copy'
  publishDir "${result_dir}/cogs_identification/", pattern: "cogs_list.txt", mode:'copy'
  publishDir "${result_dir}/result_plots/", mode:'copy', pattern: "selected_cogs.html"
  publishDir "${eggNOG_data_dir}/", pattern: "${tax_level}_members.tsv*", mode:'copy'
  publishDir "${eggNOG_data_dir}/", pattern: "extended_members/*", mode:'copy'

  input:
    val tax_level from tax_level_channel
    val cog_single_copy_prct from single_copy_treshold_channel
    val cog_prct from cog_prct_channel
    // when multi tax levels is used
    file ncbi_taxdump
    file multi_tax_levels_file

  output:
    file "identified_cogs.tsv" into cogs_selection_info
    file "${tax_level}_members.tsv*" optional true into initial_members_file
    file "extended_members/*"  optional true into extended_members_files
    file "cogs_list.txt" into cogs_list
    file "taxid_list.txt" into taxids_list
    file "selected_cogs.html" into plot_selected_OGs
    file 'cog_identification.stat' into cog_selection_stat
    env taxid_selection

    """
    echo multi_tax_levels $multi_tax_levels_file ${params.multi_tax_levels}
    if [ ${params.multi_tax_levels} == "false" ]; then
        parse_and_identify_universal_cog.py --tax_level $tax_level \
                                                      --cog_single_copy_prct $cog_single_copy_prct \
                                                      --cog_prct $cog_prct \
                                                      --check_dir ${eggNOG_data_dir} \
                                                      --get_COG_gene_name
       taxid_selection=$tax_level
    else
        # Multi tax levels
        mkdir new_taxdump
        tar -xzf $ncbi_taxdump -C new_taxdump

        from_tax_names_to_taxids.py --tax_names_file $multi_tax_levels_file \
                                    --new_taxdump_dir new_taxdump/ -v \
                                    --main_tax_level 2 -o taxid_selection.txt


        parse_and_identify_universal_cog.py --tax_level $tax_level \
                                            --taxid_selection taxid_selection.txt \
                                            --new_taxdump_dir   new_taxdump/ \
                                            --cog_single_copy_prct $cog_single_copy_prct \
                                            --cog_prct $cog_prct \
                                            --check_dir ${eggNOG_data_dir} \
                                            --get_COG_gene_name

        taxid_selection=`cat taxid_selection.txt | tr '\r\n' ' '`
    fi

    # Quick and dirty way to write basic stat..
    nb_species=`wc -l taxid_list.txt | cut -d' ' -f1`
    nb_lines=`wc -l all_cogs.tsv | cut -f1 -d' '`
    nb_cogs=`expr \$nb_lines - 1`
    nb_selected_cogs=`wc -l cogs_list.txt | cut -f1 -d' '`

    echo "eggnog_selected_cogs:\$nb_selected_cogs" > cog_identification.stat
    echo "eggnog_cogs:\$nb_cogs" >> cog_identification.stat
    echo "eggnog_species:\$nb_species" >> cog_identification.stat


    plot_cog_selection.py all_cogs.tsv --cog_single_copy_prct $cog_single_copy_prct --cog_prct $cog_prct \
                                       --min_cov_display 60  --eggnog_species \$nb_species -o selected_cogs.html

    """
}

// collect channels to become singleton channels usable multiple time
cogs_list = cogs_list.collect()
taxids_list = taxids_list.collect()


cogs_selection_info = cogs_selection_info.collect()
/*
The python script:
  - Download the whole proteome of eggnog (with all protein sequences of eggNOG file) to be used in calibration step
  - Download hmm profile of the selected OGs.
  - Create a true positive sequence file to help fetchMGs to identify true positive during the
calibration step
*/
process get_eggnog_files {
  publishDir "${eggNOG_data_dir}/", pattern:  "faa_sequences/*.faa*", mode:'copy'
  publishDir "${eggNOG_data_dir}/", pattern: "hmm_profiles/*.hmm*",  mode:'copy'
  // publishDir "${result_dir}/", pattern:  "true_positive_sequences_*"

  input:
    file cogs_list

  output:
    file "profile_hmm_links/" into hmm_lib
    file "true_positive_sequences_*" into true_positive_seq
    file "faa_sequences/*.faa*" optional true into COGs_faa
    file "hmm_profiles/*.hmm*" optional true into COGs_hmm
    file "e5.proteomes.faa" into proteome_file


    """
    get_eggnog_files.py  ${cogs_list}  --check_dir  "${eggNOG_data_dir}" -v

    """
}

// the proteome file is splitted in order to parallelize its filtering
proteome_file_splited = proteome_file.splitFasta( by: 500000 , file: true)

// if test parameter is true then only the first chunck of the splitted channel is processed
// and this chunk is splitted in smaller chunck
if (params.test) {
  proteome_file_splited = proteome_file_splited.first().splitFasta( by: 50000 , file: true)//.first().splitFasta( by: 5000 , file: true)
}


/*
The proteome file is filtered to keep only sequences that are coming from a taxid
that belong to the tax level.
The taxids are identified during the first process when the member file of the tax level
is parsed.
*/
process filter_proteome_file {
  cache 'deep'
  publishDir "${result_dir}/cogs_identification/filtered_proteome/"

  input:
    file proteome_file_splited
    file taxids_list

  output:
    file "filtered_*" into filtered_proteome_files

    """
    filter_proteome_file.py  ${proteome_file_splited} ${taxids_list}

    """
}


params.fetchMGs_cpus = 2

// Create singleton channel to be used in the process multiple time
filtered_proteome_files = filtered_proteome_files.collect()
hmm_lib = hmm_lib.collect()

// Split COGs list by one to paralelize the processing of each COG
cog_list_splitted = cogs_list.splitText (by:1)


/*
FetchMGs does a step of calibration to use optimal cutoffs for the extraction

Calibration steps for each COGs :
  use the hmm profile from eggnog
  extract gene with uncalibrated cutoffs (low cutoff) on the whole proteome of eggnog at this taxonomic level
  Use true positive sequence file to identify False positive and False negative
  Search cutoff giving the biggest F-score value

The process:
  - create the uncalibrated BitScoreCutoffs file
  - Check if the calibrated cutoff exist already in ${result_dir}/fetchMGs_cuttoff
  - If the file does not exits: fetchMGs calibration is run

At the end the calibrated cutoff files are pubblished in "${result_dir}/fetchMGs_cuttoff" by nextflow
*/

process fetchMGs_calibration {
  cpus params.fetchMGs_cpus
  cache 'deep'

  publishDir "${result_dir}/cogs_identification/fetchMGs_cuttoff", pattern:  "*MG_BitScoreCutoffs.txt"

  input:
    file "filtered_proteome_chunks/*" from filtered_proteome_files
    file true_positive_seq
    file cog_list_splitted
    file hmm_lib

  output:
    file "*MG_BitScoreCutoffs.txt" into cutoff_files
    // file "calibration/*" optional true into calibration_folder

    """
    PERL5LIB="" #  needed to let krona works in the conda env https://github.com/bioconda/bioconda-recipes/issues/4390
    # merged filtered proteome file
    for f in filtered_proteome_chunks/*; do
      cat \$f >> filtered_proteome_file.faa
    done

    cog_names=''

    # creation of fetchMGs uncalibrated BitScoreCutoffs file
    echo '#UNCALIBRATED CUTOFFS FILE' > BitScoreCutoffs.uncalibrated.txt


    while read c;
    do
       echo -e \$c'\t0\t0\t0' >> BitScoreCutoffs.uncalibrated.txt;
       cog_names=\${c}_\$cog_names
    done <  $cog_list_splitted

    time perl $baseDir/fetchMGs/fetchMGs.pl -m calibration  filtered_proteome_file.faa ${true_positive_seq} -p -l $hmm_lib -o calibration -b BitScoreCutoffs.uncalibrated.txt -x '' -t ${params.fetchMGs_cpus} -i $cog_list_splitted

    cutoff_results=\${cog_names}MG_BitScoreCutoffs.txt
    mv calibration/MG_BitScoreCutoffs.txt \$cutoff_results


    """
}


/*
Select assembly genome from the assembly_summary_refseq file.

4 different selection are made.

We want to keep only assemblies:
  -that are included in the taxonomic level. (if we study bacteria we keep only bacterial assemblies)
When there are more than one assemblies by taxid we keep reference genome upon representative and in case of equality in the category
we select assembly with the highest level of completeness.

This process take as inputs:
  - the tax_level
  - the ncbi taxdump file
  - the assembly_summary_refseq file.
These last two files are downloaded by their channel as nextflow is able
to handle remote resources as local file system objects (https://www.nextflow.io/docs/latest/script.html#http-ftp-files).


The ncbi taxdump file come as a tar.gz: new_taxdump.tar.gz ftp://ftp.ncbi.nlm.nih.gov/pub/taxonomy/new_taxdump/taxdump_readme.txt
This tar.gz contains the file 'taxidlineage.dmp':
  Taxonomy id lineage file fields:
    tax_id  -- node id
    lineage -- sequence of node ids separated by space denoting nodes' ancestors starting from the most distant one and ending with the immediate one
The assembly_summary_refseq file contains information (taxid, level of completeness, ftp_path...) on each assembly currently available in refSeq.

The process first extract from taxidlineage.dmp all the taxids that have the taxonomic level in their taxonomy and
In order to keep only assembly that are included in the taxomic level, the process filetered the assembly_summary_refseq file
by keeping assemblies with a taxid contained in the list of taxids extracted from taxidlineage.dmp

*/

process assembly_selection {
    cache 'deep'
    publishDir "${result_dir}/assembly_selections/", pattern: "*.tsv", mode:'copy'

    input:
      val taxid_selection from taxid_selection
      file summary_refseq
      file ncbi_taxdump

    output:
      file "all_assemblies.tsv" into all_assemblies
      file "ref_and_rep_assemblies.tsv" into ref_and_rep_assemblies
      file "one_assembly_per_sp.tsv" into one_assembly_per_sp
      file "rep_ref_and_10_assemblies_per_sp.tsv" into rep_ref_and_10_assemblies_per_sp
      file "assembly_selection_stat.tsv" into assembly_selection_stat
      env taxon_names into taxon_name

      """
    # Format assemblies summary with only interesting columns and add taxonomy information
    format_assemblies_summary.py $summary_refseq -t $taxid_selection\
                                  -o all_assemblies.tsv \
                                  --ncbi_taxdump $ncbi_taxdump -v
    # extract taxon name of the tax level.
    # rankedlineage.dmp  has been extracted by format_assemblies_summary.py

    taxon_names=''
    for taxid in $taxid_selection; do
        taxon_name=`grep ^"\$taxid"\$'\t' rankedlineage.dmp | cut -d\$'\t' -f3`
        taxon_names="\$taxon_name \$taxon_names "
    done

    echo \$taxon_names

    # Creation of assemblies selection
    # file created ref_and_rep_assemblies.tsv one_assembly_per_sp.tsv rep_ref_and_10_assemblies_per_sp.tsv
    assembly_selection.py all_assemblies.tsv --assembly_selections ref_and_rep_assemblies one_assembly_per_sp rep_ref_and_10_assemblies_per_sp -n 10 -v

    # Clean directory
    # rm all_assemblies.tsv rankedlineage.dmp taxids {tax_level}.txt taxidlineage.dmp
    # rm rankedlineage_{tax_level}.dmp
      """
  }


// Assemblies selection channel.
all_assemblies = all_assemblies.collect()
ref_and_rep_assemblies = ref_and_rep_assemblies.collect()
one_assembly_per_sp = one_assembly_per_sp.collect()
rep_ref_and_10_assemblies_per_sp = rep_ref_and_10_assemblies_per_sp.collect()


  if (params.primary_assemblies_selection == 'ref_and_rep_assemblies') {
    ref_and_rep_assemblies.set {primary_assemblies_selection }
  }
  else if (params.primary_assemblies_selection == 'one_assembly_per_sp') {
    one_assembly_per_sp.set {primary_assemblies_selection }
  }
  else if (params.primary_assemblies_selection == 'rep_ref_and_10_assemblies_per_sp') {
    rep_ref_and_10_assemblies_per_sp.set {primary_assemblies_selection }
  }
  else if (params.primary_assemblies_selection == 'all_assemblies') {
    all_assemblies.set {primary_assemblies_selection }
  }


if (params.extended_assemblies_selection == 'ref_and_rep_assemblies') {
  ref_and_rep_assemblies.set {extended_assemblies_selection }
}
else if (params.extended_assemblies_selection == 'one_assembly_per_sp') {
  one_assembly_per_sp.set {extended_assemblies_selection }
}
else if (params.extended_assemblies_selection == 'rep_ref_and_10_assemblies_per_sp') {
  rep_ref_and_10_assemblies_per_sp.set {extended_assemblies_selection }
}
else if (params.extended_assemblies_selection == 'all_assemblies') {
  all_assemblies.set {extended_assemblies_selection }
}

/*
Generate plots on the selected assemblies

Krona and bar plot describing the taxonomic representation and the quality of the assemblies.

*/
process plot_assemblies_stat {
    publishDir "${result_dir}/result_plots/",  mode:'copy'

    input:
      file all_assemblies
      file ref_and_rep_assemblies
      file one_assembly_per_sp
      file rep_ref_and_10_assemblies_per_sp

    output:
      file "*.html" into html_plot_assemblies

  """
  PERL5LIB="" # needed to let krona works in the conda env https://github.com/bioconda/bioconda-recipes/issues/4390
  # Generate krona from all assemblies
  cut -f1 $all_assemblies > all_assemblies.ids
  cut -f1 $rep_ref_and_10_assemblies_per_sp > rep_ref_and_10_assemblies_per_sp.ids
  cut -f1 $one_assembly_per_sp > one_assembly_per_sp.ids
  cut -f1 $ref_and_rep_assemblies > ref_and_rep_assemblies.ids
  transformed_assembly_summary_bis.py --assemblies_summary_taxonomy $all_assemblies \
                                         --dataset_ids all_assemblies.ids \
                                         rep_ref_and_10_assemblies_per_sp.ids \
                                       one_assembly_per_sp.ids \
                                       ref_and_rep_assemblies.ids > formated_assemblies.tsv

  make_krona_xml_bis.py formated_assemblies.tsv -v \
                                            --dataset_labels all_assemblies rep_ref_and_10_assemblies_per_sp one_assembly_per_sp ref_and_rep_assemblies\
                                             > assemblies.xml
  ktImportXML assemblies.xml  -o assemblies_all_selections.html


  # Generate plot describing the quality of the assemblies selected
  plot_ncbi_assemblies.py --refseq_summary $rep_ref_and_10_assemblies_per_sp --name rep_ref_and_10_assemblies_per_sp
  plot_ncbi_assemblies.py --refseq_summary $ref_and_rep_assemblies --name ref_and_rep_assemblies
  plot_ncbi_assemblies.py --refseq_summary $one_assembly_per_sp --name one_assembly_per_sp
  plot_ncbi_assemblies.py --refseq_summary $all_assemblies --name all_assemblies
  """
}

// Get size of the selected assemblies
assembly_selection_size = primary_assemblies_selection.map {it[0].countLines() -1 } // -1 to not count header line


assemblie_splitted = primary_assemblies_selection.splitText (by : params.assemblies_chunk_size + 1 , keepHeader: true)


// if test parameter is true then only the first chunck of the splitted channel is processed
// and this chunk is splitted in smaller chunck
if (params.test) {
  assembly_selection_size = Channel.value(params.assemblies_chunk_size)
  assemblie_splitted = assemblie_splitted.first().splitText (by : 5, keepHeader: true)
}

// Add chunk id to each element of the channel
count_chunk = 0
assemblie_splitted_in_tuple = assemblie_splitted.map { it -> ["${count_chunk++}", it]}


/*
Download ncbi assemblies taken from the selected assembly file

The process takes in input the channel assemblie_splitted_in_tuple and downloads the selected
assemblies.
Before downloading, the python scripts checks if the files already exist in ncbi_genomes_dir.
*/
process download_ncbi_assemblies {
    cache 'deep'
    publishDir "${result_dir}/logs/download_ncbi_assemblies/", pattern: "*.log"

    input:
      // file selected_assemblies_file from assemblie_splitted
      tuple val(chunk_id), file(selected_assemblies_file) from assemblie_splitted_in_tuple
    output:
      tuple  val(chunk_id), file("symblink_*/*.faa*") into faa_files
      tuple val(chunk_id), file("symblink_*/*.gff*") into gff_files
      tuple val(chunk_id), file("symblink_*/*.fna*") into genomic_fna_files
      file '*.log' into log_file
      """
      echo $chunk_id
      get_ncbi_assemblies.py ${selected_assemblies_file} --add_symblink --check_dir "${ncbi_genomes_dir}/assemblies" --download_lastest_version ${fetch_lastest_assembly_versions} -v
      cat .command.log | grep ^'INFO: STATBYFILE:' > primary_assemblies_chunk${chunk_id}.log

      """
  }


cutoff_file =  cutoff_files.collectFile(name:"cutoff", keepHeader:true).collect()
cogs_list = cogs_list.collect()

/*
Identification of the COGs in the assemblies with fetchMGs
Loop through the assemly and identify COGs in the proteins file

Check first if the result file already exists in the result folder to not relaunch the identification.
*/
process fetchMGs_identification {
  cache 'deep'
  cpus params.fetchMGs_cpus
  publishDir "${result_dir}/cogs_identification/fetchMGs_results/", pattern: "fetchMGs_result_${chunk_id}/*_marker_genes_scores.table",  saveAs: { filename -> ("$filename" =~ /([A-Z]{3}_[0-9]{9})/)[0][1].replaceAll(/_/, "").split(/(?<=\G\w{3})/).join('/')+"/fetchMGs_result.table" }

  input:
    tuple val(chunk_id),  file(faa_files) from faa_files
    file cutoff_file
    file cogs_list
    file hmm_lib
  output:
    tuple val(chunk_id), file("fetchMGs_result_${chunk_id}/*_marker_genes_scores.table") into extraction_tables
    file "fetchMGs_result_${chunk_id}/" into primary_cog_identification

  """
  PERL5LIB="" #  needed to let krona works in the conda env https://github.com/bioconda/bioconda-recipes/issues/4390
  mkdir fetchMGs_result_${chunk_id}

  for f in *.faa*; do
    if [ \${f: -3} == ".gz" ]; then
      assembly_name=`basename \$f _protein.faa.gz`
      zcat \$f > sequences.faa
    else
      assembly_name=`basename \$f _protein.faa`
      cat \$f > sequences.faa
    fi

    prot_file=sequences.faa

    extraction_result_file=\${assembly_name}_extraction/sequences.all.marker_genes_scores.table
    perl $baseDir/fetchMGs/fetchMGs.pl -m extraction \$prot_file -p -l $hmm_lib -o \${assembly_name}_extraction -b $cutoff_file -x '' -t ${params.fetchMGs_cpus} -i $cogs_list

    mv \$extraction_result_file fetchMGs_result_${chunk_id}/\${assembly_name}_marker_genes_scores.table
  done

  """
}

gff_files.into {gff_files; gff_files_rep_and_ref_resolution; gff_file_rrna_genes }
extraction_tables.into {extraction_tables_identification_phase; extraction_tables_extraction_phase}

chunks_extraction_table_and_gff = extraction_tables_identification_phase.join(gff_files)

/*
Retrieve genomic positions of the proteins identified by FetchMGs.
Take the protein ids identified and extract genomic location from gff files.
*/
process get_cog_genomic_locations {

  publishDir "${result_dir}/", mode:'copy', pattern: "cog_location/*.tsv", saveAs: { filename -> "cogs_identification/cogs_location/${params.primary_assemblies_selection}/${file(filename).getName()}" }
  publishDir "${result_dir}/logs/get_cog_genomic_locations", pattern: "get_cog_genomic_locations.log.${chunk_id}"

  input:
    // file "gff_dir/*" from all_gff_files
    // file "ids_dir/*" from all_extraction_tables

    tuple val(chunk_id), file("ids_dir/*" ), file("gff_dir/*") from chunks_extraction_table_and_gff
  output:
    file "cog_location/*tsv" into cog_position_files
    file "get_cog_genomic_locations.log*" into get_cog_genomic_locations_log
  """
  get_genomic_positions_by_cog.py --gff_dir gff_dir \
                                    --ids_dir ids_dir -o cog_location

  cp .command.log get_cog_genomic_locations.log.${chunk_id}
  """
}

// Gather files
cog_position_files = cog_position_files.flatten().collectFile(keepHeader:true) { f ->
       [ file(f).getName(), f.text ]
   }


position_files = cog_position_files.collect()
/*
Make pair of cogs and analyse them.
*/
process cog_pair_analysis {
  cache 'deep'
  publishDir "${result_dir}/selected_regions/regions_detail/", pattern: "all_pairs_primary_selection/*/*", mode:'copy' //, saveAs: { filename -> "region_details/${params.primary_assemblies_selection}/${file(filename).getSimpleName().split('-')[0]}/${file(filename).getSimpleName().split('-')[1]}/${file(filename).getName()}" }

  input:
    file "cog_location/*" from position_files

  output:
    file "pairs_stat.tsv" into pairs_stats
    file "all_pairs_primary_selection/*/*" into cog_pair_detail_files

  """
  for cog_po_file in cog_location/*; do
    gene_name=`basename \${cog_po_file%.*}`
    make_cog_pairs.py \$cog_po_file cog_location/ -o all_pairs_primary_selection/\$gene_name -v
  done

  mkdir stats
  for cog_pair_dir in all_pairs_primary_selection/*; do
     echo \$cog_pair_dir
     dirbasename=`basename \$cog_pair_dir`
     cog_pair_analysis.py --cog_pair_files \$cog_pair_dir/* --length_cutoff 10000 -o stats/pairs_stat.tsv.\$dirbasename
  done

  #Merge all tsv file into one file
  #https://stackoverflow.com/questions/16890582/unixmerge-multiple-csv-files-with-same-header-by-keeping-the-header-of-the-firs
  awk 'FNR==1 && NR!=1{next;}{print}' stats/*.tsv.* > pairs_stat.tsv

  """
}



pairs_stats_collected =  pairs_stats.filter { it.countLines() > 1 }.collectFile(keepHeader:true, storeDir: "${result_dir}/selected_regions/regions_stat", name:"all_regions_stat_${params.primary_assemblies_selection}.tsv", sort:true).collect()

/*
Select pairs of cogs.
*/
process cog_pair_selection {
  //cache 'deep'
  publishDir "${result_dir}/result_plots/", mode:'copy', pattern: "*.html"
  // publishDir "${result_dir}/", mode:'copy', pattern: "selected_pairs_stat.tsv"
  publishDir "${result_dir}/cogs_identification", mode:'copy', pattern: "selected_cogs_list.txt"
  //
  input:
    file pairs_stats from pairs_stats_collected
    file cogs_selection_info
    val assembly_selection_size
    val region_genome_prct_threshold from region_prct_threshold_channel

  output:
    file 'selected_pairs_stat.tsv' into selected_pairs_stat_file
    file "selected_pairs.list" into selected_pairs_list_file
    file 'selected_cogs_list.txt' into selected_cogs
    file '*.html' into html_plots

  """

  # Region selection
  cog_pair_selection.py $pairs_stats \
                                          --nb_assemblies $assembly_selection_size \
                                          --coverage_threshold $region_genome_prct_threshold -v



  # Heatmap
  plot_heatmap_pair.py  --pair_stat_file $pairs_stats \
                                        --cog_annotation $cogs_selection_info \
                                        --nb_assemblies $assembly_selection_size \


  # Region coverage and median length plot
  plot_region_selection.py \
                                --all_pairs_file $pairs_stats \
                                --selected_pairs_file selected_pairs_stat.tsv \
                                --cog_annotation $cogs_selection_info \
                                --nb_assemblies $assembly_selection_size

  """
}


selected_pairs_stat_file = selected_pairs_stat_file.collect()

selected_pairs_list_file = selected_pairs_list_file.collect()


if (params.primary_assemblies_selection == params.extended_assemblies_selection){
  // Skip the process of finding COGS in extended assemblies as extended assemblies is equal to the primary selection
  gff_files_resolution = gff_files_rep_and_ref_resolution
  genomic_fna_files_resolution = genomic_fna_files

  gff_fna_ids_files = gff_files_resolution.join(genomic_fna_files_resolution)
                       .join(extraction_tables_extraction_phase)
                       .map { it -> [ it[1], it[2], it[3]]} // remove chunk id as we don't need it anymore
                       // .subscribe { println "<<$it>>"}

}
else {

  extended_assemblies_splitted = extended_assemblies_selection.splitText (by : 2000, keepHeader: true)

  if (params.test) {
    nb_assembly_extended_selection = 400
    extended_assemblies_splitted = extended_assemblies_splitted.first().splitText (by : nb_assembly_extended_selection, keepHeader: true)
                                                             .first().splitText (by : 20, keepHeader: true)
  }

  // add chunk id in assembly chunk channel
  // count_chunk is initialize earlier when managing primary assembly
  extended_assemblies_splitted_in_tuple = extended_assemblies_splitted.map { it -> ["${count_chunk++}", it]}

  selected_cogs_collected = selected_cogs.collect()
  primary_cog_identification = primary_cog_identification.collect()

  process get_cogs_in_extended_assemblies {
    cache 'deep'
    cpus params.fetchMGs_cpus

    publishDir "${result_dir}/logs/download_ncbi_assemblies/", pattern: "*.log"
    publishDir "${result_dir}/cogs_identification", pattern: "identification/extended/*_marker_genes_scores.table",  saveAs: { filename -> ("$filename" =~ /([A-Z]{3}_[0-9]{9})/)[0][1].replaceAll(/_/, "").split(/(?<=\G\w{3})/).join('/')+"/fetchMGs_result.table" }

    input:
      tuple val(chunk_id), file(assemblies_file) from extended_assemblies_splitted_in_tuple
      file 'previous_identification_tables/*' from primary_cog_identification
      file selected_cogs_collected
      file hmm_lib
      file cutoff_file

    output:
      tuple file("symblink_*/*.gff*"), file("symblink_*/*.fna*"), file("identification/*/*_marker_genes_scores.table") into gff_fna_ids_files
      file '*.log' into log_file_extended_assemblies

    """
    PERL5LIB="" #  needed to let krona works in the conda env https://github.com/bioconda/bioconda-recipes/issues/4390
    get_ncbi_assemblies.py ${assemblies_file} --check_dir "${ncbi_genomes_dir}/assemblies" --download_lastest_version ${fetch_lastest_assembly_versions} -v --add_symblink

    cat .command.log | grep ^'INFO: STATBYFILE:' >  extended_assemblies_chunk${chunk_id}.log

    mkdir -p identification/primary/
    mkdir -p identification/extended/

    for f in symblink_*/*.faa*; do
      if [ \${f: -3} == ".gz" ]; then
        assembly_name=`basename \$f _protein.faa.gz`
      else
        assembly_name=`basename \$f _protein.faa`
      fi

      # Are cogs of the genome have been already extracted?
      previous_identification_file="previous_identification_tables/*/\${assembly_name}_marker_genes_scores.table"
      if [ -f \${previous_identification_file} ]; then
        echo \${assembly_name}_marker_genes_scores.table already exists
        realpath_previous_identification_file=`realpath \$previous_identification_file`
        ln -s \$realpath_previous_identification_file identification/primary/\${assembly_name}_marker_genes_scores.table

      else
        if [ \${f: -3} == ".gz" ]; then
          zcat \$f > sequences.faa
        else
          cat \$f > sequences.faa
        fi
        identification_result_file=tmp_result_\$assembly_name/sequences.all.marker_genes_scores.table

        time perl $baseDir/fetchMGs/fetchMGs.pl -m extraction sequences.faa -p -l $hmm_lib -o tmp_result_\$assembly_name -b $cutoff_file -x '' -t ${params.fetchMGs_cpus} -i $selected_cogs_collected
        mv \$identification_result_file identification/extended/\${assembly_name}_marker_genes_scores.table
      fi

    done

    #clean
    # https://superuser.com/questions/76061/how-do-i-make-rm-not-give-an-error-if-a-file-doesnt-exist
    rm -r tmp_result_* || true
    """

  }

}


// if (params.test) {
//   selected_pairs_stat_file = selected_pairs_stat_file.splitText (by : 20, keepHeader: true).first() //.splitText (by : 5, keepHeader: true)
// }

process make_pair_and_extract_region {
  // echo true
  cache 'deep'
  // publishDir "${result_dir}/logs/make_pair_and_extract_region", pattern: "make_pair_and_extract_region.log*"

  input:
    tuple file("assemblies_gff/*"), file("assemblies_fna/*"), file("identified_cogs/*") from gff_fna_ids_files
    file selected_pairs_stat_file
    val rrna_genes_to_fetch
  output:
    file "cog_location/*tsv" into cog_position_files_all_genomes
    file "extracted_region/*.fna" into region_extracted
    file "extracted_single_gene/*.fna" into single_gene_extracted
    file "cog_pairs/*.tsv" into pairs_stat
    file 'make_pair_and_extract_region.log*' into make_pair_and_extract_region_log

  """

  make_pair_and_extract_region.py -t identified_cogs/ \
                                                      -g assemblies_gff/ \
                                                      -f assemblies_fna/ \
                                                      -l ${params.length_cutoff} \
                                                      -p $selected_pairs_stat_file \
                                                      --rrna_genes_to_fetch $rrna_genes_to_fetch -v
  cp .command.log make_pair_and_extract_region.log
  """

 }
 make_pair_and_extract_region_log_grp = make_pair_and_extract_region_log.collect().map{f -> ["logs/make_pair_and_extract_region/make_pair_and_extract_region.log", f ]}

make_pair_and_extract_region_log

region_extracted_grp = region_extracted.flatten().map { f -> [ "selected_regions/extracted_regions/"+f.getName() , f] }
                                                        .groupTuple()

single_gene_extracted_grp = single_gene_extracted.flatten().map { f -> ["selected_regions/extracted_genes/" + f.getName(),  f] }
                                                    .groupTuple()

pairs_stat_grp = pairs_stat.flatten().map { f -> ["pairs_stat/" + f.getName(),  f] }
                                                    .groupTuple()

cog_position_files_all_genomes_grp = cog_position_files_all_genomes.flatten().map { f -> ["cog_location/${params.extended_assemblies_selection}/" + f.getName(),  f] }
                                                    .groupTuple()

file_to_concat = region_extracted_grp.mix(make_pair_and_extract_region_log_grp, single_gene_extracted_grp, pairs_stat_grp, cog_position_files_all_genomes_grp)

process gather_result_file {
  tag "${pair_file}"
  cache 'deep'
  publishDir "${result_dir}/", pattern:"**log"
  publishDir "${result_dir}/", pattern: "{**.fna, **.tsv}", mode:'copy'
  publishDir "${result_dir}/cogs_identification", pattern: "cog_location/*"


  input:
    set val(pair_file), file('file_*') from file_to_concat
  output:
    file "extracted_regions/*fna" optional true into region_extracted_concat
    file '*/**' into all_concat_file
    file "pairs_stat/*tsv" optional true into pairs_stat_extended
    file "cog_location/*/*tsv" optional true into cog_location_extended


  """
  mkdir -p `dirname $pair_file`
  file_name=`basename $pair_file`
  extension="\${file_name##*.}"

  if [ "\$extension" == "fna" ]
  then
    for f in file_*; do
      cat \$f >> $pair_file
    done
  elif [ "\$extension" == "tsv" ]
  then
    for f in file_*; do
      head -n1 \$f > $pair_file
      break
    done

    for f in file_*; do
      tail --line=+2 \$f >> $pair_file
    done
  elif [ "\$extension" == "log" ]
  then
    for f in file_*; do
      cat \$f >> $pair_file
    done
  else
    echo $pair_file is not a fna, log or tsv file
    exit 1
  fi

  """
}

cog_pair_detail_files.flatten()
                      .map { d -> [ d.getSimpleName() , d] }//.randomSample( 10 ).subscribe { println "OOK $it" }
                      .set {cog_pair_detail_files_tupled}

selected_pairs_stat_file.splitCsv(header: true, sep:'\t')
                                .map {it -> it.cog_pair[0].replaceAll(/\|/, "-")}//.subscribe { println "OOK -->$it<--" }
                                .join(cog_pair_detail_files_tupled)//.subscribe { println "OOK -->$it<--" }
                                .map {it -> it[1]} // remove region name to keep only file
                                .collect()//.subscribe { println "OOK -->$it<--" }
                                .set { selected_cog_pair_detail_files_primary}

process analyse_selected_cog_pairs_by_selections {
  publishDir "${result_dir}/selected_regions", pattern: "regions_detail/*", mode:'copy'
  publishDir "${result_dir}/selected_regions/regions_stat/", pattern: "regions_stat_*.tsv", mode:'copy'
  input:
    file "cog_pair_to_filter/${params.primary_assemblies_selection}/" from selected_cog_pair_detail_files_primary
    file "cog_pair_to_filter_extended/" from pairs_stat_extended.collect()
    file selected_pairs_list_file
    file all_assemblies
    file rep_ref_and_10_assemblies_per_sp
    file one_assembly_per_sp
    file ref_and_rep_assemblies
    val primary from Channel.value(params.primary_assemblies_selection)
    val extended from Channel.value(params.extended_assemblies_selection)
    val rrna_genes_to_fetch

  output:
    file "regions_stat_*.tsv" into selected_pair_stat_by_selections
    file "regions_stat_${primary}.tsv" into selected_pair_stat_primary
    file "regions_detail/$primary/*" into cog_pair_details_primary
    file "regions_detail/$extended/*" into cog_pair_details_extended
    file "regions_detail/*" into region_details_by_selection

  script:
    // Filter cog pair file to match different assembly selection
    """
    echo primary $primary
    echo extended $extended

    # prevent name colision.
    # When primary and extended selection are the same no need to add pair of extended into cog_pair_to_filter dir
    if [ $primary != $extended ]
    then
      mv cog_pair_to_filter_extended/ cog_pair_to_filter/$extended
    fi

    cat $selected_pairs_list_file > selected_pairs_and_rrna.txt
    for gene in $rrna_genes_to_fetch; do
      echo \$gene >>  selected_pairs_and_rrna.txt;
    done

    # filter_cog_pair_files_according_assemblies and selected pairs..
    filter_cog_pair_files_according_assemblies.py --cog_pair_to_filter_dir 'cog_pair_to_filter/' \
                                                  --all_assemblies  $all_assemblies \
                                                  --rep_ref_and_10_assemblies_per_sp  $rep_ref_and_10_assemblies_per_sp \
                                                  --one_assembly_per_sp $one_assembly_per_sp  \
                                                  --ref_and_rep_assemblies $ref_and_rep_assemblies \
                                                  --filtered_cog_pair_dir "regions_detail" \
                                                  --selected_pairs_list_file selected_pairs_and_rrna.txt -v

    for cog_pair_dir in regions_detail/*; do
        selection_name=`basename \$cog_pair_dir`
        cog_pair_analysis.py --cog_pair_files \$cog_pair_dir/* \
                             --length_cutoff ${params.length_cutoff} \
                             -o regions_stat_\${selection_name}.tsv
    done

    """

}

process plot_region_selection_with_rrna {
  publishDir "${result_dir}/result_plots/", mode:'copy', pattern: "*.html"

  input:
    file all_pairs_stats_primary from pairs_stats_collected
    file selected_pair_stat_primary
    file cogs_selection_info
    val assembly_selection_size
    val region_genome_prct_threshold from region_prct_threshold_channel
    val rrna_genes_to_fetch
  output:
    file "*html" into region_selection_with_rrna

  """
  # Region coverage and median length plot with rrna gene info...
  plot_region_selection.py \
                          --all_pairs_file $all_pairs_stats_primary \
                          --selected_pairs_file $selected_pair_stat_primary \
                          --pairs_to_highlight $rrna_genes_to_fetch\
                          --cog_annotation $cogs_selection_info \
                          --nb_assemblies $assembly_selection_size
    """

}


cog_pair_details_primary.flatten()
                      .map { d -> [ d.getSimpleName() , d] }//.randomSample( 10 ).subscribe { println "OOK $it" }
                      .set {cog_pair_detail_files_tupled}

process plot_result_by_selected_region {
  tag "$region"
  publishDir "${result_dir}/result_plots/$region", mode:'copy'
  //
  input:
    set val(region), file(pairs_details_file) from cog_pair_detail_files_tupled
    // file pairs_details_file from cog_pair_details_primary.flatten()
    file cogs_selection_info from cogs_selection_info
    file selected_assemblies from primary_assemblies_selection
  output:
    file '*.html' into region_plots
    file '*.svg' into region_plots_structure

  """

  PERL5LIB="" #  needed to let krona works in the conda env https://github.com/bioconda/bioconda-recipes/issues/4390
  plot_region_details.py  $pairs_details_file \
                                    --cog_annotation $cogs_selection_info -v \
                                    --outfile_ids assembly_ids_covered_by_region.txt

  transformed_assembly_summary.py \
          --assemblies_summary_taxonomy $selected_assemblies \
          --genomes_ids_selected assembly_ids_covered_by_region.txt \
          --assemblies_id_label covered > taxonomy_labeled.tsv

  make_krona_xml.py taxonomy_labeled.tsv --color_label covered --color_by_default > assemblies_tax.xml

  ktImportXML assemblies_tax.xml -o region_taxonomique_coverage.html

  # copy html template of the region to be exported in result dir
  cp $baseDir/html_report/region_report_template.html result_region.html
  """
}

process make_html_report {
  publishDir "${result_dir}/", mode:'copy', pattern: "*.html"
  // publishDir "${result_dir}/", mode:'copy', pattern: "result_region.html"

  input:
    file cogs_selection_info from cogs_selection_info
    val parameters from parameter_string
    file assembly_selection_stat from assembly_selection_stat
    file cog_selection_stat
    val taxon_name from taxon_name
    file selected_pairs_list_file
    file 'region_stat_tables/*' from selected_pair_stat_by_selections

  output:
    file '*.html' into main_report


  """
  echo "$parameters" > parameter.txt

  python $baseDir/html_report/shape_report_intro.py \
              --html_template $baseDir/html_report/result_report_intro.html \
              --parameters parameter.txt \
              --assembly_stat $assembly_selection_stat \
              --taxon_lvl_name "$taxon_name" \
              --cog_selection_stat $cog_selection_stat \
              --selected_pairs $selected_pairs_list_file

  python $baseDir/html_report/shape_report_cog.py \
                --html_template $baseDir/html_report/result_report_cog.html \
                --cog_table $cogs_selection_info


  python $baseDir/html_report/shape_report_assemblies.py \
                --html_template  $baseDir/html_report/result_report_assemblies.html \
                --taxon_lvl_name "$taxon_name" \
                --parameters parameter.txt \
                --assembly_stat $assembly_selection_stat


  python $baseDir/html_report/shape_report_region.py --html_template $baseDir/html_report/result_report_region.html \
                                                     --region_tables_stat region_stat_tables/* \
                                                     -v
  """
}
