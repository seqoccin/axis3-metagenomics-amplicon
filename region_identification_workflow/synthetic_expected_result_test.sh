#!/bin/bash

nextflow_resultdir='results/'
expected_result="synthetic_data/expected_result/"
test_dir=test_synthetic_data
mkdir $test_dir

# TEST diff

for result_file in $nextflow_resultdir/selected_regions/regions_detail/ref_and_rep_assemblies/*tsv;
do
  file_name=`basename $result_file`
  echo $file_name
  expected_result_file=$expected_result/cog_pairs/$file_name
  sort $result_file > $test_dir/${file_name}_OBTAINED
  sort $expected_result_file > $test_dir/${file_name}_EXPECTED
  diff test_synthetic_data/${file_name}_OBTAINED test_synthetic_data/${file_name}_EXPECTED -s
done


selected_pair_expected="synthetic_data/expected_result/selected_pairs_stat.tsv"
selected_pair_obtained="$nextflow_resultdir/selected_regions/regions_stat/regions_stat_ref_and_rep_assemblies.tsv"

cut -f1,2 $selected_pair_obtained | sort  > $test_dir/selected_pairs_stat_OBTAINED
cut -f1,2 $selected_pair_expected | sort  > $test_dir/selected_pairs_stat_EXPECTED

diff $test_dir/selected_pairs_stat_OBTAINED $test_dir/selected_pairs_stat_EXPECTED -s
