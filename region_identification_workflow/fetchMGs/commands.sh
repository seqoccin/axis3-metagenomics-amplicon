## command used
module load bioinfo/hmmer-3.1b2

./fetchMGs.pl -l test_lib/ -o calibration_test -b MG_BitScoreCutoffs.uncalibrated_COG0051.txt -m calibration merge_samples_COG0060_COG0051.faa true_positive_COG0051_sample.tsv

./fetchMGs.pl -l test_lib -o calibration_test -b MG_BitScoreCutoffs.uncalibrated_COG0051.txt -m calibration merge_samples_COG0060_COG0051.faa true_positive_COG0051_sample.tsv

# when change the header of the hmm file it seems to work
# fna sequences are required by the program but does not seems to affect the result. As far as I understand there are just printed out at the end of the extraction.

# Version problem: hmmsearch used by the programm is v3.0 and the version of the profile taken from eggNOG is v3.1b2 same as the one available on the server
# Change the version of hmmsearch used by the programm



# Download profile on eeggNOG
wget http://eggnogapi5.embl.de/nog_data/file/hmm/COG0060 -O lib_homemade/COG0060.hmm



./fetchMGs.pl -m calibration COG0081.faa  -p -l lib_homemade/ -x ''


perl fetchMGs.pl -m calibration cog_sequences/merge_COG.faa cog_sequences/merge_TP.txt  -p -l lib_homemade/ -x ''

perl fetchMGs.pl -m calibration cog_sequences/merge_COG.faa cog_sequences/merge_TP.txt  -p -l lib_homemade -x '' -b lib_homemade/MG_BitScoreCutoffs.uncalibrated.txt

# with uncalibrated cutoff set at 0
perl fetchMGs.pl -m calibration cog_sequences/merge_COG.faa cog_sequences/merge_TP.txt  -p -l lib_homemade -x '' -b lib_homemade/MG_BitScoreCutoffs_0.uncalibrated.txt -o calibration_0

# 10 min to process the 353 153 sequences over 3 COGS
time perl fetchMGs.pl -m calibration  cog_sequences/merge_all_seq.faa cog_sequences/true_positive_sequences_COGs_list_2_members_0.95.txt -p -l lib_homemade -o calibration_3cogs_vs_all_seq -b lib_homemade/MG_BitScoreCutoffs_0.uncalibrated.txt -x ''


time perl fetchMGs.pl -m calibration  cog_sequences/merge_all_seq.faa cog_sequences/true_positive_sequences_COGs_list_2_members_0.95.txt -p -l lib_homemade -o calibration0_3cogs_vs_all_seq -b lib_homemade/MG_BitScoreCutoffs_0.uncalibrated.txt -x ''

# 24 584 628
time perl /home/jmainguy/marker_identification/fetchMGs/fetchMGs.pl -m calibration  all_sequences.faa true_positive_sequences_COGs_list_2_members_0.95.txt  -p  -l profile_hmm_links -o calibration -b BitScoreCutoffs.uncalibrated.txt -x '' -t 2  -i COGs_list_2_members_0.95.txt
# 1 726 500
# 3 COGs vs all proteome !
time perl fetchMGs.pl -m calibration  cog_sequences/e5.proteomes.faa cog_sequences/true_positive_sequences_COGs_list_2_members_0.95.txt -p -l lib_homemade -o calibration0_3cogs_vs_all_proteome -b lib_homemade/MG_BitScoreCutoffs_0.uncalibrated.txt -x ''



##CALIBRATION with 16 cpu 16G and 2 cpu / calibration / COG
[24/2cd5ba] process > parse_and_identify_universal_OG [100%] 1 of 1, cached: 1 ✔
[c9/5649e3] process > get_eggNOG_fasta_sequences      [100%] 1 of 1, cached: 1 ✔
[30/179ac0] process > filter_proteome_file            [100%] 50 of 50, cached: 50 ✔
[ac/809a8d] process > fetchMGs_calibration            [100%] 79 of 79 ✔
Completed at: 22-mai-2019 11:47:40
Duration    : 37m 55s
CPU hours   : 20.0 (5,2% cached)
Succeeded   : 79
Cached      : 52

exemple  : /work/jmainguy/marker_identification/work/cf/439616d04ffd3d092c131041bb3265/ 
