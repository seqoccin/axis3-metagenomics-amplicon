faa_file=$1
cog_name=${faa_file%.faa}
seq_id_file=${faa_file%.faa}_ids.txt
true_positive_file=${faa_file%.faa}_TP.txt

grep ^'>' $faa_file | cut -d'>' -f2 > $seq_id_file

head $seq_id_file
#COG0051_seq_sample_ids.txt
rm ${true_positive_file}
while read p;
do
  echo -e $p'\t'${cog_name}>>$true_positive_file;
done <$seq_id_file
