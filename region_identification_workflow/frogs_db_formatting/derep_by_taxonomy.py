#!/usr/bin/env python3

"""
Dereplicate identical sequence with the same taxonomy.
"""

# Metadata
__author__ = 'Mainguy Jean - Plateforme bioinformatique Toulouse'
__copyright__ = 'Copyright (C) 2020 INRAE'
__license__ = 'GNU General Public License'
__version__ = '0.1'
__email__ = 'support.bioinfo.genotoul@inrae.fr'
__status__ = 'dev'


import os
import logging
from argparse import ArgumentParser, ArgumentDefaultsHelpFormatter
import sys
import csv
from Bio import SeqIO
from collections import defaultdict


def derep_seqs(seqobjs, add_abundance=False):
    uniq_seqobjs = []
    seq_to_obj = defaultdict(list)
    for seqobj in seqobjs:
        seq_to_obj[seqobj.seq].append(seqobj)

    for identical_seqobjs in  seq_to_obj.values():
        uniq_seqobj = identical_seqobjs.pop()
        uniq_seqobj.description = ''
        if add_abundance:
            
            # if uniq_seqobj.description.startswith(uniq_seqobj.id):
            #    uniq_seqobj.description = uniq_seqobj.description[len(uniq_seqobj.id):]
            uniq_seqobj.id += f'_{len(identical_seqobjs) + 1 }'
        uniq_seqobjs.append(uniq_seqobj)

    return uniq_seqobjs


def parse_arguments():
    """Parse script arguments."""
    parser = ArgumentParser(description="Dereplicate identical sequence with the same taxonomy.",
                            formatter_class=ArgumentDefaultsHelpFormatter)

    parser.add_argument("sequence_file", type=str,
                        help="Sequence file in fasta format.")
    parser.add_argument("--assembly_selection", type=str,
                        help="Assembly selection with taxonomy information.", required=True)
    parser.add_argument("-o", "--output", type=str, default=None,
                        help="Name of the output file. By default the prefix the name of the basename of the fasta file is used with the suffix taxderep_")

    parser.add_argument("-a", "--add_abundance", help="add abundance in fasta id. ie the number of time the sequence with found with same taxonomy.",
                    action="store_true")
    parser.add_argument("-v", "--verbose", help="increase output verbosity",
                        action="store_true")
    args = parser.parse_args()
    return args


def main():

    args = parse_arguments()

    if args.verbose:
        logging.basicConfig(format="%(levelname)s: %(message)s", level=logging.DEBUG)
        logging.info('Mode verbose ON')

    else:
        logging.basicConfig(format="%(levelname)s: %(message)s")

    args = parse_arguments()
    sequence_file = args.sequence_file
    assembly_selection = args.assembly_selection
    add_abundance = args.add_abundance


    derep_fasta_file = args.output if args.output else f"./taxderep_{os.path.basename(sequence_file)}"

    logging.info(f'Output file is : {derep_fasta_file}')

    output_dir = os.path.dirname(derep_fasta_file)
    os.makedirs(output_dir, exist_ok=True)

    # Parse assembly_taxonomy_file
    with open(assembly_selection) as csvfile:
        reader = csv.DictReader(csvfile, delimiter='\t')
        assembly_to_taxonomy = {d['assembly_accession']: d['taxonomy']
                             for d in reader}

     # Group seq by the same taxonomy
    taxonomy_to_seqs = defaultdict(list)
    with open(sequence_file) as fasta_fl:
        for seq in SeqIO.parse(fasta_fl, "fasta"):
            assembly_name = seq.id.split('|')[0]
            taxonomy = assembly_to_taxonomy[assembly_name]
            taxonomy_to_seqs[taxonomy].append(seq)

    # Derep sequences and write them in the output file
    with open(derep_fasta_file, 'w') as derep_fasta_fh:

        fasta_out = SeqIO.FastaIO.FastaWriter(derep_fasta_fh, wrap=None)
        fasta_out.write_header()

        for tax, seqs in taxonomy_to_seqs.items():
            uniq_seqs = derep_seqs(seqs, add_abundance)
            fasta_out.write_records(uniq_seqs)



if __name__ == '__main__':
    main()
