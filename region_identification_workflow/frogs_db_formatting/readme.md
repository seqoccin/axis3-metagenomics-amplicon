# Format FROGS sequence db


# pre formatting
Preformating of sequences with homemade python script run in format_seqs_to_frogs.sh


> NEED TO HAVE THE CORRECT BIOPYTHON VERSION 
`biopython                 1.78             py38h7b6447c_0 `




```bash
conda activate region_identification


name=COG0097-COG0200
name='16S-23S'
mkdir -p $name

python ../derep_by_taxonomy.py extracted_regions/$name.fna --assembly_selection assembly_selections/rep_ref_and_10_assemblies_per_sp.tsv -v -a -o  $name/$name.fna

python ../fasta_to_taxonomy.py $name/$name.fna \
                            --assembly_selection assembly_selections/rep_ref_and_10_assemblies_per_sp.tsv \
                            -o $name/$name.tsv \
                            -v

```


Need to be in conda env otherwise biopython fail: `conda activate region_identification`


Follow instruction of the file  `/save/frogs/how_to_databank/databank_from_fasta.sh`


* The taxo and fasta file have same number of seq/ taxo 





```bash
FASTA=$name/$name.fna
TAX=$name/$name.tsv

sed -i 's/|/_/g' $name/$name.* 

# replace space in tax by _
sed -i 's/ /_/g' $TAX

# replace * in tax by _
sed -i 's/*/_/g' $TAX

# Nombre de séquences
grep -c '>' $FASTA
wc -l $TAX
 

#check que toutes les séquences de FASTA sont dans TAX 
grep '>' $FASTA | sed 's/>//' | awk -v T=$TAX 'BEGIN{while(getline<T>0){tab[$1]=1}}{if(tab[$1]!=1){print $1}}' > ${name}_no_tax.txt
wc -l ${name}_no_tax.txt


# check du même nombre de niveau pour toutes les séquences
cut -f 2 $TAX |awk -F ";" '{print NF}' | sort -u 

# control du % de N par séquence, cela ne retourne pas d'erreurs techniques mais il est peut être favorable de filtrer ces données
cat  $FASTA |awk -F "N" '{if(substr($1,0,1) == ">"){split($1,tab," "); id=tab[1]}else{count=NF-1; print id"\t"count"\t"length($0)"\t"count*100/length($0)}}' > count_N.txt
# BLAST recommande un maximum de 40%
cat count_N.txt | awk '{c++; if($4>40){m++}}END{print m"/"c"="m*100/c"%"}'


```


## PREPARATION DES FICHIERS POUR RDP

```
export PATH=/save/frogs/src/FROGS_prod/libexec:$PATH
export PYTHONPATH=/save/frogs/src/FROGS_prod/lib:$PYTHONPATH
module load system/Python-2.7.15
module load system/Java8


##################################   PREPARATION DES FICHIERS POUR RDP ##############################
# Dans le cas d'une base à une seul amplicon. Le préciser dans la version de la base : exemple silva_132_16S.fasta
Assembly_name=refseq-rr10
version=2020-06-09

DB_NAME=${Assembly_name}_${name}_${version}
echo DB NAME ${DB_NAME}
mkdir ${DB_NAME}

# formatage de la base pour RDP
# l'option -r précise le nom des rangs taxonomiques, ici 10

python2 /save/frogs/how_to_databank/fasta2RDP.py -d $FASTA -t $TAX -r R1 R2 R3 R4 R5 R6 R7 R8  --rdp-taxonomy ${DB_NAME}/${DB_NAME}.tax --rdp-fasta ${DB_NAME}/${DB_NAME}.fasta


# recherche de taxons ambigus retrouvés plusieurs fois :
#   - soit à des rangs différents 
#   - soit au même rangs mais descendant de parents différents

cut -f 2 -d '*' ${DB_NAME}/${DB_NAME}.tax  |  cut -f 1 -d ' ' | sort | uniq -c | awk '$1!=1 && $2 !="unclassified"' > problematic_taxon
if [[ -e problematic_taxon_and_rank_number ]]
then
rm problematic_taxon_and_rank_number
fi
for i in `awk '{print $2}' problematic_taxon `
do 
exp=`echo \"\*$i \[id\"`
grep "\*"$i" \[id" ${DB_NAME}/${DB_NAME}.tax | sed 's/ \[id/*/'| awk -F "*" '{print $2,$5}' >> problematic_taxon_and_rank_number
done

```


```
# NO USE OF RDP 
# sbatch -J ${DB_NAME}_RDP -o %x.out -e %x.err -p workq --mem=80G --wrap="sh launch_classifier.sh ${DB_NAME}/${DB_NAME}.fasta ${DB_NAME}/${DB_NAME}.tax"
# cp /save/frogs/galaxy_databanks/SILVA/16S/silva_132/silva_132_16S.fasta.properties ${DB_NAME}/${DB_NAME}.fasta.properties
# BLAST
sbatch -J ${DB_NAME}_Blast -o %x.out -e %x.err --wrap="/usr/local/bioinfo/src/NCBI_Blast+/ncbi-blast-2.7.1+/bin/makeblastdb -in ${DB_NAME}/${DB_NAME}.fasta -dbtype nucl"
# TEST DB
sbatch -J test_${DB_NAME}  -o %x.out -e %x.err --mem=100G --cpus-per-task=30 --wrap="/save/frogs/src/FROGS_dev/app/affiliation_OTU.py --rdp --reference ${DB_NAME}/${DB_NAME}.fasta --input-biom /save/frogs/src/FROGS_dev/tools/affiliation_OTU/data/swarm.biom --input-fasta /save/frogs/src/FROGS_dev/tools/affiliation_OTU/data/swarm.fasta --output-biom affiliation.biom --summary affiliation.summary --nb-cpus 30 --java-mem 15"

```

Construct 16S-23S db with all refseq downloaded on 2020-11-13

create dir and symblink of assembly selection and extracted sequences:

```

mkdir all_refseq_2020-11-13


ln -s /work2/project/seqoccin/metaG/marker_identification/region_identification_workflow/bacteria_investigation/results_refseq_2020-11-13/selected_regions/extracted_regions/


ln -s /work2/project/seqoccin/metaG/marker_identification/region_identification_workflow/bacteria_investigation/results_refseq_2020-11-13/assembly_selections/

```

In all_refseq_2020-11-13 directory 
```
conda activate region_identification

assembly_selection=assembly_selections/all_assemblies_with_taxonomy.tsv
name=COG0097-COG0200
name='16S-23S'
mkdir -p $name

python ../derep_by_taxonomy.py extracted_regions/$name.fna --assembly_selection $assembly_selection -v -a -o  $name/$name.fna

python ../fasta_to_taxonomy.py $name/$name.fna \
                            --assembly_selection $assembly_selection \
                            -o $name/$name.tsv \
                            -v


```





