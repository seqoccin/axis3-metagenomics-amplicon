#!/usr/bin/env python3

"""
From a fasta file and an assembly summary, generate a fasta id to taxonomy.
"""

# Metadata
__author__ = 'Mainguy Jean - Plateforme bioinformatique Toulouse'
__copyright__ = 'Copyright (C) 2020 INRAE'
__license__ = 'GNU General Public License'
__version__ = '0.1'
__email__ = 'support.bioinfo.genotoul@inrae.fr'
__status__ = 'dev'


import os
import logging
from argparse import ArgumentParser, ArgumentDefaultsHelpFormatter
import sys
import csv
from Bio import SeqIO
from collections import defaultdict
import re

def tsv_to_dict_of_dicts(file, key_field):
    """
    Take a tsv with header and parse into dict of dict
    Uses
    * the specified field as key
    * the line turned into a dict as value .
    """
    dict_of_list_of_dict = defaultdict(list)
    with open(file) as csvfile:
        reader = csv.DictReader(csvfile, delimiter='\t')
        for l in reader:
            dict_of_list_of_dict[l[key_field]].append(l)
        return dict(dict_of_list_of_dict)

def clean_taxonomy(taxonomy):
    """
    Clean taxonomy.

    Taxonomy string example:
    Ureaplasma parvum serovar 3 str. ATCC 700970 [tax_name];Ureaplasma parvum [species];Ureaplasma [genus];Mycoplasmataceae [family];Mycoplasmatales [order];Mollicutes [class];Tenericutes [phylum];Bacteria other [kingdom];Bacteria [superkingdom]
    Remove kingdom rank and rank name in bracket.
    """

    clean_taxonomy = [re.match("(.*) \[.*\]$", taxon).group(1) for taxon in taxonomy.split(';') if not taxon.endswith('[kingdom]')]
    
        
    # remove other pattern in taxonomy
    # when a rank name is missing the name at the rank n-1 is used and other is added. 
    # Example : Bacteria;Proteobacteria;Betaproteobacteria;Betaproteobacteria_other;Betaproteobacteria_other;Candidatus_Kinetoplastibacterium;Candidatus_Kinetoplastibacterium_blastocrithidii;Candidatus_Kinetoplastibacterium_blastocrithidii_TCC012E
    

    clean_taxonomy_with_unclassified = []
    for i, taxon in enumerate(clean_taxonomy):

        if i != 0 and i != 7 and (taxon == f'{clean_taxonomy[i+1]} other' or taxon == f"{clean_taxonomy[i+1]}"):
            clean_taxonomy_with_unclassified.append('unclassified')    
        else:
            clean_taxonomy_with_unclassified.append(taxon)
            

    #print(clean_taxonomy_with_unclassified)
    
    return ';'.join(reversed(clean_taxonomy_with_unclassified))



def parse_arguments():
    """Parse script arguments."""
    parser = ArgumentParser(description="...",
                            formatter_class=ArgumentDefaultsHelpFormatter)

    parser.add_argument("sequence_file", type=str,
                        help="Sequence file in fasta format.")
    parser.add_argument("--assembly_selection", type=str,
                        help="Assembly selection with taxonomy information.", required=True)
    parser.add_argument("-o", "--output", type=str, default=None,
                        help="Name of the output file. By default the prefix the name of the basename of the fasta file is used with the extendion .tsv")
    parser.add_argument("--write_header", help="Write header into the output file.",
                        action="store_true")
    parser.add_argument("-v", "--verbose", help="increase output verbosity",
                        action="store_true")
    args = parser.parse_args()
    return args


def main():

    args = parse_arguments()

    if args.verbose:
        logging.basicConfig(format="%(levelname)s: %(message)s", level=logging.DEBUG)
        logging.info('Mode verbose ON')

    else:
        logging.basicConfig(format="%(levelname)s: %(message)s")

    args = parse_arguments()
    sequence_file = args.sequence_file
    assembly_selection = args.assembly_selection
    write_header = args.write_header

    if args.output:
        fastaid_to_taxonomy_file = args.output
    else:
        fastaid_to_taxonomy_file = f"./{'.'.join(os.path.basename(sequence_file).split('.')[:-1])}.tsv"

    logging.info(f'Output file is : {fastaid_to_taxonomy_file}')

    output_dir = os.path.dirname(fastaid_to_taxonomy_file)
    os.makedirs(output_dir, exist_ok=True)

    # Parse assembly_taxonomy_file
    with open(assembly_selection) as csvfile:
        reader = csv.DictReader(csvfile, delimiter='\t')
        assembly_to_taxonomy = {d['assembly_accession']: clean_taxonomy(d['taxonomy'])
                             for d in reader}

    assembly_to_new_seq_id = defaultdict(dict)

    with open(sequence_file) as fasta_fl, open(fastaid_to_taxonomy_file, 'w') as out_fl:

        #output
        tsv_writer = csv.DictWriter(out_fl, fieldnames=['seq_id', 'taxonomy'], delimiter='\t')
        if write_header:
            tsv_writer.writeheader()

        for i, seq in enumerate(SeqIO.parse(fasta_fl, "fasta")):
            assembly_name = seq.id.split('|')[0]
            try:
                taxonomy = assembly_to_taxonomy[assembly_name]
            except KeyError as err:
                raise KeyError(f'Assembly accession {assembly_name} is not found in the given assembly selection {assembly_selection}.')

            
            if i%5000 == 0:
                logging.info(f'analysis seq {i+1}')
            tsv_writer.writerow({"seq_id":seq.id, "taxonomy":taxonomy})



if __name__ == '__main__':
    main()
