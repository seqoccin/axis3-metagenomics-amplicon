




python derep_by_taxonomy.py rep_ref_and_10_assemblies_per_sp_refseq_2020-06-09/extracted_regions/COG0097-COG0200.fna \
                            --assembly_selection rep_ref_and_10_assemblies_per_sp_refseq_2020-06-09/assembly_selections/rep_ref_and_10_assemblies_per_sp.tsv \
                            -v -a \
                            -o rep_ref_and_10_assemblies_per_sp_refseq_2020-06-09/frogs_db_formatted/COG0097-COG0200.fna


python fasta_to_taxonomy.py rep_ref_and_10_assemblies_per_sp_refseq_2020-06-09/frogs_db_formatted/COG0097-COG0200.fna \
                            --assembly_selection rep_ref_and_10_assemblies_per_sp_refseq_2020-06-09/assembly_selections/rep_ref_and_10_assemblies_per_sp.tsv \
                            -o rep_ref_and_10_assemblies_per_sp_refseq_2020-06-09/frogs_db_formatted/COG0097-COG0200.tsv \
                            -v


python derep_by_taxonomy.py rep_ref_and_10_assemblies_per_sp_refseq_2020-06-09/extracted_regions/16S-23S.fna \
                            --assembly_selection rep_ref_and_10_assemblies_per_sp_refseq_2020-06-09/assembly_selections/rep_ref_and_10_assemblies_per_sp.tsv \
                            -v -a \
                            -o rep_ref_and_10_assemblies_per_sp_refseq_2020-06-09/frogs_db_formatted/16S-23S.fna


python fasta_to_taxonomy.py rep_ref_and_10_assemblies_per_sp_refseq_2020-06-09/frogs_db_formatted/16S-23S.fna \
                            --assembly_selection rep_ref_and_10_assemblies_per_sp_refseq_2020-06-09/assembly_selections/rep_ref_and_10_assemblies_per_sp.tsv \
                              -o rep_ref_and_10_assemblies_per_sp_refseq_2020-06-09/frogs_db_formatted/16S-23S.tsv \
                            -v
