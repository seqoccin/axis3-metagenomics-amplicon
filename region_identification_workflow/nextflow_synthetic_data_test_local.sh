#!/bin/bash
set -e

source ~/.bashrc
conda activate pipeline_main

baseDir="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
echo $baseDir

refseq_path="$PWD/synthetic_data/ncbi_data/0000-00-00_refseq_summary/"
result_dir="$PWD/"
tax_lvl=0
eggNOG_data_dir="$PWD/synthetic_data/eggnog_data/"
ncbi_genomes_dir="$PWD/synthetic_data/ncbi_data"

# rm -r $PWD/synthetic_data/
python $baseDir/generate_synthetic_data.py --grp_size 3 -v --nb_assembly 20 --fill_strategy random

echo $baseDir/nextflow run $baseDir/main.nf -profile local_machine \
                                                      -with-report $result_dir/report.html \
                                                      -with-trace $result_dir/trace \
                                                      -with-timeline $result_dir/timeline.html \
                                                      -with-dag $result_dir/flowchart.html \
                                                      --tax_level $tax_lvl \
                                                      --refseq_path $refseq_path\
                                                      -w 'work_synthetic_data' \
                                                      --eggNOG_data_dir $eggNOG_data_dir \
                                                      --ncbi_genomes_dir $ncbi_genomes_dir -resume
$baseDir/nextflow run $baseDir/main.nf -profile local_machine \
                                                      -with-report $result_dir/report.html \
                                                      -with-trace $result_dir/trace \
                                                      -with-timeline $result_dir/timeline.html \
                                                      -with-dag $result_dir/flowchart.html \
                                                      --tax_level $tax_lvl \
                                                      --refseq_path $refseq_path\
                                                      -w 'work_synthetic_data' \
                                                      --eggNOG_data_dir $eggNOG_data_dir \
                                                      --ncbi_genomes_dir $ncbi_genomes_dir

bash ../synthetic_expected_result_test.sh

#
# conda activate pipeline_main
# nextflow run ../main.nf -profile local_machine  --tax_level 0 \
#                                                       --refseq_path "$PWD/synthetic_data/ncbi_data/0000-00-00_refseq_summary/"\
#                                                       -w 'work_synthetic_data' \
#                                                       --eggNOG_data_dir "$PWD/synthetic_data/eggnog_data/" \
#                                                       --ncbi_genomes_dir "$PWD/synthetic_data/ncbi_data" -resume -with-report -with-trace
