# Flexible workflow

Goal: to be able to identify region in a set of genomes.


# Change cog identification

Inputs:
* list of taxons:  `selected_taxon.list`
List of genus often found in gut microbiome.


* eggnog member file:
```
wget http://eggnog5.embl.de/download/eggnog_5.0/per_tax_level/2/2_members.tsv.gz
wget http://eggnog5.embl.de/download/eggnog_5.0/per_tax_level/1239/1239_members.tsv.gz
```

* new taxdump:

```
wget ftp://ftp.ncbi.nlm.nih.gov/pub/taxonomy/new_taxdump/new_taxdump.tar.gz
tar -xzf new_taxdump.tar.gz
mkdir new_taxdump
mv *dmp new_taxdump
```

Problem with Bacillus Two taxon has the same name

```
1386	|	Bacillus	|	Bacillus <firmicutes>	|	scientific name	|
55087	|	Bacillus	|	Bacillus <walking sticks>	|	scientific name	|

```
