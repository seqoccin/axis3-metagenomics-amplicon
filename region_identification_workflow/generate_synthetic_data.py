#!/usr/bin/env python3

"""
Description.

:Example:
python template.py -v
"""

# Metadata
__author__ = 'Mainguy Jean - Plateforme bioinformatique Toulouse'
__copyright__ = 'Copyright (C) 2019 INRA'
__license__ = 'GNU General Public License'
__version__ = '0.1'
__email__ = 'support.bioinfo.genotoul@inra.fr'
__status__ = 'dev'


from argparse import ArgumentParser, ArgumentDefaultsHelpFormatter
import logging
import random
from Bio.Seq import Seq
import os
from Bio import SeqIO
from Bio.SeqRecord import SeqRecord
from collections import defaultdict
import csv
import tarfile
import hashlib


def md5(fname):
    """
    Taken from:
    https://stackoverflow.com/questions/3431825/generating-an-md5-checksum-of-a-file
    """
    hash_md5 = hashlib.md5()
    with open(fname, "rb") as f:
        for chunk in iter(lambda: f.read(4096), b""):
            hash_md5.update(chunk)
    return hash_md5.hexdigest()


def list_of_dict_to_tsv(list_of_dict, output_file, mode='w', writeheader=True):
    """Encode a list of dictionnary into a tsv file.

    The mode of writing can be specified.
    By default the mode is set to "w"

    """
    if len(list_of_dict) == 0:
        raise ValueError('The given list is empty: we cant write it in a file')

    with open(output_file, mode, newline='') as csvfile:
        writer = csv.DictWriter(csvfile, fieldnames=list(list_of_dict[0].keys()), delimiter='\t')
        if mode == 'w' and writeheader is True:
            writer.writeheader()
        for dict_info in list_of_dict:
            writer.writerow(dict_info)


def hmm_build(hmm_file, faa_file):
    """Build hmm profile."""
    exit_code = os.system(f'hmmbuild {hmm_file} {faa_file}  > /dev/null')
    if exit_code != 0:
        raise ValueError(f'hmmbuild failed. exit code {exit_code}. check input file {faa_file}')


def random_codingSeq(size=30, stopCodons=['TAG', 'TAA', 'TGA'], sep=''):
    """
    Generate random coding sequences.

    Taken from https://lists.open-bio.org/pipermail/biopython/2004-June/002140.html
    """
    codons = [x+y+z for x in 'AGTC' for y in 'AGTC' for z in 'AGTC' if x+y+z not in stopCodons]

    return sep.join([random.choice(codons) for x in range(int(size/3))])


def split(a, n):
    """Split a in n chunks."""
    # from https://stackoverflow.com/questions/2130016/splitting-a-list-into-n-parts-of-approximately-equal-length
    k, m = divmod(len(a), n)
    return (a[i * k + min(i, m):(i + 1) * k + min(i + 1, m)] for i in range(n))


def parse_arguments():
    """Parse script arguments."""
    parser = ArgumentParser(description="...",
                            formatter_class=ArgumentDefaultsHelpFormatter)
    parser.add_argument("-v", "--verbose", help="increase output verbosity",
                        action="store_true")

    args = parser.parse_args()
    return args


# def write_faa_file(dna_se, filename):
def group(lst, n):
    """
    Group a list into consecutive n-tuples.

    Incomplete tuples are discarded e.g:
    group([0,3,4,10,2,3], 2) => [(0,3), (4,10), (2,3)]
    taken from http://code.activestate.com/recipes/303060-group-a-list-into-sequential-n-tuples/

    >>> group(range(10), 3)
    [(0, 1, 2), (3, 4, 5), (6, 7, 8)]
    """
    return zip(*[lst[i::n] for i in range(n)])


def generate_eggnog_data(coding_dna_seqs, synthetic_eggnog_dir, tax_level):
    """Generate eggnog data."""
    hmm_dir = os.path.join(synthetic_eggnog_dir, 'hmm_profiles')
    faa_dir = os.path.join(synthetic_eggnog_dir, 'faa_sequences')
    ext_mb_dir = os.path.join(synthetic_eggnog_dir, 'extended_members')
    os.makedirs(hmm_dir, exist_ok=True)
    os.makedirs(faa_dir, exist_ok=True)
    os.makedirs(ext_mb_dir, exist_ok=True)
    # Make synthetic eggnog files
    all_taxids = [str(t) for t in range(1, 1000)]

    # proteome file on which fetchMGs will be trained
    proteome_file = os.path.join(faa_dir, 'e5.proteomes.faa')
    proteome_handler = open(proteome_file, 'w')

    # member file
    member_file = os.path.join(synthetic_eggnog_dir, f'{tax_level}_members.tsv')
    member_handler = open(member_file, 'w')

    for i, seq in enumerate(coding_dna_seqs):
        # --> faa files -->  true positive file

        cog_id = f'COGtest0{i}'
        # add random nt at the end of the seq to have some variation
        modified_seqs = [seq + random_codingSeq(size=10)
                         for _ in range(100)]
        faa_file = os.path.join(faa_dir, f'{cog_id}.faa')
        with open(faa_file, 'w') as faa_handler:
            for tax_id, modified_seq in zip(range(100), modified_seqs):
                seq_obj = Seq(modified_seq)
                # SeqRecord(Seq(seq, generic_protein), id=header, description="{}|{}".format(self.product, organism))

                prot_record = SeqRecord(seq_obj.translate(), id=f"{tax_id}.{cog_id}")

                SeqIO.write(prot_record, faa_handler, "fasta")
                SeqIO.write(prot_record, proteome_handler, "fasta")

        # --> hmm profiles from faa sequences
        hmm_file = os.path.join(hmm_dir, f'{cog_id}.hmm')
        hmm_build(hmm_file, faa_file)

        # member file where COG info are stored
        # taxlvl, GroupName, ProteinCount, SpeciesCount, ProteinIDs, taxIDS
        protein_ids = [f'{tax}.fakegene{i}_{tax}' for tax in all_taxids[i:]]
        taxids = all_taxids[i:]
        protein_count = len(all_taxids[i:])
        species_count = len(all_taxids[i:])

        cog_stat_line = ['0', cog_id, str(protein_count),
                         str(species_count), ','.join(protein_ids), ','.join(taxids)]

        member_handler.write('\t'.join(cog_stat_line)+'\n')

        # extended memeber file --> needed to get gene name
        # HEADER : taxid_geneid, gene_name, specie, taxid, aliases
        gene_name = f'gene_{i}'
        extended_member_file = os.path.join(ext_mb_dir, f'{cog_id}.tsv')
        with open(extended_member_file, 'w') as fl:
            for protid, taxid in zip(protein_ids, taxids):
                extended_line = [protid, gene_name, f'sp_name_{taxid}', taxid, 'aliases:']
                fl.write('\t'.join(extended_line)+'\n')

    proteome_handler.close()
    member_handler.close()


def generate_ncbi_data_and_expected_result(coding_dna_seqs, fill_fct,
                                           missing_seq_prct_max, synthetic_ncbi_dir, expected_result_dir,
                                           nb_assembly, grp_size, distance_sep_max=1000, tax_level=0):
    """Generate ncbi data and expected results."""
    logging.info(f'{nb_assembly} synthetic assemblies is going to be created.')
    logging.info(f'A COG will be missing at most in {missing_seq_prct_max}% of assemblies')
    logging.info(f"COGs will be grouped by {grp_size} in genomes")
    logging.info(f'Each group will be separated by {distance_sep_max} random/N nucleotides')

    nb_assembly_without_grp_max = int(nb_assembly*(missing_seq_prct_max/100))

    expected_nb_grp = len(coding_dna_seqs)/grp_size
    group_of_seqs_index = list(split(list(range(len(coding_dna_seqs))), int(expected_nb_grp)))
    nb_grp = len(group_of_seqs_index)

    missing_by_grp = list(range(0, nb_assembly_without_grp_max+1,
                                int(round(nb_assembly_without_grp_max/nb_grp)+1)))
    missing_by_grp = [0]*(nb_grp-len(missing_by_grp)) + missing_by_grp
    for i, grp in enumerate(group_of_seqs_index):
        miss = missing_by_grp[i]
        sequence_str = ','.join([str(s) for s in grp[:-1]]) + f' and {grp[-1]}'
        logging.info(
            f'''Group {i}: made of sequences {sequence_str} will be missing in {round(miss/nb_assembly*100)}% ({miss}/{nb_assembly}) assemblies''')
    # logging.info("group_of_seqs_index", group_of_seqs_index)

    # logging.info(f' from 0 to {nb_assembly_without_grp_max} with step of ')
    # logging.info('from 0 to ', nb_assembly_without_grp_max, 'with step of ',
    # int(round(nb_assembly_without_grp_max/nb_grp)+1))

    ncbi_assemblies_dir = os.path.join(synthetic_ncbi_dir, 'assemblies')
    os.makedirs(ncbi_assemblies_dir, exist_ok=True)

    summary_dir = os.path.join(synthetic_ncbi_dir, '0000-00-00_refseq_summary')

    # logging.info("missing_by_grp", missing_by_grp)
    assembly_summary_lines = []
    rankedlineage_lines = []
    taxidlineage_lines = []
    pair_stat_by_pair = defaultdict(list)
    nb_genome_with_selected_pairs = defaultdict(int)
    for ass_i in range(1, nb_assembly+1):

        taxid = str(ass_i)
        # logging.info(ass_i)
        assembly_name = f'TES_{ass_i:09}.0'
        contig_name = f'CONT_{ass_i:04}.1'  # only one contig per genome
        # initial N in contig before coding sequence
        contig_seq_lst = [fill_fct(distance_sep_max)]

        # grp of seq in assembly
        grp_in_assembly = [(grp_i, grp_seq_indexs) for grp_i, grp_seq_indexs in enumerate(
            group_of_seqs_index) if ass_i not in range(nb_assembly)[:missing_by_grp[grp_i]]]

        seq_infos = []
        seq_infos_by_grp = defaultdict(list)
        modified_seqs = [None for _ in range(len(coding_dna_seqs))]
        # Generate contig seq
        for grp_i, grp_seq_indexs in grp_in_assembly:

            for seq_i in grp_seq_indexs:
                seq = coding_dna_seqs[seq_i]
                modified_seq = seq + random_codingSeq(size=random.gauss(mu=10, sigma=5))
                modified_seqs[seq_i] = modified_seq
                contig_seq_lst.append(modified_seq)
                contig_seq_lst.append(fill_fct(10))
                contig_seq_lst.append(fill_fct(ass_i % 20))  # introduce small variation in distance

            contig_seq_lst += fill_fct(distance_sep_max)

        contig_seq = ''.join(contig_seq_lst)

        # generate gff and faa and fna
        faa_records = []
        cont_length = len(contig_seq)
        gff_contig = [contig_name, 'SyntheticData', 'region',
                      '1', str(cont_length), '.', '+', '.',  'Is_circular=false']
        gff_lines = ['\t'.join(gff_contig)]

        # logging.info('grp_in_assembly', grp_in_assembly)
        # logging.info('seq_infos', len(seq_infos))
        for grp_i, grp_seq_indexs in grp_in_assembly:

            for seq_i in grp_seq_indexs:
                # logging.info('seq_i', seq_i)
                seq = modified_seqs[seq_i]
                protein_id = F'prt_synthetic_{ass_i}.{seq_i}'

                # GFF
                start = contig_seq.index(seq) + 1  # seq has to be uniq in contig seq
                end = start + len(seq) - 1
                gff_line = [contig_name, 'SyntheticData', 'CDS', str(start),
                            str(end), '.', '+', '.', f'Name={protein_id};ID={protein_id}']
                gff_lines.append('\t'.join(gff_line))

                # FAA
                seq_obj = Seq(seq)
                faa_record = SeqRecord(seq_obj.translate(), id=protein_id)
                faa_records.append(faa_record)
                seq_info = {'seq_i': seq_i, 'grp_i': grp_i, 'seq': seq, 'start': start,
                            'end': end, 'protein_id': protein_id,
                            'cog_id': f'COGtest0{seq_i}'
                            }
                seq_infos.append(seq_info)
                seq_infos_by_grp[grp_i].append(seq_info)
        # logging.info('seq in assembly',  sum([len(grp_seq_indexs)
        #                                       for grp_i, grp_seq_indexs in grp_in_assembly]))
        # logging.info('seq_infos', len(seq_infos))
        # logging.info(seq_infos)
        assert len(seq_infos) == sum([len(grp_seq_indexs)
                                      for grp_i, grp_seq_indexs in grp_in_assembly])
        # Write gff, faa, fna file
        preassembly_path = assembly_name.replace('_', '').split('.')[0]
        assembly_path = '/'.join([preassembly_path[index: index + 3]
                                  for index in range(0, len(preassembly_path), 3)])
        assembly_dir = os.path.join(ncbi_assemblies_dir, assembly_path, assembly_name)
        os.makedirs(assembly_dir, exist_ok=True)
        md5_sums = {}

        # FNA

        fna_file = os.path.join(assembly_dir, f'{assembly_name}_genomic.fna')
        contig_seq_obj = Seq(contig_seq)
        contig_record = SeqRecord(contig_seq_obj, id=contig_name)
        SeqIO.write(contig_record, fna_file, "fasta")
        md5_sums[md5(fna_file)] = fna_file

        # GFF
        gff_file = os.path.join(assembly_dir, f'{assembly_name}_genomic.gff')
        with open(gff_file, 'w') as handler:
            handler.write('##gff-version 3\n')
            handler.write('\n'.join(gff_lines)+'\n')
        md5_sums[md5(gff_file)] = gff_file
        # FAA
        faa_file = os.path.join(assembly_dir, f'{assembly_name}_protein.faa')
        with open(faa_file, 'w') as handler:
            for faa_record in faa_records:
                SeqIO.write(faa_record, handler, "fasta")
        md5_sums[md5(faa_file)] = faa_file

        with open(os.path.join(assembly_dir, 'md5checksums.txt'), 'w') as fl:
            for md5_sum, file in md5_sums.items():
                fl.write(f'{md5_sum}  ./{os.path.basename(file)}\n')
        # Generate expected selected region
        # logging.info('seq_infos', len(seq_infos))
        for grp_i, seq_infos_in_grp in seq_infos_by_grp.items():
            # all seqs are in a grp are supposed to be associated into a selected region
            for i, seq1_info in enumerate(seq_infos_in_grp):
                cogid_1 = seq1_info['cog_id']
                for seq2_info in seq_infos_in_grp:
                    cogid_2 = seq2_info['cog_id']
                    if cogid_1 > cogid_2:
                        cog_pair = '-'.join(sorted([cogid_2, cogid_1]))
                        nb_genome_with_selected_pairs[cog_pair] += 1

        # Generate expecetd pair info in genomes:
        # TODO : Sort seqinfo on seq start !!
        # logging.info('seq_infos', len(seq_infos))
        for i, seq1_info in enumerate(seq_infos):
            prot_id1 = seq1_info['protein_id']
            cogid_1 = seq1_info['cog_id']
            start1 = seq1_info['start']
            end1 = seq1_info['end']
            for seq2_info in seq_infos[i:]:

                prot_id2 = seq2_info['protein_id']
                cogid_2 = seq2_info['cog_id']
                start2 = seq2_info['start']
                end2 = seq2_info['end']
                pair_stat = {'cog_pair': f'{cogid_1}|{cogid_2}',
                             'genes': f'{prot_id1}|{prot_id2}',
                             'strands': '+|+',
                             'ids':f'{prot_id1}|{prot_id2}',
                             'positions': f'{start1}-{end1}|{start2}-{end2}',
                             'distance': end2-start1 + 1,
                             'genome': assembly_name,
                             'seqid': contig_name}
                cog_pair = '-'.join(sorted([cogid_2, cogid_1]))
                pair_stat_by_pair[cog_pair].append(pair_stat)
        # logging.info('pair_stat_by_pair', len(pair_stat_by_pair), pair_stat_by_pair)
        # pair stat header: cog_pair	genes	strands	positions	distance	genome	seqid

        # assembly_accession	bioproject	biosample	wgs_master	refseq_category	taxid
        # species_taxid	organism_name	infraspecific_name	isolate	version_status
        # assembly_level	release_type genome_rep	seq_rel_date	asm_name	submitter
        # gbrs_paired_asm	paired_asm_comp	ftp_path	excluded_from_refseq	relation_to_type_material
        assembly_summary = [assembly_name,  # assembly_accession
                            '',  # bioproject
                            '',  # biosample
                            '',  # wgs_master
                            'reference genome',  # refseq_category
                            taxid,  # taxid
                            taxid,  # species_taxid
                            '',  # organism_name
                            '',  # infraspecific_name
                            '',  # isolate
                            '',  # version_status
                            'Scaffold',  # assembly_level
                            "",  # release_type
                            "",  # genome_rep,
                            '',  # asm_name
                            "",  # seq_rel_date
                            '',  # submitter
                            '',  # gbrs_paired_asm
                            '',  # paired_asm_comp
                            f'ftp/path/to/the/assembly/{assembly_name}',  # ftp_path
                            '',  # excluded_from_refseq
                            '',  # relation_to_type_material
                            '',  # seq_rel_date
                            ]
        assembly_summary_lines.append('\t'.join(assembly_summary))

        # Make synthetic ncbi lineage data
        # Build taxidlineage.dmp found in new_taxdump.tar.gz
        # taxid is directly linked with tax level ==> very simple taxo
        taxids_lineage = f'{tax_level} {taxid}'
        taxidlineage_lines.append(f'{taxid}\t|\t131567 {taxids_lineage}\t|')

        # Build rankedlineage.dmp
        # 1005442	|	Escherichia coli 0.1288	|	Escherichia coli	|	Escherichia	|	Enterobacteriaceae	|	Enterobacterales	|	Gammaproteobacteria	|	Proteobacteria	|		|	Bacteria	|
        rankedlineage = [taxid,
                         f'synth{taxid} sp 0',  # Escherichia coli 0.1288]
                         f'synth{taxid} sp',  # Escherichia coli
                         f'synth{taxid}',  # Escherichia
                         f'synthetic_dataceae',  # Enterobacteriaceae
                         f'synthetic_datales',  # Enterobacterales
                         f'',  # Gammaproteobacteria
                         f'synthetic_data',  # Proteobacteria
                         f'',  # None
                         f'synthetic_data',  # Bacteria
                         ]
        rankedlineage_lines.append('\t|\t'.join(rankedlineage) + '\t|')

    summary_dir = os.path.join(synthetic_ncbi_dir, '0000-00-00_refseq_summary')
    os.makedirs(summary_dir, exist_ok=True)

    # Write summary file
    summary_file = os.path.join(summary_dir, 'assembly_summary_refseq.txt')
    logging.info(f'Write summary file: {summary_file}')
    with open(summary_file, 'w') as fl:
        fl.write('#   See ftp://ftp.ncbi.nlm.nih.gov/genomes/README_assembly_summary.txt for a description of the columns in this file.\n')
        fl.write('# assembly_accession	bioproject	biosample	wgs_master	refseq_category	taxid	species_taxid	organism_name	infraspecific_name	isolate	version_status	assembly_level	release_type	genome_rep	seq_rel_date	asm_name	submitter	gbrs_paired_asm	paired_asm_comp	ftp_path	excluded_from_refseq	relation_to_type_material\n')
        fl.write('\n'.join(assembly_summary_lines)+'\n')

    # write rankedlineage.dmp
    rankedlineage_file = os.path.join(summary_dir, 'rankedlineage.dmp')
    logging.info(f'write rankedlineage.dmp {rankedlineage_file}')
    # add root rank lineage
    taxid = '0'
    root_rankelineage = [taxid,
                         f'synth{taxid} sp 0',  # Escherichia coli 0.1288]
                         f'synth{taxid} sp',  # Escherichia coli
                         f'synth{taxid}',  # Escherichia
                         f'synthetic_dataceae',  # Enterobacteriaceae
                         f'synthetic_datales',  # Enterobacterales
                         f'',  # Gammaproteobacteria
                         f'synthetic_data',  # Proteobacteria
                         f'',  # None
                         f'synthetic_data',  # Bacteria
                         ]
    rankedlineage_lines = ['\t|\t'.join(root_rankelineage) + '\t|'] + rankedlineage_lines
    with open(rankedlineage_file, 'w') as fl:
        fl.write('\n'.join(rankedlineage_lines) + '\n')

    # write taxidlineage.dmp
    taxidlineage_file = os.path.join(summary_dir, 'taxidlineage.dmp')
    logging.info(f'write taxidlineage.dmp: {taxidlineage_file}')
    with open(taxidlineage_file, 'w') as fl:
        fl.write('\n'.join(taxidlineage_lines) + '\n')

    # Write expected pair stat file
    pair_stat_dir = os.path.join(expected_result_dir, 'cog_pairs')
    os.makedirs(pair_stat_dir, exist_ok=True)
    logging.info(f'Write expected pair stat file in dir: {pair_stat_dir}')
    for pair, stat in pair_stat_by_pair.items():
        pair_stat_file = os.path.join(pair_stat_dir, f'{pair}.tsv')
        list_of_dict_to_tsv(stat, pair_stat_file)

    # Write summary stat on selected pair
    summary_stat_pair_file = os.path.join(expected_result_dir, 'selected_pairs_stat.tsv')

    summary_pair_stats = []
    logging.info(f'Write summary stat on selected pair: { summary_stat_pair_file}')
    for pair, nb_genome in nb_genome_with_selected_pairs.items():
        stat = {'cog_pair': pair, 'nb_represented_genomes': nb_genome,
                'nb_genome_with_main_orga_post_len_filtering': nb_genome,
                "max_length_cutoff": distance_sep_max}

        summary_pair_stats.append(stat)

    list_of_dict_to_tsv(summary_pair_stats, summary_stat_pair_file)

    # Create a tar gz with taxidlineage and rankedlineage
    tax_archive_file = os.path.join(summary_dir, "new_taxdump.tar.gz")
    make_tarfile(tax_archive_file, taxidlineage_file, rankedlineage_file)


def make_tarfile(output_filename, *args):
    """Make a tar gz."""
    with tarfile.open(output_filename, "w:gz") as tar:
        for file in args:
            tar.add(file, arcname=os.path.basename(file))


def parse_arguments():
    """Parse script arguments."""
    parser = ArgumentParser(description="...",
                            formatter_class=ArgumentDefaultsHelpFormatter)
    parser.add_argument("-v", "--verbose", help="increase output verbosity",
                        action="store_true")
    parser.add_argument("--outdir", type=str, default='synthetic_data',
                        help="increase output verbosity")
    parser.add_argument("--tax_level", type=str, default='0',
                        help="")
    parser.add_argument("--nb_cogs", type=int, default=8,
                        help="Number of cogs created")
    parser.add_argument("--nb_assembly", type=int, default=100,
                        help="")
    parser.add_argument("--missing_seq_prct_max", type=int, default=5,
                        help="""Maximal percentage of assembly in which cogs will be missing.
                         A range of missing percenateg will be used from 0.""")
    parser.add_argument("--grp_size", type=int, default=2,
                        help="Cogs are grouped in assemblies. Size of the group")
    parser.add_argument("--length_cutoff", type=int, default=10000,
                        help="Number of separated nucleotide between groups of cogs")
    parser.add_argument("--fill_strategy", type=str, default='N', choices=['N', 'random'],
                        help="""Fill gap with N or random nucleotide. N allows to see clearly where genes are in the sequene.
                            Possible values""")
    args = parser.parse_args()
    return args


def main():
    """Generate synthetic data."""
    args = parse_arguments()

    if args.verbose:
        logging.basicConfig(format="%(levelname)s: %(message)s", level=logging.DEBUG)
        logging.info('Mode verbose ON')

    else:
        logging.basicConfig(format="%(levelname)s: %(message)s")

    number_synthetic_cog = args.nb_cogs
    logging.info(f'Creation of {number_synthetic_cog} synthetic COGs')
    tax_level = args.tax_level

    random.seed(0)

    synthetic_data_dir = args.outdir
    synthetic_eggnog_dir = f'{synthetic_data_dir}/eggnog_data/'
    synthetic_ncbi_dir = f'{synthetic_data_dir}/ncbi_data/'
    expected_result_dir = os.path.join(synthetic_data_dir, 'expected_result')

    os.makedirs(synthetic_eggnog_dir, exist_ok=True)
    os.makedirs(synthetic_ncbi_dir, exist_ok=True)
    os.makedirs(expected_result_dir, exist_ok=True)

    # Generate protein sequences in faa and in fna
    coding_dna_seqs = []
    for i in range(number_synthetic_cog):
        coding_dna_seq = random_codingSeq(random.gauss(mu=300, sigma=10))
        # coding_dna_seq = random_codingSeq(3)*100

        coding_dna_seqs.append(coding_dna_seq)

    generate_eggnog_data(coding_dna_seqs, synthetic_eggnog_dir, tax_level)

    nb_assembly = args.nb_assembly

    # In X genome prct the group of seq is missing
    missing_seq_prct_max = args.missing_seq_prct_max

    # two genes separeted by more than this distance will not be groupped by the pipeline
    distance_sep_max = args.length_cutoff

    # group of seq that will be close in assembly
    grp_size = args.grp_size

    fill_startegy = args.fill_strategy
    if fill_startegy == 'N':
        def fill_fct(n): return 'N'*n
    elif fill_startegy == 'random':
        def fill_fct(n): return ''.join([random.choice(['T', 'A', 'G', 'C']) for _ in range(n)])
    else:
        raise ValueError('')

    # generate_ncbi_data_and_expected_result
    generate_ncbi_data_and_expected_result(coding_dna_seqs, fill_fct,
                                           missing_seq_prct_max, synthetic_ncbi_dir, expected_result_dir,
                                           nb_assembly, grp_size, distance_sep_max, tax_level)


if __name__ == '__main__':
    main()
