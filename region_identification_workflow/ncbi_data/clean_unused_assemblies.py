import os
import re
import csv


def tsv_to_list_of_dict(file):
    """Load a tsv file into a list of dictionnary"""
    with open(file) as csvfile:
        reader = csv.DictReader(csvfile, delimiter='\t')
        return list(reader)


assembly_file = "../nextflow_result_2/selected_assemblies.tsv"

assemblies_info = tsv_to_list_of_dict(assembly_file)

assembly_accessions = [d['assembly_accession'] for d in assemblies_info]

capture_assemby_name = re.compile("^([A-Z]{3}_\d+.\d+)_")


tax_dir = 'assemblies/'
genomes_dir = [os.path.join(tax_dir, d) for d in os.listdir(tax_dir)]


for i, gdir in enumerate(genomes_dir):
    if i % 100 == 0:
        print('dir processed:', i)

    for f in os.listdir(gdir):
        # print(f)
        re_result = capture_assemby_name.search(f)
        assembly_name = re_result.group(1)
        # print(assembly_name)
        if assembly_name not in assembly_accessions:
            print(assembly_name, 'not in selected assembly')
            file_path = os.path.join(gdir, f)
            os.remove(file_path)

    # remove_empty_dir
    if len(os.listdir(gdir)) == 0:
        print('rm_dir', gdir)
        os.rmdir(gdir)
