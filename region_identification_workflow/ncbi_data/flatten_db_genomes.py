
import os
import csv


def parse_tsv_file(file):
    # parse file
    with open(file) as tsvfile:
        first_line = next(tsvfile)
        print(first_line)
        header_line = next(tsvfile)[2:].split('\t')
        reader = csv.DictReader(tsvfile, fieldnames=header_line, delimiter='\t')

        return list(reader)


taxid_genomes_dir = 'assemblies'
symblink_flat_db_dir = 'assemblies_flat'
assembly_summary_file = 'assembly_summary.txt'


os.makedirs(symblink_flat_db_dir, exist_ok=True)

genomes = parse_tsv_file(assembly_summary_file)
pwd = os.getcwd()
for d in genomes:
    if d['refseq_category'] != "na":
        taxid_dir = os.path.join(pwd, taxid_genomes_dir, f'taxid_{d["taxid"]}')

        genome_name = os.path.basename(d["ftp_path"])
        new_genome_dir = os.path.join(pwd, symblink_flat_db_dir, genome_name)
        os.makedirs(new_genome_dir, exist_ok=True)

        genome_files_of_interest = [f for f in os.listdir(
            taxid_dir) if f.startswith(genome_name) and f.endswith('.gz')]

        for f in genome_files_of_interest:
            src_f = os.path.join(taxid_dir, f)
            target_f = os.path.join(new_genome_dir, f)
            # input()
            try:
                os.symlink(src_f, target_f)
            except FileExistsError:
                print("file exist")
