import os
'''
Remove old assembly coming from GCA and db files build by blast
in order to have a clean genome db
'''
tax_dir = 'assemblies/'
genomes_dir = [os.path.join(tax_dir, d) for d in os.listdir(tax_dir)]


for i, gdir in enumerate(genomes_dir):
    if i % 100 == 0:
        print('dir processed:', i)

    for f in os.listdir(gdir):
        # print(f)

        if f.startswith("GCA") or f.endswith('.faa') or f.endswith('.faa.phr') or f.endswith('.faa.pin') or f.endswith('.faa.psq'):
            file_path = os.path.join(gdir, f)
            os.remove(file_path)

    # remove_empty_dir
    if len(os.listdir(gdir)) == 0:
        print('rm_dir', gdir)
        os.rmdir(gdir)
