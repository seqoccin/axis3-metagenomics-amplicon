#!/usr/bin/env python3

"""
Description

"""

# Metadata
__author__ = 'Mainguy Jean - Plateforme bioinformatique Toulouse'
__copyright__ = 'Copyright (C) 2019 INRA'
__license__ = 'GNU General Public License'
__version__ = '0.1'
__email__ = 'support.bioinfo.genotoul@inra.fr'
__status__ = 'dev'


from argparse import ArgumentParser, ArgumentDefaultsHelpFormatter
import logging
import os
import re
import shutil


def parse_arguments():
    """Parse script arguments."""
    parser = ArgumentParser(description="...",
                            formatter_class=ArgumentDefaultsHelpFormatter)
    parser.add_argument("--flatdir", help="increase output verbosity", required=True, type=str)
    parser.add_argument('-o', "--out_treedir",
                        help="increase output verbosity", default='assemblies')
    parser.add_argument("-v", "--verbose", help="increase output verbosity",
                        action="store_true")

    args = parser.parse_args()
    return args


def main():

    args = parse_arguments()

    if args.verbose:
        logging.basicConfig(format="%(levelname)s: %(message)s", level=logging.DEBUG)
        logging.info('Mode verbose ON')

    else:
        logging.basicConfig(format="%(levelname)s: %(message)s")

    flatdir = args.flatdir
    root_treedir = args.out_treedir
    # should match assembly accesion. ie GCF_006094695.1
    pattern = re.compile("[A-Z]{3}_[0-9]{9}\.[0-9]+")
    for assembly_dir in os.listdir(flatdir):
        if not pattern.match(assembly_dir):
            continue

        logging.info(f'Assembly dir to transform {assembly_dir}')
        preassembly_path = assembly_dir.replace('_', '').split('.')[0]
        assembly_path = '/'.join([preassembly_path[index: index + 3]
                                  for index in range(0, len(preassembly_path), 3)])
        old_flat_path = os.path.join(flatdir, assembly_dir)
        new_dir_path = os.path.join(root_treedir, assembly_path)

        logging.info(f'mv {old_flat_path} to {new_dir_path}')
        #input()
        os.makedirs(new_dir_path, exist_ok=True)
        shutil.move(old_flat_path, new_dir_path)


if __name__ == '__main__':
    main()
