

# module load system/Anaconda3-5.2.0
# conda activate jupy

# get refseq_summary
refseq_summary_file='ncbi_data/bacteria_assembly_summary.txt'
wget ftp://ftp.ncbi.nlm.nih.gov/genomes/refseq/bacteria/assembly_summary.txt -O $refseq_summary_file

# get header
head -n2 $refseq_summary_file > ncbi_data/summary_header.txt

tail -n+3 $refseq_summary_file > ncbi_data/summary_no_header.txt

# split it
rm -r ncbi_data/summary_chuncks/
mkdir ncbi_data/summary_chuncks/


split -l 4000 -d ncbi_data/summary_no_header.txt ncbi_data/summary_chuncks/chunks_



for f in ncbi_data/summary_chuncks/chunks_*; do
  echo $f
  cat ncbi_data/summary_header.txt > ${f}_with_header
  cat $f >> ${f}_with_header
  cat ncbi_data/sbatch_header.txt > launcher_refseq_download.sbatch
  echo "time python scripts/download_refseq_assemblies.py --assembly_summary ${f}_with_header" >> launcher_refseq_download.sbatch
  sbatch launcher_refseq_download.sbatch


done
