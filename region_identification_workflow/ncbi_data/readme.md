## Structure of the directory

`assemblies/`: assembly files organise by taxid
`assemblies_flat`: assembly files organise by assembly name

## Estimation of the size of the db with all the genomes:

```

```



size of the current db  11603232 KO (12G)
number of genome in the current db = 5518

mean size of a genome: 11603232 / 5518 = 2103 Ko /genome

nb of genomes in refseq = 155064

Estimation of the db with all refseq genomes : 155064 * 2103 = 326099592 = 326 G


## What is the range of the biggest genomes of our db?

Get the biggest sequences we have for a pair
```
more nextflow_result_2/cog_pairs/COG0200-COG0201.tsv  |cut -f5  | sort -n  | tail
```
For this pair we have this genomes the circular sequence make the whole genome and has a length of 13033735:
```
cog_pair	genes	strands	positions	distance	genome	seqid
COG0200|COG0201	WP_012240553.1|WP_012240552.1	-|-	11068732-11069223|11067359-11068687	13033735	GCF_000067165.1_ASM6716v1	NC_010162.1
```

on all pair
`cat nextflow_result_2/cog_pairs/*.tsv  |cut -f5 | sort -rn | more`

```
13033780
13033780
13033777
13033777
13033777
```
`nextflow_result_2/cog_pairs/COG0096-COG0097.tsv:COG0096|COG0097	WP_012240558.1|WP_012240557.1	-|-	11071192-11071587|11070575-11071192	13033780	GCF_000067165.1_ASM6716v1	NC_010162.1`

GCF_000067165: length of
Genomes with at least a size of 13 M base:

https://www.ncbi.nlm.nih.gov/assembly/GCF_000067165.1/   

Total sequence length = 13,033,779

belong to the order Myxobacteria known to have big genome:  https://en.wikipedia.org/wiki/Myxobacteria


## downloading of all refseq
With Several attempts
each time changing a little bit the code to prevent the crash of the script

Use of `ncbi_data/download_refseq_assembly_by_chunck.sh` to download and split the summary file and launch the downloading of the chunks via sbatch

First attempt divide the summary into 10 chuncks: all job crashed around ~8h --> probably ncbi was no avalaible at that time
Add sleep second
Second attempt divide into 21 chuncks:
* 5 jobs crashed after 11 hours due to faillure in md5 file download
* 16 jobs completed with a run time between 14h and 26h

Third attempt divide the summary into chuncks of 2000 genomes and change memory required for the sbatch to 500M instead of default memory.
Large range of job duration due to some genomes were already download. Randomly mix the summary would have smooth the time disparity


seff off the longuest job:

```
Job ID: 7742850
Cluster: genobull
User/Group: jmainguy/BIOINFO
State: COMPLETED (exit code 0)
Cores: 1
CPU Utilized: 00:05:53
CPU Efficiency: 0.37% of 1-02:28:36 core-walltime
Job Wall-clock time: 1-02:28:36
Memory Utilized: 60.32 MB
Memory Efficiency: 2.95% of 2.00 GB
```

Downloading speed :
**3950 genomes / 612m30.948s** = **6.4 genome/min**

Final size of the database : **356G**


## reorganizing all refseq assemblies dir

To prevent excessive amount of items in a dir we can organize assembly as it is organize on the ncbi by spliting the name of the assembly accession into multiple directory:


for GCF_000271165: `/GCF/000/271/165/GCF_000271165.1_ASM27116v1`

In bash we can use this command
`echo GCF_000216195.1 | cut -d "." -f1 | sed 's/_//' |  sed 's/.\{3\}/&\//g'`
