#!/usr/bin/env python3

"""
Description

:Example:

python template.py -o sample_test_nextflow_result_2157/2020-01-21_refseq_summary/result_plots/ \
                            --main_template html_report/result_report_template.html \
                            --region_template html_report/template_report_region.html \
        --cog_table sample_test_nextflow_result_2157/2020-01-21_refseq_summary/2157_identified_universal_ogs.tsv \
        --region_table  sample_test_nextflow_result_2157/2020-01-21_refseq_summary/selected_selected_pairs_stat.tsv

"""

# Metadata
__author__ = 'Mainguy Jean - Plateforme bioinformatique Toulouse'
__copyright__ = 'Copyright (C) 2019 INRA'
__license__ = 'GNU General Public License'
__version__ = '0.1'
__email__ = 'support.bioinfo.genotoul@inra.fr'
__status__ = 'dev'


from argparse import ArgumentParser, ArgumentDefaultsHelpFormatter
import logging
import os
import pandas as pd


def parse_arguments():
    parser = ArgumentParser(description="...",
                            formatter_class=ArgumentDefaultsHelpFormatter)
    parser.add_argument("-o", "--outdir", help="Output dir", default='./')
    parser.add_argument("--main_template", help="Output dir",)
    # parser.add_argument("--region_template", help="Output dir",)
    parser.add_argument("--cog_table", help="Output dir",)
    parser.add_argument("--region_table", help="Output dir",)
    parser.add_argument("-v", "--verbose", help="increase output verbosity",
                        action="store_true")

    args = parser.parse_args()
    return args


def main():

    args = parse_arguments()

    if args.verbose:
        logging.basicConfig(format="%(levelname)s: %(message)s", level=logging.DEBUG)
        logging.info('Mode verbose ON')

    else:
        logging.basicConfig(format="%(levelname)s: %(message)s")

    outdir = args.outdir
    main_template = args.main_template
    # region_template = args.region_template
    cog_table = args.cog_table
    region_table = args.region_table

    with open(main_template) as fl:
        template_content = [l for l in fl]

    # Parse cog table and format it
    df_cog = pd.read_csv(cog_table, sep='\t')
    df_cog.rename(columns={'GroupName': 'COG id', 'cog_gene_name': 'Gene',
                           'SpeciesCount': 'Species with COG', 'nb_single_copy': 'Species with single copy'}, inplace=True)
    df_cog = df_cog[['COG id', 'Gene', 'Species with COG', 'Species with single copy']]
    # df.to_html(table_id='cog_selection', render_links=True)

    # Insert cog table:
    balise_table = ' <!-- insert_table:cog_selection -->'
    for i, l in enumerate(template_content):
        if l.rstrip() == balise_table:
            logging.info(l)
            template_content[i] = df_cog.to_html(
                table_id='cog_selection', render_links=True, justify='center')
        else:
            pass

    # Parse region table:
    selected_pairs = region_table
    df_region = pd.read_csv(selected_pairs, sep='\t')
    logging.info(df_region.columns)

    # create region html plot
    from shutil import copyfile

    # for region in df_region['cog_pair']:
    #     region_plot_dir = os.path.join(outdir, f'result_plots/{region}')
    #     os.makedirs(region_plot_dir,  exist_ok=True)
    #     # logging.info(os.listdir(region_plot_dir))
    #     copyfile(region_template, os.path.join(region_plot_dir, 'result_region.html'))

    column_new_names = {'cog_pair': "Cog pair",
                        "nb_genome_with_main_orga_post_len_filtering": "Genomes covered",
                        'mean_post_len_filtering': 'mean (pb)',
                        'median_post_len_filtering': 'median (pb)',
                        'std_post_len_filtering': 'standard deviation',
                        'min_post_len_filtering': 'minimal length',
                        'max_post_len_filtering': 'maximum length',
                        'nb_problematic_pairs_post_len_filtering': 'region duplication', }
    df_region = df_region.rename(columns=column_new_names)
    pd.set_option('display.max_colwidth', 1)
    new_names = column_new_names.values()
    df_region['Cog pair'] = '<a href="result_plots/'+df_region["Cog pair"] + \
        '/result_region.html" target="_blank">'+df_region['Cog pair'] + '</a>'

    # df_region['Cog pair'] = '<a href="' + df_region["Cog pair"] + '/result_region.html' + '</a>'

    df_region = df_region[new_names]

    balise_table = ' <!-- insert_table:region_selection_tab -->'
    for i, l in enumerate(template_content):
        if l.rstrip() == balise_table:
            logging.info(l)
            template_content[i] = df_region.to_html(
                table_id='region_selection_tab', justify='center', escape=False, render_links=True,)
            # template_content[i] += s
        else:
            pass

    report_file = os.path.join(outdir, 'main_report_result.html')
    with open(report_file, 'w') as fl:
        fl.write(''.join(template_content))
    logging.info(report_file)


if __name__ == '__main__':
    main()
