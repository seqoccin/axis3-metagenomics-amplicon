#!/usr/bin/env python3

"""
Description

:Example:

...
"""

# Metadata
__author__ = 'Mainguy Jean - Plateforme bioinformatique Toulouse'
__copyright__ = 'Copyright (C) 2019 INRA'
__license__ = 'GNU General Public License'
__version__ = '0.1'
__email__ = 'support.bioinfo.genotoul@inra.fr'
__status__ = 'dev'


from argparse import ArgumentParser, ArgumentDefaultsHelpFormatter
import logging
import os
import pandas as pd
import datetime


def parse_arguments():
    parser = ArgumentParser(description="...",
                            formatter_class=ArgumentDefaultsHelpFormatter)
    parser.add_argument("-o", "--outdir", help="Output dir", default='./')
    parser.add_argument("--html_template", help="Output dir",)
    # parser.add_argument("--region_template", help="Output dir",)
    parser.add_argument(
        "--parameters", help="Workflow parameters to be displayed in a table and in a flowchart",)
    parser.add_argument("--assembly_stat",
                        help="general stat on the assembly selection", required=True)
    parser.add_argument("--taxon_lvl_name",
                        help="Taxon level name for example Bacteria", required=True)
    parser.add_argument("--cog_selection_stat",
                        help="basic stat on the cog selection", required=True)
    parser.add_argument("--selected_pairs",
                        help="List of the selected pairs", required=True)
    parser.add_argument("-v", "--verbose", help="increase output verbosity",
                        action="store_true")

    args = parser.parse_args()
    return args


def main():

    args = parse_arguments()

    if args.verbose:
        logging.basicConfig(format="%(levelname)s: %(message)s", level=logging.DEBUG)
        logging.info('Mode verbose ON')

    else:
        logging.basicConfig(format="%(levelname)s: %(message)s")

    outdir = args.outdir
    template = args.html_template
    params_file = args.parameters
    assembly_stat = args.assembly_stat
    taxon_lvl_name = args.taxon_lvl_name
    cog_selection_stat_file = args.cog_selection_stat
    selected_pairs_fl = args.selected_pairs

    with open(selected_pairs_fl) as fl:
        selected_pairs = [l.strip() for l in fl if l]

    with open(template) as fl:
        template_content = [l for l in fl]

    with open(params_file) as fl:
        params = {l.split(':')[0]: l.rstrip().split(':')[1] for l in fl if l.strip()}

    with open(cog_selection_stat_file) as fl:
        cog_selection_stat = {l.split(':')[0]: l.rstrip().split(':')[1] for l in fl if l.strip()}

    df_param = pd.DataFrame.from_dict({k: [v] for k, v in params.items()}, orient='index')

    # Insert params table:
    balise_table = '<!-- insert_table:parameter -->'
    for i, l in enumerate(template_content):
        if l.strip() == balise_table:
            logging.info(l)
            template_content[i] = df_param.to_html(
                table_id='parameter', render_links=True, justify='center', header=False)

    # assembly_stat
    df_assembly_stat = pd.read_csv(assembly_stat, sep='\t', index_col="selection")

    # df_assembly_stat.loc['all_assemblies']['assembly']

    taxon = taxon_lvl_name
    nb_all_cogs = cog_selection_stat['eggnog_cogs']
    cog_prct = params['cog_prct']
    cog_single_copy_prct = params['cog_single_copy_prct']
    nb_cog_selected = cog_selection_stat['eggnog_selected_cogs']
    nb_sp_eggnog = cog_selection_stat['eggnog_species']

    nb_all_assemblies = df_assembly_stat.loc['all_assemblies']['assembly']
    nb_all_species = df_assembly_stat.loc['all_assemblies']['species']
    # 2020-03-17_refseq_summary finally format it to get day/month/year

    year, month, day = params['refseq_date_dir'].split('_')[0].split('-')
    try:
        refseq_date = datetime.datetime(int(year), int(month), int(day)).strftime("%b %d %Y")
    except ValueError:
        logging.warning(
            f"the date associated to refseq directory {params['refseq_date_dir']} is not shaped as a date. ")
        refseq_date = params['refseq_date_dir'].split('_')[0]

    primary_assemblies_selection = params['primary_assemblies_selection']
    primary_assemblies_selection_description = df_assembly_stat.loc[primary_assemblies_selection]['description']
    nb_assemblies_primary = df_assembly_stat.loc[primary_assemblies_selection]['assembly']
    nb_sp_primary = df_assembly_stat.loc[primary_assemblies_selection]['species']

    extended_assemblies_selection = params['extended_assemblies_selection']
    extended_assemblies_selection_description = df_assembly_stat.loc[extended_assemblies_selection]['description']
    nb_assemblies_extended = df_assembly_stat.loc[extended_assemblies_selection]['assembly']
    nb_sp_extended = df_assembly_stat.loc[extended_assemblies_selection]['species']

    nb_potential_pair = 'All potential'
    length_cutoff = params['length_cutoff']
    region_genome_prct_threshold = params['region_genome_prct_threshold']

    nb_selected_region = len(selected_pairs)

    mermaid_diagram = f"""
      graph TD
          eggNOG[("eggNOG v5.0<br>{taxon}<br>{nb_all_cogs} orthologous groups (OGs)<br>{nb_sp_eggnog} species represented")]

          cogselection(Selection of universal single copy OGs<br>sp represented > {cog_prct}%<br>single copy > {cog_single_copy_prct}%)

          COG[{nb_cog_selected} selected OGs]

          RefSeq[(RefSeq {taxon}<br>{refseq_date}<br>{nb_all_assemblies} assemblies<br>{nb_all_species} species represented)]

          assemblie1[{primary_assemblies_selection}<br>{nb_assemblies_primary} assemblies<br>{nb_sp_primary} species represented]
          assemblie2[{extended_assemblies_selection}<br>{nb_assemblies_extended} assemblies<br>{nb_sp_extended} species represented]

          assemblyselection(Assembly selection)

          fetchmgs(Genes identification with fetchMGs)

          Genes[Universal and single copy genes<br>in RefSeq assemblies]

          pairing(Genes pairing)

          Pair[{nb_potential_pair} pairs of genes<br>in all of the genomes]

          pairselection(Pair selection:<br>length < {int(length_cutoff)/1000} kb<br>same orientation in > {region_genome_prct_threshold}% of the assemblies)

          BestPair[{nb_selected_region} selected region{'' if nb_selected_region == 0 else 's'}]

          extraction(Region sequences extraction)
          Sequences[Region sequences]

          eggNOG --- cogselection;
          cogselection --> COG;
          COG --- fetchmgs;
          RefSeq --- assemblyselection;
          assemblyselection -->|Primary assembly selection| assemblie1;
          assemblie1 --- fetchmgs;
          fetchmgs --> Genes;
          Genes --- pairing;
          pairing --> Pair;
          Pair --- pairselection;
          pairselection --> BestPair;
          BestPair --- extraction;
          assemblie2 --- extraction;

          extraction --> Sequences;
          assemblyselection -->|Extended assembly selection| assemblie2;


          classDef default fill:#99999,stroke:#555555;

          classDef action fill:#ffffff,stroke:#ffffff,stroke-width:0px;
          class assemblyselection,cogselection,fetchmgs,pairing,pairselection,extraction action;

          style BestPair fill:#feaf3e
    """

    # Insert params table:
    balise_diag = '%% insert_workflow_diagram'
    for i, l in enumerate(template_content):
        if l.strip() == balise_diag:
            logging.info(l)
            template_content[i] = mermaid_diagram

    report_file = os.path.join(outdir, 'result_report_intro.html')
    with open(report_file, 'w') as fl:
        fl.write(''.join(template_content))
    logging.info(report_file)


if __name__ == '__main__':
    main()
