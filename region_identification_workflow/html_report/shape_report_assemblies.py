#!/usr/bin/env python3

"""
Description

:Example:

python template.py -o sample_test_nextflow_result_2157/2020-01-21_refseq_summary/result_plots/ \
                            --main_template html_report/result_report_template.html \
                            --region_template html_report/template_report_region.html \
        --cog_table sample_test_nextflow_result_2157/2020-01-21_refseq_summary/2157_identified_universal_ogs.tsv \
        --region_table  sample_test_nextflow_result_2157/2020-01-21_refseq_summary/selected_selected_pairs_stat.tsv

"""

# Metadata
__author__ = 'Mainguy Jean - Plateforme bioinformatique Toulouse'
__copyright__ = 'Copyright (C) 2019 INRA'
__license__ = 'GNU General Public License'
__version__ = '0.1'
__email__ = 'support.bioinfo.genotoul@inra.fr'
__status__ = 'dev'


from argparse import ArgumentParser, ArgumentDefaultsHelpFormatter
import logging
import os
import pandas as pd
import datetime


def parse_arguments():
    parser = ArgumentParser(description="...",
                            formatter_class=ArgumentDefaultsHelpFormatter)
    parser.add_argument("-o", "--outdir", help="Output dir", default='./')
    parser.add_argument("--html_template", help="html template",)
    parser.add_argument("--taxon_lvl_name",
                        help="Taxon level name for example Bacteria", required=True)
    parser.add_argument("--parameters",
                        help="Workflow parameters to be displayed in a table and in a flowchart",)
    parser.add_argument("--assembly_stat",
                        help="general stat on the assembly selection", required=True)
    parser.add_argument("-v", "--verbose", help="increase output verbosity",
                        action="store_true")

    args = parser.parse_args()
    return args


def main():

    args = parse_arguments()

    if args.verbose:
        logging.basicConfig(format="%(levelname)s: %(message)s", level=logging.DEBUG)
        logging.info('Mode verbose ON')

    else:
        logging.basicConfig(format="%(levelname)s: %(message)s")

    outdir = args.outdir
    html_template = args.html_template

    taxon_lvl_name = args.taxon_lvl_name
    params_file = args.parameters
    assembly_stat = args.assembly_stat

    with open(html_template) as fl:
        template_content = [l for l in fl]

    with open(params_file) as fl:
        params = {l.split(':')[0]: l.rstrip().split(':')[1] for l in fl if l.strip()}

    df_assembly_stat_to_write = pd.read_csv(assembly_stat, sep='\t')
    # Insert assembly selection stat table:
    balise_table = "<!-- insert_table:selection_stat -->"
    for i, l in enumerate(template_content):
        if l.strip() == balise_table:
            logging.info(l)
            template_content[i] = df_assembly_stat_to_write.to_html(
                table_id='selection_stat', render_links=True, justify='center', index=False)

    df_assembly_stat = pd.read_csv(assembly_stat, sep='\t', index_col="selection")

    nb_all_assemblies = df_assembly_stat.loc['all_assemblies']['assembly']
    nb_all_species = df_assembly_stat.loc['all_assemblies']['species']
    taxon = taxon_lvl_name
    year, month, day = params['refseq_date_dir'].split('_')[0].split('-')
    try:
        refseq_date = datetime.datetime(int(year), int(month), int(day)).strftime("%b %d %Y")
    except ValueError:
        logging.warning(
            f"the date associated to refseq directory {params['refseq_date_dir']} is not shaped as a date. ")
        refseq_date = params['refseq_date_dir'].split('_')[0]

    primary_assemblies_selection = params['primary_assemblies_selection']
    primary_assemblies_selection_description = df_assembly_stat.loc[primary_assemblies_selection]['description']
    nb_assemblies_primary = df_assembly_stat.loc[primary_assemblies_selection]['assembly']
    nb_sp_primary = df_assembly_stat.loc[primary_assemblies_selection]['species']

    extended_assemblies_selection = params['extended_assemblies_selection']
    extended_assemblies_selection_description = df_assembly_stat.loc[extended_assemblies_selection]['description']
    nb_assemblies_extended = df_assembly_stat.loc[extended_assemblies_selection]['assembly']
    nb_sp_extended = df_assembly_stat.loc[extended_assemblies_selection]['species']

    mermaid_diagram = f"""
    graph TD

        RefSeq[(RefSeq {taxon}<br>{refseq_date}<br>{nb_all_assemblies} assemblies<br>{nb_all_species} species represented)]

        assemblyselection(Assembly selection)

        assemblie1[{primary_assemblies_selection}<br>{nb_assemblies_primary} assemblies<br>{nb_sp_primary} species represented]
        assemblie2[{extended_assemblies_selection}<br>{nb_assemblies_extended} assemblies<br>{nb_sp_extended} species represented]

        identification(Region identification)
        regions[Selected regions]

        extraction(Region extraction)

        Sequences[Region sequences]

        RefSeq --- assemblyselection;
        assemblyselection -->|Primary assembly selection| assemblie1;
        assemblyselection -->|Extended assembly selection| assemblie2;

        assemblie1 --- identification
        identification --> regions
        regions --- extraction
        assemblie2 --- extraction;

        extraction --> Sequences;



    classDef default fill:#99999,stroke:#555555;

    classDef action fill:#ffffff,stroke:#ffffff,stroke-width:0px;
    class assemblyselection,identification,extraction action;

    """
    # Insert params table:
    balise_diag = '%% insert_assembly_diagram'
    for i, l in enumerate(template_content):
        if l.strip() == balise_diag:
            logging.info(l)
            template_content[i] = mermaid_diagram

    report_file = os.path.join(outdir, os.path.basename(html_template))
    with open(report_file, 'w') as fl:
        fl.write(''.join(template_content))
    logging.info(report_file)


if __name__ == '__main__':
    main()
