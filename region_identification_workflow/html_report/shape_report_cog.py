#!/usr/bin/env python3

"""
Description

:Example:

...
"""

# Metadata
__author__ = 'Mainguy Jean - Plateforme bioinformatique Toulouse'
__copyright__ = 'Copyright (C) 2019 INRA'
__license__ = 'GNU General Public License'
__version__ = '0.1'
__email__ = 'support.bioinfo.genotoul@inra.fr'
__status__ = 'dev'


from argparse import ArgumentParser, ArgumentDefaultsHelpFormatter
import logging
import os
import pandas as pd


def parse_arguments():
    parser = ArgumentParser(description="...",
                            formatter_class=ArgumentDefaultsHelpFormatter)
    parser.add_argument("-o", "--outdir", help="Output dir", default='./')
    parser.add_argument("--html_template", help="Output dir", required=True)
    # parser.add_argument("--region_template", help="Output dir",)
    parser.add_argument("--cog_table", help="Output dir", required=True)
    parser.add_argument("-v", "--verbose", help="increase output verbosity",
                        action="store_true")

    args = parser.parse_args()
    return args


def main():

    args = parse_arguments()

    if args.verbose:
        logging.basicConfig(format="%(levelname)s: %(message)s", level=logging.DEBUG)
        logging.info('Mode verbose ON')

    else:
        logging.basicConfig(format="%(levelname)s: %(message)s")

    outdir = args.outdir
    html_template = args.html_template
    cog_table = args.cog_table

    with open(html_template) as fl:
        template_content = [l for l in fl]

    # Parse cog table and format it
    df_cog = pd.read_csv(cog_table, sep='\t')
    df_cog.rename(columns={'GroupName': 'COG id', 'cog_gene_name': 'Gene',
                           'SpeciesCount': 'Species with COG', 'nb_single_copy': 'Species with single copy'}, inplace=True)
    df_cog = df_cog[['COG id', 'Gene', 'Species with COG', 'Species with single copy']]
    # df.to_html(table_id='cog_selection', render_links=True)

    # Insert cog table:
    balise_table = '<!-- insert_table:cog_selection -->'
    for i, l in enumerate(template_content):
        if l.strip() == balise_table:
            logging.info(l)
            template_content[i] = df_cog.to_html(
                table_id='cog_selection', render_links=True, justify='center')

    report_file = os.path.join(outdir, os.path.basename(html_template))
    with open(report_file, 'w') as fl:
        fl.write(''.join(template_content))
    logging.info(report_file)


if __name__ == '__main__':
    main()
