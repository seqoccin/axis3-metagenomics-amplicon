#!/usr/bin/env python3

"""
Description

:Example:

python template.py -o sample_test_nextflow_result_2157/2020-01-21_refseq_summary/result_plots/ \
                            --main_template html_report/result_report_template.html \
                            --region_template html_report/template_report_region.html \
        --cog_table sample_test_nextflow_result_2157/2020-01-21_refseq_summary/2157_identified_universal_ogs.tsv \
        --region_table  sample_test_nextflow_result_2157/2020-01-21_refseq_summary/selected_selected_pairs_stat.tsv

"""

# Metadata
__author__ = 'Mainguy Jean - Plateforme bioinformatique Toulouse'
__copyright__ = 'Copyright (C) 2019 INRA'
__license__ = 'GNU General Public License'
__version__ = '0.1'
__email__ = 'support.bioinfo.genotoul@inra.fr'
__status__ = 'dev'


from argparse import ArgumentParser, ArgumentDefaultsHelpFormatter
import logging
import os
import pandas as pd


def parse_arguments():
    parser = ArgumentParser(description="...",
                            formatter_class=ArgumentDefaultsHelpFormatter)
    parser.add_argument("-o", "--outdir", help="Output dir", default='./')
    parser.add_argument("--html_template", help="", required=True)
    parser.add_argument("--region_tables_stat",  nargs='+', help="", required=True)
    parser.add_argument("-v", "--verbose", help="increase output verbosity",
                        action="store_true")

    args = parser.parse_args()
    return args


def main():

    args = parse_arguments()

    if args.verbose:
        logging.basicConfig(format="%(levelname)s: %(message)s", level=logging.DEBUG)
        logging.info('Mode verbose ON')

    else:
        logging.basicConfig(format="%(levelname)s: %(message)s")

    outdir = args.outdir
    template = args.html_template
    region_tables = args.region_tables_stat

    with open(template) as fl:
        template_content = [l for l in fl]

    column_new_names = {'cog_pair': "Region",
                        "nb_genome_with_main_orga_post_len_filtering": "Genomes covered",
                        'mean_post_len_filtering': 'mean (pb)',
                        'median_post_len_filtering': 'median (pb)',
                        'std_post_len_filtering': 'standard deviation',
                        'min_post_len_filtering': 'minimal length',
                        'max_post_len_filtering': 'maximum length',
                        'nb_problematic_pairs_post_len_filtering': 'region duplication', }

    # Insert tables in tab
    for table_i, region_table_file in enumerate(region_tables):
        df_region = pd.read_csv(region_table_file, sep='\t')
        logging.info(f'Processing region table stat {region_table_file}')
        table_name = os.path.basename(region_table_file)[len('regions_stat_'):-len('.tsv')]

        df_region = df_region.rename(columns=column_new_names)
        pd.set_option('display.max_colwidth', 1)
        new_names = column_new_names.values()
        df_region['Region'] = '<a href="result_plots/'+df_region["Region"] + \
            '/result_region.html" target="_blank">'+df_region['Region'] + '</a>'

        # df_region['Cog pair'] = '<a href="' + df_region["Cog pair"] + '/result_region.html' + '</a>'

        df_region = df_region[new_names]
        balise_tab = f'<!-- insert_tab_line:table{table_i} -->'
        balise_table = f'<!-- insert_table:table{table_i} -->'
        for i, l in enumerate(template_content):
            if l.strip() == balise_tab:
                logging.info(l)
                template_content[i] = f'<a href="#tab-table{table_i}" data-toggle="tab">{table_name}</a>\n'

            if l.strip() == balise_table:
                logging.info(l)
                # id="myTable1" class="table table-striped table-bordered" width="100%" cellspacing="0"
                template_content[i] = df_region.to_html(
                    table_id='region_selection_tab', classes="table table-striped table-bordered", justify='center', escape=False, render_links=True)
                # template_content[i] += s
            else:
                pass
    report_file = os.path.join(outdir, 'result_report_region.html')
    with open(report_file, 'w') as fl:
        fl.write(''.join(template_content))
    logging.info(report_file)


if __name__ == '__main__':
    main()
