# Identification of region for taxonomic profiling using long-read sequencing technology


## Introduction
This workflow identifies and assesses potential target regions in prokaryotic genomes to be used in long read metabarcoding.


Target regions in long reads sequencing approaches should be:
- Flanked by conserved region in order to be able to amplify the region by PCR.
- Diverse and long enough to identify species.
- In single copy to better estimate relative abundance.

To discover such regions, the workflow identifies regions bounded by two universal and single copy genes that have a consistent lengths within the genomes.

Universal and single copy genes are first identified using eggNOG database and then retrieved in refSeq genomes.
Each potential pairs of COGs are formed and evaluated. Then the ones meeting selection criteria (consistent distance, homogenous genes orientation.. )  are selected and extracted.

### General diagram of the workflow
```mermaid
graph TD
				eggNOG[(eggNOG v5.0)]

				cogselection(Selection of universal<br>single copy genes)

				COG[Universal single copy genes]

				RefSeq[(RefSeq Assemblies)]

				assemblie1[Primary assembly selection]
				assemblie2[Extended assembly selection]

				assemblyselection(Assembly selection)

				fetchmgs(Genes identification with fetchMGs)

				Genes[Universal and single copy genes in RefSeq]

				pairing(Genes pairing)

				Pair[Pairs of genes in all of the genomes]

				pairselection(Region selection)

				BestPair[Selected regions]

				extraction(Region sequences extraction)
				Sequences[Region sequences]

				eggNOG --- cogselection;
				cogselection --> COG;
				COG --- fetchmgs;
				RefSeq --- assemblyselection;
				assemblyselection --> assemblie1;
				assemblie1 --- fetchmgs;
				fetchmgs --> Genes;
				Genes --- pairing;
				pairing --> Pair;
				Pair --- pairselection;
				pairselection --> BestPair;
				BestPair --- extraction;
				assemblie2 --- extraction;

				extraction --> Sequences;
				assemblyselection --> assemblie2;


				classDef default fill:#99999,stroke:#555555;

				classDef action fill:#ffffff,stroke:#ffffff,stroke-width:0px;
				class assemblyselection,cogselection,fetchmgs,pairing,pairselection,extraction action;

				style BestPair fill:#feaf3e
```



## Running the pipeline:

The typical command for running the pipeline is as follows:

```bash
nextflow run main.nf --tax_level 2157  -profile singularity
```

This will launch the pipeline with the `singularity` configuration profile. See below for more information about profiles.

Note that the pipeline will create the following files in your working directory:

```bash
work            # Directory containing the nextflow working files
results         # Finished results (configurable, see below)
.nextflow_log   # Log file from Nextflow
# Other nextflow hidden files, eg. history of pipeline runs and old logs.
```

The taxonomic level 2157 refers to Archea. So the workflow is going to identify conserved region in archeal genomes.


## Arguments

### Main arguments

#### `-profile`

Use this parameter to choose a configuration profile. Profiles can give configuration presets for different compute environments. Note that multiple profiles can be loaded, for example: `-profile singularity` - the order of arguments is important!

If `-profile` is not specified at all the pipeline will be run locally and expects all software to be installed and available on the `PATH`.

* `conda`
  * A generic configuration profile to be used with [conda](https://conda.io/docs/)
* `singularity`
  *  A generic configuration profile to be used with [singularity](https://sylabs.io/guides/3.0/user-guide/). The pipeline charge the singularity image`region_identification_env.sif` that can be found in the [env](env/) directory. Be aware that the image has been added to the repository using [git LFS](https://git-lfs.github.com/).  

#### `--tax_level`

Use this to specify the taxonomic level for which you want to identify regions.
For example `2` refers to Bacteria.

```bash
--tax_level 2
```

#### `--result_dir`
Use this parameter to define the result directory of the workflow.
(default: `results`)

### Argument specific to orthologous group selection
Universal and single copy gene are identified using the eggNOG database.
Two parameter are used to select universal and single copy COG:

#### `--cog_prct`
Minimal percentage of species an orthologous group needs to be found in to be selected. The set of species are the one selected and used by eggNOG v5.0 to construct their orthologous groups. For Orthologous groups of Bacteria eggNOG have selected 4445 representative species. (default: 95)

#### `--cog_single_copy_prct`
Minimal percentage of species an orthologous group needs to be found in **single copy** to be selected. (default: 95)

#### `--eggNOG_data_dir`
Directory where data downloaded from eggNOG are saved by the pipeline. The pipeline first checks if the files needed are found in this directory before downloading them. (default: `$baseDir/eggNOG_data`)



### Argument specific to refseq database


#### `--ncbi_genomes_dir`

Directory where assembly files are saved by the pipeline. The pipeline first checks if the files needed are found in this directory before downloading them. (default: `$baseDir/ncbi_data`)

#### `--refseq_path`

Path to a directory where assembly summary and NCBI taxonomy dump files can be found. If the argument is not specified the pipeline directly download the files on the ncbi server and store them in a directory.  
- The assembly summary reports metadata for the genome assemblies on the NCBI genomes FTP site and is downloaded at this adress : <ftp://ftp.ncbi.nlm.nih.gov/genomes/refseq/assembly_summary_refseq.txt>
- The NCBI Taxonomy database dump files are stored in an archive that is downloaded at this adress: <ftp://ftp.ncbi.nlm.nih.gov/pub/taxonomy/new_taxdump/new_taxdump.tar.gz>. The pipeline uses from this archive the files `taxidlineage.dmp` and `rankedlineage.dmp`.

### Argument specific to genomes selection

RefSeq database is made of many assemblies. The quality of these genomes as well as the representativity is a problem.
RefSeq assemblies fall into different categories
* reference genomes:
* representative genomes:
* na:

It is possible to run the pipeline with different selections of assemblies:
* `ref_and_rep_assemblies`: reference and representative assembly with uniq assembly taxid
* `one_assembly_per_sp`: 1 assembly per species
* `rep_ref_and_10_assemblies_per_sp` : all reference and representative genomes and up to 10 assemblies per species
* `all_assemblies` : All assemblies available for the given taxonomic level with a risk of over representing some species.


#### `--primary_assemblies_selection`
The selection of regions is based on the assemblies of this selection.

#### `--extended_assemblies_selection`
The selected regions are extracted from the assemblies of this selection.


### Argument specific to region selection

#### `--length_cutoff`
Maximum length of a region to be take into account in the process. Only  regions whit a length smaller or equal this cutoff is going to be considered. (default: 10000)


#### `--region_genome_prct_threshold`
Minimal percentage of genomes where a region need to be found to be selected. (default: 95)


#### `--rrna_genes_to_fetch`
On top of detection of new regions, the workflow can also fetch rrna genes and extract them. This is very useful to compare new identified regions with rrna genes. The possible rrna genes are: `'16S 23S 5S 16S-23S'`.
`16S-23S` refers to the region made of the 16S and the 23S rRNA genes. (default: `'16S 23S 5S 16S-23S'`)



## Output and how to interpret results

The results of the workflow are stored in a result directory and the main results and plots are displayed in a html report.

### Result directory
First of all, the result directory has a file called `workflow_parameters.txt` storing all the parameters used in the workflow execution.

Exemple:

```
tax_level:2157
cog_single_copy_prct:95
cog_prct:95
region_genome_prct_threshold:95
ncbi_genomes_dir:/work2/project/seqoccin/metaG/marker_identification/region_identification_workflow/ncbi_data
refseq_date_dir:2020-08-28_refseq_summary
length_cutoff:10000
primary_assemblies_selection:ref_and_rep_assemblies
extended_assemblies_selection:ref_and_rep_assemblies
rrna_genes_to_fetch:16S 23S 5S 16S-23S
is_this_a_test_run:false
```
The results directory contain a file called `selected_pairs_stat.tsv` giving basic statistic on the selected regions.


The result directory has also different sub-directories:

*  `assembly_selections/`:

It is where the assembly selection files are stored (information an each selection of refseq assemblies).  

* `cog_location/`:

For each selected cogs, a tsv file giving the location of the identified COG in refseq assemblies.

* `extracted_genes/`

Each COGS sequences that are part of a selected regions is extracted and stored in this directory. The extraction is performed on the assemblies of the selection given with the argument  `--extended_assemblies_selection`.

* `extracted_genes/`: Information on the  of the selected regions stored by assembly selection.

* `region_details/`: For each selected region, a tsv file giving the relevant information (location, genes... ). 

* `selected_regions_summary/`


### Html report
The html report is splitted into four main pages:

* result_report_intro.html
* result_report_cog.html
* result_report_assemblies.html
* result_report_region.html




## Quick start/Usage

On genologin:

```
module load system/singularity-3.5.3
nextflow run ../main.nf --tax_level 2157 -profile singularity
PATH=$PATH:/work2/project/seqoccin/metaG/marker_identification/region_identification_workflow/bin

module load system/Miniconda3-4.7.10
```
