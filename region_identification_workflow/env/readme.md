# Command used to build singularity image from conda yml

In the conda yml keep only name of software that you really need: conda will manage dependencies.


```bash
sudo singularity build region_identification_env_clean.sif singularity_recipe_clean
```

Check if image works:

```bash
singularity exec region_identification_env_clean.sif blastp -h

```


```bash
../nextflow run ../main.nf -with-singularity ../env/region_identification_env_clean.sif --tax_level 0 \
                                                                                        --refseq_path "$PWD/synthetic_data/ncbi_data/0000-00-00_refseq_summary/" \
                                                                                        -w 'work_synthetic_data' \
                                                                                        --eggNOG_data_dir "$PWD/synthetic_data/eggnog_data/" \
                                                                                        --ncbi_genomes_dir "$PWD/synthetic_data/ncbi_data"\
                                                                                         -profile singularity
```
