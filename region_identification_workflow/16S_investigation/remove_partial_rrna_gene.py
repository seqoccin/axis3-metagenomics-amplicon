#!/usr/bin/env python3

"""
Description

:Example:
python template.py -v
"""

# Metadata
__author__ = 'Mainguy Jean - Plateforme bioinformatique Toulouse'
__copyright__ = 'Copyright (C) 2019 INRA'
__license__ = 'GNU General Public License'
__version__ = '0.1'
__email__ = 'support.bioinfo.genotoul@inra.fr'
__status__ = 'dev'


from argparse import ArgumentParser, ArgumentDefaultsHelpFormatter
import logging
import pandas as pd
import plotly.express as px
import numpy as np
import csv
import os
from Bio import SeqIO


def parse_arguments():
    """Parse script arguments."""
    parser = ArgumentParser(description="...",
                            formatter_class=ArgumentDefaultsHelpFormatter)
    parser.add_argument('-g', "--genomic_info_file")
    parser.add_argument('-s', "--fasta_file")
    parser.add_argument('-o', "--outdir", default='./')
    parser.add_argument("-v", "--verbose", help="increase output verbosity",
                        action="store_true")

    args = parser.parse_args()
    return args


def main():

    args = parse_arguments()

    if args.verbose:
        logging.basicConfig(format="%(levelname)s: %(message)s", level=logging.DEBUG)
        logging.info('Mode verbose ON')

    else:
        logging.basicConfig(format="%(levelname)s: %(message)s")

    genomic_info_file = args.genomic_info_file
    fasta_file = args.fasta_file
    outdir = args.outdir

    df = pd.read_csv(genomic_info_file, sep='\t')

    df['start_str'] = df['start'].apply(str)
    df['end_str'] = df['end'].apply(str)
    df['seq_header'] = df['genome']+'|'+df['seqid'] + '|' + df['start_str'] + '-' + df['end_str']

    seq_header_partial = list(df[df['partial'] == True]["seq_header"])
    seq_header_not_partial = list(df[df['partial'] == False]["seq_header"])

    logging.info(f'remove partial sequence from fasta file {fasta_file}')
    s_processed = 0

    # output file
    filtered_fasta_file = os.path.join(outdir, 'complete_' + os.path.basename(fasta_file))

    with open(fasta_file) as fl_r, open(filtered_fasta_file, 'w') as fl_w:
        for record in SeqIO.parse(fl_r, "fasta"):
            header = record.id
            if header in seq_header_not_partial:
                s_processed += 1
                SeqIO.write(record, fl_w, "fasta")
            elif header in seq_header_partial:
                pass
            else:
                logging.critical(f'seq with {header} is not register in file {genomic_info_file}')
                exit(1)
            if s_processed % 10000 == 0:
                logging.info(f'sequences processed {s_processed}')


if __name__ == '__main__':
    main()
