

Make an assembly selection with assemblies where a full 16S gene is retrieved:

```bash
# Make the assembly list of interest
grep ^'>' complete_16S.fna | cut -f'1' -d'|' | cut -f2 -d'>' | sort | uniq > 16S_assembly_list.txt

python assembly_list_to_assembly_summary_file.py --assembly_summary rep_ref_and_10_assemblies_per_sp.tsv --assembly_list 16S_assembly_list.txt --out_assembly_summary 16S_complete_rep_and_ref.tsv

```
