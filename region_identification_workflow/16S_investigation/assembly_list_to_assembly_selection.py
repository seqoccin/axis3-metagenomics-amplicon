#!/usr/bin/env python3

"""
Create a sub assembly selection from a list of assembly accession.

The assemblies of the given assembly list are selected and form a new assembly selection.

:Example:

"""

# Metadata
__author__ = 'Mainguy Jean - Plateforme bioinformatique Toulouse'
__copyright__ = 'Copyright (C) 2019 INRA'
__license__ = 'GNU General Public License'
__version__ = '0.1'
__email__ = 'support.bioinfo.genotoul@inra.fr'
__status__ = 'dev'


from argparse import ArgumentParser, ArgumentDefaultsHelpFormatter, FileType
import logging
import pandas as pd
import sys


def parse_arguments():
    """Parse script arguments."""
    parser = ArgumentParser(description="...",
                            formatter_class=ArgumentDefaultsHelpFormatter)
    parser.add_argument("--assembly_summary", help="file with assembly annotation", required=True)
    parser.add_argument("--assembly_list",  nargs='?', type=FileType('r'),
                        default=sys.stdin, help="List of assembly to keep.")
    parser.add_argument("--out_assembly_summary", type=str, required=True,
                        help="The new assembly selection.")
    parser.add_argument("-v", "--verbose", help="increase output verbosity",
                        action="store_true")

    args = parser.parse_args()
    return args


def main():

    args = parse_arguments()

    if args.verbose:
        logging.basicConfig(format="%(levelname)s: %(message)s", level=logging.DEBUG)
        logging.info('Mode verbose ON')

    else:
        logging.basicConfig(format="%(levelname)s: %(message)s")

    assembly_summary = args.assembly_summary
    assembly_list = args.assembly_list
    out_assembly_summary = args.out_assembly_summary

    assembly_list = [l.rstrip() for l in assembly_list if l]

    df = pd.read_csv(assembly_summary, sep='\t')

    selected_df = df[df['assembly_accession'].isin(assembly_list)]

    if len(selected_df['assembly_accession']) != len(assembly_list):
        logging.critical(f"Some assemblies from the given list were not found in the assembly summary  {assembly_summary} ")
        logging.critical(f"(selected_summary {len(selected_df['assembly_accession'])} != list {len(assembly_list)})")
        missing_assembly = set(selected_df['assembly_accession'])
        logging.warning(f'Some assemblies from the given list were not found in the assembly summary {assembly_summary}')
    selected_df.to_csv(out_assembly_summary, sep='\t', index=False, line_terminator='\n')


if __name__ == '__main__':
    main()
