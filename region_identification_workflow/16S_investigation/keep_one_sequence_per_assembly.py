#!/usr/bin/env python3

"""
Description

:Example:
python template.py -v
"""

# Metadata
__author__ = 'Mainguy Jean - Plateforme bioinformatique Toulouse'
__copyright__ = 'Copyright (C) 2019 INRA'
__license__ = 'GNU General Public License'
__version__ = '0.1'
__email__ = 'support.bioinfo.genotoul@inra.fr'
__status__ = 'dev'


from argparse import ArgumentParser, ArgumentDefaultsHelpFormatter
import logging
import pandas as pd
import plotly.express as px
import numpy as np
import csv
import os
from Bio import SeqIO


def parse_arguments():
    """Parse script arguments."""
    parser = ArgumentParser(description="...",
                            formatter_class=ArgumentDefaultsHelpFormatter)
    parser.add_argument('-s', "--fasta_files", nargs='+', required=True)
    parser.add_argument('-o', "--outdir", default='./')
    parser.add_argument("-v", "--verbose", help="increase output verbosity",
                        action="store_true")

    args = parser.parse_args()
    return args


def main():

    args = parse_arguments()

    if args.verbose:
        logging.basicConfig(format="%(levelname)s: %(message)s", level=logging.DEBUG)
        logging.info('Mode verbose ON')

    else:
        logging.basicConfig(format="%(levelname)s: %(message)s")

    fasta_files = args.fasta_files
    outdir = args.outdir
    os.makedirs(outdir, exist_ok=True)
    logging.info(f'Output dir is {outdir}')
    logging.info(f'keep one sequence per assembly')
    
    for fasta_file in fasta_files:
        logging.info(f'Process of {fasta_file}')
        s_removed = 0
        s_kept = 0
        assembly_already_treated = set()
        # output file
        filtered_fasta_file = os.path.join(outdir, os.path.basename(fasta_file))
        assert os.path.isfile(filtered_fasta_file) is False, f'out file {filtered_fasta_file} exists already'
           

        with open(fasta_file) as fl_r, open(filtered_fasta_file, 'w') as fl_w:
            for record in SeqIO.parse(fl_r, "fasta"):
                header = record.id
                assembly = header.split('|')[0]

                if assembly in assembly_already_treated:
                    s_removed += 1
                else:
                    fl_w.write(f">{record.description}\n")
                    fl_w.write(f"{record.seq}\n")
                    s_kept += 1
                    assembly_already_treated.add(assembly)

        logging.info(f'sequences processed {s_kept + s_removed} : remove {s_removed} kept {s_kept}')


if __name__ == '__main__':
    main()
