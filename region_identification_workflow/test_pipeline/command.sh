../nextflow run ../main.nf \
	-profile laptop \
	-with-trace result_test/trace \
	-with-report \
	--result_dir result_test/ \
	--tax_level 2 \
	--test true \
	-w work_test \
	--refseq_path /home/jean/marker_identification/region_identification_workflow/ncbi_data/2020-03-17_refseq_summary \
	--eggNOG_data_dir /home/jean/marker_identification/region_identification_workflow/eggNOG_data_test/ \
	--extended_assemblies_selection one_assembly_per_sp \
	# -resume
