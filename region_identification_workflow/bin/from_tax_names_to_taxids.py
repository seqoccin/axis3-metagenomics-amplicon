#!/usr/bin/env python3

"""
Description

:Example:
python template.py -v
"""

# Metadata
__author__ = 'Mainguy Jean - Plateforme bioinformatique Toulouse'
__copyright__ = 'Copyright (C) 2020 INRAE'
__license__ = 'GNU General Public License'
__version__ = '0.1'
__email__ = 'support.bioinfo.genotoul@inra.fr'
__status__ = 'dev'


from argparse import ArgumentParser, ArgumentDefaultsHelpFormatter
import logging
import os

from publicdb import ncbi


def parse_arguments():
    """Parse script arguments."""
    parser = ArgumentParser(description="...",
                            formatter_class=ArgumentDefaultsHelpFormatter)
    parser.add_argument("-v", "--verbose", help="increase output verbosity",
                        action="store_true")
    parser.add_argument("--main_tax_level", type=str, default='2',
                        help="taxid of root taxonomic level. All taxon names proided belong to this taxonomic level. Default is Bacteria")
    parser.add_argument("--tax_names_file", type=str, required=True,
                        help="File with a list of tax names seprated by new line.")

    parser.add_argument("--new_taxdump_dir", type=str, default='.',
                        help="Directory of ncbi new taxdump.")

    parser.add_argument("-o", "--output_file", type=str, default='taxids.list',
                        help="File with the list of taxids corresponding to the provided taxon names.")
    args = parser.parse_args()
    return args


def main():

    args = parse_arguments()

    if args.verbose:
        logging.basicConfig(format="%(levelname)s: %(message)s", level=logging.DEBUG)
        logging.info('Mode verbose ON')

    else:
        logging.basicConfig(format="%(levelname)s: %(message)s")

    tax_names_file = args.tax_names_file
    new_taxdump_dir = args.new_taxdump_dir
    output_file = args.output_file
    main_tax_level = args.main_tax_level

    taxidlineage_file = os.path.join(new_taxdump_dir, "taxidlineage.dmp")
    names_file = os.path.join(new_taxdump_dir, "names.dmp")

    # Parse selected taxon file and their retrieve taxids
    selected_taxons = []
    with open(tax_names_file) as fl:
        for line in fl:
            if line.startswith('#'):
                continue
            selected_taxons.append(line.strip())

    # PARSE taxidlineage_file
    taxid_to_lineage = ncbi.parse_taxidlineage_file(taxidlineage_file)

    # from name to taxids
    selected_taxids = {}
    with open(names_file) as fl:
        for line in fl:
            splitted_line = line.rstrip().replace('\t|', '').split('\t')
            name = splitted_line[1]
            taxid = splitted_line[0]
            if name in selected_taxons:
                if main_tax_level not in taxid_to_lineage[taxid]:
                    logging.warning(
                        f'Taxon >>{line.rstrip()}<< not a part of main_tax_level={main_tax_level}')
                    continue
                selected_taxids[taxid] = name
                logging.info(f'{name}  taxid={taxid}')

    assert len(selected_taxids) == len(selected_taxons)

    logging.info(f'Writing corresponding taxids in {output_file}')
    with open(output_file, 'w') as fl:
        fl.write('\n'.join(selected_taxids))


if __name__ == '__main__':
    main()
