#!/usr/bin/env python3

"""
Make pair of COGs in the different assemblies.

Input:
    A file containing positions of a COG in the different assemblies
    A directory where position files of all the COGs are stored

The script takes the given postion file of a COG and
make pairs of COGs with all position files found in the given directory.
To not make the same pair of COGs twice, the COG name of the input file need to be greater
than any other COG name found in the directory to actually build pair.

For example COG002  in input would make pair with COG003 and above and not with COG001.
It is when COG001 is given in input that it will pair with COG002.

:Example:
$ python make_cog_pairs.py <cog_position_file> <cog_position_dir>
"""

# Metadata
__author__ = 'Mainguy Jean - Plateforme bioinformatique Toulouse'
__copyright__ = 'Copyright (C) 2019 INRA'
__license__ = 'GNU General Public License'
__version__ = '0.1'
__email__ = 'support.bioinfo.genotoul@inra.fr'
__status__ = 'dev'


import csv
from argparse import ArgumentParser, ArgumentDefaultsHelpFormatter
import os
import logging
import tools

import regions_analysis_fct


def make_cog_pairs(input_cog_po_by_seqid, other_cog_position_file):
    """Make pairs of cogs between the input cog and the other_cog_position_file."""
    pair_infos = []
    with open(other_cog_position_file) as csvfile:
        reader = csv.DictReader(csvfile, delimiter='\t')
        for other_gene_position in reader:
            seqid = other_gene_position['seqid']
            for gene_position in input_cog_po_by_seqid[seqid]:
                pair_infos += regions_analysis_fct.get_gene_pair_info(
                    gene_position, other_gene_position)

    return pair_infos


def parse_arguments():
    """Parse script arguments."""
    parser = ArgumentParser(description="""
    This script takes the given postion file of a COG and
    make pairs of COGs with all position files found in the given directory.
    To not make the same pair of COGs twice, the COG name of the input file need to be greater
    than any other COG name found in the directory to actually make pairs.\n

    For example COG002  in input would make pair with COG003 and above and not with COG001.
    It is when COG001 is given in input that it will pair with COG002.""",
                            formatter_class=ArgumentDefaultsHelpFormatter)

    parser.add_argument("cog_position_file", type=str,
                        help="File containing position of a COG in all genomes")
    parser.add_argument("cog_position_dir", type=str,
                        help="Directory where all position files are stored")
    parser.add_argument("-o", '--outdir', type=str, default='cog_pairs',
                        help="Directory where freshly computed cog pair files are written")
    parser.add_argument("-v", "--verbose", help="increase output verbosity",
                        action="store_true")
    args = parser.parse_args()
    return args


def main():
    args = parse_arguments()
    if args.verbose:
        logging.basicConfig(format="%(levelname)s: %(message)s", level=logging.DEBUG)
        logging.info('Mode verbose ON')

    else:
        logging.basicConfig(format="%(levelname)s: %(message)s")

    cog_position_file = args.cog_position_file
    cog_position_dir = args.cog_position_dir

    output_pair_dir = args.outdir
    os.makedirs(output_pair_dir, exist_ok=True)
    # pair_infos_iter = get_cog_cogs_pairs(cog_position_file, cog_position_dir, output_pair_dir)
    cog_positions_by_seqid = tools.tsv_to_dict_of_dicts(cog_position_file, "seqid")
    cog_file_name = os.path.basename(cog_position_file)

    for other_cog_file_name in os.listdir(cog_position_dir):

        # simple rule to not compute twice the pairing between two COGs:
        if cog_file_name >= other_cog_file_name:

            other_cog_position_file = os.path.join(cog_position_dir, other_cog_file_name)
            cog1 = cog_file_name[:-len(".tsv")]
            cog2 = other_cog_file_name[:-len(".tsv")]
            cog_pair_name = '-'.join(sorted([cog1, cog2]))
            logging.info(f'Make pair between {cog_position_file} and {other_cog_position_file}')
            pair_infos = make_cog_pairs(cog_positions_by_seqid, other_cog_position_file)

            if pair_infos:
                output_pair_file = os.path.join(output_pair_dir, f'{cog_pair_name}.tsv')
                tools.list_of_dict_to_tsv(pair_infos, output_pair_file)

            else:
                logging.warning(f'{cog_pair_name} have not been found together at all. ')


if __name__ == '__main__':
    main()
