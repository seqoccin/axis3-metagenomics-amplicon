#!/usr/bin/env python3

"""
Select assemblies.

Example:
python extended_assembly_selection.py all_assemblies_with_taxonomy.tsv \
                 --assembly_selection 'ref_and_rep_assemblies one_assembly_per_sp rep_ref_and_10_assemblies_per_sp' \
                 -n 10 -v
"""

# Metadata
__author__ = 'Mainguy Jean - Plateforme bioinformatique Toulouse'
__copyright__ = 'Copyright (C) 2019 INRA'
__license__ = 'GNU General Public License'
__version__ = '0.1'
__email__ = 'support.bioinfo.genotoul@inra.fr'
__status__ = 'dev'

from argparse import ArgumentParser, ArgumentDefaultsHelpFormatter
import pandas as pd
import logging
import os


def select_extended_assembly(dfg, n_assembly_per_sp):
    logging.debug('=='*50+'\n')
    logging.debug(dfg[['assembly_level', 'refseq_category', "assembly_accession"]])

    df_select = dfg[dfg['refseq_category'].isin(['reference genome', 'representative genome'])]
    if len(df_select) > n_assembly_per_sp:
        logging.info(
            f'species_taxid {dfg["species_taxid"].iloc[0]} has {len(df_select)} assemblies in reference or representative genomes categories.')
    if len(df_select) < n_assembly_per_sp:
        df_na = dfg[dfg['refseq_category'] == 'na'].head(n_assembly_per_sp - len(df_select))
        # print(type(df_na))
        df_select = pd.concat([df_select, df_na])
    logging.debug('selection '+'>'*50)
    logging.debug(df_select[['assembly_level', 'refseq_category', "assembly_accession"]])

    return df_select


def parse_arguments():
    """Parse script arguments."""
    parser = ArgumentParser(description="...",
                            formatter_class=ArgumentDefaultsHelpFormatter)

    parser.add_argument("assembly_summary", type=str,
                        help="ncbi assembly summary containing information about all assemblies: taxids, ftp_path, level of assembly...")
    # parser.add_argument("--taxids", type=str, default=None,
    #                     help="taxids of interest: assemblies with a taxid included in this list is selected")
    parser.add_argument("--assembly_selections", nargs='+', required=True,
                        help="""Name of the selection to output.
                        Possible selection names: ref_and_rep_assemblies one_assembly_per_sp rep_ref_and_10_assemblies_per_sp""")
    parser.add_argument("-n", "--nb_assembly_per_sp", type=int, default=10,
                        help="Number of assembly max per species")
    parser.add_argument("-o", "--output_dir", type=str, default='./',
                        help="output file : final assembly selection")
    parser.add_argument("-v", "--verbose", help="increase output verbosity",
                        action="store_true")
    parser.add_argument("--debug", help="debug mode",
                        action="store_true")
    args = parser.parse_args()
    return args


def main():
    args = parse_arguments()

    if args.verbose:
        logging.basicConfig(format="%(levelname)s: %(message)s", level=logging.INFO)
        logging.info('Mode verbose ON')

    else:
        logging.basicConfig(format="%(levelname)s: %(message)s")

    if args.debug:
        logging.basicConfig(format="%(levelname)s: %(message)s", level=logging.DEBUG)
        logging.info('Mode debug ON')

    else:
        logging.basicConfig(format="%(levelname)s: %(message)s")

    assembly_selections = args.assembly_selections

    # input
    assembly_summary_file = args.assembly_summary
    nb_assembly_per_sp = args.nb_assembly_per_sp

    # output
    output_dir = args.output_dir

    possible_assembly_selections = ["ref_and_rep_assemblies",
                                    "one_assembly_per_sp",
                                    "rep_ref_and_10_assemblies_per_sp", ]

    selection_description = {"ref_and_rep_assemblies":  "reference and representative assembly with uniq assembly taxid",
                             "one_assembly_per_sp":  "1 assembly per species",
                             "rep_ref_and_10_assemblies_per_sp":  f"all reference and representative genomes and up to {nb_assembly_per_sp} assemblies per species",
                             "all_assemblies":  "All assemblies available for the given taxonomic level with a risk of over representating some species.",
                             }

    # check selections
    for s in assembly_selections:
        if s not in possible_assembly_selections:
            raise NameError(
                f'The given selection "{s}" is not one of the possible selection: {possible_assembly_selections}')

    assembly_selection_stats = []

    df = pd.read_csv(assembly_summary_file, sep='\t')

    selection = "all_assemblies"
    selection_stat = {
        "selection": selection,
        "description": selection_description[selection],
        "assembly": df['assembly_accession'].count(),
        "species": df['species_taxid'].nunique(),
    }
    assembly_selection_stats.append(selection_stat)

    # transform assembly level as category to be able to sort them
    # https://stackoverflow.com/questions/23482668/sorting-by-a-custom-list-in-pandas
    assembly_lvl_sorted = ["Complete Genome", 'Chromosome', 'Scaffold', 'Contig']
    df.assembly_level = df.assembly_level.astype("category")
    df.assembly_level.cat.set_categories(assembly_lvl_sorted, inplace=True)

    refseq_categories = ['reference genome', 'representative genome', 'na']
    df.refseq_category = df.refseq_category.astype("category")
    df.refseq_category.cat.set_categories(refseq_categories, inplace=True)

    df.sort_values(['refseq_category', 'assembly_level'],
                   ascending=[True, True], inplace=True)

    if "rep_ref_and_10_assemblies_per_sp" in assembly_selections:
        logging.info(
            f'Select all reference and representative genomes and up to {nb_assembly_per_sp} assemblies per species')
        select_df = df.groupby('species_taxid').apply(
            lambda dfg: select_extended_assembly(dfg, nb_assembly_per_sp)).reset_index(drop=True)

        extended_selection_file = os.path.join(
            output_dir, f"rep_ref_and_{nb_assembly_per_sp}_assemblies_per_sp.tsv")
        logging.info(f'Write selection into {extended_selection_file}')
        select_df.to_csv(extended_selection_file, sep='\t', index=False)

        selection = "rep_ref_and_10_assemblies_per_sp"
        selection_stat = {
            "selection": selection,
            "description": selection_description[selection],
            "assembly": select_df['assembly_accession'].count(),
            "species": select_df['species_taxid'].nunique(),
        }
        assembly_selection_stats.append(selection_stat)

    if "ref_and_rep_assemblies" in assembly_selections:
        n = 1
        logging.info(
            f'Select reference and representative genomes with uniq assembly taxid')
        df_select = df[df['refseq_category'] != 'na']

        select_df = df_select.groupby('taxid').apply(
            lambda dfg: dfg.head(n)).reset_index(drop=True)

        selected_assemblies_file = os.path.join(output_dir, f"ref_and_rep_assemblies.tsv")
        logging.info(f'Write selection into {selected_assemblies_file}')
        select_df.to_csv(selected_assemblies_file, sep='\t', index=False)

        selection = "ref_and_rep_assemblies"
        selection_stat = {
            "selection": selection,
            "description": selection_description[selection],
            "assembly": select_df['assembly_accession'].count(),
            "species": select_df['species_taxid'].nunique(),
        }
        assembly_selection_stats.append(selection_stat)

    if "one_assembly_per_sp" in assembly_selections:
        n = 1
        logging.info(
            f'Select {n} genomes per species')
        select_df = df.groupby('species_taxid').apply(
            lambda dfg: dfg.head(n)).reset_index(drop=True)

        selection_file = os.path.join(
            output_dir, f"one_assembly_per_sp.tsv")
        logging.info(f'Write selection into {selection_file}')
        select_df.to_csv(selection_file, sep='\t', index=False, line_terminator='\n')

        selection = "one_assembly_per_sp"
        selection_stat = {
            "selection": selection,
            "description": selection_description[selection],
            "assembly": select_df['assembly_accession'].count(),
            "species": select_df['species_taxid'].nunique(),
        }
        assembly_selection_stats.append(selection_stat)

    selection_stat_file = os.path.join(
        output_dir, f"assembly_selection_stat.tsv")
    logging.info(f'Write selection stat file into {selection_stat_file}')
    df_stat = pd.DataFrame.from_dict(assembly_selection_stats)
    df_stat.to_csv(selection_stat_file, sep='\t', index=False, line_terminator='\n')


if __name__ == '__main__':
    main()
