#!/usr/bin/env python3

"""
Select cog pairs.

:Example:
python cog_pair_selection.py -v
"""

# Metadata
__author__ = 'Mainguy Jean - Plateforme bioinformatique Toulouse'
__copyright__ = 'Copyright (C) 2019 INRA'
__license__ = 'GNU General Public License'
__version__ = '0.1'
__email__ = 'support.bioinfo.genotoul@inra.fr'
__status__ = 'dev'


from argparse import ArgumentParser, ArgumentDefaultsHelpFormatter
import logging
import pandas as pd


def is_cog_with_itself(pair):
    """Check if cog pair is made by the same cog."""
    return True if len(set(pair.split('-'))) == 1 else False


def parse_arguments():
    """Parse script arguments."""
    parser = ArgumentParser(description="Select cog pairs.",
                            formatter_class=ArgumentDefaultsHelpFormatter)
    parser.add_argument("pair_stat_file", help="pair stat")
    parser.add_argument("--nb_assemblies", type=int, required=True,
                        help="nb total of genomes used to calculate coverage values")
    parser.add_argument("--coverage_threshold", type=int, default=95,
                        help="minimal percentage of genomes containing the region")
    parser.add_argument("-v", "--verbose", help="increase output verbosity",
                        action="store_true")
    parser.add_argument("--force_selection", nargs='+', default=None,
                        help="Force selection for the given regions",)

    args = parser.parse_args()
    return args


def main():
    """Select cog pairs."""
    args = parse_arguments()

    if args.verbose:
        logging.basicConfig(format="%(levelname)s: %(message)s", level=logging.DEBUG)
        logging.info('Mode verbose ON')

    else:
        logging.basicConfig(format="%(levelname)s: %(message)s")

    pair_stat_file = args.pair_stat_file
    nb_assemblies = args.nb_assemblies
    coverage_threshold = args.coverage_threshold
    force_selection_regions = args.force_selection

    df_raw = pd.read_csv(pair_stat_file, sep='\t')

    # Split cog pair to have a column for each cog of the pair
    df_raw[['cog1', 'cog2']] = df_raw['cog_pair'].str.split('-', expand=True)

    # Remove region that are made of the same cog ie : cog1 with cog1
    filter_pair_vs_itself = df_raw['cog_pair'].apply(is_cog_with_itself)
    df = df_raw.loc[~filter_pair_vs_itself]

    # Pair selection
    selec_filter = 100*df['nb_genome_with_main_orga_post_len_filtering'] / \
        nb_assemblies >= coverage_threshold
    df_selected = df.loc[selec_filter]

    if len(df_selected) == 0:
        logging.warning(f'No region is fitting the coverage threshold of {coverage_threshold}.')

    logging.info(f'{len(df_selected)} regions are found in {coverage_threshold}% of the genomes')

    if force_selection_regions:
        logging.warning(f'Attempt to include by force {force_selection_regions} into the selection')
        df_not_selected = df_raw[df_raw['cog_pair'].isin(df_selected['cog_pair']) == False]
        assert len(df_not_selected) + len(df_selected) == len(df_raw)
        df_forced_selected = df_not_selected[df_not_selected['cog_pair'].isin(
            force_selection_regions)]
        logging.warning(
            f'{len(df_forced_selected)} regions has been added by force to the selection')
        # df_selected = pd.concat([df_selected, df_forced_selected], axis=0)
        df_selected_and_forced = pd.concat([df_selected, df_forced_selected], ignore_index=True)
        # df_selected_and_forced = df_selected_and_forced.drop_duplicates()
        df_selected = df_selected_and_forced

    logging.info(f'{len(df_selected)} regions selected')

    logging.info('writing... selected_pairs_stat.tsv')
    df_selected.to_csv('selected_pairs_stat.tsv', sep='\t', index=False)

    # Write cogs that are part of the selected regions
    selected_cogs_set_raw = set(df_selected['cog1']) | set(df_selected['cog2'])
    logging.info(
        f'{len(selected_cogs_set_raw)} cogs are part of the {len(df_selected)} selected regions')

    selected_cogs_set = {c for c in selected_cogs_set_raw if c not in ['16S', '23S', '5S']}

    if selected_cogs_set != selected_cogs_set_raw:
        str = f'rrna genes ({selected_cogs_set_raw - selected_cogs_set})'
        str += ' have been removed from cog list. But are still analysed by the pipeline'
        logging.warning(str)

    logging.info('writing... selected_cogs_list.txt')
    with open('selected_cogs_list.txt', 'w') as fl:
        fl.write('\n'.join(selected_cogs_set)+'\n')

    selected_pairs_set = set(df_selected['cog_pair'])
    with open("selected_pairs.list", 'w') as fl:
        fl.write('\n'.join(selected_pairs_set)+'\n')


if __name__ == '__main__':
    main()
