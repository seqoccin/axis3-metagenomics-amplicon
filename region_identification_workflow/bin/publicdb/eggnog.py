#!/usr/bin/env python3

"""Functions downloading and parsing eggnog files."""

# Metadata
__author__ = 'Mainguy Jean - Plateforme bioinformatique Toulouse'
__copyright__ = 'Copyright (C) 2019 INRA'
__license__ = 'GNU General Public License'
__version__ = '0.1'
__email__ = 'support.bioinfo.genotoul@inra.fr'
__status__ = 'dev'


# module importation
import os
import gzip
from Bio import SeqIO
import logging
import time


def download_eggnog_files(url, output_file, attempt=0, attempt_max=5):
    """Download eggnog file.

    :param url:
    :param output_file:
    :type url: str
    :type output_file: str

    :return output_file: the downloaded file path
    :rtype: str

    .. note:: if the downloaded output_file is gzipped the extension '.gz' is added to the file name

    """
    download_err_file = os.path.join(os.path.dirname(output_file), ".download_report.txt")
    exit_code = os.system(f'wget {url} -O {output_file} 2> {download_err_file}')  # > /dev/null')

    # Managing Error:
    if exit_code != 0:
        error_message = f"Downloading Error, wget exit code = {exit_code}."
        error_message += f' Check {os.path.abspath(download_err_file)}.download_report'
        logging.warning(error_message)
        if attempt > attempt_max:
            raise ValueError(error_message)
        else:
            logging.warning(f"waiting : {10 + 10*attempt} sec")
            time.sleep(1 + 10*attempt)
            download_eggnog_files(url, output_file, attempt+1)
    if os.stat(output_file).st_size == 0:
        raise ValueError(f"Output file {output_file} is empty. Check if the COG id is correct.")

    # Check if the file is zipped
    # If so, the extension '.gz' is added to the name of the file
    file_type = os.popen(f'file {output_file} -bi').read().rstrip()
    if (file_type == 'application/x-gzip; charset=binary' or file_type == 'application/gzip; charset=binary') and not output_file.endswith('.gz'):
        os.rename(output_file, output_file + '.gz')
        output_file = output_file + '.gz'

    return output_file


def get_eggnog_members_file(eggnog_members_url, check_dir=None, output_dir='.'):
    """
    Return the eggnog member file.

    :param eggnog_members_url: url of the desired file
    :param eggnog_members_url_template: base of the extended members files url
    :param check_dir: directory where the function first checks if the member file exist
    :param output_dir: directory where the function download the file

    :type eggnog_members_url: str
    :type eggnog_members_url_template: str
    :type check_dir: str
    :type output_dir: str

    :return eggnog_members_file: the eggnog file
    :rtype: str


    :Example:
    Get member file of the firmicutes (tax level = 1239)
    >>> get_eggnog_members_file('http://eggnog5.embl.de/download/eggnog_5.0/per_tax_level/1239/1239_members.tsv.gz', check_dir='eggNOG_data')
    '1239_members.tsv.gz'
    """
    eggnog_members_file = os.path.basename(eggnog_members_url)

    if check_dir:
        eggnog_members_file_in_checkdir = os.path.join(check_dir, eggnog_members_file)

    if os.path.isfile(eggnog_members_file):
        logging.info(f"{eggnog_members_file} exists in current dir")
    elif check_dir and os.path.isfile(eggnog_members_file_in_checkdir):
        logging.info(f"{eggnog_members_file} exists in check dir")
        eggnog_members_file = eggnog_members_file_in_checkdir
    elif check_dir and os.path.isfile(eggnog_members_file_in_checkdir[:-len('.gz')]):
        logging.info(f"{eggnog_members_file} exists in check dir without .gz")
        eggnog_members_file = eggnog_members_file_in_checkdir[:-len('.gz')]
        logging.info(eggnog_members_file)
    else:
        logging.info(f"{eggnog_members_file} doesn't exist. Let's download it {eggnog_members_url}")
        eggnog_members_file = download_eggnog_files(eggnog_members_url, eggnog_members_file)

    return eggnog_members_file


def get_COG_gene_name(cog_name: str,
                      extended_members_url_base="http://eggnogapi5.embl.de/nog_data/text/extended_members/",
                      check_dir=None, output_dir='.', attempt_max=5):
    """Return gene name associated with a COG.

    Retrieve gene name associated with a COG by using the extended member file of the COG.
    The extended member file gather information about all the genes that are
    part of the COG including the gene name.
    The extended member file is first searched in the directory given by the check_dir argument
    If the file is not there it is downloaded using the extended_members_url_base follow by the
    COG name in a folder called extended_members created in the current directory
    or if the argument check_dir is specified the script first check if the file
    have been previously downloaded.
    The name found associated in most of the genes is set as the name of the COG.

    :param cog_name: name of the COG
    :param extended_members_url_base: base of the extended members files url
    :param check_dir: directory where the function first check if the extended member file exist
    :param output_dir: directory where the function download the file
    :param attempt_max: maximum number of attempt when downloading fail.


    :type cog_name: str
    :type extended_members_url_base: str
    :type check_dir: str
    :type output_dir: str
    :type attempt_max: int

    :return cog_gene_name: the name associated with the COG
    :rtype: str

    :Example:

    >>> get_COG_gene_name("COG0085")
    'rpoB'

    When a wrong COG id is used on eggnog, the wget command download an empty file
    Then script then raises an Exception
    >>> get_COG_gene_name("COGThatDoesNotExist")
    Traceback (most recent call last):
        ...
    ValueError: Output file extended_members/COGThatDoesNotExist.tsv is empty. Check if the COG id is correct.


    When the id does not starts as a regular COG the wget command fail:
    >>> get_COG_gene_name("IdThatDoesNotExist", attempt_max=0) #doctest: +IGNORE_EXCEPTION_DETAIL
    Traceback (most recent call last):
        ...
    ValueError: Downloading Error, wget exit code = 2048. Check extended_members/IdThatDoesNotExist.tsv.download_report

    """
    extended_members_dir = 'extended_members'

    os.makedirs(extended_members_dir, exist_ok=True)

    gene_names = {}
    ext_members_file = os.path.join(extended_members_dir, cog_name+'.tsv')
    if check_dir:
        ext_members_file_in_check_dir = os.path.join(
            check_dir, extended_members_dir, cog_name+'.tsv')
        logging.info(ext_members_file_in_check_dir)

    if os.path.isfile(ext_members_file) and os.stat(ext_members_file).st_size != 0:
        logging.info(f'{ext_members_file} exists already')

    elif os.path.isfile(ext_members_file+'.gz') and os.stat(ext_members_file+'.gz').st_size != 0:
        ext_members_file += '.gz'
        logging.info(f'{ext_members_file} exists already')

    # Check in check_dir if the file exists:
    elif check_dir and os.path.isfile(ext_members_file_in_check_dir) and os.stat(ext_members_file_in_check_dir).st_size != 0:
        logging.info(f'{ext_members_file} exists in check_dir: {check_dir}')
        ext_members_file = ext_members_file_in_check_dir

    elif check_dir and os.path.isfile(ext_members_file_in_check_dir+'.gz') and os.stat(ext_members_file_in_check_dir+'.gz').st_size != 0:
        logging.info(f'{ext_members_file} exists in check_dir: {check_dir}')
        ext_members_file = ext_members_file_in_check_dir + '.gz'

    else:
        logging.info(f"{ext_members_file} does not exist already. Let's download it")
        url = os.path.join(extended_members_url_base, cog_name)
        ext_members_file = download_eggnog_files(url, ext_members_file, attempt_max=attempt_max)

    logging.info(ext_members_file)
    proper_open = gzip.open if ext_members_file.endswith('.gz') else open
    with proper_open(ext_members_file, 'rt') as fl:
        for l in fl:
            line = l.rstrip()
            taxid_geneid, gene_name, specie, taxid, aliases = line.split('\t')
            tax_id, geneid = taxid_geneid.split('.', 1)
            gene_names[gene_name] = gene_names.setdefault(gene_name, 0) + 1
    # take the most abundant name
    cog_gene_name = sorted(gene_names.items(), key=lambda kv: kv[1], reverse=True)[0][0]

    return cog_gene_name


def parse_eggnog_members_file(file, taxids_sub_selection=None, min_genomes_threshold=1):
    """Return cog statistics from eggnog member file.

    Parse the given eggnog member file. An eggnog member file is a tsv containing information
    on all orthologous groups at a specified taxon.
    For each group, we collect different statistics:
    * The number of protein across all genomes
    * The number genomes that contain member of the group
    * The number of genome where the group is represente by only one gene

    The min_genomes_threshold parameter allows to process only groups that are
    found in a minimum number of genomes

    :param file: path to an eggnog member file
    :param taxids_sub_selection: set of eggnog taxids on which cog selection is based.
    :param min_genomes_threshold: minimal number of genomes that need to be associated with a cog in order to consider it

    :type file: str
    :type min_genomes_threshold: int
    :type taxids_sub_selection: list

    :return cogs, taxids: list of dictionary containing the cog statistics and set of all taxids found in the member file
    :rtype: tuple

    :Example:

    >>> parse_eggnog_members_file("eggnog_member_file_sample.tsv")[0]
    [{'GroupName': 'Group1', 'ProteinCount': 5, 'SpeciesCount': 4, 'nb_single_copy': 3}, {'GroupName': 'Group2', 'ProteinCount': 3, 'SpeciesCount': 3, 'nb_single_copy': 3}]

    >>> parse_eggnog_members_file("eggnog_member_file_sample.tsv", min_genomes_threshold=4)[0]
    [{'GroupName': 'Group1', 'ProteinCount': 5, 'SpeciesCount': 4, 'nb_single_copy': 3}]

    >>> parse_eggnog_members_file("eggnog_member_file_sample.tsv", min_genomes_threshold=4)[1] == {'1', '3', '4', '2'}
    True
    """
    cogs = []
    proper_open = gzip.open if file.endswith('.gz') else open
    taxids_set = set()
    with proper_open(file, "rt") as fl:
        for i, line in enumerate(fl):
            line = line.rstrip()
            taxlvl, GroupName, ProteinCount, SpeciesCount, ProteinIDs, taxIDS = line.split('\t')
            tax_ids = taxIDS.split(',')
            if taxids_sub_selection:
                tax_ids = [taxid for taxid in tax_ids if taxid in taxids_sub_selection]

            # taxid in a set to get the total number of genomes at the end
            taxids_set |= set(tax_ids)
            sp_count = len(tax_ids)

            if sp_count >= min_genomes_threshold:
                prot_count = 0
                # Count gene copy
                copy_counts = {}
                for protid in ProteinIDs.split(','):
                    tax_id, protein_id = protid.split('.', 1)
                    if tax_id in tax_ids:
                        copy_counts.setdefault(tax_id, []).append(protein_id)
                        prot_count += 1

                nb_single_copy = sum(
                    (1 for gene_copy in copy_counts.values() if len(gene_copy) == 1))

                cog = {'GroupName': GroupName,
                       'ProteinCount': int(prot_count),
                       'SpeciesCount': int(sp_count),
                       'nb_single_copy': int(nb_single_copy)
                       }

                cogs.append(cog)

    if len(cogs) == 0:
        raise ValueError('No COGs identified due to the min_genome_treshold')

    return cogs, taxids_set


def get_nog_data(cogs_list, out_dir, url, extension,  check_dir=None):
    """
    Get eggnog data.

    First check if the wanted file are not in in current dir or checkdir (if specified).
    Download eggog data for all cog in the cogs_list using the eggnog url and the extension of the desired files.
    Extension can be for example .hmm or .faa
    """
    og_fasta_files = []

    for cog in cogs_list:
        file = os.path.join(out_dir, cog + extension)
        if check_dir:
            file_in_check_dir = os.path.join(check_dir, cog + extension)
        if os.path.isfile(file):
            pass
        elif os.path.isfile(file+'.gz'):
            file += '.gz'
        # Check in check_dir if the file exist:
        elif check_dir and os.path.isfile(file_in_check_dir):
            file = file_in_check_dir
        elif check_dir and os.path.isfile(file_in_check_dir+'.gz'):
            file = file_in_check_dir + '.gz'

        else:
            final_url = os.path.join(url, cog)
            # extension .gz can be added if file is zipped
            file = download_eggnog_files(final_url, file)

        og_fasta_files.append(file)

    return og_fasta_files


def create_true_positive_file(fasta_files, true_positive_file):
    """Create a true positive file.

    FetchMGs needs to know which sequences belong to which COG in order to perform
    the calibration step. The script reads the fasta_files and writes in a tsv
    the sequence id and the corresponding OGs id. The OG ids are coming from the name
    of the fasta_files.

    :param fasta_files: fasta files of the identified OGs
    :param true_positive_file: output file

    :type fasta_files: str
    :type true_positive_file: str
    """
    with open(true_positive_file, 'w') as fl:

        for fasta_file in fasta_files:
            proper_open = gzip.open if fasta_file.endswith('.gz') else open

            # Get the OG ID from the name of the fasta file
            cog_file_name = os.path.basename(fasta_file)
            cog_name = cog_file_name.split('.')[0]

            # write the id of sequence and the ID of the corresponding OG
            with proper_open(fasta_file, 'rt') as handle:
                for record in SeqIO.parse(handle, "fasta"):
                    fl.write(f'{record.id}\t{cog_name}\n')


def filter_fasta_file(fasta_file, taxids_list):
    """Filter fasta file that belong to any taxid of the provided taxids_list.

    The sequence id is composed of the taxid and the name of the gene seperated
    by a dot: example >1000565.METUNv1_00041

    Only the sequences that belong to any taxid of the provided list

    :param fasta_file: proteome file of eggnog
    :param taxids_list: list of taxid separated by a newline

    :type fasta_file: str
    :type taxids_list: list
    """
    logging.info('filter proteome file')
    s_processed = 0

    # output file
    filtered_fasta_file = 'filtered_' + os.path.basename(fasta_file)

    with open(fasta_file) as fl_r, open(filtered_fasta_file, 'w') as fl_w:
        for record in SeqIO.parse(fl_r, "fasta"):
            taxid = record.id.split('.', 1)[0]
            if taxid in taxids_list:
                s_processed += 1
                SeqIO.write(record, fl_w, "fasta")

            if s_processed % 10000 == 0:
                logging.info(f'sequences processed {s_processed}')
    return filtered_fasta_file


if __name__ == '__main__':
    import doctest
    doctest.testmod()
