#!/usr/bin/env python3

"""
Functions downloading and parsing ncbi files.

"""

# Metadata
__author__ = 'Mainguy Jean - Plateforme bioinformatique Toulouse'
__copyright__ = 'Copyright (C) 2019 INRA'
__license__ = 'GNU General Public License'
__version__ = '0.1'
__email__ = 'support.bioinfo.genotoul@inra.fr'
__status__ = 'dev'


# module importation
import os
import logging
import csv
import hashlib
import time


def ftp_download(path, out_dir, check_dir=None):
    """
    Download by rsync the given path.

    Check if the file we want to download is already in current directory or in check dir.
    """
    if check_dir:
        logging.info(f"Check if file exist in {check_dir}")
        outfile_in_check_dir = os.path.join(check_dir, os.path.basename(path))
        logging.info(f'File in check dir: {outfile_in_check_dir} ')
        if os.path.isfile(outfile_in_check_dir):
            logging.info("File is in check dir")
            return outfile_in_check_dir
        elif os.path.isfile(outfile_in_check_dir[:-len('.gz')]):
            logging.info("File is in check dir without the '.gz' extension")
            return outfile_in_check_dir[:-len('.gz')]
        else:
            logging.info("it is not a file")

    output_file = os.path.join(out_dir, os.path.basename(path))
    logging.info(f"check file in current dir.. {output_file}")
    if not os.path.isfile(output_file) and not os.path.isfile(output_file[:-len('.gz')]):

        if path.startswith('ftp'):
            path = path.replace('ftp', 'rsync', 1)  # replace first ftp

        os.system(f'rsync --copy-links --times --verbose {path} {out_dir}/  # > /dev/null')
    else:
        logging.info(f'{output_file} File is already there.. it is not downloaded again')
    return output_file


def read_assembly_summary(asssembly_summary):
    """
    Read assembly summary.

    An assembly_summary file is a tab seperated file containing information
    on assembly
    The two first line are commentary line starting with #
    The second # line is the header of the file

    This function read the assembly file using the csv.DictReader which transform
    a line in a dictionary using fieldnames (coming from the second # line) as keys

    ... Note
    See ftp://ftp.ncbi.nlm.nih.gov/genomes/README_assembly_summary.txt for a description of the columns in this file.
    """
    logging.info(asssembly_summary)
    with open(asssembly_summary, 'r') as fl:
        # The file start with two line of strating with '#'
        header_readme = next(fl)
        fieldnames_line = next(fl)
        assert fieldnames_line.startswith(
            '# '), 'first line of assembly summary does not start with a #'
        assert header_readme.startswith(
            '# '), 'second line of assembly summary does not start with a #'
        # extract fieldnames
        fieldnames = fieldnames_line[len('# '):].rstrip().split('\t')
        header = header_readme + fieldnames_line
        logging.info(header)
        # yield header
        # # parse the file with the csv module that transform each line in dict
        tsv_reader = csv.DictReader(fl, delimiter='\t', fieldnames=fieldnames)
        for assembly in tsv_reader:
            # print(assembly)
            yield assembly


def select_best_assembly(assembly1, assembly2):
    """
    Select the best assembly between two possible one given in argument.

    The selection is based on first the refseq category:
        reference genome is better than representative genome which is better than 'na' category

    In case of equality of category, the selection is based on the level of completeness of the assembly

    :Example:

    >>> assembly1 = {'refseq_category':'representative genome', 'assembly_level':"Complete Genome"}
    >>> assembly2 = {'refseq_category':'representative genome', 'assembly_level':"Scaffold"}
    >>> select_best_assembly(assembly1, assembly2) == assembly1
    True
    >>> assembly3 = {'refseq_category':'reference genome', 'assembly_level':"Chromosome"}
    >>> select_best_assembly(assembly1, assembly3) == assembly3
    True
    """
    categories_rank = {'reference genome': 1, 'representative genome': 2, "na": 3}
    assembly_lvl_ranks = {"Complete Genome": 1, 'Chromosome': 2, 'Scaffold': 3, 'Contig': 4}

    # rank of the genome refseq category
    rank_cat_assembly1 = categories_rank[assembly1['refseq_category']]
    rank_cat_assembly2 = categories_rank[assembly2['refseq_category']]

    # rank of the level of assembly :
    rank_lvl_assembly1 = assembly_lvl_ranks[assembly1['assembly_level']]
    rank_lvl_assembly2 = assembly_lvl_ranks[assembly2['assembly_level']]

    # Refseq Category is checked first
    if rank_cat_assembly1 < rank_cat_assembly2:
        return assembly1
    elif rank_cat_assembly2 < rank_cat_assembly1:
        return assembly2

    # Same refseq category then we check the assembly_level
    else:  # rank_cat_assembly1 == rank_cat_assembly2:
        if rank_lvl_assembly1 < rank_lvl_assembly2:
            return assembly1
        else:
            return assembly2


def get_taxid_ranked_lineage(file):
    """
    Parse rankedlineage.dmp file from ncbi new_taxdump.

    Add rank to it to prevent confusion
    and return dict with key=taxid and values=dict of rank:taxon

    ... Note
    Header from ftp://ftp.ncbi.nlm.nih.gov/pub/taxonomy/new_taxdump/taxdump_readme.txt
    """
    ranks = ['tax_id', 'tax_name', 'species', 'genus', 'family', 'order',
             'class', 'phylum', 'kingdom', 'superkingdom']
    taxid_ranked = {}
    with open(file) as fl:
        for l in fl:
            splited_line = l.rstrip().replace('\t|', '').split('\t')
            line_dict = {rank: name for rank, name in zip(ranks, splited_line)}
            taxid = line_dict.pop('tax_id')
            taxid_ranked[taxid] = line_dict

            earlier_taxon = 'other'
            for rank in ranks[1:][:: -1]:
                taxon = line_dict[rank]
                # manage when taxonomy is empty at some rank
                if taxon == '':
                    taxon = earlier_taxon
                    line_dict[rank] = f'{taxon} other [{rank}]'

                    # Small fix for species
                    if rank == 'species' and line_dict['tax_name']:
                        tax_name = line_dict['tax_name']
                        line_dict[rank] = f'{tax_name} [{rank}]'

                else:
                    line_dict[rank] = f'{taxon} [{rank}]'

                earlier_taxon = taxon

    return taxid_ranked


def md5(fname):
    """
    Taken from:
    https://stackoverflow.com/questions/3431825/generating-an-md5-checksum-of-a-file
    """
    hash_md5 = hashlib.md5()
    with open(fname, "rb") as f:
        for chunk in iter(lambda: f.read(4096), b""):
            hash_md5.update(chunk)
    return hash_md5.hexdigest()


def download_assembly(output_dir, assembly_accession, ftp_path, suffix_list, download_lastest_version=False):
    """
    Download by rsync the given path.

    Check if the file we want to download is already in current directory or in check dir.
    """

    preassembly_path = assembly_accession.replace('_', '').split('.')[0]
    assembly_path = '/'.join([preassembly_path[index: index + 3]
                              for index in range(0, len(preassembly_path), 3)])

    assembly_output_dir = os.path.join(output_dir,  assembly_path, assembly_accession)
    logging.debug(f'assembly_output_dir {assembly_output_dir}')
    os.makedirs(assembly_output_dir, exist_ok=True)

    md5checksums_fl = os.path.join(assembly_output_dir, 'md5checksums.txt')
    md5checksums_ftp_path = os.path.join(ftp_path, 'md5checksums.txt')

    if download_lastest_version and os.path.isfile(md5checksums_fl):
        md5checksums_fl_old = os.path.join(assembly_output_dir, 'md5checksums.txt.old')
        os.rename(md5checksums_fl, md5checksums_fl_old)

    if not os.path.isfile(md5checksums_fl) or os.stat(md5checksums_fl).st_size == 0:

        attempt = 0
        attempt_max = 5
        while attempt < 5:
            # print(attempt)
            exit_code = os.system(f'wget {md5checksums_ftp_path}  -O {md5checksums_fl} -q')
            if exit_code == 0 and os.stat(md5checksums_fl).st_size > 0:
                # downloading has succeded 
                break
            else:            
                logging.warning(f'download of md5 has failed (attempt {attempt/attempt_max}). exit code={exit_code} and md5 file size is {os.stat(md5checksums_fl).st_size}')
                attempt += 1

        if not os.path.isfile(md5checksums_fl) or os.stat(md5checksums_fl).st_size == 0:
            raise FileNotFoundError(
                f'{md5checksums_fl} is not found, the downloading step of md5 check sum has failed')

    with open(md5checksums_fl, 'r') as fl:
        file_to_md5checksums_dict = {os.path.basename(
            l.split('  ')[1].rstrip()): l.split('  ')[0] for l in fl}
    # print(file_to_md5checksums_dict)
    max_attempt = 10
    files = []
    for suffix in suffix_list:

        file_name = os.path.basename(ftp_path) + suffix
        ftp_path_to_file = os.path.join(ftp_path, file_name)
        file_path = os.path.join(assembly_output_dir, file_name)

        attempt = 0
        flag_done = False
        sleep_second = 5

        # Stat to be printed in logs
        was_already_downloaded = os.path.isfile(file_path)
        md5_fails = 0

        while flag_done is False and attempt < max_attempt:
            if os.path.isfile(file_path):
                pass
            elif os.path.isfile(file_path[:-len('.gz')]):
                file_path = file_path[:-len('.gz')]
                file_name = os.path.basename(file_path)
            else:
                # download_count += 1
                logging.info(f'Download of {file_name}')
                # os.system(f'wget {ftp_path}  -O {file_path} -q')
                exit_code = os.system(
                    f"rsync --copy-links -q --times {ftp_path_to_file.replace('ftp', 'rsync', 1)} {assembly_output_dir}/  # > /dev/null")
                logging.info(f'exit code {exit_code}')
            if not os.path.isfile(file_path):
                attempt += 1
                sleep_second += 5
                logging.warning(
                    f"fail in downloading (attempt:{attempt}/{max_attempt})... let's wait {sleep_second} seconds and try again")
                time.sleep(sleep_second)
                continue

            if md5(file_path) == file_to_md5checksums_dict[file_name]:

                flag_done = True
                files.append(file_path)
            else:
                md5_fails += 1
                logging.warning(f'md5 checksum failed for {file_path}')
                os.remove(file_path)
                attempt += 1
                logging.warning(f"fail in downloading (attempt:{attempt}/{max_attempt})..")

        if not os.path.isfile(file_path):
            raise FileNotFoundError(f'{file_path} is not found, the downloading step has failed')

        new_file_version = md5_fails > 0 and was_already_downloaded
        stat_by_file = {'assembly_accession': assembly_accession,
                        'file': file_name,
                        'attempt': attempt,
                        'md5_fails': md5_fails,
                        'was_already_downloaded': was_already_downloaded,
                        'new_file_version': new_file_version
                        }
        stat_by_file_str = '\t'.join([f'{k}={v}' for k, v in stat_by_file.items()])
        logging.info(f'STATBYFILE:{stat_by_file_str}')

    logging.info(f'Files retrieved for {assembly_accession}')

    return files


def parse_taxidlineage_file(taxidlineage_file):
    """Parse taxidlineage file from ncbi new taxdump."""
    taxid_to_lineage = {}
    with open(taxidlineage_file) as fl:
        for line in fl:
            taxid, taxidlineage = line.rstrip().replace('\t|', '').split('\t')
            taxid_to_lineage[taxid] = taxidlineage.rstrip().split(' ')
    return taxid_to_lineage


if __name__ == '__main__':
    import doctest
    doctest.testmod()
