import csv
import os
import sys
import shutil


def tsv_to_list_of_dict(file):
    """Load a tsv file into a list of dictionnary"""
    with open(file) as csvfile:
        reader = csv.DictReader(csvfile, delimiter='\t')
        return list(reader)


def main():
    trace_file = 'nextflow_result_2/trace.1'
    work_dir = 'work/'
    trace_info = tsv_to_list_of_dict(trace_file)
    dir_hash_to_keep = [d['hash'] for d in trace_info]

    for dir in os.listdir(work_dir):
        if len(dir) != 2:
            prin(dir)

            continue
        subdirs = (os.path.join(dir, sub_dir)
                   for sub_dir in os.listdir(os.path.join(work_dir, dir)))
        for sub_dir in subdirs:
            print(sub_dir[:9])
            sub_dir_hash = sub_dir[:9]
            if sub_dir_hash in dir_hash_to_keep:
                print('in trace... ')
            else:
                print('not in trace lets delete')
                shutil.rmtree(os.path.join(work_dir, sub_dir))


if __name__ == '__main__':
    main()
