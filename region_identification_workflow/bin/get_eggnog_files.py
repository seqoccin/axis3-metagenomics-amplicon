#!/usr/bin/env python3

"""Download eggnog files of the previously identified orthologs groups.

The downloaded files are:
* the whole proteome of eggnog which is used in calibration of fetchMGs
* hmm profiles of the identified ogs: used by fetchMGs to identified genes
* faa sequences of the identiied ogs: used to create a true positive files used by fetchMGs to calibrate cutoffs

Before downloading the script checks if the files are in the check dir to not download them twice.

When the files are not in the checkdir the files are downloaded in specific directories.
These directories are published by nextflow in the check dir to insure that the downloaded files
are at the end in the check dir.

Symbolics link of the proteome file and hmm files are created in specific directories regardless if they
are comming from a fresh download or the check dir. These symblink are used by nextflow in further process

:Example:
$ python scripts/get_eggnog_files.py  list_of_cog_id.txt

"""

# Metadata
__author__ = 'Mainguy Jean - Plateforme bioinformatique Toulouse'
__copyright__ = 'Copyright (C) 2019 INRA'
__license__ = 'GNU General Public License'
__version__ = '0.1'
__email__ = 'support.bioinfo.genotoul@inra.fr'
__status__ = 'dev'


import os
from argparse import ArgumentParser, ArgumentDefaultsHelpFormatter
import logging

from publicdb import eggnog
import tools


def parse_arguments():
    """Parse script arguments."""
    parser = ArgumentParser(description="...",
                            formatter_class=ArgumentDefaultsHelpFormatter)
    parser.add_argument("cogs_list_file", type=str,
                        help="List of OGs IDs separated by newline")
    parser.add_argument("--check_dir", type=str, default='eggNOG_data/',
                        help="Check if needed file exist in this directory. If it is the case, we don't redownload it")
    parser.add_argument("-v", "--verbose", help="increase output verbosity",
                        action="store_true")

    args = parser.parse_args()
    return args


def main():

    args = parse_arguments()

    # Set the logging level. When the flag verbose is defined to true
    # logging are displayed from level info
    if args.verbose:
        logging.basicConfig(format="%(levelname)s: %(message)s", level=logging.DEBUG)
        logging.info("Verbose output.")
    else:
        logging.basicConfig(format="%(levelname)s: %(message)s")

    check_dir = args.check_dir
    cogs_list_file = args.cogs_list_file

    # Output directories
    faa_dir = './faa_sequences'
    hmm_dir = './hmm_profiles'

    # Output files
    true_positive_file = 'true_positive_sequences_' + os.path.basename(cogs_list_file)
    hmm_dir_symlink = 'profile_hmm_links'

    # eggnog URLs
    fasta_seq_url = "http://eggnogapi5.embl.de/nog_data/text/fasta/"
    hmm_profile_url = "http://eggnogapi5.embl.de/nog_data/file/hmm/"
    eggnog_proteome_url = 'http://eggnog5.embl.de/download/eggnog_5.0/e5.proteomes.faa'

    # Check dirs paths of the different kind of files download
    fasta_seq_check_dir = os.path.join(check_dir, "faa_sequences")
    hmm_profile_check_dir = os.path.join(check_dir, 'hmm_profiles')

    # Create the different output directories
    os.makedirs(hmm_dir, exist_ok=True)
    os.makedirs(faa_dir, exist_ok=True)
    os.makedirs(hmm_dir_symlink, exist_ok=True)

    # Get the name of the COGs
    with open(cogs_list_file) as fl:
        cogs_list = [c for c in fl.read().split('\n') if c]
        logging.info(f'nb cogs {len(cogs_list)}')

    # Get Proteome of eggnog :
    proteome_file_basename = os.path.basename(eggnog_proteome_url)
    proteome_file_in_faa_dir = os.path.join(faa_dir, proteome_file_basename)
    proteome_file_in_check_dir = os.path.join(fasta_seq_check_dir, proteome_file_basename)

    if os.path.exists(proteome_file_in_faa_dir):
        logging.info("proteome file exists in current dir")
        proteome_file = proteome_file_in_faa_dir

    elif os.path.exists(proteome_file_in_check_dir):
        logging.info("proteome file exists in check dir")
        proteome_file = proteome_file_in_check_dir

    else:
        logging.info('download proteome file..')
        proteome_file = eggnog.download_eggnog_files(
            eggnog_proteome_url, proteome_file_in_faa_dir)

    # Creation of proteome file symlink in current dir to be used by nextflow in further process
    os.symlink(os.path.realpath(proteome_file), proteome_file_basename)

    # get faa sequences of the selected COGs
    logging.info('Get faa sequences..')
    og_fasta_files = eggnog.get_nog_data(
        cogs_list, faa_dir, fasta_seq_url, '.faa', fasta_seq_check_dir)
    # Create true positive file used by fetchMGs during the calibration step
    logging.info('create_true_positive_file.')
    eggnog.create_true_positive_file(og_fasta_files, true_positive_file)

    # Get hmm profiles of selected COGs:
    logging.info('Get hmm profile..')
    hmm_profile_files = eggnog.get_nog_data(
        cogs_list, hmm_dir, hmm_profile_url, '.hmm', hmm_profile_check_dir)

    # Unzip hmm files and create a symblink
    for f in hmm_profile_files:
        if f.endswith('.gz'):
            os.system(f'gunzip {f}')
            f = f[:-len(".gz")]

        # create a symblink of the hmm file to be then used by nextflow
        if not os.path.exists(os.path.join(hmm_dir_symlink, os.path.basename(f))):
            os.symlink(os.path.realpath(f), os.path.join(hmm_dir_symlink, os.path.basename(f)))


if __name__ == '__main__':
    main()
