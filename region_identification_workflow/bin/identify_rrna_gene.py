#!/usr/bin/env python3

"""
Description

:Example:
identify_rrna_gene.py --gff_files GCF_*/*gff* -v
"""

# Metadata
__author__ = 'Mainguy Jean - Plateforme bioinformatique Toulouse'
__copyright__ = 'Copyright (C) 2019 INRA'
__license__ = 'GNU General Public License'
__version__ = '0.1'
__email__ = 'support.bioinfo.genotoul@inra.fr'
__status__ = 'dev'


from argparse import ArgumentParser, ArgumentDefaultsHelpFormatter
import logging
import gzip
import re
import os
import tools


def retrieve_rrna_genes_from_gff(gff_file):
    """

    .. note:: see gff fields: https://www.ensembl.org/info/website/upload/gff.html
    """
    gene_infos = []
    circular_regions = {}
    length_regions = {}

    partial_genes_count = 0
    complete_genes_count = 0

    name_pattern = re.compile('(\d{1,2}S)\w{0,1} (ribosomal |)RNA', re.IGNORECASE)
    name_pattern_alternative = re.compile('ribosomal RNA[-\ ]{1}(\d{1,2}S)', re.IGNORECASE)

    proper_open = gzip.open if gff_file.endswith('.gz') else open
    with proper_open(gff_file, "rt") as fl:
        for l in fl:
            if l.startswith('#'):
                continue

            seqid, source, type, start, end, score, strand, phase, attributes = l.rstrip().split('\t')

            if type == 'region':
                length_regions[seqid] = end

                if 'Is_circular=true' in attributes:
                    circular_regions[seqid] = True
                else:
                    circular_regions[seqid] = False

            if type == 'rRNA':
                attributes_dict = {attr.split('=')[0]: attr.split('=')[1]
                                   for attr in attributes.split(';')}
                try:
                    product = attributes_dict['product']
                except KeyError:
                    logging.critical(
                        f'product is not found in attributes section of the rRNA gff line\nline:\n{l}')
                    continue
                try:
                    gene_id = attributes_dict['ID']
                except KeyError:
                    logging.critical(
                        f'ID is not found in attributes section of the rRNA gff line\nline:\n{l}')
                    continue

                if name_pattern.match(product):
                    # logging.info(name_pattern.search(product).group(1))
                    rna_name = name_pattern.match(product).group(1)

                elif name_pattern_alternative.match(product):
                    rna_name = name_pattern_alternative.match(product).group(1)
                else:
                    logging.warning(
                        f'regex patterns {name_pattern} and {name_pattern_alternative} did not match rRNA name in product section: {product} in {gff_file}')
                    continue

                print(l)  # print to keep all rrna line of interest..
                partial = 'partial=true' in attributes
                if partial:
                    # we don't take this rna gene because it is not complete...
                    partial_genes_count += 1
                else:
                    complete_genes_count += 1
                    gene_infos.append({"gene_id": gene_id,
                                       "seqid": seqid,
                                       "product": product,
                                       "gene_name": rna_name,
                                       "start": start,
                                       "end": end,
                                       "strand": strand,
                                       "cog_id": rna_name,
                                       'circular': circular_regions[seqid],
                                       'seqid_length': length_regions[seqid],
                                       'partial': partial})

        logging.debug(
            f'rRNA identification: {os.path.realpath(gff_file)}: {partial_genes_count + complete_genes_count} rrna genes: {complete_genes_count} complete and {partial_genes_count} partial.')
        logging.debug(f"rRNA identification: rRNA gene found: { {info['gene_name'] for info in gene_infos} }")
        return gene_infos


def parse_arguments():
    """Parse script arguments."""
    parser = ArgumentParser(description="...",
                            formatter_class=ArgumentDefaultsHelpFormatter)
    parser.add_argument("--gff_files", required=True, nargs='+',
                        help='Path of a gff file or a folder of files.')
    parser.add_argument("-v", "--verbose", help="increase output verbosity",
                        action="store_true")
    parser.add_argument("-o", "--outdir", default='cog_genomic_position', help="output directory")

    args = parser.parse_args()
    return args


def main():

    args = parse_arguments()

    if args.verbose:
        logging.basicConfig(format="%(levelname)s: %(message)s", level=logging.DEBUG)
        logging.info('Mode verbose ON')

    else:
        logging.basicConfig(format="%(levelname)s: %(message)s")
    # Output dir
    output_dir = args.outdir
    os.makedirs(output_dir, exist_ok=True)

    file_handlers_dict = {'open_files': []}

    gff_files = args.gff_files
    for gff_file in gff_files:
        genome_name = os.path.basename(gff_file[:gff_file.rindex('_genomic.gff')])

        gene_infos = retrieve_rrna_genes_from_gff(gff_file)
        for gene_info in gene_infos:

            cog_id = gene_info['cog_id']
            gene_info['genome'] = genome_name
            filename = os.path.join(output_dir, cog_id+'.tsv')
            tools.write_dict_to_tsv(gene_info, file_handlers_dict, filename)

    for f in file_handlers_dict['open_files']:
        f.close()

    # Output file
    # gene_id	seqid	start	end	strand	circular	seqid_length	cog_id	genome


if __name__ == '__main__':
    main()
