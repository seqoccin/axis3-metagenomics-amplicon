#!/usr/bin/env python3

"""
Parse eggnog files and identify COGs that fit the given single copy threshold.

:Example:
$ python scripts/parse_and_identify_universal_cog.py --tax_level 237 \
                                                    --check_dir eggNOG_data \
                                                    --single_copy_treshold 0.95 \
                                                    --get_COG_gene_name

"""

# Metadata
__author__ = 'Mainguy Jean - Plateforme bioinformatique Toulouse'
__copyright__ = 'Copyright (C) 2019 INRA'
__license__ = 'GNU General Public License'
__version__ = '0.1'
__email__ = 'support.bioinfo.genotoul@inra.fr'
__status__ = 'dev'


import logging
from argparse import ArgumentParser, ArgumentDefaultsHelpFormatter
import os
import gzip

from publicdb import eggnog
from publicdb import ncbi
import tools


def manage_multi_tax_levels(taxid_selection, new_taxdump_dir, main_tax_level, eggnog_file):

    taxidlineage_file = os.path.join(new_taxdump_dir, "taxidlineage.dmp")
    merged_file = os.path.join(new_taxdump_dir, "merged.dmp")
    names_file = os.path.join(new_taxdump_dir, "names.dmp")

    with open(taxid_selection) as fl:
        selected_taxids = [l.strip() for l in fl if not l.startswith('#')]

    # PARSE taxidlineage_file
    taxid_to_lineage = ncbi.parse_taxidlineage_file(taxidlineage_file)

    # Parse merge_fl
    old_to_new_taxid = {}
    with open(merged_file) as fl:
        for line in fl:
            old_taxid, new_taxid = line.rstrip().replace('\t|', '').split('\t')
            old_to_new_taxid[old_taxid] = new_taxid

    # PARSE EGGNOG MEMBER FILE
    eggnog_taxids = set()
    proper_open = gzip.open if eggnog_file.endswith('.gz') else open
    with proper_open(eggnog_file, "rt") as fl:
        for line in fl:
            line = line.rstrip()
            taxlvl, GroupName, ProteinCount, SpeciesCount, ProteinIDs, taxIDS = line.split('\t')
            tax_ids = taxIDS.split(',')
            # taxid in a set to get the total number of genomes at the end
            eggnog_taxids |= set(tax_ids)

    # Selection of specific eggnog taxids that are part of the specified tax levels.
    selected_eggnog_taxids = []
    selected_taxon_to_egg_taxid = {taxid: [] for taxid in selected_taxids}
    for egg_taxid in eggnog_taxids:
        try:
            lineage = taxid_to_lineage[egg_taxid]
        except KeyError:
            new_taxid = old_to_new_taxid[egg_taxid]
            lineage = taxid_to_lineage[new_taxid]

        for taxid in lineage:
            if taxid in selected_taxids:
                selected_eggnog_taxids.append(egg_taxid)
                # taxon_name = selected_taxids[taxid]
                selected_taxon_to_egg_taxid[taxid].append(egg_taxid)

    logging.info(f"{len(selected_eggnog_taxids)}/{len(eggnog_taxids)} eggnog taxids are selected.")
    # logging.info('=='*10)
    # [logging.info(f'{k}:{len(v)}') for k, v in selected_taxon_to_egg_taxid.items()]
    # logging.info('=='*10)
    return selected_eggnog_taxids


def parse_arguments():
    """Parse script arguments."""
    parser = ArgumentParser(description="...",
                            formatter_class=ArgumentDefaultsHelpFormatter)
    parser.add_argument("--tax_level", type=str, required=True, default='2',
                        help="Is the ncbi taxon id of the taxonomic level studied.")

    parser.add_argument("--cog_single_copy_prct", type=float, default=95,
                        help="""an orthologs group is selected if it is found in single copy in more than a specified
                            percentage of the total genomes""")
    parser.add_argument("--cog_prct", type=float, default=95,
                        help="""a orthologs group is selected if it is found in more than a
                         specified percentage of the total genomes""")
    parser.add_argument("--check_dir", type=str, default='.',
                        help="Check if needed file exist in the given directory. If it is the case, we don't redownload it")

    parser.add_argument('--sample_test_run', type=str, default=False,
                        help='if it is "true" then only a third of OGs are selected')

    parser.add_argument('--get_COG_gene_name', action='store_true',
                        help='Get gene name associated with the selected COGs')

    parser.add_argument("--taxid_selection", type=str, default=None,
                        help="File with a list of taxids on which cog selection will be based upon.")

    parser.add_argument("--new_taxdump_dir", type=str, default='.',
                        help="Directory of new taxdump.")

    parser.add_argument("-v", "--verbose", help="increase output verbosity",
                        action="store_true")

    args = parser.parse_args()
    return args


def main():
    """Parse eggnog files and identify COGs that fit the given single copy threshold."""
    args = parse_arguments()

    # Set the logging level. When the flag verbose is defined to true
    # logging are displayed from level info
    if args.verbose:
        logging.basicConfig(format="%(levelname)s: %(message)s", level=logging.DEBUG)
        logging.info("Verbose output.")
    else:
        logging.basicConfig(format="%(levelname)s: %(message)s")

    # Arguments given when the scripts is called
    single_copy_treshold = args.cog_single_copy_prct/100  # 0.95
    genomes_threshold = args.cog_prct/100
    tax_level = args.tax_level
    check_dir = args.check_dir

    # Creation of the url of the member file corresponding to the tax level
    eggnog_members_url = f'http://eggnog5.embl.de/download/eggnog_5.0/per_tax_level/{tax_level}/{tax_level}_members.tsv.gz'

    # Ouput files
    selected_cogs_stat_tsv = "identified_cogs.tsv"  # 'cogs_single_copy.json'
    cogs_stat_tsv = "all_cogs.tsv"  # 'cogs_single_copy.json'
    cogs_list_file = f"cogs_list.txt"
    taxid_list_file = "taxid_list.txt"

    logging.info(f'tax_level = {tax_level}')

    logging.info(f'get_eggnog_members_file:')

    eggnog_members_file = eggnog.get_eggnog_members_file(
        eggnog_members_url, check_dir=check_dir)

    logging.info(f"eggnog_members_file: {eggnog_members_file}")

    if not args.taxid_selection:
        cogs, taxids = eggnog.parse_eggnog_members_file(eggnog_members_file)

    # when multi tax levels is on
    else:
        logging.info(f'Multi tax levels selection.')
        taxid_selection = args.taxid_selection
        new_taxdump_dir = args.new_taxdump_dir
        # cog selection will be made based on this selection of taxids that are included in the tax levels of multi_tax_levels
        eggnog_taxids_sub_selection = manage_multi_tax_levels(
            taxid_selection, new_taxdump_dir, tax_level, eggnog_members_file)

        cogs, taxids = eggnog.parse_eggnog_members_file(
            eggnog_members_file, taxids_sub_selection=eggnog_taxids_sub_selection)

    total_genomes = len(taxids)
    logging.info(f"number of genomes: {total_genomes}")

    # filter to only keep cogs that fit the threshold
    selected_ogs = [c for c in cogs if c['nb_single_copy']/total_genomes >=
                    single_copy_treshold and c['SpeciesCount']/total_genomes >= genomes_threshold]

    if len(selected_ogs) == 0:
        raise ValueError(
            f'None of the OGs fit the single copy threshold of {single_copy_treshold*100}')
    logging.info(
        f"{len(selected_ogs)} cogs are found in at least {genomes_threshold*100}% of the genomes.")
    logging.info(f"And in single copy in {single_copy_treshold*100}% of the genomes")

    if args.sample_test_run == 'true':
        number_of_cogs_kept = int(len(selected_ogs)/3) + 1
        logging.warning(f'Sample test run on a sample of {number_of_cogs_kept} Cogs')
        selected_ogs = selected_ogs[:number_of_cogs_kept]

    # Write COGS list
    with open(cogs_list_file, 'w') as fl:
        fl.write('\n'.join([c["GroupName"] for c in selected_ogs])+'\n')

    # Write taxids list to filter proteome file
    with open(taxid_list_file, 'w') as fl:
        fl.write('\n'.join([t for t in taxids])+'\n')

    # Get the name associated with COG id
    if args.get_COG_gene_name:
        logging.info(f"gene name of the COG are being retrieved")
        for c in cogs:
            if c in selected_ogs:
                logging.info(c['GroupName'])
                c['cog_gene_name'] = eggnog.get_COG_gene_name(
                    c['GroupName'], check_dir=args.check_dir)
            else:
                c['cog_gene_name'] = c['GroupName']
    # variable cogs is updated by cog_possible_gene_names
    # write identified cogs in a tsv
    tools.list_of_dict_to_tsv(selected_ogs, selected_cogs_stat_tsv)
    tools.list_of_dict_to_tsv(cogs, cogs_stat_tsv)


if __name__ == '__main__':

    main()
