#!/usr/bin/env python3

"""Add taxonomy column to assembly file."""

# Metadata
__author__ = 'Mainguy Jean - Plateforme bioinformatique Toulouse'
__copyright__ = 'Copyright (C) 2019 INRA'
__license__ = 'GNU General Public License'
__version__ = '0.1'
__email__ = 'support.bioinfo.genotoul@inra.fr'
__status__ = 'dev'


import csv
from argparse import ArgumentParser, ArgumentDefaultsHelpFormatter

from publicdb import ncbi


def parse_arguments():
    """Parse script arguments."""
    parser = ArgumentParser(description="Add taxonomy column to assembly file.",
                            formatter_class=ArgumentDefaultsHelpFormatter)

    parser.add_argument("assembly_file", type=str,
                        help="tsv file with info on the assembly.")
    parser.add_argument("taxonomy_file", type=str,
                        help="ncbi taxonomy file rankedlineage.dmp extracted from new_taxdump.tar.gz ")
    parser.add_argument("-o", "--output", type=str, default='assemblies_with_tax.tsv',
                        help="output file : final assembly selection")
    args = parser.parse_args()
    return args


def main():
    """Add taxonomy column to assembly file."""
    args = parse_arguments()
    assembly_file = args.assembly_file
    taxonomy_file = args.taxonomy_file
    output_file = args.output

    taxid_ranked_lineage = ncbi.get_taxid_ranked_lineage(taxonomy_file)

    with open(assembly_file) as input_file, open(output_file, "w") as csvfile:
        reader = csv.DictReader(input_file, delimiter='\t')

        for i, d in enumerate(reader):
            d['taxonomy'] = ';'.join(taxid_ranked_lineage[d['taxid']].values())

            if i == 0:
                writer = csv.DictWriter(csvfile, fieldnames=list(d.keys()), delimiter='\t')
                writer.writeheader()

            writer.writerow(d)


if __name__ == '__main__':
    main()
