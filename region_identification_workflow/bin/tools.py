#!/usr/bin/env python3

"""General functions use across all the project."""

# Metadata
__author__ = 'Mainguy Jean - Plateforme bioinformatique Toulouse'
__copyright__ = 'Copyright (C) 2019 INRA'
__license__ = 'GNU General Public License'
__version__ = '0.1'
__email__ = 'support.bioinfo.genotoul@inra.fr'
__status__ = 'dev'


# module importation
import logging
import json
import csv
from collections import defaultdict


def encoder(filename, f):
    """Encode a python structure in a json file."""
    with open(filename, 'w', encoding='utf8') as file:
        json.dump(f, file, indent=2)


def decoder(filename):
    """Load a json file and return the corresponding structure."""
    with open(filename, 'r', encoding='utf8') as file:
        return json.load(file)


def tsv_to_list_of_dict(file):
    """Load a tsv file into a list of dictionnary."""
    with open(file) as csvfile:
        reader = csv.DictReader(csvfile, delimiter='\t')
        return list(reader)


def list_of_dict_to_tsv(list_of_dict, output_file, mode='w', writeheader=True):
    """Encode a list of dictionnary into a tsv file.

    The mode of writing can be specified.
    By default the mode is set to "w"

    """
    if len(list_of_dict) == 0:
        raise ValueError('The given list is empty: we cant write it in a file')

    with open(output_file, mode, newline='') as csvfile:
        writer = csv.DictWriter(csvfile, fieldnames=list(list_of_dict[0].keys()), delimiter='\t')
        if mode == 'w' and writeheader is True:
            writer.writeheader()
        for dict_info in list_of_dict:
            writer.writerow(dict_info)


def print_verbose(string):
    """
    Print the given string using the logging info level.

    When verbose mode is specified the logging info is set to info in the main function
    and the string in argument is then displayed
    """
    logging.info(string)


def tsv_to_dict_of_dicts(file, key_field):
    """
    Take a tsv with header and parse into dict of dict.

    Uses
    * the specified field as key
    * the line turned into a dict as value .
    """
    dict_of_list_of_dict = defaultdict(list)
    with open(file) as csvfile:
        reader = csv.DictReader(csvfile, delimiter='\t')
        for l in reader:
            dict_of_list_of_dict[l[key_field]].append(l)
        return dict_of_list_of_dict


def write_dict_to_tsv(dict_to_write, handlers_dict, filename):
    """Write dict object to tsv."""
    if filename in handlers_dict:
        handlers_dict[filename].writerow(dict_to_write)
    else:
        fh = open(filename, 'w', newline='')
        writer = csv.DictWriter(fh, fieldnames=list(dict_to_write.keys()), delimiter='\t')
        writer.writeheader()
        writer.writerow(dict_to_write)
        handlers_dict[filename] = writer
        try:
            handlers_dict['open_files'].append(fh)
        except KeyError:
            handlers_dict['open_files'] = []
            handlers_dict['open_files'].append(fh)
