#!/usr/bin/env python3

"""
Download ncbi assemblies taken from a given selected assembly file.

The script takes a tsv file (selected_assemblies) with information about assemblies
to download. This tsv file should at least contain a ftp_path column with ftp_paths
of the folder of the assemblies.
Before downloading, the scripts checks if the files already exist in a check dir.

:Example:
$ python download_ncbi_genome.py <selected_assemblies_file>
"""

# Metadata
__author__ = 'Mainguy Jean - Plateforme bioinformatique Toulouse'
__copyright__ = 'Copyright (C) 2019 INRA'
__license__ = 'GNU General Public License'
__version__ = '0.1'
__email__ = 'support.bioinfo.genotoul@inra.fr'
__status__ = 'dev'

import os
from argparse import ArgumentParser, ArgumentDefaultsHelpFormatter
import csv
import logging
from publicdb import ncbi
import time


def parse_arguments():
    """Parse script arguments."""
    parser = ArgumentParser(description="...",
                            formatter_class=ArgumentDefaultsHelpFormatter)

    parser.add_argument("selected_assemblies", type=str,
                        help="""tsv file with a least a ftp_path column with
                        ftp_path to the folder of the selected assemblies""")
    parser.add_argument("--check_dir", type=str, default='ncbi_data/assemblies',
                        help="Check if needed file exist in this directory. If it is the case, we don't redownload it")
    parser.add_argument("--download_lastest_version", type=str, choices=['True', 'False', 'true', 'false'], default="True",
                        help="Download lastest version of assembly files.")
    parser.add_argument("-v", "--verbose", help="increase output verbosity",
                        action="store_true")
    parser.add_argument("--add_symblink", help="Add symblink of downloaded assemblies into the current directory. Usefull inside the nextflow pipeline.",
                    action="store_true")

    args = parser.parse_args()
    return args


def main():
    args = parse_arguments()
    if args.verbose:
        logging.basicConfig(format="%(levelname)s: %(message)s", level=logging.DEBUG)
        logging.info('Mode verbose ON')

    else:
        logging.basicConfig(format="%(levelname)s: %(message)s")

    selected_assemblies = args.selected_assemblies
    check_dir = args.check_dir
    add_symblink = args.add_symblink
    logging.info(f'add symblink in current dir {add_symblink}')

    download_lastest_version = args.download_lastest_version == 'True' or args.download_lastest_version == 'true'
    suffix_list = ['_protein.faa.gz', '_genomic.gff.gz', "_genomic.fna.gz"]
    logging.info(f'Script checks if assembly exist in {check_dir}')

    with open(selected_assemblies) as csvfile:
        header = next(csvfile).split('\t')
        reader = csv.DictReader(csvfile, fieldnames=header, delimiter='\t')

        for row in reader:
            if row['ftp_path'] == 'na':
                logging.warning(f'{row["assembly_accession"]} has no ftp paths')
                continue

            assembly_accession = row["assembly_accession"]
 
            #download_assembly(output_dir, check_dir, assembly_accession, ftp_path, suffix_list)
            files = ncbi.download_assembly(
                check_dir, assembly_accession, row['ftp_path'], suffix_list, download_lastest_version)
                
            if add_symblink:
                dir_symblink = 'symblink_' + assembly_accession
                os.makedirs(dir_symblink, exist_ok=True)
        
            for file in files:
                # extra layer of checking
                if not os.path.isfile(file):
                    raise FileNotFoundError(f'{file} is not found, the downloading step has failed')
                # Create symbilink
                if add_symblink:
                    os.symlink(os.path.realpath(file), os.path.join(
                        dir_symblink, os.path.basename(file)))


if __name__ == '__main__':
    main()
