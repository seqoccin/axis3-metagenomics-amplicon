#!/usr/bin/env python3

"""

:Example:
$ python transformed_assembly_summary.py \
    --assemblies_summary_taxonomy all_assemblies_with_taxonomy.tsv \
    --genomes_ids_selected selected_assembly_id.txt
"""

# Metadata
__author__ = 'Mainguy Jean - Plateforme bioinformatique Toulouse'
__copyright__ = 'Copyright (C) 2019 INRA'
__license__ = 'GNU General Public License'
__version__ = '0.1'
__email__ = 'support.bioinfo.genotoul@inra.fr'
__status__ = 'dev'


from argparse import ArgumentParser, ArgumentDefaultsHelpFormatter
import logging
import csv
import os


def parse_arguments():
    parser = ArgumentParser(description="...",
                            formatter_class=ArgumentDefaultsHelpFormatter)

    parser.add_argument("--assemblies_summary_taxonomy", help="", required=True)
    parser.add_argument("--dataset_ids", default=None, nargs='+',
                        help="list of file that constitue a dataset. Names of the files are used to defined the column name")
    parser.add_argument("--col_to_transform", default=' ', help="")
    parser.add_argument("--genomes_ids_to_color", default=None,
                        help="list of genome id that are covered globally across dataset")

    parser.add_argument("--color_labl", default='selected',
                        help="label associated with the assemblies given in input ie: selected or covered")
    parser.add_argument("-v", "--verbose", help="increase output verbosity",
                        action="store_true")

    args = parser.parse_args()
    return args


def main():

    args = parse_arguments()

    if args.verbose:
        logging.basicConfig(format="%(levelname)s: %(message)s", level=logging.DEBUG)
        logging.info('Mode verbose ON')

    else:
        logging.basicConfig(format="%(levelname)s: %(message)s")

    col_to_transforms = args.col_to_transform.split()
    assemblies_summary_taxonomy = args.assemblies_summary_taxonomy
    genomes_ids_datasets_files = args.dataset_ids
    color_labl = args.color_labl
    genomes_ids_to_color = args.genomes_ids_to_color

    logging.info(f'col_to_transforms: {col_to_transforms}')
    col_to_transforms_possible_val = {c: set() for c in col_to_transforms}
    assembly_id_to_line = {}
    with open(assemblies_summary_taxonomy) as csvfile:
        reader = csv.DictReader(csvfile, delimiter='\t')
        for l in reader:
            assembly_id_to_line[l['assembly_accession']] = l
            for col in col_to_transforms:
                try:
                    col_to_transforms_possible_val[col].add(l[col])
                except KeyError:
                    raise KeyError(f"{col} is not a column of the input file {assemblies_summary_taxonomy}")
    logging.info(col_to_transforms_possible_val)

    output_col_name = []
    logging.info(f'genomes_ids_selected {genomes_ids_datasets_files}')
    if genomes_ids_datasets_files:

        logging.info('genomes_ids_selected is ON')
        genomes_ids_per_file = {}
        for genomes_ids_file in genomes_ids_datasets_files:
            with open(genomes_ids_file) as fl:
                genomes_ids_file_simple_name = os.path.basename(genomes_ids_file).split('.')[0]
                genomes_ids_per_file[genomes_ids_file_simple_name] = {l.rstrip() for l in fl}

            logging.info(f'{len(genomes_ids_file)} assembly id selected')
            output_col_name.append(f'{genomes_ids_file_simple_name}')

    if genomes_ids_to_color:
        logging.info('genomes_ids_selected is ON')
        with open(genomes_ids_to_color) as fl:
            assembly_ids_covered = {l.rstrip() for l in fl}
        logging.info(f'{len(assembly_ids_covered)} assembly id selected')
        output_col_name.append(color_labl)

    for col_name, possible_vals in col_to_transforms_possible_val.items():
        if len(possible_vals) < 5:
            output_col_name += list(possible_vals)

    output_col_name.append('taxonomy')
    print('\t'.join([c for c in output_col_name]))
    logging.info(f'genomes_ids_datasets_files {genomes_ids_datasets_files}')
    for id, line in assembly_id_to_line.items():
        line_transformed = {}
        line_transformed['taxonomy'] = line['taxonomy']

        if genomes_ids_datasets_files:
            for file_name, genomes_ids in genomes_ids_per_file.items():
                line_transformed[f'{file_name}'] = '1' if id in genomes_ids else '0'

        if genomes_ids_to_color:
            line_transformed[color_labl] = '1' if id in assembly_ids_covered else '0'

        for col_name, possible_vals in col_to_transforms_possible_val.items():
            for v in possible_vals:
                line_transformed[v] = '1' if line[col_name] == v else '0'

        line_str = '\t'.join([line_transformed[c] for c in output_col_name])
        print(line_str)


if __name__ == '__main__':
    main()
