#!/usr/bin/env python3

"""
Create plot describing the selection of regions.

:Example:
python plot_region_selection.py -h
"""

# Metadata
__author__ = 'Mainguy Jean - Plateforme bioinformatique Toulouse'
__copyright__ = 'Copyright (C) 2019 INRA'
__license__ = 'GNU General Public License'
__version__ = '0.1'
__email__ = 'support.bioinfo.genotoul@inra.fr'
__status__ = 'dev'


from argparse import ArgumentParser, ArgumentDefaultsHelpFormatter
import logging
import csv
import plotly.graph_objects as go
import pandas as pd


def is_cog_with_itself(pair):
    """Is the cog pair is made of twice the same cog."""
    return True if len(set(pair.split('-'))) == 1 else False


def get_gene_name(cog_id, cog_to_gene):
    try:
        return cog_to_gene[cog_id]
    except KeyError:
        logging.warning(f'No Gene name found for {cog_id}')
        return cog_id


def from_cog_pair_to_gene_pair(cog_pair, cog_to_gene):
    """Get gene names associated with cog ids of the pair."""
    try:
        cog1, cog2 = cog_pair.split('-')
    except ValueError:
        logging.warning(f'Region name {cog_pair} is not formatted like a pair of genes/COGs')
        return cog_pair
    return f'{get_gene_name(cog1, cog_to_gene)}-{get_gene_name(cog2, cog_to_gene)}'


def parse_arguments():
    """Parse script arguments."""
    parser = ArgumentParser(description="Create plot describing the selection of regions.",
                            formatter_class=ArgumentDefaultsHelpFormatter)
    parser.add_argument("--all_pairs_file", help="pair stat file", required=True,)
    parser.add_argument("--selected_pairs_file", help="selected pair stat file", required=True,)
    parser.add_argument("--pairs_to_highlight",
                        help="supplementary pairs  to highlight", nargs='+', default=[],)
    parser.add_argument("--cog_annotation", required=True,
                        help="Cog annotation tsv file with at least column GroupName and cog_gene_name")
    parser.add_argument("--nb_assemblies", type=int, default=1,
                        help="minimal prct of genomes to display")
    parser.add_argument("-v", "--verbose", help="increase output verbosity",
                        action="store_true")
    args = parser.parse_args()
    return args


def main():

    args = parse_arguments()

    if args.verbose:
        logging.basicConfig(format="%(levelname)s: %(message)s", level=logging.DEBUG)
        logging.info('Mode verbose ON')

    else:
        logging.basicConfig(format="%(levelname)s: %(message)s")

    all_pairs_fl = args.all_pairs_file
    selected_pairs_fl = args.selected_pairs_file
    nb_assemblies = args.nb_assemblies
    pairs_to_highlight = args.pairs_to_highlight

    cog_annotation_file = args.cog_annotation

    color_regions_highlighted = [
        '#1f77b4',  # muted blue
        '#2ca02c',  # cooked asparagus green
        '#ff7f0e',  # safety orange
        '#9467bd',  # muted purple
        '#8c564b',  # chestnut brown
        '#e377c2',  # raspberry yogurt pink
        '#7f7f7f',  # middle gray
        '#bcbd22',  # curry yellow-green
        '#17becf'   # blue-teal,
        '#d62728',  # brick red
    ]
    with open(cog_annotation_file) as tsvfile:
        reader = csv.DictReader(tsvfile, delimiter='\t')
        cog_to_gene = {d['GroupName']: d['cog_gene_name'] for d in reader}

    df_all = pd.read_csv(all_pairs_fl, sep='\t')

    df_selec = pd.read_csv(selected_pairs_fl, sep='\t')

    # add gene pair information by translating cog name to gene name
    df_all['gene_pair'] = df_all['cog_pair'].apply(
        from_cog_pair_to_gene_pair,  cog_to_gene=cog_to_gene)
    df_selec['gene_pair'] = df_selec['cog_pair'].apply(
        from_cog_pair_to_gene_pair,  cog_to_gene=cog_to_gene)

    # Filtering region to remove the one that have a very weak coverage
    filter_len = df_all['nb_genome_with_main_orga_post_len_filtering'] > nb_assemblies/5
    df_all = df_all.loc[filter_len]

    # Remove region that are made of the same cog ie : cog1 with cog1
    filter_pair_vs_itself = df_all['cog_pair'].apply(is_cog_with_itself)
    df_all = df_all.loc[~filter_pair_vs_itself]

    # Add coverage and text column
    df_all['coverage'] = df_all['nb_genome_with_main_orga_post_len_filtering']/nb_assemblies
    df_all['text'] = df_all["cog_pair"] + "<br>" + df_all["gene_pair"]

    df_selec['coverage'] = df_selec['nb_genome_with_main_orga_post_len_filtering']/nb_assemblies
    df_selec['text'] = df_selec["cog_pair"] + "<br>" + df_selec["gene_pair"]

    # PLOT MEDIAN LENGTH VS COVERAGE
    # Plot
    traces = []
    trace = go.Scatter(
        x=df_all['coverage'],
        y=df_all['median_post_len_filtering'],
        marker=dict(size=8, color='darkgrey'),
        mode="markers",
        showlegend=True,
        name='All regions',
        hoverinfo='text',
        text=df_all['text']
    )
    traces.append(trace)

    # Plot selected region
    trace = go.Scatter(
        x=df_selec['coverage'],
        y=df_selec['median_post_len_filtering'],
        marker=dict(size=9, color='crimson'),
        mode="markers",
        showlegend=True,
        name='Selected regions',
        hoverinfo='text',
        text=df_selec['text']
    )

    traces.append(trace)
    i = 0
    for region in pairs_to_highlight:
        if region in list(df_selec['cog_pair']):
            color = color_regions_highlighted[i]
            i += 1
            logging.warning(f'Region {region} is going to be highlighted in the plot')
            df_to_highlight = df_selec[df_selec['cog_pair'] == region]

            trace = go.Scatter(
                x=df_to_highlight['coverage'],
                y=df_to_highlight['median_post_len_filtering'],
                marker=dict(size=9, color=color),
                mode="markers",
                showlegend=True,
                name=region,
                hoverinfo='text',
                text=df_to_highlight['text']
            )
            traces.append(trace)

        else:
            logging.warning(f'{region} is not found in selected pairs. It cannot be highlighted')
    fig = go.Figure(traces)
    fig.update_layout(title="",
                      yaxis_title="median length", xaxis_title="Genome coverage",)
    logging.info('writing... region_length_vs_coverage.html')
    fig.write_html('region_length_vs_coverage.html')

    # PLOT SELECTED REGION BY MEDIAN LENGTH

    df_selec = df_selec.sort_values('median_post_len_filtering')
    traces = []
    showlegend = True if pairs_to_highlight else False
    trace = go.Scatter(
        x=df_selec['cog_pair'],
        y=df_selec['median_post_len_filtering'],
        marker=dict(size=8,  color='crimson'),
        mode="markers",
        showlegend=showlegend,
        name='Selected regions',
        hoverinfo='text',
        text=df_selec['text'],

        error_y=dict(
            type='data',  # value of error bar given in data coordinates
            array=df_selec['std_post_len_filtering'],
            visible=True),
    )
    traces.append(trace)
    i = 0
    for region in pairs_to_highlight:
        if region in list(df_selec['cog_pair']):
            color = color_regions_highlighted[i]
            i += 1
            logging.warning(f'Region {region} is going to be highlighted in the plot')
            df_to_highlight = df_selec[df_selec['cog_pair'] == region]

            trace = go.Scatter(
                x=df_to_highlight['cog_pair'],
                y=df_to_highlight['median_post_len_filtering'],
                marker=dict(size=8, color=color),
                mode="markers",
                showlegend=True,
                name=region,
                hoverinfo='text',
                text=df_to_highlight['text'],
                error_y=dict(
                    type='data',  # value of error bar given in data coordinates
                    array=df_to_highlight['std_post_len_filtering'],
                    visible=True),
            )
            traces.append(trace)
        else:
            logging.warning(f'{region} is not found in selected pairs. It cannot be highlighted')
    fig = go.Figure(traces)
    fig.update_layout(title="",
                      yaxis_title="median length of the region", xaxis_title="Selected regions",)
    if len(df_selec) > 30:
        fig.update_xaxes(showticklabels=False)
    logging.info('writing... selected_regions_median_length.html')
    fig.write_html('selected_regions_median_length.html')


if __name__ == '__main__':
    main()
