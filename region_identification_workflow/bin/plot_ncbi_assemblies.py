#!/usr/bin/env python3

"""
Plot refseq categories and the assembly level in refseq assemblies

:Example:
$ python scripts/bar_plot_refseq_assemblies.py --refseq_summary refseq_summary.tsv
"""

# Metadata
__author__ = 'Mainguy Jean - Plateforme bioinformatique Toulouse'
__copyright__ = 'Copyright (C) 2019 INRA'
__license__ = 'GNU General Public License'
__version__ = '0.1'
__email__ = 'support.bioinfo.genotoul@inra.fr'
__status__ = 'dev'

from argparse import ArgumentParser, ArgumentDefaultsHelpFormatter
import logging
import csv
from collections import defaultdict
import plotly.graph_objects as go
import os

"""
# refseq_category
   RefSeq Category: whether the assembly is a reference or representative genome
   in the NCBI Reference Sequence (RefSeq) project classification.

* **reference genome:**
    a manually selected high quality genome
    assembly that NCBI and the community have
    identified as being important as a standard
    against which other data are compared


* **representative genome:**
    a genome computationally or manually selected
    as a representative from among the best
    genomes available for a species or clade that
    does not have a designated reference genome

* **na:**
    no RefSeq category assigned to this assembly


# Assembly_level
Assembly level: the highest level of assembly for any object in the genome
assembly.

* **Complete genome:**
    all chromosomes are gapless and have no runs of 10 or
    more ambiguous bases (Ns), there are no unplaced or
    unlocalized scaffolds, and all the expected chromosomes
    are present (i.e. the assembly is not noted as having
    partial genome representation). Plasmids and organelles
    may or may not be included in the assembly but if
    present then the sequences are gapless.
* **Chromosome:**
    there is sequence for one or more chromosomes. This
    could be a completely sequenced chromosome without gaps
    or a chromosome containing scaffolds or contigs with
    gaps between them. There may also be unplaced or
    unlocalized scaffolds.
* **Scaffold:**
    some sequence contigs have been connected across gaps to
    create scaffolds, but the scaffolds are all unplaced or
    unlocalized.
* **Contig:**
    nothing is assembled beyond the level of sequence
    contigs
"""


def parse_arguments():
    parser = ArgumentParser(description="...",
                            formatter_class=ArgumentDefaultsHelpFormatter)
    parser.add_argument("--refseq_summary", help="refseq summary table", )

    parser.add_argument("-n", "--name", help="output base name ", default='refseq_assemblies')
    parser.add_argument("-o", "--outdir", help="output diectory ", default='./')
    parser.add_argument("-v", "--verbose", help="increase output verbosity",
                        action="store_true")

    args = parser.parse_args()
    return args


def main():

    args = parse_arguments()

    if args.verbose:
        logging.basicConfig(format="%(levelname)s: %(message)s", level=logging.DEBUG)
        logging.info('Mode verbose ON')

    else:
        logging.basicConfig(format="%(levelname)s: %(message)s")

    refseq_summary = args.refseq_summary
    name = args.name
    outputdir = args.outdir
    refseq_categories = defaultdict(list)
    with open(refseq_summary) as csvfile:
        genomes_dict_iter = csv.DictReader(csvfile, delimiter='\t')

        for g in genomes_dict_iter:
            refseq_categories[g['refseq_category']].append(g)
    traces = []

    assembly_lvl_cat = ['Complete Genome', 'Chromosome', 'Scaffold', 'Contig']
    cat_color = {"reference genome": "#3F88C5", 'representative genome': "#ED6F5B", 'na': "#E0CA3C"}
    for cat, assemblies in refseq_categories.items():

        assembly_lvl_count = defaultdict(int)

        for d in assemblies:
            assembly_lvl_count[d['assembly_level']] += 1

        assembly_lvl_cat = [c for c in assembly_lvl_cat if c in assembly_lvl_count]
        # print(assembly_lvl_cat)
        y = [assembly_lvl_count[c] for c in assembly_lvl_cat]
        trace = go.Bar(
            x=assembly_lvl_cat,
            y=y,
            name=cat,
            text=y,
            textposition='auto',
            marker_color=cat_color[cat]
        )
        traces.append(trace)

    layout = go.Layout(
        barmode='stack'
    )
    fig = go.Figure(data=traces, layout=layout)
    fig.write_html(os.path.join(outputdir, name + '_bar_plot.html'))

    labels = []
    values = []
    colors = []
    for cat, assemblies in refseq_categories.items():
        labels.append(cat)
        values.append(len(assemblies))
        colors.append(cat_color[cat])

    fig = go.Figure(data=[go.Pie(labels=labels, values=values)])

    fig.update_traces(hoverinfo='label+percent', textinfo='label+value', textfont_size=18,
                      marker=dict(colors=colors))

    fig.write_html(os.path.join(outputdir, name + '_pie_plot.html'))


if __name__ == '__main__':
    main()
