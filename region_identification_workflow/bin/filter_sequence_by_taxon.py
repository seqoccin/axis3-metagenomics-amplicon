#!/usr/bin/env python3


import gzip
import os
import logging
from argparse import ArgumentParser, ArgumentDefaultsHelpFormatter
import tools
import csv
from Bio import SeqIO
import re


def parse_arguments():
    parser = ArgumentParser(description="...",
                            formatter_class=ArgumentDefaultsHelpFormatter)

    parser.add_argument("sequence_file", type=str,
                        help="")
    parser.add_argument("all_assemblies_with_taxonomy", type=str,
                        help="")
    parser.add_argument("taxon_name", type=str,
                        help="")
    parser.add_argument("--assembly_list_to_keep", type=str, default=None,
                        help="")
    args = parser.parse_args()
    return args


def main():
    args = parse_arguments()
    sequence_file = args.sequence_file
    all_assemblies_with_taxonomy = args.all_assemblies_with_taxonomy
    taxon_name = args.taxon_name
    filter_seq_file = f"{taxon_name.split(' ')[0]}_" + os.path.basename(sequence_file)
    capture_assemby_name = re.compile("^([A-Z]{3}_\d+.\d+)")

    if args.assembly_list_to_keep:
        with open(args.assembly_list_to_keep) as fl:
            assembly_to_keep = {a.rstrip() for a in fl}
    else:
        assembly_to_keep = set()
    print(len(assembly_to_keep))
    # Parse assembly_taxonomy_file

    with open(all_assemblies_with_taxonomy) as fl:
        reader = csv.DictReader(fl, delimiter='\t')
        for d in reader:
            # print(d)
            tax = d['taxonomy'].rstrip().split(';')
            if taxon_name in tax:
                assembly_to_keep.add(d['assembly_accession'].rstrip())

    print(len(assembly_to_keep))

    with open(sequence_file) as fl, open(filter_seq_file, 'w') as out:
        for seq in SeqIO.parse(fl, "fasta"):
            assembly_name = capture_assemby_name.search(seq.id).group(1)
            # print(assembly_name)

            if assembly_name in assembly_to_keep:
                SeqIO.write(seq, out, "fasta")


if __name__ == '__main__':
    main()
