#!/usr/bin/env python3

"""
Filter proteome file.

The eggnong proteome file consists of all sequences used in eggnog
Here, we only need sequences that belong to the taxonomic level that we study.
Fasta headers start with the taxid of the sequence.

The script takes the proteome file or chunck of the proteome file and the list
of taxids needed. The script parses  the header and write only sequences
that belong to a taxid of the list.

:Example:
$ python filter_proteome_file.py <fasta_file> <taxids_list_file>
"""

# Metadata
__author__ = 'Mainguy Jean - Plateforme bioinformatique Toulouse'
__copyright__ = 'Copyright (C) 2019 INRA'
__license__ = 'GNU General Public License'
__version__ = '0.1'
__email__ = 'support.bioinfo.genotoul@inra.fr'
__status__ = 'dev'


from argparse import ArgumentParser, ArgumentDefaultsHelpFormatter
import logging

from publicdb import eggnog
import tools


def parse_arguments():
    """Parse script arguments."""
    parser = ArgumentParser(description="""Filter proteome file

    The eggnong proteome file consists of all sequences used in eggnog
    Here, we only need sequences that belong to the taxonomic level that we study.
    Fasta headers start with the taxid of the sequence.

    The script takes the proteome file or chunck of the proteome file and the list
    of taxids needed. The script parses  the header and write only sequences
    that belong to a taxid of the list.""",
                            formatter_class=ArgumentDefaultsHelpFormatter)
    parser.add_argument("fasta_file", type=str,
                        help="fasta file ")
    parser.add_argument("taxids_list_file", type=str,
                        help="")
    parser.add_argument("-v", "--verbose", help="increase output verbosity",
                        action="store_true")

    args = parser.parse_args()
    return args


def main():

    args = parse_arguments()

    # Set the logging level. When the flag verbose is defined to true
    # logging are displayed from level info
    if args.verbose:
        logging.basicConfig(format="%(levelname)s: %(message)s", level=logging.DEBUG)
        logging.info("Verbose output.")
    else:
        logging.basicConfig(format="%(levelname)s: %(message)s")

    taxids_list_file = args.taxids_list_file
    fasta_file = args.fasta_file

    # read taxid list
    with open(taxids_list_file) as fl:
        taxids_list = [c for c in fl.read().split('\n') if c]
        logging.info(f'nb taxids {len(taxids_list)}')

    # filter and write sequences that belong to a taxid of the list
    eggnog.filter_fasta_file(fasta_file, taxids_list)


if __name__ == '__main__':
    main()
