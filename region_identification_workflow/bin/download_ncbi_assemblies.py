#!/usr/bin/env python3

"""
Download all refseq assemblies.

:Example:
$ python download_refseq_assemblies
"""

# Metadata
__author__ = 'Mainguy Jean - Plateforme bioinformatique Toulouse'
__copyright__ = 'Copyright (C) 2019 INRA'
__license__ = 'GNU General Public License'
__version__ = '0.1'
__email__ = 'support.bioinfo.genotoul@inra.fr'
__status__ = 'dev'


from argparse import ArgumentParser, ArgumentDefaultsHelpFormatter
import os
import logging
import time
import tools
from publicdb import ncbi


def parse_arguments():

    parser = ArgumentParser(description="...",
                            formatter_class=ArgumentDefaultsHelpFormatter)

    parser.add_argument("--assembly_summary", type=str, required=True,
                        help="TSV file with at least assembly_accession and ftp path column")
    parser.add_argument("--outdir", type=str, default='./',
                        help="tsv file with at least assembly_accession and ftp path column")
    parser.add_argument("-v", "--verbose", help="increase output verbosity",
                        action="store_true")

    args = parser.parse_args()
    return args


def main():

    args = parse_arguments()

    if args.verbose:
        logging.basicConfig(format="%(levelname)s: %(message)s", level=logging.INFO)
        logging.info('Mode verbose ON')

    else:
        logging.basicConfig(format="%(levelname)s: %(message)s")

    assembly_summary = args.assembly_summary
    assemblies_dir = args.outdir

    assemblies_info = tools.tsv_to_list_of_dict(assembly_summary)

    suffix_list = ['_protein.faa.gz', '_genomic.gff.gz', "_genomic.fna.gz"]
    assembly_dir = 'ncbi_data/refseq_assemblies'
    # os.makedirs(assembly_dir, exist_ok=True)

    download_attempts = 0
    logging.info(f'OUT DIR {assemblies_dir}')
    for i, assembly in enumerate(assemblies_info):
        if assembly['ftp_path'] == 'na':
            logging.warning(f'{assembly["assembly_accession"]} has no ftp paths')
            continue

        ftp_path = assembly['ftp_path']
        assembly_accession = assembly['assembly_accession']

        logging.debug(assembly_accession)
        logging.debug(ftp_path)
        files = ncbi.download_assembly(assemblies_dir, assembly_accession, ftp_path, suffix_list)

    logging.info(f'Files from {i} genomes have been downloaded or retrieved.')


if __name__ == '__main__':
    main()
