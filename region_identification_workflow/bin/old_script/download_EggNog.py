#!/usr/bin/env python3

from urllib.request import Request, urlopen
import gzip
import sys
import csv
import os


def download_EggNOG_files_OLD(url, output_file):
    # OLD
    req = Request(url)
    req.add_header('Accept-Encoding', 'gzip')
    response = urlopen(req)
    content = gzip.decompress(response.read())
    decomp_req = content.splitlines()
    file_content = '\n'.join([line.decode('utf-8') for line in decomp_req])

    with open(output_file, 'w') as fw:
        fw.write(file_content)


def download_EggNOG_files(url, output_file):
    """

    if the downloaded output_file is gzipped
    then the downloaded file is rename by adding .gz
    ONLY if output_file doesn't have already the .gz
    """
    os.system(f'wget {url} -O {output_file}')  # > /dev/null')

    file_type = os.popen(f'file {output_file} -bi').read().rstrip()
    if (file_type == 'application/x-gzip; charset=binary' or file_type == 'application/gzip; charset=binary') and not output_file.endswith('.gz'):
        os.rename(output_file, output_file + '.gz')
        output_file = output_file + '.gz'

    # if not output_file.endswith('.gz'):
    # assert os.popen(f'file {output_file} -bi').read().rstrip() == 'text/plain; charset=us-ascii'
    return output_file


def build_eggNOG_url(cog_id):
    # Accessing EggNOG raw data
    # RESTFul (web) API
    # http://eggnogdb.embl.de/#/app/api
    url = f"http://eggnogapi.embl.de/nog_data/text/hmm/{cog_id}"
    return url


if __name__ == '__main__':

    COG_list_file = sys.argv[1]
    COG_dir = sys.argv[2]

    print(COG_list_file)
    COG_set = set()
    with open(COG_list_file) as csvfile:
        reader = csv.DictReader(csvfile, delimiter='\t')

        for row in reader:
            COG_set.add(row['COG ID'])

    for cog_id in COG_set:
        print(cog_id)
        hmm_file = os.path.join(COG_dir, f'{cog_id}.hmm')
        if not os.path.isfile(hmm_file):
            url = build_eggNOG_url(cog_id)
            download_EggNOG_files(url, hmm_file)
