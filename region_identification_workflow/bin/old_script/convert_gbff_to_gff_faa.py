import csv
import gzip
from Bio import SeqIO
from argparse import ArgumentParser, ArgumentDefaultsHelpFormatter
import os
import logging
from Bio.SeqRecord import SeqRecord
from Bio.Seq import Seq
from Bio.Alphabet import generic_protein


def initiate_gff_file(gff_writer):
    genome_header = ["seqid",  # name of the chromosome or scaffold; chromosome names can be given with or without the 'chr' prefix. Important note: the seq ID must be one used within Ensembl, i.e. a standard chromosome name or an Ensembl identifier such as a scaffold ID, without any additional content such as species or assembly. See the example GFF output below.
                     # name of the program that generated this feature, or the data source (database or project name)
                     "source",
                     "type",  # type of feature. Must be a term or accession from the SOFA sequence ontology
                     # Start position of the feature, with sequence numbering starting at 1.
                     "start",
                     "end",  # End position of the feature, with sequence numbering starting at 1.
                     "score",  # A floating point value.
                     "strand",  # defined as + (forward) or", # (reverse).
                     "phase",  # One of '0', '1' or '2'. '0' indicates that the first base of the feature is the first base of a codon, '1' that the second base is the first base of a codon, and so on..
                     "attributes",  # A semicolon#separated list of tag#value pairs, providing additional information about each feature. Some of these tags are predefined, e.g. ID, Name, Alias, Parent", # see the GFF documentation for more details.
                     ]

    gff_writer.write("##gff-version 3\n")
    genome_csv_writer = csv.DictWriter(gff_writer, fieldnames=genome_header, delimiter='\t')
    # genome_csv_writer.writeheader()

    return genome_csv_writer


def write_gff_annotation(writer, feat, seqid):
    if "protein_id" not in feat.qualifiers and "pseudo" in feat.qualifiers:
        logging.warning(f'{feat} Pseudo Gene: no protein_id')

    try:
        name = feat.qualifiers['locus_tag'][0]
    except KeyError:
        logging.warning('NO locus tag for CDS:\n' + str(feat))
        name = '??'
    # attribute = f'GCF..;taxon_id_{cds.segment.taxon_id};cluster_{cluster_nb}'
    info_dict = {"attributes": f"Name={name}",
                 "source": '.',
                 "score": '.',
                 "phase": '.'}
    # Write the cds:
    info_dict["strand"] = '+' if feat.location.strand > 0 else '-'
    info_dict["seqid"] = seqid
    info_dict["type"] = feat.type
    info_dict["start"] = int(feat.location.start)  # we don't write fuzzy position such as <2...
    info_dict["end"] = int(feat.location.end)  # doesn't take into account stop codon
    writer.writerow(info_dict)


def get_seq_obj(feat):
    try:
        name = feat.qualifiers['locus_tag'][0]
    except KeyError:
        logging.critical('NO locus tag for CDS:\n' + str(feat))
        exit(1)

    try:
        seq = feat.qualifiers['translation'][0]
    except KeyError:
        logging.critical('NO translation tag for CDS:\n' + str(feat))
        exit(1)

    return SeqRecord(Seq(seq, generic_protein), id=name, description='')


def parse_arguments():
    parser = ArgumentParser(description="...",
                            formatter_class=ArgumentDefaultsHelpFormatter)
    parser.add_argument("genbank_file", type=str, help="")

    # parser.add_argument("--", type=str, default='test/identified_EggNOG_genes.gff',
    #                     help="")
    args = parser.parse_args()
    return args


def main():
    args = parse_arguments()
    gb_file = args.genbank_file
    # gff_csv_writer, handle_genome_out = initiate_gff_file(genome_file_name)

    # Manage compress and not compress gb file
    proper_open = gzip.open if gb_file.endswith('.gz') else open

    base = gb_file[:gb_file.rindex('.')] if not gb_file.endswith(
        '.gz') else gb_file[:-len('.gz')][:gb_file.rindex('.')]
    if base.endswith('_genomic'):
        base = base[:-len('_genomic')]
    gff_file = f'{base}_genomic.gff'
    faa_file = f'{base}_protein.faa'

    with proper_open(gb_file, "rt") as handle, open(gff_file, 'w') as gff_writer, open(faa_file, 'w') as faa_writer:

        gff_csv_writer = initiate_gff_file(gff_writer)

        for i, record in enumerate(SeqIO.parse(handle, "genbank")):
            for feat in record.features:
                if feat.type == 'CDS':
                    write_gff_annotation(gff_csv_writer, feat, record.id)

                    cds = get_seq_obj(feat)
                    SeqIO.write(cds, faa_writer, "fasta")


if __name__ == '__main__':
    main()
