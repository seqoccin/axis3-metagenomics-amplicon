import os
import sys
from argparse import ArgumentParser, ArgumentDefaultsHelpFormatter
import csv
import gzip
import re
import logging
from argparse import ArgumentParser, ArgumentDefaultsHelpFormatter
import parser
from collections import defaultdict


def identify_blast_hit_of_interest(blast_result_fl, fieldnames, pident_threshold, coverage_threshold):
    subject_matching_queries = defaultdict(list)
    query_matching_subjects = defaultdict(list)
    with open(blast_result_fl) as csvfile:
        reader = csv.DictReader(csvfile, fieldnames=fieldnames, delimiter='\t')
        for row in reader:
            # remove taxid from the gene id of the query (eggnog gene) to be homogenous in the next steps
            # print(row)
            # row['qseqid'] = row['qseqid'].split('.', 1)[1]

            # Coverage calculation
            row['subject_cov'] = ((int(row['send']) - int(row['sstart'])+1) / int(row['slen']))*100
            row['query_cov'] = ((int(row['qend']) - int(row['qstart'])+1) / int(row['qlen']))*100

            # Filter sequence based on the pident and coverage of the longuest sequences.
            if int(row['qlen']) > int(row['slen']):
                row['coverage_smallest_seq'] = row['subject_cov']
                row['coverage_longuest_seq'] = row['query_cov']
            else:
                row['coverage_smallest_seq'] = row['query_cov']
                row['coverage_longuest_seq'] = row['subject_cov']

            if float(row['pident']) >= pident_threshold and row['coverage_longuest_seq'] >= coverage_threshold:
                subject_matching_queries[row['sseqid']].append(row)
                query_matching_subjects[row['qseqid']].append(row)

    return subject_matching_queries, query_matching_subjects


def retrieve_gene_info(matching_seq, gff_file, result_file):
    """
    GFF HEADER:
    seqid - name of the chromosome or scaffold; chromosome names can be given with or without the 'chr' prefix. Important note: the seq ID must be one used within Ensembl, i.e. a standard chromosome name or an Ensembl identifier such as a scaffold ID, without any additional content such as species or assembly. See the example GFF output below.
    source - name of the program that generated this feature, or the data source (database or project name)
    type - type of feature. Must be a term or accession from the SOFA sequence ontology
    start - Start position of the feature, with sequence numbering starting at 1.
    end - End position of the feature, with sequence numbering starting at 1.
    score - A floating point value.
    strand - defined as + (forward) or - (reverse).
    phase - One of '0', '1' or '2'. '0' indicates that the first base of the feature is the first base of a codon, '1' that the second base is the first base of a codon, and so on..
    attributes - A semicolon-separated list of tag-value pairs, providing additional information about each feature. Some of these tags are predefined, e.g. ID, Name, Alias, Parent - see the GFF documentation for more details.
    """
    identified_CDS = []
    name_pattern = re.compile("Name=([^;]+)")
    gene_info = {}
    proper_open = gzip.open if gff_file.endswith('.gz') else open
    with proper_open(gff_file, "rt") as fl:
        for l in fl:
            # print(l)
            if l.startswith('#'):
                continue

            seqid, source, type, start, end, score, strand, phase, attributes = l.rstrip().split('\t')

            if not type == 'CDS':
                continue
            try:
                prot_name = name_pattern.search(attributes).group(1)
            except AttributeError:
                logging.info(f'Prot Name not found in CDS line: {l}')
                continue

            if prot_name in matching_seq:
                # by default take the first EggNOG
                identified_CDS.append(prot_name)
                # Gather eggnome genes name in one to be the key of the info gene dict
                # Ideally there is only one corresponding eggnog gene
                # but in case of conflict more than one gene can match with the same ncbi gene
                # print(matching_seq[prot_name])
                # print(type(matching_seq))
                # for row in matching_seq[prot_name]:
                #     print('row', row)

                corresponding_eggnog_names = '|'.join(
                    [row['qseqid'] for row in matching_seq[prot_name]])
                gene_info[corresponding_eggnog_names] = l

    # Checking...
    if not set(identified_CDS) == set(matching_seq):
        logging.critical(f'Not all EggNOG genes have been found in the gff of the assembly . Missing genes: {set(matching_seq)- set(identified_CDS)}')

    parser.encoder(result_file, gene_info)


def get_gene_id_from_fasta(fasta_file):
    id_list = []
    with open(fasta_file) as fl:
        for l in fl:
            if l.startswith('>'):
                id_list.append(l.split('>', 1)[1].rstrip())
    return id_list


def parse_arguments():
    parser = ArgumentParser(description="...",
                            formatter_class=ArgumentDefaultsHelpFormatter)
    parser.add_argument("blast_results_file", type=str, help="")
    parser.add_argument("gff_file", type=str, help="")
    parser.add_argument("EggNog_genes_faa", type=str, help="")

    parser.add_argument("--result_gff_file", type=str, default='test/identified_EggNOG_genes.gff',
                        help="")
    parser.add_argument("--outfmt", type=str, default='qseqid qstart qend qlen sseqid sstart send length pident qcovs evalue',
                        help="")

    parser.add_argument("--pident_threshold", type=int, default=95, help="")
    parser.add_argument("--coverage_threshold", type=int, default=65, help="")
    parser.add_argument("--logfile", type=str, default=False)

    args = parser.parse_args()
    return args


def main():

    args = parse_arguments()

    outfmt = args.outfmt

    blast_results_file = args.blast_results_file  # "test/1406840_OG_genes.faa"
    gff_file = args.gff_file  # 'test/identified_EggNOG_genes.gff'
    EggNog_genes_faa = args.EggNog_genes_faa

    pident_threshold = args.pident_threshold  # 95
    coverage_threshold = args.coverage_threshold  # 75
    result_gff_file = args.result_gff_file
    json_file_recovery_stat = "blastp_recovery_stat.json"
    recovery_stat = {'expected_genes': [],
                     'retrieved_genes': []}
    col_names = outfmt.split(' ')
    if args.logfile:
        logging.basicConfig(filename=args.logfile, level=logging.WARNING)
    else:
        pass

    matching_seq, blast_eggnog_genes = identify_blast_hit_of_interest(
        blast_results_file, col_names, pident_threshold, coverage_threshold)
    eggnog_genes_ids = get_gene_id_from_fasta(EggNog_genes_faa)

    if not set(eggnog_genes_ids) == set(blast_eggnog_genes):
        logging.critical(f'{len(set(eggnog_genes_ids) - set(blast_eggnog_genes))} EggNOG genes have not found their counterpart in assembly genes')
        print(f'{len(set(eggnog_genes_ids) - set(blast_eggnog_genes))} EggNOG genes have not found their counterpart in assembly genes')
    retrieve_gene_info(matching_seq, gff_file, result_gff_file)

    recovery_stat['expected_genes'] = list(set(eggnog_genes_ids))
    recovery_stat['retrieved_genes'] = list(set(blast_eggnog_genes))
    recovery_stat['missing_genes'] = list(set(eggnog_genes_ids) - set(blast_eggnog_genes))
    parser.encoder(json_file_recovery_stat, recovery_stat)


if __name__ == '__main__':
    main()
