#!/usr/bin/env python3

"""Select assemblies."""

# Metadata
__author__ = 'Mainguy Jean - Plateforme bioinformatique Toulouse'
__copyright__ = 'Copyright (C) 2019 INRA'
__license__ = 'GNU General Public License'
__version__ = '0.1'
__email__ = 'support.bioinfo.genotoul@inra.fr'
__status__ = 'dev'


from argparse import ArgumentParser, ArgumentDefaultsHelpFormatter
import tools

from publicdb import ncbi
import logging


def parse_arguments():
    """Parse script arguments."""
    parser = ArgumentParser(description="...",
                            formatter_class=ArgumentDefaultsHelpFormatter)

    parser.add_argument("assembly_summary", type=str,
                        help="ncbi assembly summary containing information about all assemblies: taxids, ftp_path, level of assembly...")
    parser.add_argument("taxids", type=str,
                        help="taxids of interest: assemblies with a taxid included in this list is selected")
    parser.add_argument("-o", "--output", type=str, default='selected_assemblies.tsv',
                        help="output file : final assembly selection")
    parser.add_argument("-v", "--verbose", help="increase output verbosity",
                        action="store_true")
    args = parser.parse_args()
    return args


def main():
    args = parse_arguments()

    if args.verbose:
        logging.basicConfig(format="%(levelname)s: %(message)s", level=logging.DEBUG)
        logging.info('Mode verbose ON')

    else:
        logging.basicConfig(format="%(levelname)s: %(message)s")

    # input
    assembly_summary_file = args.assembly_summary
    taxids_file = args.taxids
    # output
    selected_assemblies_file = args.output

    # Get taxids of interest
    with open(taxids_file) as fl:
        taxids_file_content = fl.read()
        taxids = [int(taxid) for taxid in taxids_file_content.split('\n') if taxid != '']

    taxid_assembly_info = {}
    refseq_categories_of_interest = ['reference genome', 'representative genome']

    tsv_reader = tools.tsv_to_list_of_dict(assembly_summary_file)

    for i, assembly in enumerate(tsv_reader):
        if i % 10000 == 0:
            logging.info(i)
        if int(assembly['taxid']) in taxids and assembly['refseq_category'] in refseq_categories_of_interest:

            current_taxid = int(assembly['taxid'])
            if current_taxid in taxid_assembly_info:
                previous_assembly = taxid_assembly_info[current_taxid]
                assembly = ncbi.select_best_assembly(assembly, previous_assembly)

            taxid_assembly_info[int(assembly['taxid'])] = assembly
    logging.info(f'{len(taxid_assembly_info)} assemblies selected')
    tools.list_of_dict_to_tsv(list(taxid_assembly_info.values()),
                              selected_assemblies_file)


if __name__ == '__main__':
    main()
