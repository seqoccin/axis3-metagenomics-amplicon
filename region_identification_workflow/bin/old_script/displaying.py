#!/usr/bin/env python3

"""

Functions to format data in order to display it

"""

# Metadata
__author__ = 'Mainguy Jean - Plateforme bioinformatique Toulouse'
__copyright__ = 'Copyright (C) 2019 INRA'
__license__ = 'GNU General Public License'
__version__ = '0.1'
__email__ = 'support.bioinfo.genotoul@inra.fr'
__status__ = 'dev'


from collections import defaultdict


def human_format(num):
    if type(num) == str:
        num = round(float(num))

    magnitude = 0
    while abs(num) >= 1000:
        magnitude += 1
        num /= 1000.0
    # add more suffixes if you need them
    return '%.2f%s' % (num, ['', 'K', 'M', 'G', 'T', 'P'][magnitude])


def get_dict_matrix(pair_stats_list):

    # Create dict of dict matrix
    i = 0
    cog_cog_dict = defaultdict(dict)
    for pair_stat in pair_stats_list:
        i += 1
        cog1, cog2 = pair_stat['cog_pair'].split('-')

        cog_cog_dict[cog1][cog2] = pair_stat
        cog_cog_dict[cog2][cog1] = pair_stat
    print(i, 'pairs')
    return cog_cog_dict


def get_list_matrix(cogs, cog_cog_dict, pairs_vs_themself=False):
    """
    pairs_vs_themself: bool --> do we want to display in the heatmap cogs against them self?
    """
    stat_rows = []
    # Create a double list as expected by the heatmap
    for i, c1 in enumerate(cogs):
        # print(c1)
        stat_row = []
        for c2 in cogs[i:][::-1]:
            if not pairs_vs_themself and c1 == c2:
                #print(cog1, cog2, 'Not displayed')
                continue
            pair_stat = cog_cog_dict[c1][c2]
            pair_stat['cogy'] = c1
            pair_stat['cogx'] = c2
            stat_row.append(pair_stat)
        stat_rows.append(stat_row)
    return stat_rows


def get_z_legend_matrix(cogs, cog_cog_dict, cog_names, pairs_vs_themself=False):

    stat_rows = []
    # Create a double list as expected by the heatmap
    for i, c1 in enumerate(cogs):
        stat_row = []
        for c2 in cogs[i:][::-1]:
            if not pairs_vs_themself and c1 == c2:
                continue
            stat = cog_cog_dict[c1][c2]
            legend_list = []
            legend_list.append(f'x:{c2} ({cog_names[c2]})')
            legend_list.append(f'y:{c1} ({cog_names[c1]})')

            legend_list.append(f'mean:{human_format(stat["mean"])}')
            legend_list.append(f'median:{human_format(stat["median"])}')
            legend_list.append(f'std:{human_format(stat["std"])}')
            # legend_list.append(f'nb_represented_genomes:{stat["nb_represented_genomes"]}%')
            legend_list.append(f'nb_genome_with_main_orga:{stat["nb_genome_with_main_orga"]}')
            legend_list.append(f'nb_problematic_pairs:{stat["nb_problematic_pairs"]}')
            legend_list.append(f'std_500: {stat["nb_genome_std_500"]}')

            pair_stat = '<br>'.join(legend_list)

            stat_row.append(pair_stat)
        stat_rows.append(stat_row)
    return stat_rows


def get_z_matrix(cogs, cog_cog_dict, key, pairs_vs_themself=False):
    """
    pairs_vs_themself: bool --> do we want to display in the heatmap cogs against them self?
    """
    stat_rows = []
    # Create a double list as expected by the heatmap
    for i, c1 in enumerate(cogs):
        stat_row = []
        for c2 in cogs[i:][::-1]:
            if not pairs_vs_themself and c1 == c2:
                #print(cog1, cog2, 'Not displayed')
                continue
            try:
                pair_stat = cog_cog_dict[c1][c2][key]
            except KeyError:
                pair_stat = ''
            stat_row.append(pair_stat)
        stat_rows.append(stat_row)
    return stat_rows


def apply_to_list_of_list(fct, list_of_list):
    transformed_list_of_list = []
    for row in list_of_list:
        transformed_row = [fct(i) for i in row]
        transformed_list_of_list.append(transformed_row)
    return transformed_list_of_list
