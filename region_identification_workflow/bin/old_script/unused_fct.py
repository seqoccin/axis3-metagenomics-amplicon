import gzip
import os
import parser
from Bio import Entrez
import logging
from urllib.error import HTTPError
import time
from argparse import ArgumentParser, ArgumentDefaultsHelpFormatter
from collections import defaultdict


def get_fasta_file_by_taxid(og_fasta_files, taxid_fasta_dir):
    record_dict = defaultdict(list)
    header_by_taxids = defaultdict(list)
    for file in og_fasta_files:

        proper_open = gzip.open if file.endswith('.gz') else open

        with proper_open(file, 'rt') as handle:
            for record in SeqIO.parse(handle, "fasta"):
                # tools.print_verbose(record.id)
                taxid, geneid = record.id.split('.', 1)

                if record.id in header_by_taxids[taxid]:
                    logging.warning(f'sequence {record.id} has been already added. This gene is found in two Cogs')
                    continue
                header_by_taxids[taxid].append(record.id)
                record_dict[taxid].append(record)

    for taxid, records in record_dict.items():
        # with open(os.path.join(out_dir, f'{taxid}_OG_genes.faa'), 'a') as fl:
        SeqIO.write(records, os.path.join(taxid_fasta_dir, f'{taxid}_OG_genes.faa'), "fasta")


def filter_proteome_file(proteome_file, taxids_list):
    tools.print_verbose('filter proteome file')
    s_processed = 0
    filtered_proteome_file = 'filtered_' + os.path.basename(proteome_file)
    with open(proteome_file) as fl_r, open(filtered_proteome_file, 'w') as fl_w:
        for record in SeqIO.parse(fl_r, "fasta"):
            taxid = record.id.split('.', 1)[0]
            if taxid in taxids_list:
                s_processed += 1
                SeqIO.write(record, fl_w, "fasta")

            if s_processed % 10000 == 0:
                tools.print_verbose('sequences processed', s_processed)


def total_size(o):
    """Return the approximate memory footprint an object and all of its contents.

    Automatically finds the contents of the following builtin containers and
    their subclasses:  tuple, list, deque, dict, set and frozenset.

    """
    def dict_handler(d): return chain.from_iterable(d.items())
    all_handlers = {tuple: iter,
                    list: iter,
                    deque: iter,
                    dict: dict_handler,
                    set: iter,
                    frozenset: iter,
                    }
    seen = set()                      # track which object id's have already been seen
    default_size = sys.getsizeof(0)       # estimate sizeof object without __sizeof__

    def sizeof(o):
        if id(o) in seen:       # do not double count the same object
            return 0
        seen.add(id(o))
        s = sys.getsizeof(o, default_size)

        for typ, handler in all_handlers.items():
            if isinstance(o, typ):
                s += sum(map(sizeof, handler(o)))
                break
        return s

    return sizeof(o)


def compute_distances(distance_per_cog_pair, previous_genes, gene, distances_in_genome=None):

    for p_gene in previous_genes:
        dist_infos = get_gene_pair_distance(p_gene, gene)
        sorted_cog_pair = tuple(sorted([p_gene["cog_id"], gene["cog_id"]]))

        for dist_info in dist_infos:
            dist_info['genome'] = gene['genome']

        if distances_in_genome:
            distances_in_genome[p_gene["id"]][gene["id"]] = dist_info
        # distance_matrix[gene["eggnog_id"]][p_gene["eggnog_id"]] = dist_info
        distance_per_cog_pair[sorted_cog_pair] += dist_infos


def get_compatible_gene_pairs(pairs_dict, previous_genes, gene):

    for p_gene in previous_genes:
        if p_gene['seqid'] == gene['seqid']:

            sorted_cog_pair = tuple(sorted([p_gene["cog_id"], gene["cog_id"]]))
            pair_info = {'seqid': gene['seqid'],
                         'circular': gene['circular'],
                         'seqid_length': gene['seqid_length'],
                         'genome': gene['genome']}

            relevant_gene_key = ['start', 'end', 'strand', 'cog_id', 'prot_id']
            pair_info.update({'gene_a|'+k: p_gene[k] for k in relevant_gene_key})
            pair_info.update({'gene_b|'+k: gene[k] for k in relevant_gene_key})

        # distance_matrix[gene["eggnog_id"]][p_gene["eggnog_id"]] = dist_info
            pairs_dict[sorted_cog_pair].append(pair_info)


def get_gene_pair_distance(gene_a, gene_b):
    """Take two gene dicts and return distance."""
    # TODO We need to know if the source seq is circular
    # TODO manage of circularity
    # find gene that start first in the sequence
    dist_infos = []
    gene_1, gene_2 = (gene_a, gene_b) if int(gene_a['start']) <= int(gene_b['start']) else (
        gene_b, gene_a)

    if gene_a['seqid'] != gene_b['seqid']:
        # the gene are not from the same dna mol/scaffold/contig
        return []
        distance = None
        strands = None
    else:

        if gene_a['circular']:
            # gene a and b seq id is circular ..
            # two distances are then computed
            # gene2 to gene1
            seqid_length = gene_a['seqid_length']
            distance_circ = int(seqid_length) - int(gene_2['start']) + 1 + int(gene_1['end'])

            cog_pair = f"{gene_2['cog_id']}|{gene_1['cog_id']}"
            strands = f"{gene_2['strand']}|{gene_1['strand']}"
            genes = f"{gene_2['id']}|{gene_1['id']}"
            dist_info_circular = {"cog_pair": cog_pair, "strands": strands,
                                  "genes": genes, "distance": distance_circ}
            dist_infos.append(dist_info_circular)

        cog_pair = f"{gene_1['cog_id']}|{gene_2['cog_id']}"
        distance = int(gene_2['end']) - int(gene_1['start']) + 1
        # strands = ("{gene_1['strand']}, {gene_2['strand']})
        strands = f"{gene_1['strand']}|{gene_2['strand']}"
        genes = f"{gene_1['id']}|{gene_2['id']}"

        dist_info = {"cog_pair": cog_pair, "strands": strands, "genes": genes, "distance": distance}
        dist_infos.append(dist_info)

        # if gene_a['circular'] and 15000 > distance_circ:
        #     print(dist_infos)
        #     input()
    return dist_infos


def save_matrix(matrix, out_file):
    # matrix is a double dict

    fieldnames = ['gene_id'] + list(matrix)[::-1]  # key of the first dict
    print(len(fieldnames))
    with open(out_file, 'w', newline='') as csvfile:

        writer = csv.DictWriter(csvfile, fieldnames=fieldnames, delimiter='\t')

        writer.writeheader()

        for k, d in matrix.items():
            print(d)
            d = {k: f'{sub_d["distance"]} {sub_d["strands"]}' for k, sub_d in d.items()}
            # for v in d.values()]
            d["gene_id"] = k
            writer.writerow(d)


def get_ref_and_repr_assemblies(bact_summary):
    # taxid_assembly_info = {}
    assembly_lvl_rank = {"Complete Genome": 1, 'Chromosome': 2, 'Scaffold': 3, 'Contig': 4}
    taxid_assembly_info = defaultdict(list)
    with open(bact_summary, 'r') as fl:
        header_readme = next(fl)
        fieldnames_line = next(fl)
        assert fieldnames_line.startswith('# ')
        assert header_readme.startswith('# ')
        fieldnames = fieldnames_line[len('# '):]
        for l in fl:
            if l.startswith('#'):
                continue
            splitted_l = l.split('\t')
            refseq_category = splitted_l[4]

            if refseq_category in ['reference genome', 'representative genome']:
                species_taxid = splitted_l[6]
                ftp_path = splitted_l[19]
                taxid = splitted_l[5]
                assembly_level = splitted_l[11]
                organism_name = splitted_l[7]

                excluded_from_refseq = splitted_l[20]

                info_dict = {"ftp_path": ftp_path,
                             "assembly_level": assembly_level,
                             "assembly_level_rank": assembly_lvl_rank[assembly_level],
                             "organism_name": organism_name,
                             "refseq_category": refseq_category,
                             "taxid": taxid,
                             "species_taxid": species_taxid,
                             "full_line": l}

                # Select only one assembly per taxid based on assembly_level
                if taxid in taxid_assembly_info:
                    if info_dict['assembly_level_rank'] > taxid_assembly_info[taxid]['assembly_level_rank']:
                        taxid_assembly_info[taxid] = info_dict
                else:
                    taxid_assembly_info[taxid] = info_dict

    return taxid_assembly_info, fieldnames  # , taxid_assembly_info


######
def global_aln_test()
    from Bio import pairwise2
    from Bio.pairwise2 import format_alignment
    import time
    j = 0

    region = 'COG0097-COG0201'
    fasta_file = f'nextflow_result_2/genomic_regions_extracted/{region}.fna'
    records = [record for record in SeqIO.parse(open(fasta_file), "fasta")][500:]
    print('seq', len(records))

    start = time.perf_counter()
    for i, record1 in enumerate(records):
        for record2 in records[i+1:]:
            j += 1
            print(j)
            seqA, seqB, score, begin, end = pairwise2.align.globalxx(
                record1.seq, record2.seq, one_alignment_only=True)[0]
            identity = score/end
            # print(identity)
            if j > 10:
                break
        if j > 10:
            break
    finish = time.perf_counter()
    print(f'{j} pairs processed in {finish - start} sec')
    print(len(records))
    print(j)
#####
