from argparse import ArgumentParser, ArgumentDefaultsHelpFormatter
import csv
import os


def get_best_hsp(hsps):
    return sorted(hsps, key=lambda t: float(t['bitscore']), reverse=True)[0]


def parse_arguments():
    parser = ArgumentParser(description="...",
                            formatter_class=ArgumentDefaultsHelpFormatter)

    parser.add_argument("blat_result_file", type=str,
                        help="")
    parser.add_argument("blat_fmt", type=str,
                        help="")
    parser.add_argument("desired_outfmt", type=str,
                        help="")

    args = parser.parse_args()
    return args


def get_seqlen_with_seqid(seqid):
    '''
    seqid='Assembly_ID|contigID|16395-17932'
    seqid='SOMETHING|SOMETHING|start-end'
    '''
    positions = seqid.split('|')[2]
    start, end = positions.split('-')
    return int(end) - int(start) + 1


def main():
    args = parse_arguments()

    blat_result_file = args.blat_result_file
    blat_fmt_list = args.blat_fmt.split(' ')
    output_fmt_list = args.desired_outfmt.split(' ')

    out_dir = '.'  # os.path.dirname(blat_result_file)
    preprocess_result_file = os.path.join(
        out_dir, 'preprocessed_' + os.path.basename(blat_result_file))

    with open(blat_result_file) as fl, open(preprocess_result_file, 'w') as out:

        reader = csv.DictReader(fl, delimiter='\t', fieldnames=blat_fmt_list)
        writer = csv.DictWriter(out, fieldnames=output_fmt_list, delimiter='\t')

        tuple_ids = None
        hsps = []
        for d in reader:
            # print(d)
            if d['qseqid'] == d['sseqid']:
                continue

            d['qlen'] = get_seqlen_with_seqid(d['qseqid'])
            d['slen'] = get_seqlen_with_seqid(d['sseqid'])

            if int(d['qlen']) < int(d['slen']):
                continue
            elif int(d['qlen']) == int(d['slen']) and d['qseqid'] < d['sseqid']:
                continue

            if not tuple_ids:
                tuple_ids = (d['qseqid'], d['sseqid'])

            if tuple_ids and tuple_ids != (d['qseqid'], d['sseqid']):
                hsp = get_best_hsp(hsps)
                del hsp['gap_openings']
                del hsp['mismatches']
                hsp['qcovhsp'] = (int(hsp['length'])/d['qlen'])*100
                hsp['qcovs'] = None

                writer.writerow(hsp)
                tuple_ids = (d['qseqid'], d['sseqid'])
                hsps = []

            hsps.append(d)


if __name__ == '__main__':
    main()
