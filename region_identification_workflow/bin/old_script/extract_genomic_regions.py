from Bio import SeqIO
from argparse import ArgumentParser, ArgumentDefaultsHelpFormatter
import gzip
import csv
from collections import defaultdict
import os
import regions_analysis_fct
from Bio.SeqFeature import SeqFeature, FeatureLocation
import logging

# def get_genomic_region_info(dist_info):
#     region_info = {"genome": dist_info[], "seqid": dist_info[]}
#
#     region_info['gene1'], region_info['gene2'] = dist_info["genes"].split('|')
#     region_info['gene1_position'], region_info['gene2_position'] = dist_info['positions'].split('|')
#
#     print(genomic_region_info['organisation'])
#     print(dist_info)
#     input()


def gather_info_by_genomes(file_to_pair_stat):
    genomic_regions_by_genome = defaultdict(list)
    for file, pair_stat in file_to_pair_stat.items():
        # Get main orga of the pair
        organisations = {orga_val.split(':')[0]: int(orga_val.split(':')[1])
                         for orga_val in pair_stat['organisations'].split('|')}
        main_orga = max(organisations, key=organisations.get)
        max_length_cutoff = float(pair_stat['max_length_cutoff'])
        with open(file) as csvfile:
            reader = csv.DictReader(csvfile, delimiter='\t')
            for dist_info in reader:
                organisation = regions_analysis_fct.get_pair_organisation(dist_info)
                # filter pairs: get only the ones that have the main organisation
                # and have distance close to the median by a factor 10
                # to prevent any extravagantly long sequences to process
                if int(dist_info['distance']) <= max_length_cutoff and organisation == main_orga:
                    genomic_regions_by_genome[dist_info['genome']].append(dist_info)
                    #     # get_genomic_region_info(dist_info)
                    #     logging.warning(f'A region length seems to be very different from the median ({median}): {dist_info}')
                    # if :
    return genomic_regions_by_genome


def tsv_to_list_of_dict(file):
    with open(file) as csvfile:
        reader = csv.DictReader(csvfile, delimiter='\t')
        return list(reader)


def get_region_start_and_end(position_string):
    """
    >> get_region_start_and_end("529951-530454|532135-532674")
    (529951, 532674)
    """
    region_start = int(position_string.split('|')[0].split('-')[0])
    region_end = int(position_string.split('|')[1].split('-')[1])

    return region_start, region_end


def get_seq_of_gene(record, start, end, strand):
    if start < end:
            # everything is normal
        location = FeatureLocation(start-1, end)  # minus one to be in zero base

    elif start >= end:
        # Then the region is overlapping the 0 of the chromosome
        location = FeatureLocation(start-1, len(record)) + FeatureLocation(0, end)
        # print("MULPTIPLE LOCATION ", location)
    else:
        logging.critical('Problem in location of seq')
        exit(1)

    feat = SeqFeature(location,  strand=int(f"{strand}1"))
    return feat.extract(record)


def get_seq_of_region(record, region):

    region_start, region_end = get_region_start_and_end(region['positions'])

    cog_pair_list = region['cog_pair'].split('|')
    cogA = sorted(cog_pair_list)[0]
    index_of_cogA = cog_pair_list.index(cogA)
    strand_of_cogA = region['strands'].split('|')[index_of_cogA]
    # print(region)
    # print("cog_pair_list", cog_pair_list)
    # print('cogA', cogA)
    # print('index of cogA', index_of_cogA)
    # print('strand_of_cogA', strand_of_cogA)
    #
    # print(record)
    # print('len record', len(record))
    # print(region['positions'])
    # print(region_start, region_end)

    # correction_start = 1 if strand_of_cogA == '-' else 0
    # correction_end = 0 if strand_of_cogA == '-' else 1
    # print("correction start", correction_start, "end", correction_end)
    # SeqFeature is created with start, end and strand of the targeted regions:
    # location = FeatureLocation(region_start+correction_start, region_end+correction_end)

    # print(s.seq)
    # print(s)
    # print("LONGUEUR", len(s))
    seq = get_seq_of_gene(record, region_start, region_end, strand_of_cogA)
    return seq


def extract_sequences(genome_file, regions, fh_of_cog_pair_seqs, output_dir):
    nb_region_extracted = 0
    proper_open = gzip.open if genome_file.endswith('.gz') else open
    regions_by_seqid = defaultdict(list)

    for r in regions:
        regions_by_seqid[r['seqid']].append(r)

    # print(regions_by_seqid)
    print('\n\n>>>>>>>>')
    print(genome_file)
    print(len(regions), 'seq to extract in this genomes')
    # regions_by_seqid = {r['seqid']: r for r in regions}
    # print(genome_file)
    with proper_open(genome_file, 'rt') as handle:

        for record in SeqIO.parse(handle, "fasta"):

            if record.id in regions_by_seqid:
                print()
                print(len(regions_by_seqid[record.id]),
                      'seq to extract in', record.id)
                for region in regions_by_seqid[record.id]:
                    cog_pair_sorted = '-'.join(sorted(region['cog_pair'].split('|')))

                    cog_pair_file = os.path.join(output_dir, cog_pair_sorted + '.fna')
                    region_start, region_end = get_region_start_and_end(region['positions'])

                    seq = get_seq_of_region(record, region)
                    seq.id = f'{region["genome"]}|{region_start}-{region_end}'
                    try:
                        fh = fh_of_cog_pair_seqs[cog_pair_file]
                    except KeyError:
                        fh = open(cog_pair_file, 'w')
                        fh_of_cog_pair_seqs[cog_pair_file] = fh

                    SeqIO.write(seq, fh, "fasta")
                    nb_region_extracted += 1

    assert nb_region_extracted == len(regions), f'{nb_region_extracted}  extracted vs {len(regions)} expected'

    print("<<<<<<")


def parse_arguments():
    parser = ArgumentParser(description="...",
                            formatter_class=ArgumentDefaultsHelpFormatter)
    parser.add_argument("selected_pairs", type=str,
                        help="")
    parser.add_argument("genome_dir", type=str,
                        help="")
    parser.add_argument("cog_pairs_info_dir", type=str,
                        help="")
    parser.add_argument("--check_dir", type=str, default='.',
                        help="")

    args = parser.parse_args()
    return args


def main():
    args = parse_arguments()
    selected_pairs = args.selected_pairs
    cog_pairs_info_dir = args.cog_pairs_info_dir
    genome_dir = args.genome_dir

    if not os.path.isdir(args.check_dir):
        logging.warning(f'Check_dir {args.check_dir} does not exist')
        # raise FileNotFoundError(f'Check_dir {args.check_dir} does not exist')

    dir_symblink = "regions_fasta_symblink"
    os.makedirs(dir_symblink, exist_ok=True)
    outdir = "genomic_regions_extracted"
    os.makedirs(outdir, exist_ok=True)
    info_by_genomes = defaultdict(list)
    cog_pairs_stat = tsv_to_list_of_dict(selected_pairs)
    print(len(cog_pairs_stat))
    # Check if output_file are already in check dir
    regions_already_in_checkdir = {}
    for pair_stat in cog_pairs_stat:
        output_file = os.path.join(args.check_dir, pair_stat["cog_pair"] + '.fna')
        print(output_file)

        if os.path.isfile(output_file):
            os.symlink(os.path.realpath(output_file), os.path.join(
                dir_symblink, pair_stat["cog_pair"] + '.fna'))
            regions_already_in_checkdir[pair_stat["cog_pair"]] = output_file

    print(len(regions_already_in_checkdir), 'files not to compute because it is in check dir')
    # pair stat file
    file_to_pair_stat = {os.path.join(cog_pairs_info_dir, pairs_stat["cog_pair"]+'.tsv'): pairs_stat
                         for pairs_stat in cog_pairs_stat if pairs_stat["cog_pair"] not in regions_already_in_checkdir}

    genomic_regions_by_genome = gather_info_by_genomes(file_to_pair_stat)
    print(len(genomic_regions_by_genome))
    genome_suffix = "_genomic.fna.gz"
    fh_of_cog_pair_seqs = {}
    i = 0
    for genome, regions in genomic_regions_by_genome.items():
        i += 1
        if i % 50 == 0:
            print('nb genomes=', i)
        genome_file = os.path.join(genome_dir, genome+genome_suffix)

        extract_sequences(genome_file, regions, fh_of_cog_pair_seqs, output_dir=outdir)

    for fh in fh_of_cog_pair_seqs.values():
        fh.close()

    for output_file in fh_of_cog_pair_seqs:
        filename = os.path.basename(output_file)
        os.symlink(os.path.realpath(output_file), os.path.join(dir_symblink, filename))


if __name__ == '__main__':
    main()
