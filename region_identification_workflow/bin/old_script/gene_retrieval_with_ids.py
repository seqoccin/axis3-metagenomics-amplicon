import os
import parser
from Bio import SeqIO
import gzip
import logging
import prepare_assemblies_downloading
from argparse import ArgumentParser, ArgumentDefaultsHelpFormatter


def retrieve_gene_info(genes, assembly_files_iter, nb_assembly_max):

    # # gb_files
    # gb_files = (os.path.join(out_dir, f)
    #             for f in os.listdir(out_dir) if f.endswith('.gbff.gz') or f.endswith('.gbff'))
    # Creation of aliases dict:
    assemblies = []
    extended_geneids = {}
    for g, info in genes.items():
        for alias in info['aliases']:
            extended_geneids[alias] = info

    # dict of dict key assembly : value dict with as key tag and as value information..
    assembly_genes_information_retrieved = {}
    nb_original_gene = len(genes)
    i = 0
    for gb_file in assembly_files_iter:
        i += 1
        genes_location_dict = {}
        # print("searching in:", gb_file)
        if not os.path.isfile(gb_file):
            if os.path.isfile(gb_file[:-len(".gz")]):
                gb_file = gb_file[:-len(".gz")]
            else:
                logging.critical(f'assembly file {gb_file} does not exist')
                continue

        proper_open = gzip.open if gb_file.endswith('.gz') else open
        with proper_open(gb_file, "rt") as handle:
            for record in SeqIO.parse(handle, "genbank"):
                dna_molecul_acc = f"{record.annotations['accessions'][0]}.{record.annotations['sequence_version']}"

                for f in record.features:

                    if f.type == 'gene' or f.type == 'CDS':
                        possible_tags = [item for k, v in f.qualifiers.items() for item in v if k in [
                            'gene', 'gene_synonym', 'old_locus_tag', 'locus_tag']]

                        for tag in possible_tags:
                            if tag in genes:
                                genes_location_dict.setdefault(tag, []).append({
                                    'location': str(f.location), 'mol_acc': dna_molecul_acc, 'OG': genes[tag]['GroupName'], "feature_type": f.type, 'working_id': tag})
                            elif tag in extended_geneids:
                                original_name = extended_geneids[tag]['original_name']
                                genes_location_dict.setdefault(original_name, []).append({
                                    'location': str(f.location), 'mol_acc': dna_molecul_acc, 'OG': genes[original_name]['GroupName'], "feature_type": f.type, 'working_id': tag})

        assembly_genes_information_retrieved[gb_file] = genes_location_dict

        if nb_original_gene == len(genes_location_dict):
            print('all genes have been retrieved')
            break
        if i == nb_assembly_max:
            logging.warning(
                f'{nb_assembly_max} assemblies have been checked so far... we stop here')
            break

        # elif nb_original_gene > len(genes_location_dict):
        #     assemblies.append(gb_file)
        #     pass
            # logging.warning(f"Not all genes have been retrieved ({len(genes_location_dict)}/{nb_original_gene}) Let's check the next assembly")
    return assembly_genes_information_retrieved
    # if nb_original_gene > len(genes_location_dict):
    #     logging.critical(f'{taxid}::Not all genes have been retrieved ({len(genes_location_dict)}/{nb_original_gene}) even with the help of aliases.')
    #     logging.warning(f'{assemblies}')
    #     missing_genes = {g: info for g, info in genes.items() if g not in genes_location_dict}
    #
    #     work = 'Exemple of genes id that worked:\n'
    #     max_i = 10
    #     i = 0
    #     for g, infos in genes_location_dict.items():
    #         i += 1
    #         work += g + '\n'
    #         for info in infos:
    #             work += str(info) + '\n'
    #         if i > max_i:
    #             break
    #     logging.warning(work)
    #     logging.warning('~~'*20)
    #     missing = 'Missing genes details' + '\n'
    #     for g, info in missing_genes.items():
    #         missing += g + '\n'
    #         missing += "  " + str(info) + '\n'
    #     logging.warning(missing)
    #     logging.warning('NEXT'+'='*20)
    #     return 'not_good' if len(genes_location_dict) == 0 else 'partially_good'
    # print('_'*10)
    # logging.warning(f'{list(genes.keys())}')


def get_file_download_iter(ftp_paths, out_dir, suffix='_genomic.gbff.gz', check_dir=None):
    for path in ftp_paths:
        file_path = os.path.join(path, f'{os.path.basename(path)}{suffix}')
        yield prepare_assemblies_downloading.ftp_download(file_path, out_dir, check_dir)


def parse_arguments():
    parser = ArgumentParser(description="...",
                            formatter_class=ArgumentDefaultsHelpFormatter)
    parser.add_argument("ncbi_genomes_dir", type=str, help="")

    parser.add_argument("--nb_assembly_max", type=int, default=1,
                        help="")

    parser.add_argument("--gene_info_file", type=str, default="eggNOG_genes_general_info.json",
                        help="")

    parser.add_argument("--sorted_path_file", type=str, default="assemblies_sorted_paths.json",
                        help="")
    parser.add_argument("--download_dir", type=str, default="./",
                        help="directory where ncbi genomes are downloaded")
    # parser.add_argument("--json_gene_retrieved", type=str, default="genes_retrieved_per_assembly.json",
    #                     help="")
    args = parser.parse_args()
    return args


def main():
    print('Let"s retrieve_gene_info...')
    args = parse_arguments()
    download_dir = args.download_dir  # 'Genomes/'
    ncbi_genomes_dir = args.ncbi_genomes_dir
    gene_info_file = args.gene_info_file
    sorted_path_file = args.sorted_path_file
    nb_assembly_max = args.nb_assembly_max

    # OUTPUT FILES
    json_gene_retrieved = "genes_retrieved_by_gene_ids.json"
    json_file_recovery_stat = "id_match_recovery_stat.json"

    recovery_stat = {}

    genes = parser.decoder(gene_info_file)

    ftp_paths = parser.decoder(sorted_path_file)
    # out_dir = os.path.join(taxids_dir, taxid)

    assembly_files_iter = get_file_download_iter(
        ftp_paths, download_dir, check_dir=ncbi_genomes_dir)
    assembly_genes_information_retrieved = retrieve_gene_info(
        genes, assembly_files_iter, nb_assembly_max)

    parser.encoder(json_gene_retrieved, assembly_genes_information_retrieved)

    recovery_stat_list = []
    for gb_file, retrieved_genes in assembly_genes_information_retrieved.items():

        assembly_name = os.path.basename(gb_file)[:-len('_genomic.gbff.gz')]
        recovery_stat = {'expected_genes': list(set(genes)),
                         'retrieved_genes': list(set(retrieved_genes)),
                         'missing_genes': list(set(genes) - set(retrieved_genes)),
                         'name': assembly_name}

        recovery_stat_list.append(recovery_stat)

    sorted_recovery_stat_list = [d for d in sorted(
        recovery_stat_list, key=lambda x:len(x['missing_genes']))]
    parser.encoder(json_file_recovery_stat, sorted_recovery_stat_list)


if __name__ == '__main__':
    main()
