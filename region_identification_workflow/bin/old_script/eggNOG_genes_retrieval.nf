#!/usr/bin/env nextflow

//module load bioinfo/Nextflow-v0.32.0

params.eggnog_members_url = 'http://eggnog5.embl.de/download/eggnog_5.0/per_tax_level/2/2_members.tsv.gz'
// params.eggnog_members_url = "http://eggnog5.embl.de/download/eggnog_5.0/per_tax_level/237/237_members.tsv.gz"
// params.eggnog_members_url =  "http://eggnog5.embl.de/download/eggnog_5.0/per_tax_level/186801/186801_members.tsv.gz"


params.extended_members_url_base ="http://eggnogapi5.embl.de/nog_data/text/extended_members/"
params.single_copy_treshold = 0.95


params.local_genomes = true

root_taxid_captured = params.eggnog_members_url =~ /\/(\d+_members).tsv.gz/
root_taxid = root_taxid_captured[0][1]
println root_taxid
if(params.local_genomes==false) {
  results_dir ="nextflow_result_${root_taxid}"
}
else {
  results_dir ="nextflow_result_${root_taxid}_local_eggNOG_genomes"
}



// genomes_directory = file("nextflow_result_$root_taxid/Genomes/")

/**
 The python script executed by this process:
 Downlaod eggnog members file which contains information for each orthologous groups (OGs).
 The url of the file is specified by the variable : params.eggnog_members_url
 The file is parsed and universal and single copy OGs are identified.
 An OG is selected if it is found in single copy in more than a specified percentage of the total genomes.
 This percentage threshold is defined by the variable params.single_copy_treshold.

 The script downloads for each OGs the extended members file corresponding.
 This file contains information for each gene of the group.
 Gene id aliases are extracted to be used in the retrieval of gene location based on gene id
 The names of all the genes are also extracted and the most used one is selected as the name of the OG

 General information about the selected OGs is stored in a json file: ${root_taxid}_identified_universal_ogs.json

 Genes of the selected OGs are grouped by their tax_ids.
 And for each tax_id a directory is created and a json file containing information about the selected genes is stored.

 For this process directories and the output file are stored in a specified directory instead of in a nextflow work directory.
 This is because we don't want to download genomes multiple time and therefore we need a not changing directory in the next steps.

 #### Solution to not have a StoreDir: publish genomes in a directory and input this dir when dowbload a genomes. :-/
 * ```
 */

initial_process_genomes_dir = "."

process parse_and_identify_universal_OG {
  label 'python'
  cache 'deep'
  // storeDir "nextflow_result_${root_taxid}/"
  publishDir "${results_dir}/", pattern: "*eggnog_genes_general_info.json", saveAs: { filename -> "taxids/"+("$filename" =~ /(taxid_\d+)/)[0][1]+"/eggnog_genes_general_info.json" }
  publishDir "${results_dir}/", pattern: "*_identified_universal_ogs.json"
  publishDir "${results_dir}/", pattern: "*.tsv.gz"
  publishDir "${results_dir}/extended_members_files/", pattern: "extended_members/*"

  output:
    file "$initial_process_genomes_dir" into general_taxids_dir
    file "${root_taxid}_identified_universal_ogs.json" into json_cogs
    file "*eggnog_genes_general_info.json" into info_genes
    file "taxid_list.json" into taxid_list
    file "${root_taxid}.tsv.gz" into initial_members_file
    file "extended_members/*" into extended_members_files

    """
    echo PARSE AND IDENTIFY
    python $PWD/scripts/parse_and_identify_universal_OG.py  --eggNog_dir "./" \
                                                            --extended_members_url_base ${params.extended_members_url_base} \
                                                            --single_copy_treshold ${params.single_copy_treshold} \
                                                            --taxids_dir  ${initial_process_genomes_dir} \
                                                            --eggnog_members_url ${params.eggnog_members_url} \
                                                            #--sample_test

    """
}

params.refseq_ftp_bact_path = 'ftp://ftp.ncbi.nlm.nih.gov/genomes/refseq/bacteria/assembly_summary.txt'
params.genbank_ftp_bact_path = 'ftp://ftp.ncbi.nlm.nih.gov/genomes/genbank/bacteria/assembly_summary.txt'
params.ftp_bact_path_readme = 'ftp://ftp.ncbi.nlm.nih.gov/genomes/README_assembly_summary.txt'

params.local_genomes_dir = "$PWD/eggNOG_data/taxids/"


params.ncbi_genomes_dir = "$PWD/NCBI_Genomes"

/*
Download refseq and genbank assembly summary files.
These files contain information about assemblies (genomes) available in rather refseq or genbank.
Assembly are sorted first by their level of completeness and by the database prioritizing RefSeq upon Genbank
Assembly are sorted first by their level of completeness and by the database prioritizing RefSeq upon Genbank
Information about assemblies (ftp path, level of completeness...) of the taxids identified
in the parse_and_identify_universal_OG process are stored in a json file, one for each taxid.
*/
if(params.local_genomes==false) {
  ncbi_genomes_dir = params.ncbi_genomes_dir
  process prepare_assemblies_downloading {
    label 'python'
    cache 'deep'
    // publishDir "${results_dir}/taxid_*/"
    publishDir "${results_dir}/", pattern: "*assemblies_info.json", saveAs: { filename -> "taxids/"+("$filename" =~ /(taxid_\d+)/)[0][1]+"/assemblies_info.json" }

    input:
      // file Genome_dir_initial_process from general_taxids_dir
      file taxid_list

    output:
      // file 'Genomes/taxid_*' into taxid_dirs_list
      file "*_assemblies_info.json" into assemblies_info
      file "*_assemblies_sorted_paths.json" into sorted_genome_paths
      // file "Genomes/taxid_*/assemblies_sorted_paths.json" into sorted_genome_paths

      """
      python $PWD/scripts/prepare_assemblies_downloading.py --taxids_list ${taxid_list} \
                                               --refseq_ftp_bact_path ${params.refseq_ftp_bact_path} \
                                               --genbank_ftp_bact_path ${params.genbank_ftp_bact_path} \
                                               --ftp_bact_path_readme ${params.ftp_bact_path_readme}
      """
  }
}
else {
    println "Genome are exclusively from a local source"
    ncbi_genomes_dir = params.local_genomes_dir

    process prepare_assemblies_local_path {
      label 'python'
      cache 'deep'
      // publishDir "${results_dir}/taxid_*/"
      // publishDir "${results_dir}/", pattern: "*assemblies_info.json", saveAs: { filename -> "taxids/"+("$filename" =~ /(taxid_\d+)/)[0][1]+"/assemblies_info.json" }

      input:
        // file Genome_dir_initial_process from general_taxids_dir
        file taxid_list

      output:
        // file 'Genomes/taxid_*' into taxid_dirs_list
        file "*_assemblies_local_paths.json" into sorted_genome_paths
        // file "Genomes/taxid_*/assemblies_sorted_paths.json" into sorted_genome_paths

        """
        python $PWD/scripts/prepare_assemblies_local_path.py ${taxid_list} ${params.local_genomes_dir}
        """
    }
}




/*
Download faa eggNOG sequence for each selected OGs and organise them by taxid
*/
process get_EggNOG_fasta_sequences {
  label 'python'
  cache 'deep'
  publishDir "${results_dir}/eggnog_fasta_sequences/"
  publishDir "${results_dir}/", saveAs: { filename -> "taxids/taxid_"+("$filename" =~ /(\d+)_OG/)[0][1]+"/eggnog_fasta_sequences.faa" }

  input:
    file json_cogs

  output:
    file '*_OG_genes.faa' into taxid_OG_faa_list

    """
    python $PWD/scripts/get_EggNOG_fasta_sequences.py --fasta_OG_sequences_dir  "./" \
                                                      --fasta_OG_taxid_sequences_dir  "./" \
                                                      --json_cogs ${json_cogs}
    """
}


sorted_genome_paths .flatten()
                    .map { it -> tuple((it =~ /taxid_(\d+)/)[0][1], it) }
                    .set {tax_sorted_genome_paths}

tax_sorted_genome_paths.into { tax_sorted_genome_paths_geneid; tax_sorted_genome_paths_blastp_copy}


// taxid_dirs_list.flatten()
//           .map { it -> tuple((it =~ /taxid_(\d+)/)[0][1], it) }
//           .into { taxid_taxid_dirs_for_blast; taxid_taxid_dirs_for_geneid }


params.blast_evalue=0.005
params.outfmt='qseqid qstart qend qlen sseqid sstart send slen length pident qcovhsp evalue bitscore'
params.pident = 95
params.coverage_threshold = 65
params.assembly_depth_max = 10

taxid_taxid_OG_faa = taxid_OG_faa_list.flatMap()
                                      .map { it -> tuple((it =~ /(\d+)_OG_genes.faa/)[0][1], it) }

// Create set of taxid and gene info json file
flat_info_genes = info_genes.flatten()
                            .map { it -> tuple((it =~ /taxid_(\d+)/)[0][1], it) }


flat_info_genes.into{ info_genes_for_blast; info_genes_for_geneid}



//
// ncbi_genomes_files = Channel.fromPath( "./nextflow_result_${root_taxid}/ncbi_downloaded_files/**/") //"$PWD/nextflow_result_${root_taxid}/ncbi_downloaded_files/*")
//                                           .map { it -> tuple((it =~ /taxid_(\d+)/)[0][1], it) }
//                                           .groupTuple()
//
//
// ncbi_genomes_files_all = Channel.fromPath( "./nextflow_result_${root_taxid}/ncbi_downloaded_files/**/*").toList()
//
// blastp_set_channel = taxid_taxid_OG_faa.concat(tax_sorted_genome_paths_blastp_copy, info_genes_for_blast, ncbi_genomes_files)
//                           .groupTuple(size:4, remainder:false)
//                           .map {it -> [it[0] ]+ it[1] } //.subscribe { println it }

blastp_set_channel = taxid_taxid_OG_faa.concat(tax_sorted_genome_paths_blastp_copy, info_genes_for_blast)
                          .groupTuple(size:3, remainder:false)
                          .map {it -> [it[0] ]+ it[1] }

/*
Retrieve gene genomic locations with blastp
Uses assembly sorted paths and downloads faa and gff files of the first assembly.
Make a blast db with the assembly sequences and use faa sequence of eggNOG as query.
Assembly and eggnog are considering matching if eggnog coverage >= 65% and identit >= 95%. ?? TODO to fined
The identified assembly sequence is retrieved in the gff file and genomic locations are stored in a json file
When not all eggnogs sequence are retrieved, sequences from the next assembly file are donwloaded and blasted...

Two potential conflicts with blast :
an eggNOG gene matchs with no sequence
an eggNOG gene matchs with more than one sequence
an assembly gene matchs with more than one eggnog gene.
*/

//

// println ncbi_genomes_dir
// ncbi_genomes_dir.subscribe { println "--−> $it" }



process gene_retrieval_with_blastp {

    label 'python'
    cache 'deep'
    publishDir "${ncbi_genomes_dir}/taxid_${taxid}/", pattern: "*gz", mode:'copy'
    publishDir "${results_dir}/taxids/taxid_${taxid}/"

    input:
      // set val(taxid), file(download_dir),  file(og_faa), file(sorted_path_file), file(genes_info) from blastp_set_channel
      // file "*" from ncbi_genomes_files_all
      set val(taxid), file(og_faa), file(sorted_path_file), file(genes_info) from blastp_set_channel



    output:
      file "blastp_recovery_stat.json" into blast_recovery_stat
      file "*gz" optional true into ncbi_genomes
      file "*_genes_retrieved_blastp.json" optional true into retrieved_genes_blastp
      file "*_blast_result.out" optional true into blast_result
      file "*blastp_identification.log" into blast_retrieval_log

    """
    if ! type "blastp" > /dev/null; then
      module load bioinfo/ncbi-blast-2.7.1+
    fi

    python $PWD/scripts/gene_retrieval_with_blastp.py "${ncbi_genomes_dir}/taxid_${taxid}/" $og_faa  \
                                                --nb_assembly_max ${params.assembly_depth_max} \
                                                --sorted_path_file $sorted_path_file \
                                                --outfmt "${params.outfmt}" \
                                                --blast_evalue ${params.blast_evalue} \
                                                --pident_threshold ${params.pident} \
                                                --coverage_threshold ${params.coverage_threshold} \
                                                --logfile 'blastp_identification.log' \
                                                --gene_info_file ${genes_info}

    """
  }

//
// tax_sorted_genome_paths_geneid.concat(info_genes_for_geneid, taxid_taxid_dirs_for_geneid)
//                               .groupTuple(size:3, remainder:false)
//                               .map {it -> [it[0] ]+ it[1] }
//                               .set { geneid_set_channel}

tax_sorted_genome_paths_geneid.concat(info_genes_for_geneid)
                              .groupTuple(size:2, remainder:false)
                              .map {it -> [it[0] ]+ it[1] }
                              .set { geneid_set_channel}


process identified_eggNOG_genes_from_geneids {
  label 'python'
  cache 'deep'
  publishDir "${ncbi_genomes_dir}/taxid_${taxid}/", pattern: "*gz", mode:'copy'
  publishDir "${results_dir}/log_identification/" , pattern: '*.log'
  publishDir "${results_dir}/taxids/taxid_${taxid}/"

  input:
    // set  taxid, file(paths_fl), file(genes_fl), file(download_dir) from geneid_set_channel
    set  taxid, file(paths_fl), file(genes_fl) from geneid_set_channel
  output:
    file "*.json" into json_recovery_stat_geneid_match
    file "*.gbff.gz" optional true into gbff_files
  """
  python $PWD/scripts/gene_retrieval_with_ids.py   "${ncbi_genomes_dir}/taxid_${taxid}/" \
                                                   --nb_assembly_max ${params.assembly_depth_max}  \
                                                   --gene_info_file $genes_fl \
                                                   --sorted_path_file $paths_fl \
  """
}
