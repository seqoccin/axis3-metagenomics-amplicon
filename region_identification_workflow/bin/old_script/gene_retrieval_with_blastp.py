import os
import parser
import prepare_assemblies_downloading
from argparse import ArgumentParser, ArgumentDefaultsHelpFormatter
import logging
import gene_retrieval_with_ids
import identified_eggNOG_genes_from_blastp_result as parse_and_identify
import gzip


def makeblastdb(db_faa, dbtype):
    os.system(f'makeblastdb -in {db_faa} -dbtype {dbtype}')


def blastp(db_faa, query_faa, outfmt, evalue, out):
    os.system(
        f'blastp -db {db_faa} -query {query_faa} -outfmt "6 {outfmt}" -evalue {evalue} -out {out}')


def gunzip(source_filepath, block_size=65536):
    # Taken from https://stackoverflow.com/questions/52332897/how-to-extract-a-gz-file-in-python
    dest_filepath = source_filepath[:-len('.gz')]
    with gzip.open(source_filepath, 'rb') as s_file, \
            open(dest_filepath, 'wb') as d_file:
        while True:
            block = s_file.read(block_size)
            if not block:
                break
            else:
                d_file.write(block)
        d_file.write(block)
    return dest_filepath


def resolve_ducplicate_genes_conflicts(conflicts,  matching_seq):
    """
    In case of duplicate genes, the pair of genes from eggnog can match the corresponding pair raising a conflict.
    Which eggnog gene correspond to which ncbi gene. Solve by keeping gene in pair by their highest score
    For te moment we don't check if the genes are coming from the same COG as it is kind of implicite.
    """

    identified_conflicts = []
    # Idnetify duplicate subject:
    # print('CONFLICT !!! ',  conflicts)
    conflict_query_multiple_hit = list(conflicts['eggnog_multiple_hit'].items())
    for i, (q, blast_results) in enumerate(conflict_query_multiple_hit[:-1]):
        # print(i, q, blast_results)
        if len(blast_results) < 2:
            continue
        # print(blast_results)
        subj_score_tuples = [(blast_result['sseqid'], blast_result['bitscore'], blast_result)
                             for blast_result in blast_results]
        # sort list of tuple by bitscore
        subj_score_tuples = sorted(subj_score_tuples, key=lambda t: t[1], reverse=True)

        duplication_conflict_with_q = {q: subj_score_tuples}

        for q_next, blast_results_next in conflict_query_multiple_hit[i+1:]:
            # print('-->', q_next, blast_results_next)
            if set([res['sseqid'] for res in blast_results_next]) == set([res['sseqid'] for res in blast_results]):
                # print('  -->Duplication event')
                duplication_conflict_with_q[q_next] = sorted([
                    (blast_result['sseqid'], blast_result['bitscore'], blast_result) for blast_result in blast_results_next], key=lambda t: t[1], reverse=True)

        if len(duplication_conflict_with_q) > 1:
            duplication_conflict_with_q_sorted = sorted(
                duplication_conflict_with_q.items(), key=lambda kv: kv[1][0][1], reverse=True)
            identified_conflicts.append(duplication_conflict_with_q_sorted)

    # print(identified_conflicts)
    # Resolution
    # association base on best score
    associate_subjects = []
    solved_conflict_count = 0
    for identified_conflict in identified_conflicts:
        subject_association_resolved = {}
        for i, (query, subj_score_tuples) in enumerate(identified_conflict):
            # print(query, '-->', subj_score_tuples)
            # subject_and_score = [(subject, score) for subject, score in subj_score_tuples if subject not associate_subjects][0]
            #
            # why [0][0] --> Select first subject and subject name not score
            best_subject = subj_score_tuples[0][0]

            if best_subject in associate_subjects:
                logging.warning(
                    'Ducplicated sequences conflict not resolved, eggNOG genes have best score with the same ncbi gene')
                break
            else:
                associate_subjects.append(best_subject)
                # print(query, 'with', best_subject)
                subject_association_resolved[best_subject] = [subj_score_tuples[0][2]]
        if len(subject_association_resolved) == len(identified_conflict):
            matching_seq.update(subject_association_resolved)
            logging.warning(f'Duplicated sequence solved')
            solved_conflict_count += 1
    return solved_conflict_count


def retrieve_gene_from_blastp_results(blast_result_fl, pident_threshold, coverage_threshold, outfmt, gene_info):

    ncbi_matching_eggnog, eggnog_matching_ncbi = parse_and_identify.identify_blast_hit_of_interest(
        blast_result_fl, outfmt, pident_threshold, coverage_threshold)

    # remove taxid from the gene id of the query (eggnog gene) to be homogenous in the next steps
    eggnog_matching_ncbi = {geneid.split(
        '.', 1)[1]: blast_lines for geneid, blast_lines in eggnog_matching_ncbi.items()}
    for geneid, blast_lines in eggnog_matching_ncbi.items():
        for line in blast_lines:
            line['qseqid'] = line['qseqid'].split('.', 1)[1]

    # Check if EggNOG genes are found only once:
    conflicts = {}
    conflicts['eggnog_multiple_hit'] = {q: ss for q,
                                        ss in eggnog_matching_ncbi.items() if len(ss) > 1}
    conflicts['ncbi_multiple_hit'] = {s: qs for s,
                                      qs in ncbi_matching_eggnog.items() if len(qs) > 1}

    # eggnog_genes_ids = parse_and_identify.get_gene_id_from_fasta(eggNOG_faa_fl)
    expected_eggnog_genes = gene_info.keys()

    if not set(expected_eggnog_genes) == set(eggnog_matching_ncbi):
        logging.critical(
            f'{len(set(expected_eggnog_genes) - set(eggnog_matching_ncbi))} EggNOG genes have not found their counterparts in assembly gene')
        print(f'{len(set(expected_eggnog_genes) - set(eggnog_matching_ncbi))} EggNOG genes have not found their counterparts in assembly gene')

    resolve_ducplicate_genes_conflicts(conflicts,  ncbi_matching_eggnog)

    recovery_stat = {'expected_genes': list(set(expected_eggnog_genes)),
                     'retrieved_genes': list(set(eggnog_matching_ncbi)),
                     'missing_genes': list(set(expected_eggnog_genes) - set(eggnog_matching_ncbi))}
    # Add multiple hit conflicts
    recovery_stat.update(conflicts)

    if conflicts['eggnog_multiple_hit']:
        logging.critical(
            f'{blast_result_fl} eggnog_multiple_hit {conflicts["eggnog_multiple_hit"]}')
    if conflicts['ncbi_multiple_hit']:
        logging.critical(f'{blast_result_fl} ncbi_multiple_hit {conflicts["ncbi_multiple_hit"]}')

    return recovery_stat, ncbi_matching_eggnog


def parse_arguments():
    parser = ArgumentParser(description="...",
                            formatter_class=ArgumentDefaultsHelpFormatter)

    parser.add_argument("ncbi_genomes_dir", type=str, help="")
    parser.add_argument("eggNOG_faa", type=str, help="")
    parser.add_argument("--nb_assembly_max", type=int, default=1,
                        help="")

    parser.add_argument("--gene_info_file", type=str, default="eggNOG_genes_general_info.json",
                        help="")

    parser.add_argument("--sorted_path_file", type=str, default="assemblies_sorted_paths.json",
                        help="")

    parser.add_argument("--outfmt", type=str, default='qseqid qstart qend qlen sseqid sstart send slen length pident qcovhsp evalue bitscore',
                        help="")

    parser.add_argument("--blast_evalue", type=float, default=0.005,
                        help="")
    parser.add_argument("--download_dir", type=str, default="./",
                        help="directory where ncbi genomes are downloaded")

    parser.add_argument("--pident_threshold", type=int, default=95, help="")
    parser.add_argument("--coverage_threshold", type=int, default=65, help="")
    parser.add_argument("--logfile", type=str, default=False)
    args = parser.parse_args()
    return args


def main():
    print('Let"s retrieve_gene_info...')
    args = parse_arguments()
    download_dir = args.download_dir
    ncbi_genomes_dir = args.ncbi_genomes_dir
    # gene_info_file = args.gene_info_file
    sorted_path_file = args.sorted_path_file
    nb_assembly_max = args.nb_assembly_max
    blast_evalue = args.blast_evalue
    outfmt = args.outfmt
    eggNOG_faa_fl = args.eggNOG_faa
    gene_info_file = args.gene_info_file

    pident_threshold = args.pident_threshold
    coverage_threshold = args.coverage_threshold

    json_file_recovery_stat = "blastp_recovery_stat.json"

    print(download_dir)
    # print(os.listdir(ncbi_genomes_dir))

    # if args.logfile:
    #     logging.basicConfig(filename=args.logfile, level=logging.WARNING)
    # else:
    #     pass
    ftp_paths = parser.decoder(sorted_path_file)
    gene_info = parser.decoder(gene_info_file)
    # eggNOG_genes = parser.decoder(gene_info_file)
    recovery_stat_list = []

    genome_faa_files_iter = gene_retrieval_with_ids.get_file_download_iter(
        ftp_paths, download_dir, '_protein.faa.gz', ncbi_genomes_dir)

    gbff_file_iter = gene_retrieval_with_ids.get_file_download_iter(
        ftp_paths, download_dir, '_genomic.gff.gz', ncbi_genomes_dir)

    for genome_faa_file, gff_file in zip(genome_faa_files_iter, gbff_file_iter):
        genome_prefix_name = os.path.basename(genome_faa_file).split('.', 1)[0]

        # Remove all handlers associated with the root logger object.
        # (from https://stackoverflow.com/questions/12158048/changing-loggings-basicconfig-which-is-already-set)
        for handler in logging.root.handlers[:]:
            logging.root.removeHandler(handler)
        logging.basicConfig(filename=f"{genome_prefix_name}_{args.logfile}", level=logging.WARNING)

        if not os.path.isfile(genome_faa_file) or not os.path.isfile(gff_file):
            if os.path.isfile(genome_faa_file[:-len(".gz")]):
                genome_faa_file = genome_faa_file[:-len(".gz")]
            else:
                logging.critical(f"FILE {genome_faa_file} DOES NOT EXIT.")
                continue
            if os.path.isfile(gff_file[:-len(".gz")]):
                gff_file = gff_file[:-len(".gz")]
            else:
                logging.critical(f"FILE {genome_faa_file} DOES NOT EXIT.")
                continue
        assembly_name = os.path.basename(gff_file)[:-len('_genomic.gff.gz')]

        if genome_faa_file.endswith('.gz'):  # blast need a unzziped file
            genome_faa_file = gunzip(genome_faa_file)

        blast_result_fl = f'{assembly_name}_blast_result.out'

        if not os.path.isfile(blast_result_fl):
            makeblastdb(genome_faa_file, 'prot')
            blastp(genome_faa_file, eggNOG_faa_fl, outfmt, blast_evalue, blast_result_fl)
        else:
            logging.warning(f"{blast_result_fl} exist already. We don't recompute blast step")

        recovery_stat, ncbi_matching_eggnog = retrieve_gene_from_blastp_results(
            blast_result_fl, pident_threshold, coverage_threshold, outfmt.split(' '), gene_info)
        recovery_stat['name'] = assembly_name
        recovery_stat_list.append(recovery_stat)

        parse_and_identify.retrieve_gene_info(
            ncbi_matching_eggnog, gff_file, f'{assembly_name}_genes_retrieved_blastp.json')

        if len(recovery_stat['missing_genes']) == 0 or len(recovery_stat_list) >= nb_assembly_max:
            break

    sorted_recovery_stat_list = [d for d in sorted(
        recovery_stat_list, key=lambda x:len(x['missing_genes']))]

    parser.encoder(json_file_recovery_stat, sorted_recovery_stat_list)


if __name__ == '__main__':
    main()
