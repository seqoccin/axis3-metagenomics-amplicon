import gzip
import os
import logging
from argparse import ArgumentParser, ArgumentDefaultsHelpFormatter
import csv
import re


def retrieve_gene_info(seq_ids, gff_file):
    """
    Retieve gene information with the protein ids in the gff file.

    :param seq_ids: protein ids of the protein of interest
    :type seq_ids: list or dict
    :param gff_file: Gff file of the genome from which the protein ids are from
    :type gff_file: str
    :return gene_info: a dictionary wirth prot_ids as key and another dictionary as value with valuable information such as location, seqid and strand.
    :rtype: dict

    :Example:

    >>> function_count_genes("../data/GCF_000002985.6_WBcel235_genomic.gff")
    20090

    .. seealso:: chooseGenes()


    GFF HEADER:
    seqid - name of the chromosome or scaffold; chromosome names can be given with or without the 'chr' prefix. Important note: the seq ID must be one used within Ensembl, i.e. a standard chromosome name or an Ensembl identifier such as a scaffold ID, without any additional content such as species or assembly. See the example GFF output below.
    source - name of the program that generated this feature, or the data source (database or project name)
    type - type of feature. Must be a term or accession from the SOFA sequence ontology
    start - Start position of the feature, with sequence numbering starting at 1.
    end - End position of the feature, with sequence numbering starting at 1.
    score - A floating point value.
    strand - defined as + (forward) or - (reverse).
    phase - One of '0', '1' or '2'. '0' indicates that the first base of the feature is the first base of a codon, '1' that the second base is the first base of a codon, and so on..
    attributes - A semicolon-separated list of tag-value pairs, providing additional information about each feature. Some of these tags are predefined, e.g. ID, Name, Alias, Parent - see the GFF documentation for more details.
    """
    gene_info = {}
    circular_regions = {}
    length_regions = {}
    name_pattern = re.compile("Name=([^;]+)")
    proper_open = gzip.open if gff_file.endswith('.gz') else open
    with proper_open(gff_file, "rt") as fl:
        for l in fl:
            # print(l)
            if l.startswith('#'):
                continue

            seqid, source, type, start, end, score, strand, phase, attributes = l.rstrip().split('\t')

            if type == 'region':
                if 'Is_circular=true' in attributes:
                    circular_regions[seqid] = True
                    length_regions[seqid] = end

                else:
                    circular_regions[seqid] = False
                    length_regions[seqid] = end

            if not type == 'CDS':
                continue
            try:
                prot_name = name_pattern.search(attributes).group(1)
            except AttributeError:
                logging.info(f'Prot Name not found in CDS line: {l}')
                continue

            if prot_name in seq_ids:
                gene_info[prot_name] = {"prot_id": prot_name,
                                        "seqid": seqid,
                                        "start": start,
                                        "end": end,
                                        "strand": strand,
                                        'circular': circular_regions[seqid],
                                        'seqid_length': length_regions[seqid]}

    # Checking...
    assert set(gene_info) == set(
        seq_ids), f'Not all genes have been found in the gff. Missing genes: {set(seq_ids)- set(gene_info)}'

    return gene_info


def parse_ids_file(ids_file):
    """
    Count the protein-coding genes in the GFF file in order to randomly
     pick among them for simulate the transcripts.

    :param gff: Name of the genome reference GFF file.
    :type gff: str
    :return nb_genes: Number of genes.
    :rtype: int

    :Example:

    >>> function_count_genes("../data/GCF_000002985.6_WBcel235_genomic.gff")
    20090

    .. seealso:: chooseGenes()
    """
    ids_cog_dict = {}
    with open(ids_file) as csvfile:
        reader = csv.reader(csvfile,  delimiter='\t')
        for row in reader:
            if row[0].startswith('#'):
                continue
            ids_cog_dict[row[0]] = row[2]

    return ids_cog_dict


def write_tsv_from_list_of_dict(list_of_dict, output_file):

    with open(output_file, 'w', newline='') as csvfile:
        writer = csv.DictWriter(csvfile, fieldnames=list(list_of_dict[0].keys()), delimiter='\t')
        writer.writeheader()
        for dict_info in list_of_dict:
            writer.writerow(dict_info)


def get_genomic_locations(genes_id_file, gff_file):
    ids_cog_dict = parse_ids_file(genes_id_file)

    genes_info = retrieve_gene_info(ids_cog_dict, gff_file)

    return ids_cog_dict, genes_info


def parse_arguments():
    parser = ArgumentParser(description="...",
                            formatter_class=ArgumentDefaultsHelpFormatter)

    parser.add_argument("genes_id_file", type=str, help="")
    parser.add_argument("gff_file", type=str, help="")
    args = parser.parse_args()
    return args


def main():
    args = parse_arguments()

    basename = os.path.basename(args.gff_file)
    output_file = basename[:basename.rindex('.gff')] + "_identified_gene_locations.tsv"

    ids_cog_dict, genes_info = get_genomic_locations(args.genes_id_file, args.gff_file)

    # merge ids_cog_dict and genes_info to get COG information in the dict
    # and flatten genes_info to get a list of dict
    genes_info_list = []
    for prot_id, info_dict in genes_info.items():
        info_dict['cog_id'] = ids_cog_dict[prot_id]
        genes_info_list.append(info_dict)

    write_tsv_from_list_of_dict(genes_info_list, output_file)


if __name__ == '__main__':
    main()
