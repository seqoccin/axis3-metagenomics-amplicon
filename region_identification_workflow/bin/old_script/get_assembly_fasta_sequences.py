import os
import parser
import prepare_assemblies_downloading
from argparse import ArgumentParser, ArgumentDefaultsHelpFormatter
import logging


def parse_arguments():
    parser = ArgumentParser(description="...",
                            formatter_class=ArgumentDefaultsHelpFormatter)

    parser.add_argument("--taxids_dir", type=str, default='Genomes/',
                        help="")

    parser.add_argument("--assembly_file_suffix", type=str, default='_protein.faa.gz;_genomic.gbff.gz',
                        help="")
    args = parser.parse_args()
    return args


def main():
    print("Get assembly fasta sequences..")

    args = parse_arguments()

    taxids_dir = args.taxids_dir

    json_in_taxid_dir = 'assemblies_info.json'

    assembly_file_suffix_list = args.assembly_file_suffix.split(';')

    # the path are sorted by assembly completeness level
    taxid_dir_list = [os.path.join(taxids_dir, d)
                      for d in os.listdir(taxids_dir) if d.startswith("taxid_")]

    for taxid_dir in taxid_dir_list:
        try:
            assemblies_info = parser.decoder(os.path.join(taxid_dir, json_in_taxid_dir))
        except FileNotFoundError:
            logging.warning(f"File not found {os.path.join(taxid_dir, json_in_taxid_dir)}... ")
            continue
        best_assembly = assemblies_info[0]
        print(assemblies_info)
        print(best_assembly['ftp_path'], taxid_dir)

        for assembly_file_suffix in assembly_file_suffix_list:
            path = os.path.join(best_assembly['ftp_path'], os.path.basename(
                best_assembly['ftp_path']) + assembly_file_suffix)
            # print(path)
            prepare_assemblies_downloading.ftp_download(path, taxid_dir)


if __name__ == '__main__':
    main()
