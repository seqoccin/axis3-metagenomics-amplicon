import gzip
import json


def encoder(filename, f):
    with open(filename, 'w', encoding='utf8') as file:
        json.dump(f, file, indent=2)


def decoder(filename):
    with open(filename, 'r', encoding='utf8') as file:
        return json.load(file)


def parse_eggnog_members_file(file, min_genomes_threshold=0):
    cogs = []
    proper_open = gzip.open if file.endswith('.gz') else open
    taxIDS_set = set()
    with proper_open(file, "rt") as fl:
        for l in fl:
            l = l.rstrip()
            taxlvl, GroupName, ProteinCount, SpeciesCount, ProteinIDs, taxIDS = l.split('\t')
            tax_ids = taxIDS.split(',')
            taxIDS_set |= set(tax_ids)
            if int(SpeciesCount) > min_genomes_threshold:

                # Count gene copy
                copy_counts = {}
                for protid in ProteinIDs.split(','):
                    tax_id, protein_id = protid.split('.', 1)
                    copy_counts.setdefault(tax_id, []).append(protein_id)

                nb_single_copy = sum((1 for t, g in copy_counts.items() if len(g) == 1))

                cog = {'GroupName': GroupName,
                       'ProteinCount': int(ProteinCount),
                       'SpeciesCount': int(SpeciesCount),
                       'nb_single_copy': int(nb_single_copy)  # ,
                       # 'ProteinIDs': ProteinIDs.split(','),
                       # 'taxIDS': tax_ids
                       }

                cogs.append(cog)

    return cogs, taxIDS_set
