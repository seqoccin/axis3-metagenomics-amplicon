import csv
from argparse import ArgumentParser, ArgumentDefaultsHelpFormatter


def list_of_dict_to_tsv(list_of_dict, output_file, mode='w'):
    assert len(list_of_dict) > 0, 'Empty list of dict! cannot write to tsv'

    with open(output_file, mode, newline='') as csvfile:
        writer = csv.DictWriter(csvfile, fieldnames=list(list_of_dict[0].keys()), delimiter='\t')
        if mode == 'w':
            writer.writeheader()
        for dict_info in list_of_dict:
            writer.writerow(dict_info)


def parse_arguments():
    parser = ArgumentParser(description="...",
                            formatter_class=ArgumentDefaultsHelpFormatter)

    parser.add_argument("pairs_stat_file", type=str,
                        help="")
    parser.add_argument("--key_filter", type=str, default='nb_genome_with_main_orga',
                        help="")
    parser.add_argument("--threshold", type=int, default=5000,
                        help="")
    parser.add_argument("--output", type=str, default="selected_pairs.tsv",
                        help="")
    args = parser.parse_args()

    return args


def main():
    args = parse_arguments()
    pairs_stat_file = args.pairs_stat_file
    threshold = args.threshold
    key_filter = args.key_filter

    output_file = "selected_pairs.tsv"
    selected_pairs = []

    with open(pairs_stat_file) as csvfile:
        reader = csv.DictReader(csvfile, delimiter='\t')
        for d in reader:
            # Check the key filter is above given theshold
            # and that pair is not the same against itself
            if int(d[key_filter]) > threshold and len(set(d['cog_pair'].split('-'))) == 2:
                selected_pairs.append(d)
    if len(selected_pairs) == 0:
        raise ValueError('None of the pairs are satisfying selection threshold')

    list_of_dict_to_tsv(selected_pairs, output_file)


if __name__ == '__main__':
    main()
