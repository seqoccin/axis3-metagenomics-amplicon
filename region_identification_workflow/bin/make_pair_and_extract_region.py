#!/usr/bin/env python3

"""
Retrieve genomic positions of the proteins identified by FetchMGs.

:Example:
python
"""

# Metadata
__author__ = 'Mainguy Jean - Plateforme bioinformatique Toulouse'
__copyright__ = 'Copyright (C) 2019 INRA'
__license__ = 'GNU General Public License'
__version__ = '0.1'
__email__ = 'support.bioinfo.genotoul@inra.fr'
__status__ = 'dev'


from collections import defaultdict
from argparse import ArgumentParser, ArgumentDefaultsHelpFormatter
import os
import logging
import re
import tools
import identify_rrna_gene
# import make_cog_pairs
import regions_analysis_fct
from Bio import SeqIO
import gzip
from Bio.SeqFeature import SeqFeature, FeatureLocation


def get_assemblies_files(ids_dir, gff_dir, fna_dir):
    files_per_assembly = defaultdict(dict)
    capture_assemby_name = re.compile("([A-Z]{3}_\d+.\d+)")

    for data_file in os.listdir(ids_dir):
        genome_name = capture_assemby_name.search(data_file).group(1)
        files_per_assembly[genome_name]["ids"] = os.path.join(ids_dir, data_file)

    for data_file in os.listdir(gff_dir):
        if data_file.endswith('.gff') or data_file.endswith('.gff.gz'):
            genome_name = capture_assemby_name.search(data_file).group(1)
            files_per_assembly[genome_name]["gff"] = os.path.join(gff_dir, data_file)

    for data_file in os.listdir(fna_dir):
        if data_file.endswith('.fna') or data_file.endswith('.fna.gz'):
            genome_name = capture_assemby_name.search(data_file).group(1)
            files_per_assembly[genome_name]["fna"] = os.path.join(fna_dir, data_file)

    return files_per_assembly


def write_info_by_cog(genes_info, csv_handlers_dict, genome_name, output_dir, key_name):

    for gene_info in genes_info:
        filename = os.path.join(output_dir,  gene_info[key_name]+'.tsv')
        tools.write_dict_to_tsv(gene_info, csv_handlers_dict, filename)


def format_gene_for_extraction(gene_info, store_dir):
    """
    Format gene info to be extracted from the genome by the function extract_sequences
    """
    start, end = int(gene_info['start']),  int(gene_info['end'])
    gene_region = {'region_name': gene_info['cog_id'],
                   'seqid': gene_info['seqid'],
                   'start': start,
                   'end': end,
                   'strand': int(f"{gene_info['strand']}1"),
                   'header': f'{gene_info["genome"]}|{gene_info["seqid"]}|{start}-{end}',
                   'description': f"{gene_info['gene_name']} {gene_info['gene_id']}",
                   'store_dir': store_dir
                   }
    return gene_region


def format_gene_for_extraction_old(region, genes, store_dir):

    cog1, cog2 = region['cog_pair'].split('|')
    gene1, gene2 = region['genes'].split('|')
    id1, id2 = region['ids'].split('|')
    strand1, strand2 = region['strands'].split('|')
    position1, position2 = region['positions'].split('|')
    if id1 not in genes:
        start1, end1 = position1.split('-')
        gene1_region = {'region_name': cog1,
                        'seqid': region['seqid'],
                        'start': int(start1),
                        'end': int(end1),
                        'strand': int(f"{strand1}1"),
                        'header': f'{region["genome"]}|{region["seqid"]}|{start1}-{end1}',
                        'description': f'{gene1} {id1}',
                        'store_dir': store_dir
                        }
        genes[id1] = gene1_region

    if id2 not in genes:
        start2, end2 = position2.split('-')
        gene2_region = {'region_name': cog2,
                        'seqid': region['seqid'],
                        'start': int(start2),
                        'end': int(end2),
                        'strand': int(f"{strand2}1"),
                        'header': f'{region["genome"]}|{region["seqid"]}|{start2}-{end2}',
                        'description': f'{gene2} {id2}',
                        'store_dir': store_dir
                        }
        genes[id2] = gene2_region


def get_start_and_end(position_string):
    """
    >> get_region_start_and_end("529951-530454|532135-532674")
    (529951, 532674)
    """
    region_start = int(position_string.split('|')[0].split('-')[0])
    region_end = int(position_string.split('|')[1].split('-')[1])

    return region_start, region_end


def format_pair_for_extraction(pair_info, store_dir):

    cog1, cog2 = sorted(pair_info['cog_pair'].split('|'))
    cog_sorted = '-'.join((cog1, cog2))

    start, end = get_start_and_end(pair_info['positions'])

    # Get strand
    cog_pair_list = pair_info['cog_pair'].split('|')
    cogA = sorted(cog_pair_list)[0]
    index_of_cogA = cog_pair_list.index(cogA)
    strand_of_cogA = pair_info['strands'].split('|')[index_of_cogA]

    seq_header = f'{pair_info["genome"]}|{pair_info["seqid"]}|{start}-{end}'
    description = f'{pair_info["genes"]} {pair_info["ids"]}'

    regions_to_extract = {'region_name': cog_sorted,
                          'seqid': pair_info['seqid'],
                          'start': start,
                          'end': end,
                          'strand': int(f"{strand_of_cogA}1"),
                          'header': seq_header,
                          'description': description,
                          'store_dir': store_dir
                          }
    return regions_to_extract


def get_seq_of_gene(record, start, end, strand):
    if start < end:
        # everything is normal
        location = FeatureLocation(start-1, end)  # minus one to be in zero base

    elif start >= end:
        # Then the region is overlapping the 0 of the chromosome
        location = FeatureLocation(start-1, len(record)) + FeatureLocation(0, end)
        # print("MULPTIPLE LOCATION ", location)
    else:
        logging.critical('Problem in location of seq')
        exit(1)

    feat = SeqFeature(location,  strand=strand)
    return feat.extract(record)


def extract_sequences(genome_file, regions, fh_of_cog_pair_seqs):
    nb_region_extracted = 0
    proper_open = gzip.open if genome_file.endswith('.gz') else open
    regions_by_seqid = defaultdict(list)

    for r in regions:
        regions_by_seqid[r['seqid']].append(r)

    with proper_open(genome_file, 'rt') as handle:

        for record in SeqIO.parse(handle, "fasta"):

            if record.id in regions_by_seqid:
                for region in regions_by_seqid[record.id]:
                    cog_pair_file = os.path.join(
                        region['store_dir'], region['region_name'] + '.fna')
                    try:
                        fh = fh_of_cog_pair_seqs[cog_pair_file]
                    except KeyError:
                        fh = open(cog_pair_file, 'w')
                        
                        fh_of_cog_pair_seqs[cog_pair_file] = fh

                    seq = get_seq_of_gene(record, region['start'], region['end'], region['strand'])
                    seq.id = region['header']
                    seq.description = region['description']

                    fh.write(f">{region['header']} {region['description']}\n")
                    fh.write(f"{str(seq.seq)}\n")

                    nb_region_extracted += 1

    assert nb_region_extracted == len(
        regions), f'{nb_region_extracted}  extracted vs {len(regions)} expected'


def parse_arguments():
    parser = ArgumentParser(description="...",
                            formatter_class=ArgumentDefaultsHelpFormatter)

    parser.add_argument('-t', "--cog_table_dir", type=str, help="")
    parser.add_argument('-g', "--gff_dir", type=str, help="")
    parser.add_argument('-f', "--fna_dir", type=str, help="")
    parser.add_argument('-p', "--cog_pairs", type=str,
                        help="cog_pair stat file to investigate with main orientation to looking for")
    parser.add_argument('-l', "--length_cutoff", type=int,
                        help="maximal length of a region to be considered and extracted")
    parser.add_argument("-v", "--verbose", help="increase output verbosity",
                        action="store_true")
    parser.add_argument("--rrna_genes_to_fetch", nargs='+',
                        default=[], help="rrna genes to fetch in gff",)
    parser.add_argument("--logfile", help="file where logging will be written.",
                        type=str, default=None,)

    args = parser.parse_args()
    return args


def main():
    args = parse_arguments()
    if args.verbose:
        level = logging.INFO
    else:
        level = logging.WARNING

    if args.logfile:
        # log are written in logfile
        logging.basicConfig(filename=args.logfile,
                            filemode='a', format="%(levelname)s: %(message)s", level=level)
    else:
        logging.basicConfig(format="%(levelname)s: %(message)s", level=level)

    cog_table_dir, gff_dir, fna_dir = args.cog_table_dir,  args.gff_dir,  args.fna_dir
    length_cutoff = args.length_cutoff
    cog_pairs_file = args.cog_pairs
    rrna_genes_to_fetch = args.rrna_genes_to_fetch

    cog_info_output_dir = "cog_location"
    os.makedirs(cog_info_output_dir, exist_ok=True)

    extract_seq_dir = "extracted_region"
    os.makedirs(extract_seq_dir, exist_ok=True)

    extract_single_gene_dir = "extracted_single_gene"
    os.makedirs(extract_single_gene_dir, exist_ok=True)

    region_info_output_dir = 'cog_pairs'
    os.makedirs(region_info_output_dir, exist_ok=True)

    csv_handlers_dict = {'open_files': []}
    extracted_region_handler = {}

    selected_pairs = {d['cog_pair']: d for d in tools.tsv_to_list_of_dict(cog_pairs_file)}
    selected_cogs = {pair.split(
        '-')[0] for pair in selected_pairs} | {pair.split('-')[1] for pair in selected_pairs}

    logging.info(f'{len(selected_pairs)} regions selected made from {len(selected_cogs)} COGs')

    if rrna_genes_to_fetch:
        logging.warning(f'ribosomal RNA genes : {rrna_genes_to_fetch} are being retrieved in genomes')
        if '16S-23S' in rrna_genes_to_fetch:
            main_orga = '16S>-23S>'
            selected_pairs['16S-23S'] = {'main_orga': main_orga}
            logging.warning(f'Region 16S-23S has been added to the list of selected pairs with main orga : {main_orga}')
    files_per_assembly = get_assemblies_files(cog_table_dir, gff_dir, fna_dir)

    for genome_name, files_dict in files_per_assembly.items():
        regions_to_extract = []
        genes_to_extract = {}

        if len(files_dict) != 3:
            logging.critical(f'Not 3 files for {genome_name}:--> {files_dict}')
            exit(1)

        ids_file, gff_file, fna_file = files_dict['ids'], files_dict['gff'], files_dict['fna']

        gene_infos_unfiltered = regions_analysis_fct.get_genomic_locations(ids_file, gff_file)
        # Filter genes to keep only the one that are part of a selected pair
        # when fetch mgs results have been recycled from process upstream process
        # some unwanted cogs are present and need to be ignored..
        gene_infos = [info for info in gene_infos_unfiltered if info['cog_id'] in selected_cogs]
        logging.debug(f'{genome_name}: {len(gene_infos)} COGs gene info to write down')
        if rrna_genes_to_fetch:
            rrna_infos = identify_rrna_gene.retrieve_rrna_genes_from_gff(gff_file)
            gene_infos += rrna_infos

        for gene_info in gene_infos:
            gene_info['genome'] = genome_name

        write_info_by_cog(gene_infos, csv_handlers_dict, genome_name,
                          cog_info_output_dir, key_name='cog_id')
        dist_infos = []
        if rrna_genes_to_fetch:
            # format for extraction all rrna genes that are in rrna_genes_to_fetch
            for rrna_info in rrna_infos:
                if rrna_info['gene_name'] in rrna_genes_to_fetch:
                    formatted_gene = format_gene_for_extraction(rrna_info, extract_single_gene_dir)
                    genes_to_extract[rrna_info['gene_id']] = formatted_gene

                    rrna_dist_infos = regions_analysis_fct.get_gene_pair_info(
                        rrna_info, rrna_info)
                    for dist_info in rrna_dist_infos:
                        dist_info['region_name'] = rrna_info['gene_name']
                    dist_infos += rrna_dist_infos

        for i, gene_info in enumerate(gene_infos):
            for gene_info_next in gene_infos[i:]:
                cog_pair = '-'.join(sorted((gene_info['cog_id'], gene_info_next['cog_id'])))
                if cog_pair in selected_pairs:

                    pair_dist_infos = regions_analysis_fct.get_gene_pair_info(
                        gene_info, gene_info_next)
                    dist_infos += pair_dist_infos

                    for dist_info in pair_dist_infos:
                        orga = regions_analysis_fct.get_pair_organisation(dist_info)
                        dist_info['region_name'] = '-'.join(
                            sorted(dist_info['cog_pair'].split('|')))

                        if orga == selected_pairs[cog_pair]['main_orga'] and dist_info['distance'] <= length_cutoff:

                            formatted_region = format_pair_for_extraction(
                                dist_info, extract_seq_dir)
                            regions_to_extract.append(formatted_region)

                            # format_gene_for_extraction(dist_info, genes_to_extract,
                            #                        extract_single_gene_dir)
                            logging.debug(
                                f"Write info on {gene_info['gene_id']} and {gene_info['gene_id']}")
                            if gene_info['gene_id'] not in genes_to_extract:

                                formatted_gene = format_gene_for_extraction(
                                    gene_info, extract_single_gene_dir)
                                genes_to_extract[gene_info['gene_id']] = formatted_gene
                            else:
                                logging.debug(
                                    f"{gene_info['gene_id']} is already in genes_to_extract")

                            if gene_info_next['gene_id'] not in genes_to_extract:
                                formatted_gene = format_gene_for_extraction(
                                    gene_info_next, extract_single_gene_dir)
                                genes_to_extract[gene_info_next['gene_id']] = formatted_gene
                            else:
                                logging.debug(
                                    f"{gene_info_next['gene_id']} is already in genes_to_extract")

        write_info_by_cog(dist_infos, csv_handlers_dict, genome_name,
                          region_info_output_dir, key_name='region_name')

        regions_to_extract += list(genes_to_extract.values())
        extract_sequences(fna_file, regions_to_extract, extracted_region_handler)

    for f in csv_handlers_dict['open_files']:
        f.close()

    for f in extracted_region_handler.values():
        f.close()


if __name__ == '__main__':
    main()
