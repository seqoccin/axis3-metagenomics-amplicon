#!/usr/bin/env python3

"""


:Example:
$ python transformed_assembly_summary.py
"""

# Metadata
__author__ = 'Mainguy Jean - Plateforme bioinformatique Toulouse'
__copyright__ = 'Copyright (C) 2019 INRA'
__license__ = 'GNU General Public License'
__version__ = '0.1'
__email__ = 'support.bioinfo.genotoul@inra.fr'
__status__ = 'dev'


from argparse import ArgumentParser, ArgumentDefaultsHelpFormatter
import logging
import pandas as pd


def parse_arguments():
    parser = ArgumentParser(description="...",
                            formatter_class=ArgumentDefaultsHelpFormatter)

    parser.add_argument("--assemblies_summary_taxonomy", help="", required=True)
    parser.add_argument("--resolution_tax_result",  required=True,  help="")
    parser.add_argument('-o', "--outfile",  default='assembly_summary_transformed.tsv',  help="")
    parser.add_argument("--pident_threshold", default=95, type=float, help="")
    parser.add_argument("-v", "--verbose", help="increase output verbosity",
                        action="store_true")
    args = parser.parse_args()
    return args


def main():

    args = parse_arguments()

    if args.verbose:
        logging.basicConfig(format="%(levelname)s: %(message)s", level=logging.DEBUG)
        logging.info('Mode verbose ON')

    else:
        logging.basicConfig(format="%(levelname)s: %(message)s")

    pident_threshold = args.pident_threshold
    assemblies_summary_taxonomy = args.assemblies_summary_taxonomy
    resolution_tax_result_fl = args.resolution_tax_result
    outfile = args.outfile

    resol_df = pd.read_csv(resolution_tax_result_fl, sep='\t')
    taxids_ok = resol_df[resol_df['prct_ident_threshold'] == pident_threshold]['taxids_ok']
    # print(list(taxids_ok.array)[0])
    # print(type(list(taxids_ok.array)[0]))
    # print(type(taxids_ok))
    # print("series:", taxids_ok)
    # print(pd.isnull(taxids_ok))
    if type(list(taxids_ok.array)[0]) == str:
        sp_taxid_ok = list(taxids_ok.array)[0].split(',')
    else:
        sp_taxid_ok = []
    logging.info(f'{len(sp_taxid_ok)} species are unambiguously identified based on a {pident_threshold}% identity threshold')

    assemblies_summary = pd.read_csv(assemblies_summary_taxonomy, sep='\t')
    assemblies_summary['unambiguously_identified'] = assemblies_summary['species_taxid'].map(
        lambda x: str(x) in sp_taxid_ok)
    assemblies_summary['unambiguously_identified'].value_counts()
    df = assemblies_summary[['unambiguously_identified', 'taxonomy']]
    df.to_csv(outfile, sep='\t', index=False)


if __name__ == '__main__':
    main()
