#!/usr/bin/env python3

"""
Description

all_assemblies includes rep_ref_and_10_assemblies_per_sp, one_assembly_per_sp and ref_and_rep_assemblies
rep_ref_and_10_assemblies_per_sp includes  one_assembly_per_sp and ref_and_rep_assemblies
one_assembly_per_sp does not include anyother selection
ref_and_rep_assemblies does not include anyother selection

:Example:
filter_cog_pair_files_according_assemblies.py --primary_selection_name $primary \
                                              --extended_selection_name $extended \
                                              --all_assemblies  $all_assemblies \
                                              --rep_ref_and_10_assemblies_per_sp  $rep_ref_and_10_assemblies_per_sp \
                                              --one_assembly_per_sp $one_assembly_per_sp  \
                                              --ref_and_rep_assemblies $ref_and_rep_assemblies \
                                              --cog_pair_dir "cog_pairs"
"""

# Metadata
__author__ = 'Mainguy Jean - Plateforme bioinformatique Toulouse'
__copyright__ = 'Copyright (C) 2019 INRA'
__license__ = 'GNU General Public License'
__version__ = '0.1'
__email__ = 'support.bioinfo.genotoul@inra.fr'
__status__ = 'dev'


from argparse import ArgumentParser, ArgumentDefaultsHelpFormatter
import logging
import os
import csv


def filter_cog_pairs(cog_pair_dir_input, selection, cog_pair_dir_out, selected_pairs):

    if os.path.exists(cog_pair_dir_out):
        logging.warning(f'cog_pair_dir_out {cog_pair_dir_out} exists already...')
        return
    else:
        logging.info(
            f'Filter cog pairs {cog_pair_dir_input} to {cog_pair_dir_out} using selection {selection}')
        os.makedirs(cog_pair_dir_out)

    with open(selection) as fl:
        reader = csv.DictReader(fl, delimiter='\t')
        assemblies_to_keep = [d['assembly_accession'] for d in reader]

    for cog_pair_file in os.listdir(cog_pair_dir_input):
        pair = ''.join(cog_pair_file.split('.')[:-1])
        if pair in selected_pairs:
            cog_pair_fl_path = os.path.join(cog_pair_dir_input, cog_pair_file)
            cog_pair_fl_filtered = os.path.join(cog_pair_dir_out, cog_pair_file)

            logging.info(f'Writing file {cog_pair_fl_filtered}')
            with open(cog_pair_fl_path) as fl,  open(cog_pair_fl_filtered, 'w') as fl_out:
                reader = csv.DictReader(fl, delimiter='\t')
                writer = csv.DictWriter(fl_out, fieldnames=reader.fieldnames, delimiter='\t')

                writer.writeheader()
                keep = 0

                for i, line_dict in enumerate(reader):
                    pair = '-'.join(sorted(line_dict['cog_pair'].split('|')))
                    # a bit weak: complete genome acession look like that GCF_001854245.1_ASM185424v1
                    # and we only want GCF_001854245.1
                    genome = '_'.join(line_dict['genome'].split('_')[:2])
                    if genome in assemblies_to_keep:
                        writer.writerow(line_dict)
                        keep += 1
                logging.info(f'{cog_pair_fl_path}:{keep} pairs kept on {i+1} initial pairs')


def parse_arguments():
    """Parse script arguments."""
    parser = ArgumentParser(description="...",
                            formatter_class=ArgumentDefaultsHelpFormatter)

    parser.add_argument("--cog_pair_to_filter_dir", type=str, help="", required=True)

    parser.add_argument("--all_assemblies", type=str, help="", required=True)
    parser.add_argument("--rep_ref_and_10_assemblies_per_sp", type=str, help="", required=True)
    parser.add_argument("--ref_and_rep_assemblies", type=str, help="", required=True)
    parser.add_argument("--one_assembly_per_sp", type=str, help="", required=True)

    parser.add_argument("--filtered_cog_pair_dir", type=str, help="cog_pairs")
    parser.add_argument("--selected_pairs_list_file", type=str, required=True)

    parser.add_argument("-v", "--verbose", help="increase output verbosity",
                        action="store_true")

    args = parser.parse_args()
    return args


def main():

    args = parse_arguments()

    if args.verbose:
        logging.basicConfig(format="%(levelname)s: %(message)s", level=logging.DEBUG)
        logging.info('Mode verbose ON')

    else:
        logging.basicConfig(format="%(levelname)s: %(message)s")

    all_assemblies = args.all_assemblies
    rep_ref_and_10_assemblies_per_sp = args.rep_ref_and_10_assemblies_per_sp
    ref_and_rep_assemblies = args.ref_and_rep_assemblies
    one_assembly_per_sp = args.one_assembly_per_sp

    cog_pair_dir = args.filtered_cog_pair_dir
    selected_pairs_list_file = args.selected_pairs_list_file

    cog_pair_to_filter_dir = args.cog_pair_to_filter_dir

    input_cog_pair_selections_avalaible = [
        cog_pair_dir for cog_pair_dir in os.listdir(cog_pair_to_filter_dir)]

    with open(selected_pairs_list_file) as fl:
        selected_pairs = [l.strip() for l in fl if l]
    # # check
    # if not os.path.isdir(cog_pair_primary_dir):
    #     raise ValueError(f'Cog pair dir {cog_pair_dir} does not contain a directory cog pair of primary assembly selection {primary_selection_name}')
    #
    # if not os.path.isdir(cog_pair_extended_dir):
    #     raise ValueError(f'Cog pair dir {cog_pair_dir} does not contain a directory cog pair of extended assembly selection {cog_pair_extended_dir}')

    if 'all_assemblies' in input_cog_pair_selections_avalaible:
        # filter cog pair for all selection
        cog_pair_dir_to_filter = os.path.join(
            cog_pair_to_filter_dir, 'all_assemblies')

        selections = [all_assemblies,
                      rep_ref_and_10_assemblies_per_sp,
                      ref_and_rep_assemblies,
                      one_assembly_per_sp]

        for selection in selections:
            cog_pair_dir_out = os.path.join(cog_pair_dir,  ''.join(selection.split('.')[:-1]))
            filter_cog_pairs(cog_pair_dir_to_filter, selection,
                             cog_pair_dir_out, selected_pairs)

    elif 'rep_ref_and_10_assemblies_per_sp' in input_cog_pair_selections_avalaible:
        # filter cog pair for all selection except all_assemblies
        logging.info('rep_ref_and_10_assemblies_per_sp in input_cog_pair_selections_avalaible')
        cog_pair_dir_to_filter = os.path.join(
            cog_pair_to_filter_dir, 'rep_ref_and_10_assemblies_per_sp')

        selections = [rep_ref_and_10_assemblies_per_sp,
                      ref_and_rep_assemblies,
                      one_assembly_per_sp]

        for selection in selections:
            cog_pair_dir_out = os.path.join(cog_pair_dir, ''.join(selection.split('.')[:-1]))
            filter_cog_pairs(cog_pair_dir_to_filter, selection,
                             cog_pair_dir_out, selected_pairs)

    else:
        if 'one_assembly_per_sp' in input_cog_pair_selections_avalaible:
            logging.info('one_assembly_per_sp in input_cog_pair_selections_avalaible')
            cog_pair_dir_to_filter = os.path.join(cog_pair_to_filter_dir, 'one_assembly_per_sp')
            cog_pair_dir_out = os.path.join(cog_pair_dir, 'one_assembly_per_sp')
            filter_cog_pairs(cog_pair_dir_to_filter, one_assembly_per_sp,
                             cog_pair_dir_out, selected_pairs)

        if 'ref_and_rep_assemblies' in input_cog_pair_selections_avalaible:
            logging.info('ref_and_rep_assemblies in input_cog_pair_selections_avalaible')
            cog_pair_dir_to_filter = os.path.join(cog_pair_to_filter_dir, 'ref_and_rep_assemblies')
            cog_pair_dir_out = os.path.join(cog_pair_dir, 'ref_and_rep_assemblies')
            filter_cog_pairs(cog_pair_dir_to_filter, ref_and_rep_assemblies,
                             cog_pair_dir_out, selected_pairs)


if __name__ == '__main__':
    main()
