#!/usr/bin/env python3

"""
Analysis of cog pair.

Compute stat on the pair such as median/min/max/mean length, orientation...

:Example:
$ python cog_pair_analysis.py <cog_pairs>
"""

# Metadata
__author__ = 'Mainguy Jean - Plateforme bioinformatique Toulouse'
__copyright__ = 'Copyright (C) 2019 INRA'
__license__ = 'GNU General Public License'
__version__ = '0.1'
__email__ = 'support.bioinfo.genotoul@inra.fr'
__status__ = 'dev'


from argparse import ArgumentParser, ArgumentDefaultsHelpFormatter
import os
import logging
import sys
import regions_analysis_fct
import csv


def parse_arguments():
    """Parse script arguments."""
    parser = ArgumentParser(description="...",
                            formatter_class=ArgumentDefaultsHelpFormatter)
    group = parser.add_mutually_exclusive_group()
    parser.add_argument("-f", "--cog_pair_files", nargs='+',
                        required=True, help="tsv file of a cog_pair")
    parser.add_argument("-o", "--output", type=str, default='pairs_stat.tsv',
                        help="Output file")
    # parser.add_argument("--assembly_selection_filter", type=str, default=None,
    #                     help="Assembly summary file containing a selection of assembly. Cog pairs analysis is based only on this selection of assembly.")
    parser.add_argument("--length_cutoff", type=int, default=10000,
                        help="Maximal length cutoff for a region to be analysed")
    parser.add_argument("-v", "--verbose", help="increase output verbosity",
                        action="store_true")
    parser.add_argument("--debug", help="Debug mode",
                        action="store_true")
    args = parser.parse_args()
    return args


def main():
    args = parse_arguments()
    if args.debug:
        logging.basicConfig(format="%(levelname)s: %(message)s", level=logging.DEBUG)
        logging.debug('Mode debug ON')

    else:
        if args.verbose:
            logging.basicConfig(format="%(levelname)s: %(message)s", level=logging.INFO)
            logging.info('Mode verbose ON')

        else:
            logging.basicConfig(format="%(levelname)s: %(message)s")

    cog_pair_files = args.cog_pair_files

    max_length_cutoff = args.length_cutoff

    pair_stat_output_fl = args.output  # 'pairs_stat.tsv'

    # assembly_selection = args.assembly_selection_filter

    prct = 0
    nb_files = len(cog_pair_files)
    interval_print = 1 + int(nb_files/100)
    logging.info(f'Write {pair_stat_output_fl}')
    with open(pair_stat_output_fl, 'w') as all_fh:

        for i, f in enumerate(cog_pair_files):
            logging.debug(f'analyse_cog_pair {i}th file: {f}')
            pair_summary, _ = regions_analysis_fct.analyse_cog_pair(f, max_length_cutoff)

            # Write all pairs
            try:
                writer.writerow(pair_summary)
            except NameError:
                writer = csv.DictWriter(all_fh, fieldnames=list(
                    pair_summary.keys()), delimiter='\t')
                writer.writeheader()
                writer.writerow(pair_summary)

            if i % interval_print == 0:
                prct = round(i/nb_files*100)
                sys.stderr.write(f'\r|{"="*round(prct)}>{prct}%')
                sys.stderr.flush()

    sys.stderr.write(f'\r|{"="*100}>{100}%\n')


if __name__ == '__main__':
    main()
