#!/usr/bin/env python3

"""
Retrieve genomic positions of the proteins identified by FetchMGs.

:Example:
python get_genomic_positions_by_cog.py gff_and_ids_dir
"""

# Metadata
__author__ = 'Mainguy Jean - Plateforme bioinformatique Toulouse'
__copyright__ = 'Copyright (C) 2019 INRA'
__license__ = 'GNU General Public License'
__version__ = '0.1'
__email__ = 'support.bioinfo.genotoul@inra.fr'
__status__ = 'dev'


from collections import defaultdict
from argparse import ArgumentParser, ArgumentDefaultsHelpFormatter, FileType
import os
import logging
import sys
import tools

import regions_analysis_fct


def pair_gff_and_ids_files(gff_dir, ids_dir):
    """Pair gff and ids files in a dictionary."""
    files_per_genomes = defaultdict(dict)
    for file in os.listdir(gff_dir):
        if file.endswith('_genomic.gff.gz') or file.endswith('_genomic.gff'):
            genome_name = file[:file.rindex('_genomic.gff')]
            files_per_genomes[genome_name]["gff"] = os.path.join(gff_dir, file)

    for file in os.listdir(ids_dir):
        if os.path.isdir(file) and file.endswith("_extraction"):
            genome_name = file[:file.rindex('_extraction')]
            ids_file = os.path.join(ids_dir, file, "sequences.all.marker_genes_scores.table")
            files_per_genomes[genome_name]["ids"] = ids_file
        elif file.endswith('_marker_genes_scores.table'):
            genome_name = file[:file.rindex('_marker_genes_scores.table')]
            files_per_genomes[genome_name]["ids"] = os.path.join(ids_dir, file)

    return files_per_genomes


def parse_arguments():
    """Parse script arguments."""
    parser = ArgumentParser(description="...",
                            formatter_class=ArgumentDefaultsHelpFormatter)
    parser.add_argument("--gff_dir", required=True,
                        help="Directory where gff files are stored",)
    parser.add_argument("--ids_dir", required=True,
                        help="Directory where identified CDS ids files are stored",)
    parser.add_argument("-o", "--outdir", default='cog_genomic_position', help="output directory")
    parser.add_argument("-v", "--verbose", help="increase output verbosity",
                        action="store_true")
    parser.add_argument("--logfile", help="file where logging will be written.",
                        type=str, default=None,)

    args = parser.parse_args()
    return args


def main():
    """Retrieve genomic positions of the proteins identified by FetchMGs."""
    args = parse_arguments()

    if args.verbose:
        level = logging.INFO
    else:
        level = logging.WARNING

    if args.logfile:
        # log are written in logfile
        logging.basicConfig(filename=args.logfile,
                            filemode='a', format="%(levelname)s: %(message)s", level=level)
    else:
        logging.basicConfig(format="%(levelname)s: %(message)s", level=level)
    # Output dir
    output_dir = args.outdir
    os.makedirs(output_dir, exist_ok=True)

    # Pair gff and ids file
    gff_dir = args.gff_dir
    ids_dir = args.ids_dir
    files_per_genomes = pair_gff_and_ids_files(gff_dir, ids_dir)

    logging.info(
        f'Genomic location of identified proteins will be extracted from {len(files_per_genomes)} gff files.')
    file_handlers_dict = {}
    i = 0
    for genome_name, genomes_info_dict in files_per_genomes.items():
        i += 1
        if i % 50 == 0:
            prct = round(i/len(files_per_genomes)*100, 1)
            sys.stderr.write(f'\r|{"="*round(prct)}>{prct}%')
            sys.stderr.flush()

        assert len(
            genomes_info_dict) == 2, f'extraction file or gff file is missing: {genomes_info_dict}'

        gene_infos = regions_analysis_fct.get_genomic_locations(
            genomes_info_dict['ids'], genomes_info_dict['gff'])

        for gene_info in gene_infos:

            cog_id = gene_info['cog_id']
            gene_info['genome'] = genome_name
            filename = os.path.join(output_dir, cog_id+'.tsv')
            tools.write_dict_to_tsv(gene_info, file_handlers_dict, filename)

    for f in file_handlers_dict['open_files']:
        f.close()
    prct = round(i/len(files_per_genomes)*100, 1)
    sys.stderr.write(f'\r|{"="*round(prct)}>{prct}%\n')


if __name__ == '__main__':
    main()
