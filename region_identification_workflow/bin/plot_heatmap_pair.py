#!/usr/bin/env python3

"""
Plot heatmap.

:Example:
python template.py -v
"""

# Metadata
__author__ = 'Mainguy Jean - Plateforme bioinformatique Toulouse'
__copyright__ = 'Copyright (C) 2019 INRA'
__license__ = 'GNU General Public License'
__version__ = '0.1'
__email__ = 'support.bioinfo.genotoul@inra.fr'
__status__ = 'dev'


from argparse import ArgumentParser, ArgumentDefaultsHelpFormatter
import logging
import plotly.graph_objects as go
from collections import defaultdict

import tools


def get_dict_matrix(cogs_list, pair_stats_list):
    """Create double dict structure."""
    # Create dict of dict matrix
    i = 0
    # create a virgin dict with the cog list
    # to ensure that every possible pair is represented in the heatmap
    # even if it was not found associated in any genomes.
    cog_cog_dict = {}
    for cog1 in cogs_list:
        cog_cog_dict[cog1] = {}
        for cog2 in cogs_list:
            cog_cog_dict[cog1][cog2] = None

    for pair_stat in pair_stats_list:
        i += 1
        cog1, cog2 = pair_stat['cog_pair'].split('-')

        cog_cog_dict[cog1][cog2] = pair_stat
        cog_cog_dict[cog2][cog1] = pair_stat

    return cog_cog_dict


def get_z_matrix(cogs, cog_cog_dict, key, pairs_vs_themself=False):
    """
    Get a matrix needed for ploting the heatmap.

    cogs: list of cogs that we want to display
    cog_cog_dict: dict of dict with stat on which combination of cogs
    key: The stat we want to display in the heatmap.
    pairs_vs_themself: bool - - > do we want to display in the heatmap cogs against them self?
    """
    stat_rows = []
    # Create a double list as expected by the heatmap
    for i, c1 in enumerate(cogs):
        stat_row = []
        for c2 in cogs[i:][::-1]:
            if not pairs_vs_themself and c1 == c2:
                continue
            try:
                pair_stat = cog_cog_dict[c1][c2][key]
            except TypeError:
                pair_stat = None
            stat_row.append(pair_stat)
        stat_rows.append(stat_row)
    return stat_rows


def apply_to_list_of_list(fct, list_of_list):
    """Apply function to a list of list."""
    transformed_list_of_list = []
    for row in list_of_list:
        transformed_row = [fct(i) for i in row]
        transformed_list_of_list.append(transformed_row)
    return transformed_list_of_list


def get_gene_name(cog_id, cog_to_gene):
    try:
        return cog_to_gene[cog_id]
    except KeyError:
        logging.warning(f'No Gene name found for {cog_id}')
        return cog_id


def get_z_legend_matrix(cogs, cog_cog_dict, cog_names, total_genome, pairs_vs_themself=False):
    """Build a matrix storing legend of the heatmap."""
    stat_rows = []
    # Create a double list as expected by the heatmap
    for i, c1 in enumerate(cogs):
        stat_row = []
        for c2 in cogs[i:][::-1]:
            if not pairs_vs_themself and c1 == c2:
                continue
            stat = cog_cog_dict[c1][c2]
            legend_list = []
            legend_list.append(f'x:{c2} ({get_gene_name(c2, cog_names)})')
            legend_list.append(f'y:{c1} ({get_gene_name(c1, cog_names)})')

            try:
                main_orga_prct = round(int(stat["nb_genome_with_main_orga"])/total_genome * 100, 1)
                legend_list.append(f'Genome with main orga:{stat["nb_genome_with_main_orga"]} ({main_orga_prct}%)')
            except TypeError:
                # pair is never found in genome so no stat exist
                legend_list.append(f'Pair never found in genomes')

            pair_stat = '<br>'.join(legend_list)

            stat_row.append(pair_stat)
        stat_rows.append(stat_row)
    return stat_rows


def parse_arguments():
    """Parse script arguments."""
    parser = ArgumentParser(description="...",
                            formatter_class=ArgumentDefaultsHelpFormatter)
    parser.add_argument("--pair_stat_file", help="pair stat")
    parser.add_argument("--cog_annotation", required=True,
                        help="Cog annotation tsv file with at least column GroupName and cog_gene_name")

    # parser.add_argument("--nb_genomes", type=int, default=1, help="Number of genome considered in the analysis",
    #                     action="store_true")
    parser.add_argument("--nb_assemblies", type=int, default=1,
                        help="minimal prct of genomes to display")
    parser.add_argument("-v", "--verbose", help="increase output verbosity",
                        action="store_true")

    args = parser.parse_args()
    return args


def main():
    """Plot heatmap."""
    args = parse_arguments()

    if args.verbose:
        logging.basicConfig(format="%(levelname)s: %(message)s", level=logging.DEBUG)
        logging.info('Mode verbose ON')

    else:
        logging.basicConfig(format="%(levelname)s: %(message)s")

    pair_stat_file = args.pair_stat_file
    cog_annotation_file = args.cog_annotation

    pair_stats = tools.tsv_to_list_of_dict(pair_stat_file)
    logging.info(f'{len(pair_stats)} pairs of COGS...')
    cogs_in_pair = {p['cog_pair'].split('-')[0]
                    for p in pair_stats} | {p['cog_pair'].split('-')[1] for p in pair_stats}

    cogs_info = tools.tsv_to_list_of_dict(cog_annotation_file)
    cog_names = {info['GroupName']: info['cog_gene_name'] for info in cogs_info}
    if len(cogs_info) > len(cogs_in_pair):
        logging.warning(f"{len(cogs_in_pair)} in pair and we have info for  {len(cogs_info)} cogs")

    total_genomes = args.nb_assemblies

    all_cogs = list(set(cog_names) | cogs_in_pair)
    cog_cog_dict = get_dict_matrix(all_cogs, pair_stats)

    # Sort COGS on a key to get nice heatmap
    k = 'nb_genome_with_main_orga'
    cog_mean_value = {}
    for cog, cog_dict in cog_cog_dict.items():
        cog_mean_value[cog] = sum([float(d[k])
                                   for d in cog_dict.values() if d is not None])/len(cog_dict)
    sort_cogs = sorted(cog_mean_value, key=cog_mean_value.get, reverse=True)

    x = sort_cogs[::-1]
    y = sort_cogs[::-1]

    # layout of all heatmap
    layout = go.Layout(
        xaxis=dict(side='top', type="category", showticklabels=True, tickfont=dict(size=12)),
        yaxis=dict(type="category", tickfont=dict(size=12)),
        titlefont=dict(size=100),
        paper_bgcolor='rgba(0,0,0,0)',
        plot_bgcolor='rgba(0,0,0,0)')

    # hover text of the heatmapcached
    z_legend = get_z_legend_matrix(sort_cogs, cog_cog_dict, cog_names,
                                   total_genomes, pairs_vs_themself=False)

    logging.info('Heatmap of nb_genome_with_main_orga')
    z_main_orga = get_z_matrix(sort_cogs, cog_cog_dict, 'nb_genome_with_main_orga', False)

    def prct_of_total_genome(i):
        if i is None:
            return None
        return float(i)/total_genomes*100
    z_main_orga_prct = apply_to_list_of_list(prct_of_total_genome, z_main_orga)

    trace = go.Heatmap(z=z_main_orga_prct[::-1], x=x, y=y,
                       colorscale='Viridis',
                       hoverinfo='text',
                       text=z_legend[::-1],
                       colorbar=dict(title='Percentage of <br>genomes with the dominant<br>orientation'))

    fig = go.Figure(data=[trace], layout=layout)
    logging.info('Write: heatmap_main_orga.html')
    fig.write_html('heatmap_main_orga.html')


if __name__ == '__main__':
    main()
