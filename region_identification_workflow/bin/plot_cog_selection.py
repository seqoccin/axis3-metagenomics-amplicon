#!/usr/bin/env python3

"""
Plot COGs and highligth them that fit the given thresholds

:Example:
$ python scripts/plot_selected_OGs.py all_ogs.tsv -p parameters.json  -o selected_OGs.html
"""

# Metadata
__author__ = 'Mainguy Jean - Plateforme bioinformatique Toulouse'
__copyright__ = 'Copyright (C) 2019 INRA'
__license__ = 'GNU General Public License'
__version__ = '0.1'
__email__ = 'support.bioinfo.genotoul@inra.fr'
__status__ = 'dev'


import plotly.graph_objects as go
import pandas as pd
from argparse import ArgumentParser, ArgumentDefaultsHelpFormatter
import logging
import tools


def parse_arguments():
    parser = ArgumentParser(description="...",
                            formatter_class=ArgumentDefaultsHelpFormatter)
    parser.add_argument("cogs_parsed_file", type=str,
                        help="tsv file with info on eggnog cogs with at least columns : 'GroupName	SpeciesCount	nb_single_copy	cog_gene_name'")

    parser.add_argument("--cog_single_copy_prct", type=float, default=95,
                        help="a orthologs group is selected if it is found in single copy in more than a specified percentage of the total genomes")
    parser.add_argument("--cog_prct", type=float, default=95,
                        help="a orthologs group is selected if it is found in more than a specified percentage of the total genomes")
    parser.add_argument("--min_cov_display", type=int, default=0,
                        help="minimal prct of genomes to display")
    parser.add_argument("--eggnog_species", type=int, default=1,
                        help="Total number of species take into account by eggnog")

    parser.add_argument("-v", "--verbose", help="increase output verbosity",
                        action="store_true")

    parser.add_argument("-o", "--output", help="output file name", default='selected_cogs.html')

    args = parser.parse_args()
    return args


def main():
    args = parse_arguments()

    file = args.cogs_parsed_file
    cog_single_copy_prct = args.cog_single_copy_prct
    cog_prct = args.cog_prct

    # Set the logging level. When the flag verbose is defined to true
    # logging are displayed from level info
    if args.verbose:
        logging.basicConfig(format="%(levelname)s: %(message)s", level=logging.DEBUG)
        logging.info("Verbose output.")
    else:
        logging.basicConfig(format="%(levelname)s: %(message)s")

    total_genomes = args.eggnog_species

    if args.min_cov_display is None:

        min_coverage = min(cog_single_copy_prct, cog_prct) - 15
    else:
        min_coverage = args.min_cov_display

    df = pd.read_csv(file, sep="\t")
    df = df[100 * df['SpeciesCount']/total_genomes >= min_coverage]
    traces = []
    trace = go.Scatter(
        x=[(c['SpeciesCount']/total_genomes)*100 for i, c in df.iterrows()],
        y=[(c['nb_single_copy']/total_genomes)*100 for i, c in df.iterrows()],
        text=[f"{c['GroupName']}<br>In {round(c['SpeciesCount']/total_genomes*100,2)}% sp.<br> {round(c['nb_single_copy']/total_genomes*100,2)}% sp in single copy" for i, c in df.iterrows()],
        mode='markers',
        name='All COGs', hoverinfo='text',
        marker=dict(
            size=9,
            color='rgba(60,60,75,0.9)',
        )
    )
    traces.append(trace)

    df_select = df[(100*df['nb_single_copy']/total_genomes >= cog_single_copy_prct)
                   & (100*df['SpeciesCount']/total_genomes >= cog_prct)]
    # COGS that are > threshold of single copy
    trace = go.Scatter(
        x=[c['SpeciesCount']/total_genomes*100 for i, c in df_select.iterrows()],
        y=[c['nb_single_copy']/total_genomes*100
            for i, c in df_select.iterrows()],
        mode='markers',
        name=f'COGs in > {cog_prct}% species<br>with single copy >{cog_single_copy_prct}%',
        text=[f"{c['GroupName']}<br>In {round(c['SpeciesCount']/total_genomes*100,2)}% sp.<br>In single copy {round(c['nb_single_copy']/total_genomes*100,2)}% sp." for i, c in df.iterrows()],
        hoverinfo='text',
        marker=dict(
            color='rgba(60,60,75,0.9)',
            size=9,
            line=dict(
                color='rgb(230,10,50,0.2)',
                width=2
            )
        )
    )
    traces.append(trace)

    layout = go.Layout(yaxis=dict(title='% of species with the COG in single copy '),
                       xaxis=dict(title='% of species with the COG'))

    fig = go.Figure(data=traces, layout=layout)
    # pio.write_html(fig, file='selected_OGs.html', auto_open=True)
    fig.write_html(file='selected_cogs.html')


if __name__ == '__main__':
    main()
