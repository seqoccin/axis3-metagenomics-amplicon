#!/bin/bash
set -e

cd "$( dirname "${BASH_SOURCE[0]}" )"

TEST_RESULT=$(python ../../publicdb/eggnog.py)

if [ "$TEST_RESULT" != "" ]
then
    echo -e "publicdb/eggnog.py: \e[91mTEST HAS FAILED\e[0m"
    echo $TEST_RESULT
else
   echo -e "publicdb/eggnog.py: \e[92mTEST PASSED!\e[0m"
fi


TEST_RESULT=$(python ../../publicdb/ncbi.py)

if [ "$TEST_RESULT" != "" ]
then
    echo -e "publicdb/ncbi.py: \e[91mTEST HAS FAILED\e[0m"
    echo $TEST_RESULT
else
   echo -e "publicdb/ncbi.py: \e[92mTEST PASSED!\e[0m"
fi
