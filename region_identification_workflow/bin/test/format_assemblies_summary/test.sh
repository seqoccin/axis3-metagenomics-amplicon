#!/bin/bash
set -e

cd "$( dirname "${BASH_SOURCE[0]}" )"

python ../../format_assemblies_summary.py -t 2157 --ncbi_taxdump head1k_new_taxdump.tar.gz head5k_assembly_summary_refseq.txt -o output_to_test_assemblies_with_tax.tsv

DIFF=$(diff verified_output_assemblies_with_tax.tsv output_to_test_assemblies_with_tax.tsv)

if [ "$DIFF" != "" ]
then
    echo -e "format_assemblies_summary.py: \e[91mTEST HAS FAILED\e[0m The two output files are different."
else
   echo -e "format_assemblies_summary.py: \e[92mTEST HAS PASSED!\e[0m The two output files are identical."
fi
