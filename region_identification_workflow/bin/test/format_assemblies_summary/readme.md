# Test
## test construction
The purpose of this directory is to test the script `format_assemblies_summary.py`

The file `head5k_assembly_summary_refseq.txt` is a sample of the file `assembly_summary_refseq.txt` downloaded on the 2019-10-23 with the link ftp://ftp.ncbi.nlm.nih.gov/genomes/refseq/assembly_summary_refseq.txt

it has been created using the following command:
```
wget ftp://ftp.ncbi.nlm.nih.gov/genomes/refseq/assembly_summary_refseq.txt
head -n5000 assembly_summary_refseq.txt > head5k_assembly_summary_refseq.txt
```


The archive `head1k_new_taxdump.tar.gz` is a sample of the archive new_taxdump.tar.gz downloaded at ftp://ftp.ncbi.nlm.nih.gov/pub/taxonomy/new_taxdump/new_taxdump.tar.gz
Only the two files rankedlineage.dmp and taxidlineage.dmp are used by the script so only them are kept and subsample.

it has been created using the following command:
```
wget ftp://ftp.ncbi.nlm.nih.gov/pub/taxonomy/new_taxdump/new_taxdump.tar.gz

tar -xzf new_taxdump.tar.gz
head  -n1000 rankedlineage.dmp > head1k_rankedlineage.dmp
head  -n1000 taxidlineage.dmp > head1k_taxidlineage.dmp

mv head1k_rankedlineage.dmp rankedlineage.dmp
mv head1k_taxidlineage.dmp taxidlineage.dmp

tar -czvf head1k_new_taxdump.tar.gz rankedlineage.dmp  taxidlineage.dmp
```


## test execution

To test the script :

be sure to have python 3 as default python. (`python -V`)

```
bash test.sh
```
