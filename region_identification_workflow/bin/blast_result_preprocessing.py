#!/usr/bin/env python3


"""

"""

# Metadata
__author__ = 'Mainguy Jean - Plateforme bioinformatique Toulouse'
__copyright__ = 'Copyright (C) 2019 INRA'
__license__ = 'GNU General Public License'
__version__ = '0.1'
__email__ = 'support.bioinfo.genotoul@inra.fr'
__status__ = 'dev'


import csv
from argparse import ArgumentParser, ArgumentDefaultsHelpFormatter
import os
#import blast_analysis_fct as analysis


def get_best_hsp(hsps):
    return sorted(hsps, key=lambda t: float(t['bitscore']), reverse=True)[0]


def iter_preprocessed_blast_result(blast_result_file, outfmt):
    """
    Preprocess of blast result
    """
    hsps = []
    tuple_ids = None

    with open(blast_result_file) as fl:
        reader = csv.DictReader(fl, delimiter='\t', fieldnames=outfmt)
        for d in reader:
            if d['qseqid'] == d['sseqid']:
                continue

            if int(d['qlen']) < int(d['slen']):
                continue
            elif int(d['qlen']) == int(d['slen']) and d['qseqid'] < d['sseqid']:
                continue

            if not tuple_ids:
                # first line: tuple_ids is not initialize
                tuple_ids = (d['qseqid'], d['sseqid'])

            # Current line is another couple of sequence
            # Best hsp of previous couple is determined and yield
            if tuple_ids and tuple_ids != (d['qseqid'], d['sseqid']):
                hsp = get_best_hsp(hsps)
                yield hsp

                # new tuple ids and hsp list
                tuple_ids = (d['qseqid'], d['sseqid'])
                hsps = []

            hsps.append(d)
    # yield last hsp
    hsp = get_best_hsp(hsps)
    yield hsp


def parse_arguments():
    parser = ArgumentParser(description="...",
                            formatter_class=ArgumentDefaultsHelpFormatter)
    parser.add_argument("blast_result", type=str,
                        help="Blast result file or directory containing only blast result file to analyse")
    parser.add_argument("--output_dir", type=str, default='preprocessed_blast_results',
                        help="Blast result file.")

    parser.add_argument("--outfmt", type=str, default='qseqid qstart qend qlen sseqid sstart send slen length pident qcovhsp qcovs evalue bitscore',
                        help="output format of the blast")

    args = parser.parse_args()
    return args


def main():
    args = parse_arguments()
    outfmt_string = args.outfmt
    outfmt = outfmt_string.split(' ')

    output_dir = args.output_dir

    os.makedirs(output_dir, exist_ok=True)

    blast_result = args.blast_result
    if os.path.isdir(blast_result):
        blast_result_files = [os.path.join(blast_result, file) for file in os.listdir(blast_result)]
    elif os.path.isfile(blast_result):
        blast_result_files = [blast_result]
    else:
        raise ValueError(f'The argument blast_results ({blast_results}) provided is not a file or a directory containing blast result')

    for blast_result_file in blast_result_files:
        output_file_name = f'preprocessed_{os.path.basename(blast_result_file)}'
        filtered_blast_file = os.path.join(output_dir, output_file_name)

        with open(filtered_blast_file, 'w') as out:
            writer = csv.DictWriter(out, fieldnames=outfmt, delimiter='\t')

            for best_hsp in iter_preprocessed_blast_result(blast_result_file, outfmt):
                writer.writerow(best_hsp)


if __name__ == '__main__':
    main()
