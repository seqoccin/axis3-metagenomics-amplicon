#!/usr/bin/env python3

"""
Description

:Example:
python template.py -v
"""

# Metadata
__author__ = 'Mainguy Jean - Plateforme bioinformatique Toulouse'
__copyright__ = 'Copyright (C) 2019 INRA'
__license__ = 'GNU General Public License'
__version__ = '0.1'
__email__ = 'support.bioinfo.genotoul@inra.fr'
__status__ = 'dev'


from argparse import ArgumentParser, ArgumentDefaultsHelpFormatter
import logging
import csv
import regions_analysis_fct
from dna_features_viewer import GraphicFeature, GraphicRecord
import matplotlib.pyplot as plt
import numpy as np
from collections import defaultdict
import plotly.figure_factory as ff
import plotly.graph_objects as go
import re


def visualize_genomic_region(region_elements, ax=None):
    features = []
    current_po = 0
    for el in region_elements:
        el['start'] = current_po + 1
        current_po += el['length']
        el['end'] = current_po

    for el in region_elements:
        feat = GraphicFeature(start=el['start'], end=el['end'], strand=el['strand'],
                              color=el['color'], label=f'{el["name"]}\n(~{el["length"]}nt)')

        features.append(feat)

    feat = GraphicFeature(start=region_elements[0]['start'], end=region_elements[-1]['end'], strand=0,
                          color='w', label=f'region: {region_elements[0]["name"]}-{region_elements[-1]["name"]}\n(~{current_po}nt)')
    # features.append(feat)
    record = GraphicRecord(sequence_length=current_po+100, first_index=0, features=features)
    return record.plot(ax=ax, figure_width=10, annotate_inline=False, with_ruler=True)


def display_region_schematic_organisation(region_structural_info, colors=["violet", "#341677", "#ff6363", 'grey'], ax=None):
    cog1 = region_structural_info['cog1']
    cog2 = region_structural_info['cog2']
    strand1 = region_structural_info['strand1']
    strand2 = region_structural_info['strand2']
    gene1 = region_structural_info['gene1']
    gene2 = region_structural_info['gene2']

    length1 = region_structural_info['cog1_median']
    length2 = region_structural_info['cog2_median']
    length_intergenic = region_structural_info['intergenic_region_median']

    name1 = f'{gene1} ({cog1})' if gene1 and cog1 != gene1 else cog1
    name2 = f'{gene2} ({cog2})'if gene2 and cog2 != cog2 else cog2

    gene1 = {'length': length1, 'name': name1, 'strand': int(strand1+'1'), 'color': colors[1]}
    gene2 = {'length': length2, 'name': name2, 'strand': int(strand2+'1'), 'color': colors[2]}
    inter_G = {'length': length_intergenic,
               'name': 'intergenic_region', 'strand': 0, 'color': colors[3]}
    # print(gene1)
    # print(gene2)
    if name1 == name2 and length1 == length1 and strand1 == strand1 and length1 == -length_intergenic:
        logging.warning(
            f'Region seems to be made of the same gene {name1}...Plot only the first gene.')
        list_to_plot = [gene1]
    else:
        list_to_plot = [gene1, inter_G, gene2]

    return visualize_genomic_region(list_to_plot, ax)


def plot_region_general_info(region_structural_infos, colors=["violet", "#341677", "#ff6363", 'grey']):
    nb_sub_plot = len(region_structural_infos)
    mpl_fig, axes = plt.subplots(nb_sub_plot, 1, figsize=(8, 3*nb_sub_plot), sharex=True)
    plt.subplots_adjust(wspace=1, hspace=1)

    # display(Markdown(f'## Region length distribution'))

    # print(pair_example)
    region_structural_infos_sorted = sorted(
        region_structural_infos, key=lambda k: k['cog1_median']+k['intergenic_region_median']+k['cog2_median'])

    for i, region_structural_info in enumerate(region_structural_infos_sorted):
        region_name = region_structural_info['region_name'] if not region_structural_info[
            'is_single_gene'] else region_structural_info['region_name'].split('-')[0]

        try:
            ax = axes[i]
        except TypeError:
            ax = axes
        ax.title.set_text(region_name)
        ax.title.set_fontsize(15)

        display_region_schematic_organisation(region_structural_info, colors=colors, ax=ax)

    return mpl_fig


def get_gene_name(cog_id, cog_to_gene):
    try:
        return cog_to_gene[cog_id]
    except KeyError:
        logging.warning(f'No Gene name found for {cog_id}')
        return cog_id


def get_region_structural_info(pair_details, cog_to_gene=None):
    def get_length(start, end): return max(int(start), int(end)) - min(int(start), int(end)) + 1
    region_structural_info = defaultdict(list)

    pair = pair_details[0]
    cog1_inital, cog2_inital = pair['cog_pair'].split('|')

    strand1, strand2 = pair['strands'].split('|')

    if strand1 == strand2 and strand1 == '-':
        strand1 = '+'
        strand2 = '+'
        cog1_inital, cog2_inital = cog2_inital, cog1_inital

    region_structural_info['cog_pair'] = pair['cog_pair']
    region_structural_info['cog1'] = cog1_inital
    region_structural_info['cog2'] = cog2_inital

    region_structural_info['strand1'] = strand1
    region_structural_info['strand2'] = strand2
    region_name = f'{cog1_inital}-{cog2_inital}'
    name1 = cog1_inital
    name2 = cog2_inital

    if cog_to_gene:
        gene1 = get_gene_name(cog1_inital, cog_to_gene)
        gene2 = get_gene_name(cog2_inital, cog_to_gene)
        region_structural_info['gene1'] = gene1
        region_structural_info['gene2'] = gene2

        region_name = f'{gene1}-{gene2} ({cog1_inital}-{cog2_inital})' if gene1 != cog1_inital or gene2 != cog2_inital else f"{cog1_inital}-{cog2_inital}"
        name1 = f'{gene1} ({cog1_inital})' if gene1 != cog1_inital else cog1_inital
        name2 = f'{gene2} ({cog2_inital})'if gene2 != cog2_inital else cog2_inital

    length_elements = defaultdict(list)
    flag_single_gene = True
    for pair in pair_details:
        cogA, cogB = pair['cog_pair'].split('|')

        cogA_coord, cogB_coord = [tuple(coord_string.split('-'))
                                  for coord_string in pair['positions'].split('|')]
        if cogA == cog1_inital and cogB == cog2_inital:
            cog1 = cogA
            cog1_coord = cogA_coord
            cog2 = cogB
            cog2_coord = cogB_coord
        else:
            cog2 = cogA
            cog2_coord = cogA_coord
            cog1 = cogB
            cog1_coord = cogB_coord

        cog1_length = get_length(cog1_coord[0], cog1_coord[1])
        cog2_length = get_length(cog2_coord[0], cog2_coord[1])

        intergenique_length = int(pair['distance']) - (cog1_length + cog2_length)

        length_elements['intergenic_region'].append(intergenique_length)
        length_elements['region'].append(int(pair['distance']))

        length_elements[cog1].append(cog1_length)
        length_elements[cog2].append(cog2_length)

        if cog1_coord != cog2_coord:
            flag_single_gene = False
    region_structural_info['cog1_median'] = np.median(length_elements[cog1])
    region_structural_info['cog2_median'] = np.median(length_elements[cog2])
    region_structural_info['intergenic_region_median'] = np.median(
        length_elements['intergenic_region'])
    region_structural_info['region_name'] = region_name

    length_elements_clean = {region_name: length_elements['region'],
                             name1: length_elements[cog1],
                             name2: length_elements[cog2],
                             'intergenic_region': length_elements['intergenic_region']}

    region_structural_info['length_elements'] = length_elements_clean

    region_structural_info['is_single_gene'] = flag_single_gene
    return region_structural_info


def plot_length_distribution(pair_summaries, colors=["violet", "#341677", "#ff6363", 'grey'], is_single_gene=False):
    hist_data_region = []
    group_labels_region = []
    figs_dict = {}
    for i, pair_summary in enumerate(pair_summaries):
        # pairs_main_orga_len_filter = pair_summary['pairs_main_orga_post_len_filtering']
        region_name = pair_summary['cog_pair'].replace('|', '-')
        length_elements = pair_summary['length_elements']

        hist_data = []
        group_labels = []
        colors_to_apply = colors
        if is_single_gene:
            logging.warning(f'Region to plot is a single gene: {region_name} Plot only ')
            assert len(set(region_name.split('-'))) == 1, f'region is not a single gene {region_name}..'
            cog1 = region_name.split('-')[0]
            length_elements = {cog1: pair_summary['length_elements'][region_name]}
            colors_to_apply = [colors[1]]
            region_name = cog1

        # region_length = [int(p['distance']) for p in pairs_main_orga_len_filter]

        # group_labels.append(region_name)
        # hist_data.append(region_length)

        # hist_data_region.append(region_length)
        # group_labels_region.append(region_name)

        for element_name, lengths in length_elements.items():

            group_labels.append(f'{element_name}')
            hist_data.append(lengths)

            # logging.info(f'lengths of {element_name}: {lengths}')
            if '-' in element_name:  # if element is the whole region
                hist_data_region.append(lengths)
                group_labels_region.append(element_name)

        # logging.info(f'len hist data {len(hist_data)}')
        # logging.info(f'len group_labels {len(group_labels)}')
        # logging.info(f'hist_data={hist_data}')
        # logging.info(f'group_labels={group_labels}')
        fig = ff.create_distplot(hist_data[::-1], group_labels[::-1],
                                 show_hist=False, colors=colors_to_apply[::-1])
        fig.update_layout(title_text=region_name, xaxis=go.layout.XAxis(title=go.layout.xaxis.Title(
            text="Length (pb)")),)

        # fig.show()
        figs_dict[region_name] = fig

    if len(pair_summaries) > 1:
        fig = ff.create_distplot(hist_data_region, group_labels_region, show_hist=False)
        fig.update_layout(title_text='Regions Length distribution',)
        # fig.show()
        concat_name = '_'.join(figs_dict.keys()) + '_comparison'
        figs_dict[concat_name] = fig
    return figs_dict


def parse_arguments():
    parser = ArgumentParser(description="...",
                            formatter_class=ArgumentDefaultsHelpFormatter)
    parser.add_argument("region_stat_details",
                        help="tsv file with regions coordinates in all the assemblies")
    parser.add_argument("--cog_annotation", default=None,
                        help="Cog annotation tsv file with at least column GroupName and cog_gene_name")
    parser.add_argument("--outfile_ids", default='assembly_ids_covered_by_region.txt',
                        help="assembly id covered by the regions to generate krona ")

    parser.add_argument("-v", "--verbose", help="increase output verbosity",
                        action="store_true")

    args = parser.parse_args()
    return args


def main():

    args = parse_arguments()

    if args.verbose:
        logging.basicConfig(format="%(levelname)s: %(message)s", level=logging.INFO)
        logging.info('Mode verbose ON')

    else:
        logging.basicConfig(format="%(levelname)s: %(message)s")

    region_stat_details_file = args.region_stat_details
    cog_annotation_file = args.cog_annotation
    outfile_ids = args.outfile_ids
    length_cutoff = 10000

    if cog_annotation_file:
        logging.info('cog_annotation_file has been provided.. gene name of cog will be display')
        with open(cog_annotation_file) as tsvfile:
            reader = csv.DictReader(tsvfile, delimiter='\t')
            cog_to_gene = {d['GroupName']: d['cog_gene_name'] for d in reader}
    else:
        logging.info('cog_annotation_file has not been provided')
        cog_to_gene = None

    colors = ["violet", "#341677", "#ff6363", 'grey']
    # Parse region_stat_details_file
    pair_summary, pair_details = regions_analysis_fct.analyse_cog_pair(
        region_stat_details_file, length_cutoff)

    # PLOT schematic_organisation of the region
    region_structural_info = get_region_structural_info(
        pair_details['pairs_main_orga_post_len_filtering'], cog_to_gene)

    region = region_structural_info['cog_pair']
    if region.split('|')[0] == region.split('|')[1]:
        logging.warning(f'region {region} is made of twice the same gene {region.split("|")[1]}.')

    region_plot = plot_region_general_info([region_structural_info])

    file_name = f"{region.replace('|' ,'-')}_schematic_organisation.svg"
    file_name = 'schematic_organisation.svg'
    logging.info(f'writing {file_name}')
    region_plot.savefig(file_name)

    # PLOT length distribution
    pair_details.update(region_structural_info)
    figs_dict = plot_length_distribution(
        [pair_details], colors, is_single_gene=region_structural_info['is_single_gene'])
    for name, fig in figs_dict.items():
        logging.info(f'writing length_distribution.html ...')
        fig.write_html(f'length_distribution.html')

    # Write assembly id covered by the region to then be displayed in a krona

    logging.info(f'writing {outfile_ids} ...')
    capture_assemby_name = re.compile("^([A-Z]{3}_\d+.\d+)")
    with open(f'{outfile_ids}', 'w') as fl:
        for p in pair_details['pairs_main_orga_post_len_filtering']:
            # logging.info(p['genome'])
            assembly_id = capture_assemby_name.search(p['genome']).group(1)
            fl.write(assembly_id+'\n')


if __name__ == '__main__':
    main()
