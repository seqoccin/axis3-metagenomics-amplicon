#!/usr/bin/env python3

"""
Format and filter assembly file.

Take as input the refseq assembly summary, the ncbi taxonomy called ncbi_taxdump and a taxid.
The script format refseq assembly summary to keep only few columns and add taxonomy to the assembly.
On top of that the script filter out assembly that does not belong the taxid.
refseq assembly summary: ftp://ftp.ncbi.nlm.nih.gov/genomes/refseq/assembly_summary_refseq.txt
ncbi_taxdump: ftp://ftp.ncbi.nlm.nih.gov/pub/taxonomy/new_taxdump/new_taxdump.tar.gz
"""

# Metadata
__author__ = 'Mainguy Jean - Plateforme bioinformatique Toulouse'
__copyright__ = 'Copyright (C) 2019 INRA'
__license__ = 'GNU General Public License'
__version__ = '0.1'
__email__ = 'support.bioinfo.genotoul@inra.fr'
__status__ = 'dev'


import csv
from argparse import ArgumentParser, ArgumentDefaultsHelpFormatter

from publicdb import ncbi
import tarfile
import logging


def parse_arguments():
    """Parse script arguments."""
    parser = ArgumentParser(description="Format and filter assembly file.",
                            formatter_class=ArgumentDefaultsHelpFormatter)

    parser.add_argument("assembly_file", type=str,
                        help="ncbi assembly summary file with info on the assemblies. can be download at ftp://ftp.ncbi.nlm.nih.gov/genomes/refseq/assembly_summary_refseq.txt")
    parser.add_argument("-t", "--tax_levels", default=['2'], nargs='+',
                        help="Taxonomic levels under investigation: 2 for Bacteria")
    parser.add_argument("-o", "--output", type=str, default='assemblies_with_tax.tsv',
                        help="output file : final assembly selection")
    parser.add_argument("--ncbi_taxdump", type=str, default='new_taxdump.tar.gz',
                        help="tar gz of ncbi taxonomy. can be downloaded at ftp://ftp.ncbi.nlm.nih.gov/pub/taxonomy/new_taxdump/new_taxdump.tar.gz ")
    parser.add_argument("-v", "--verbose", help="increase output verbosity",
                        action="store_true")

    args = parser.parse_args()
    return args


def get_all_taxids_of_tax_level(tax_levels, taxidlineage):
    taxids = set()
    with open(taxidlineage) as fl:
        for line in fl:
            taxids_lineage = line.strip().split('|')[1].strip().split(' ')
            if any((True for tax_level in tax_levels if tax_level in taxids_lineage)):
                taxids.add(line.split('|')[0].strip())
    return taxids


def main():

    args = parse_arguments()

    if args.verbose:
        logging.basicConfig(format="%(levelname)s: %(message)s", level=logging.DEBUG)
        logging.info('Mode verbose ON')

    else:
        logging.basicConfig(format="%(levelname)s: %(message)s")

    raw_assembly_file = args.assembly_file
    output_file = args.output
    ncbi_taxdump = args.ncbi_taxdump
    tax_levels = [tax_lvl.strip() for tax_lvl in args.tax_levels]

    taxidlineage = 'taxidlineage.dmp'
    rankedlineage = 'rankedlineage.dmp'

    tsv_reader = ncbi.read_assembly_summary(raw_assembly_file)
    logging.info(tsv_reader)

    # Handle taxonomy information
    tar_tax_dump = tarfile.open(name=ncbi_taxdump)

    logging.info(f'Extracting {taxidlineage} and {rankedlineage} from {ncbi_taxdump}')
    tar_tax_dump.extract(taxidlineage)
    tar_tax_dump.extract(rankedlineage)

    # Get all taxid of the tax_levels
    taxids_of_tax_lvl = get_all_taxids_of_tax_level(tax_levels, taxidlineage)
    logging.info(f"{len(taxids_of_tax_lvl)} taxids belong to {tax_levels}")

    # get_taxid_ranked_lineage
    taxid_ranked_lineage = ncbi.get_taxid_ranked_lineage(rankedlineage)

    # header to keep
    header_to_keep = ['assembly_accession', 'refseq_category',
                      'taxid', 'species_taxid',
                      'organism_name', 'version_status',
                      'assembly_level', 'genome_rep',
                      'ftp_path', 'excluded_from_refseq', 'taxonomy']

    with open(output_file, "w") as csvfile:
        writer = csv.DictWriter(csvfile, fieldnames=header_to_keep, delimiter='\t')
        writer.writeheader()

        for i, d in enumerate(tsv_reader):
            if d['taxid'] in taxids_of_tax_lvl:
                d['taxonomy'] = ';'.join(taxid_ranked_lineage[d['taxid']].values())
                d = {h: d[h] for h in header_to_keep}
                writer.writerow(d)


if __name__ == '__main__':
    main()
