#!/bin/bash
set -e

singularity_img="../env/region_identification_env.sif"
tax_lvl=0


module load system/singularity-3.0.1

module load system/Anaconda3-5.2.0;
source /usr/local/bioinfo/src/Anaconda/Anaconda3-5.2.0/etc/profile.d/conda.sh
conda activate nextflow_cluster

python ../generate_synthetic_data.py --grp_size 3 -v --nb_assembly 20 --fill_strategy random --outdir 'synthetic_data'


refseq_path="synthetic_data/ncbi_data/0000-00-00_refseq_summary/"
eggNOG_data_dir="$PWD/synthetic_data/eggnog_data/"
ncbi_genomes_dir="$PWD/synthetic_data/ncbi_data"
result_dir=$PWD

../nextflow run ../main.nf -profile singularity -with-singularity ../env/region_identification_env.sif \
                                                      -with-report $result_dir/report.html \
                                                      -with-trace $result_dir/trace \
                                                      -with-timeline $result_dir/timeline.html \
                                                      -with-dag $result_dir/flowchart.html \
                                                      --tax_level $tax_lvl \
                                                      --refseq_path $refseq_path\
                                                      -w 'work_synthetic_data' \
                                                      --eggNOG_data_dir $eggNOG_data_dir \
                                                      --ncbi_genomes_dir $ncbi_genomes_dir

bash ../synthetic_expected_result_test.sh

#
# conda activate pipeline_main
# nextflow run ../main.nf -profile local_machine  --tax_level 0 \
#                                                       --refseq_path "$PWD/synthetic_data/ncbi_data/0000-00-00_refseq_summary/"\
#                                                       -w 'work_synthetic_data' \
#                                                       --eggNOG_data_dir "$PWD/synthetic_data/eggnog_data/" \
#                                                       --ncbi_genomes_dir "$PWD/synthetic_data/ncbi_data" -resume -with-report -with-trace
