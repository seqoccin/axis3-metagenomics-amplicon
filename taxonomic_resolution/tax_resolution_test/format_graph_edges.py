#!/usr/bin/env python3

"""
Description

:Example:
python template.py -v
"""

# Metadata
__author__ = 'Mainguy Jean - Plateforme bioinformatique Toulouse'
__copyright__ = 'Copyright (C) 2019 INRA'
__license__ = 'GNU General Public License'
__version__ = '0.1'
__email__ = 'support.bioinfo.genotoul@inra.fr'
__status__ = 'dev'


from argparse import ArgumentParser, ArgumentDefaultsHelpFormatter
import logging
import csv
import os
from random import shuffle


def format_edges(reduced_edges, taxon1, taxon2, ident, old_edge_line):
    node_couple = tuple(sorted([taxon1, taxon2]))

    if node_couple not in reduced_edges:
        reduced_edges[node_couple] = {"node1": taxon1, 'node2': taxon2,
                                      'ident': ident, 'seq_ids': old_edge_line, 'nb_seq': 1}
    elif reduced_edges[node_couple]["ident"] > ident:
        reduced_edges[node_couple]['ident'] = ident
        reduced_edges[node_couple]['seq_ids'] += f';{old_edge_line}'
        reduced_edges[node_couple]['nb_seq'] += 1
    else:
        reduced_edges[node_couple]['seq_ids'] += f';{old_edge_line}'
        reduced_edges[node_couple]['nb_seq'] += 1
        logging.debug(f'{old_edge_line} taxonomic_resolutions {taxon2} and {taxon2} already recorded and ident is >=: {node_couple}-> {reduced_edges[node_couple]}')


def parse_arguments():
    """Parse script arguments."""
    parser = ArgumentParser(description="...",
                            formatter_class=ArgumentDefaultsHelpFormatter)
    parser.add_argument(
        "-e", "--edges", help="file of edges with node1, node2 and weigth", required=True)
    parser.add_argument("-n", "--nodes", help="file with all nodes", required=True)
    parser.add_argument("--assembly_summary", help="file with assembly annotation", required=True)
    parser.add_argument(
        "-o", "--outdir", help="Dire where formatted files graoh are written", required=True)
    parser.add_argument("-v", "--verbose", help="increase output verbosity",
                        action="store_true")
    parser.add_argument("-d", "--debug", help="increase output verbosity",
                        action="store_true")
    parser.add_argument("-i", "--min_ident",
                        help="minimal identity to consider", default=95, type=int)

    args = parser.parse_args()
    return args


def main():

    args = parse_arguments()

    # logging.basicConfig(format="%(levelname)s: %(message)s")
    if args.verbose:
        logging.basicConfig(format="%(levelname)s: %(message)s", level=logging.INFO)
        logging.info('Mode verbose ON')

    if args.debug:
        logging.basicConfig(format="%(levelname)s: %(message)s", level=logging.DEBUG)
        logging.info('Mode debug ON')

    node_file = args.nodes
    edge_file = args.edges
    assembly_summary = args.assembly_summary
    outdir = args.outdir
    min_ident = args.min_ident

    edges_at_assembly_lvl_file = os.path.join(outdir, 'edges_at_assembly_lvl.tsv')
    edges_at_species_lvl_file = os.path.join(outdir, 'edges_at_species_lvl.tsv')
    edges_at_seq_lvl_file = os.path.join(outdir, 'edges_at_seq_lvl.tsv')
    assembly_summary_formatted = os.path.join(outdir, f'formatted_{os.path.basename(assembly_summary)}')
    logging.info(f'Reading assembly_summary {assembly_summary}')
    with open(assembly_summary) as fl:
        reader = csv.DictReader(fl, delimiter='\t')
        assembly_to_sp = {d['assembly_accession']: d['species_taxid'] for d in reader}
    logging.info(f'{len(assembly_to_sp)} assembly found in assembly_summary {assembly_summary} for {len(set(list(assembly_to_sp.values())))} species')

    logging.info(f'Reading node_file {node_file}')
    with open(node_file) as fl:
        nodes = [l.strip() for l in fl]
    logging.info(f'{len(nodes)} nodes in node_file')
    assemblies = {n.split('|')[0] for n in nodes}
    species = {assembly_to_sp[a] for a in assemblies}

    if len(set(nodes)) != len(nodes):
        logging.critical("Duplicated nodes in nodes file !")

    species_edges = {}
    assembly_edges = {}

    logging.info(f'Reading edge_file {edge_file}')
    with open(edge_file) as fl:
        for i, l in enumerate(fl):
            if i % 5000 == 0:
                logging.info(f'{i} edges have been checked for the moment')
                logging.info(f'{len(assembly_edges)} assembly_edges ')
                logging.info(f'{len(species_edges)} species_edges ')
            logging.debug(l)
            node1, node2, ident = l.rstrip().split('\t')
            ident = float(ident)
            if ident < min_ident:
                continue
            if node1 == node2:
                continue
            if node1 not in nodes or node2 not in nodes:
                logging.critical('Node in edges are not found in node list')

            old_edge_line = ' '.join(l.rstrip().split('\t'))

            assembly1 = node1.split('|')[0]
            assembly2 = node2.split('|')[0]
            if assembly1 != assembly2:
                format_edges(assembly_edges, assembly1, assembly2, ident, old_edge_line)

            sp1 = assembly_to_sp[assembly1]
            sp2 = assembly_to_sp[assembly2]
            if sp1 != sp2:
                format_edges(species_edges, sp1, sp2, ident, old_edge_line)

    # Check assembly that are not connected to any other assembly
    assembly_list_in_edges = {couple_a[0] for couple_a in assembly_edges} | {
        couple_a[1] for couple_a in assembly_edges}
    assembly_not_in_edges = assemblies - assembly_list_in_edges
    logging.info(f'assemblies : {len(assemblies)}')
    logging.info(f'assembly_list_in_edges : {len(assembly_list_in_edges)}')
    logging.info(f'assembly_not_in_edges : {len(assembly_not_in_edges)}')

    species_list_in_edges = {couple_a[0] for couple_a in species_edges} | {
        couple_a[1] for couple_a in species_edges}
    species_not_in_edges = species - species_list_in_edges

    logging.info(f'species : {len(species)}')
    logging.info(f'species_list_in_edges : {len(species_list_in_edges)}')
    logging.info(f'species_not_in_edges : {len(species_not_in_edges)}')

    lonely_assemblies = {}
    lonely_species = {}

    for node in nodes:
        assembly = node.split('|')[0]
        if assembly not in assembly_list_in_edges:
            logging.debug(assembly)

            if assembly in lonely_assemblies:
                lonely_assemblies[assembly]['seq_ids'] += f';{node}'
                lonely_assemblies[assembly]['nb_seq'] += 1
            else:
                lonely_assemblies[assembly] = {"node1": assembly,
                                               'node2': '', 'ident': '', 'seq_ids': node, 'nb_seq': 1}

        species = assembly_to_sp[assembly]
        if species not in species_list_in_edges:

            if species in lonely_species:
                lonely_species[species]['seq_ids'] += f';{node}'
                lonely_species[species]['nb_seq'] += 1
            else:
                lonely_species[species] = {"node1": species, 'node2': '',
                                           'ident': '', 'seq_ids': node, 'nb_seq': 1}
                lonely_species[species]['nb_seq'] += 1

    assert len(lonely_assemblies) == len(assembly_not_in_edges), f"lonely_assemblies {len(lonely_assemblies)} and assembly_not_in_edges {len(assembly_not_in_edges)}  have different size"
    assert len(lonely_species) == len(species_not_in_edges)

    with open(edges_at_assembly_lvl_file, 'w') as fl:
        writer = csv.DictWriter(
            fl, fieldnames=["node1", 'node2', 'ident', 'seq_ids', 'nb_seq'], delimiter='\t')
        writer.writeheader()
        for dict_info in assembly_edges.values():
            writer.writerow(dict_info)
        for dict_info in lonely_assemblies.values():
            writer.writerow(dict_info)

    with open(edges_at_species_lvl_file, 'w') as fl:
        writer = csv.DictWriter(
            fl, fieldnames=["node1", 'node2', 'ident', 'seq_ids', 'nb_seq'], delimiter='\t')
        writer.writeheader()
        for dict_info in species_edges.values():
            writer.writerow(dict_info)
        for dict_info in lonely_species.values():
            # writer.writerow(dict_info)
            fl.write(dict_info['node1']+'\n')

    with open(edges_at_seq_lvl_file, 'w') as flout, open(edge_file) as flin:
        writer = csv.DictWriter(
            flout, fieldnames=["node1", 'node2', 'ident', 'seq_ids'], delimiter='\t')
        writer.writeheader()
        nodes_in_edges = set()
        for l in flin:
            node1, node2, ident = l.rstrip().split('\t')
            node_d = {"node1": node1, 'node2': node2, 'ident': ident, 'seq_ids': ''}
            writer.writerow(node_d)
            nodes_in_edges.add(node1)
            nodes_in_edges.add(node2)

        nodes_not_in_edges = set(nodes) - nodes_in_edges
        for lonely_node in nodes_not_in_edges:

            writer.writerow({"node1": lonely_node, 'node2': lonely_node,
                             'ident': '', 'seq_ids': ''})

    sp_to_color = {}
    colors_assemblies = list(range(len(assembly_to_sp)))
    shuffle(colors_assemblies)
    colors_species = list(range(len(assembly_to_sp)))
    shuffle(colors_species)
    with open(assembly_summary) as flin, open(assembly_summary_formatted, 'w') as flout:
        reader = csv.DictReader(flin, delimiter='\t')
        headers = ["assembly_accession",
                   "refseq_category",
                   "taxid",
                   "species_taxid",
                   "organism_name",
                   "assembly_level",
                   'color_assemblies',
                   'color_species',
                   'taxonomy']

        writer = csv.DictWriter(flout, fieldnames=headers, delimiter='\t')
        writer.writeheader()
        for d in reader:
            d['color_assemblies'] = colors_assemblies.pop()

            if d['species_taxid'] not in sp_to_color:
                sp_to_color[d['species_taxid']] = colors_species.pop()

            d['color_species'] = sp_to_color[d['species_taxid']]
            writer.writerow({col: d[col] for col in headers})


if __name__ == '__main__':
    main()
