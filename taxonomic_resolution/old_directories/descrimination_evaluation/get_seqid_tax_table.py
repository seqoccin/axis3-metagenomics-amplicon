
import os
import sys


module_path = os.path.abspath("scripts/")
sys.path.append(module_path)
module_path = os.path.abspath("descrimination_evaluation/")
sys.path.append(module_path)


import tools
# from blast_analysis_fct import get_node_to_lineage_dict, get_taxid_ranked_lineage, get_taxid_ranked_lineage
import blast_analysis_fct


def get_taxid_ranked_lineage_old(file):
    """
    # Header from
    # ftp://ftp.ncbi.nlm.nih.gov/pub/taxonomy/new_taxdump/taxdump_readme.txt

    """
    ranks = ['tax_id', 'tax_name', 'species', 'genus', 'family', 'order',
             'class', 'phylum', 'kingdom', 'superkingdom']
    key_field = 'tax_id'
    taxid_ranked = {}
    with open(file) as fl:
        for l in fl:
            splited_line = l.rstrip().replace('\t|', '').split('\t')
            line_dict = {rank: name for rank, name in zip(ranks, splited_line)}
            taxid = line_dict.pop('tax_id')
            taxid_ranked[taxid] = line_dict

            name_ahead = taxid
            for rank, name in line_dict.items():
                # manage when taxonomy is empty at some rank
                if name == '':
                    name = name_ahead
                    if name_ahead == line_dict['tax_name']:
                        name = 'other'
                line_dict[rank] = f'{name}'
                name_ahead = name

    return taxid_ranked


def main():
    print('go')
    ranked_linage_file = "descrimination_evaluation/all_refseq_genome_taxonomy/selected_taxids_ranked.txt"
    selected_assemblies_fl = 'descrimination_evaluation/all_refseq_genome_taxonomy/all_assemblies.tsv'
    assembly_dict = tools.tsv_to_dict_of_dicts(selected_assemblies_fl, "assembly_accession")
    corresp_table_fl = 'descrimination_evaluation/all_refseq_genome_taxonomy/assembly_taxonomy.tsv'
    # blast_analysis_fct.get_taxid_ranked_lineage(ranked_linage_file)

    taxid_ranked = blast_analysis_fct.get_taxid_ranked_lineage(ranked_linage_file)
    corres_table = []
    for assembly, assembly_info in assembly_dict.items():
        # print(assembly_info)
        tax = taxid_ranked[assembly_info[0]['taxid']]
        tax_string = ';'.join(list(tax.values())[::-1])
        # print(tax_string)
        # input()
        corresp = {'assembly': assembly, 'taxonomy': tax_string}
        corres_table.append(corresp)

    tools.list_of_dict_to_tsv(corres_table, corresp_table_fl)


if __name__ == '__main__':
    main()
