#!/usr/bin/env python3

"""
Functions to analyse blast results.

"""

# Metadata
__author__ = 'Mainguy Jean - Plateforme bioinformatique Toulouse'
__copyright__ = 'Copyright (C) 2019 INRA'
__license__ = 'GNU General Public License'
__version__ = '0.1'
__email__ = 'support.bioinfo.genotoul@inra.fr'
__status__ = 'dev'

import csv
from collections import defaultdict
import matplotlib.pyplot as plt
import networkx as nx
from math import sqrt, ceil
import matplotlib.gridspec as gridspec
import logging

import os
import sys
# import plotly.graph_objects as go
# import tools

module_path = os.path.abspath("scripts/")
sys.path.append(module_path)
module_path = os.path.abspath("descrimination_evaluation/")
sys.path.append(module_path)

from ete3 import Tree, TreeStyle, add_face_to_node, TextFace
import re
from Bio import SeqIO
import numpy as np
import re
import time
import regions_analysis_fct
import displaying_fct


def get_region_general_info(regions, cog_id_to_gene_name, region_infos, taxid_ranked, assembly_dict, max_length_cutoff, selected_assemblies=None, cog_pair_dir="nextflow_result_2/cog_pairs/", cog_genomic_position_dir="nextflow_result_2/cog_genomic_position/", prepro_dir="descrimination_evaluation/all_regions_analysis/preprocessed_blast_results/"):
    capture_assemby_name = re.compile("^([A-Z]{3}_\d+.\d+)")
    for i, region in enumerate(regions):
        print(i+1, region)
        if region in region_infos:
            continue
        #
        # cog_pair_dir = "nextflow_result_2/cog_pairs/"
        cog_pair_file = os.path.join(cog_pair_dir, region+'.tsv')

        pair_summary, pair_details = regions_analysis_fct.analyse_cog_pair(
            cog_pair_file, max_length_cutoff)

        pair_example = pair_details['pairs_main_orga_post_len_filtering'][0]

        cog1, cog2 = pair_example["cog_pair"].split('|')
        gene1 = cog_id_to_gene_name[cog1]
        gene2 = cog_id_to_gene_name[cog2]
        region_name = f'{gene1}-{gene2} ({cog1}-{cog2})'

        pairs_main_orga_length_filtered = pair_details['pairs_main_orga_post_len_filtering']

        if selected_assemblies is not None:
            pairs_filtered = [
                p for p in pairs_main_orga_length_filtered if capture_assemby_name.search(p['genome']).group(1) in selected_assemblies]

            logging.warning(f'{len(pairs_main_orga_length_filtered) - len(pairs_filtered)} pairs have been removed because not in assembly selection')
        else:
            pairs_filtered = pairs_main_orga_length_filtered

        seq_id_filtered = [regions_analysis_fct.get_seq_if_from_pair_info(
            p) for p in pairs_filtered]

        preprocess_result = os.path.join(prepro_dir, f'preprocessed_{region}_blast_result.tsv')
        # fasta_file = f'nextflow_result_2/genomic_regions_extracted/{region}.fna'
        G, trees, phylodiv_list, assemblies_identified_at_species_lvl, seq_id_identified_at_species_lvl = get_resolution_stat(
            preprocess_result, taxid_ranked, assembly_dict, seq_id_filtered)

        cog1_genomic_position_file = os.path.join(cog_genomic_position_dir, cog1+'.tsv')
        summary_cog1, genome_set = regions_analysis_fct.get_stat_of_cog_found_in_genomes(
            cog1_genomic_position_file)
        summary_cog1['genome_set'] = set(genome_set)

        cog2_genomic_position_file = os.path.join(cog_genomic_position_dir, cog2+'.tsv')
        summary_cog2, genome_set = regions_analysis_fct.get_stat_of_cog_found_in_genomes(
            cog2_genomic_position_file)
        summary_cog2['genome_set'] = set(genome_set)

        print('nb genome where gene both gene is found',
              len(set(summary_cog1['genome_set']) & set(summary_cog2['genome_set'])))

        pairs_main_orga_post_len_filtering = pair_details['pairs_main_orga_post_len_filtering']
        length_elements = displaying_fct.get_region_structural_info(
            pairs_main_orga_post_len_filtering)

        pair_summary['cog1'] = cog1
        pair_summary['cog2'] = cog2
        pair_summary['gene1'] = gene1
        pair_summary['gene2'] = gene2
        pair_summary['gene_pair'] = f"{cog_id_to_gene_name[cog1]}-{cog_id_to_gene_name[cog2]}"
        pair_summary['region_name'] = region_name

        pair_summary['length_elements'] = length_elements

        pair_summary[cog1] = summary_cog1
        pair_summary[cog2] = summary_cog2

        pair_summary['graph'] = G
        pair_summary['trees'] = trees
        pair_summary['mean_phylo_div'] = np.mean(phylodiv_list)
        pair_summary['phylodiversities'] = phylodiv_list
        pair_summary['nb_genome_identified_at_species_lvl'] = len(
            assemblies_identified_at_species_lvl)
        pair_summary['assemblies_identified_at_species_lvl'] = assemblies_identified_at_species_lvl
        pair_summary['seq_id_identified_at_species_lvl'] = seq_id_identified_at_species_lvl

        pair_summary.update(pair_details)

        region_infos[region] = pair_summary


def nx_graph_to_tsv(G, output_file):
    with open(output_file, 'w') as fl:
        fl.write('\t'.join(['node1', 'node2', 'identity']) + '\n')
        [fl.write(node_node_id+'\n')
         for node_node_id in nx.generate_edgelist(G, data=['weight'], delimiter='\t')]
        # write node that does not have neighbors ( singleton)
        [fl.write(node+'\n') for node in nx.nodes(G) if not next(nx.neighbors(G, node), False)]


def get_graph_from_blast_result_old(preprocess_result, cov_threshold, pident_threshold, outfmt):
    """
    """

    G = nx.Graph()
    with open(preprocess_result) as fl:
        reader = csv.DictReader(fl, delimiter='\t', fieldnames=outfmt)

        for d in reader:
            G.add_node(d['qseqid'])
            G.add_node(d['sseqid'])
            if int(d['qcovhsp']) >= cov_threshold and float(d['pident']) >= pident_threshold:
                G.add_edge(d['qseqid'], d['sseqid'], weight=float(d['pident']))

    return G


def get_graph_from_blast_result_slow(preprocess_result, filtered_seq_ids, cov_threshold, pident_threshold, outfmt):
    """
    """
    G = nx.Graph()

    with open(preprocess_result) as fl:
        reader = csv.DictReader(fl, delimiter='\t', fieldnames=outfmt)
        ts = time.perf_counter()
        for d in reader:
            if d['qseqid'] not in filtered_seq_ids or d['sseqid'] not in filtered_seq_ids:
                continue
            # Add link
            if int(d['qcovhsp']) >= cov_threshold and float(d['pident']) >= pident_threshold:
                G.add_edge(d['qseqid'], d['sseqid'], weight=float(d['pident']))
        te = time.perf_counter()
        print(f'   initial part of graph construction  {(te - ts):.4f}s')
    # Add siprint( f'{func.__name__!r}  {(te - ts):.4f}s')ngleton seq that does not have any line in blast result
    ts = time.perf_counter()
    nodes = nx.nodes(G)
    for seq_id in filtered_seq_ids:
        if seq_id not in nodes:
            G.add_node(seq_id)
    te = time.perf_counter()
    print(f'   Add singleton part:  {(te - ts):.4f}s')
    return G


def get_graph_from_blast_result(preprocess_result, filtered_seq_ids, cov_threshold, pident_threshold, outfmt):
    """
    """
    G = nx.Graph()
    G.add_nodes_from(filtered_seq_ids)

    with open(preprocess_result) as fl:
        reader = csv.DictReader(fl, delimiter='\t', fieldnames=outfmt)
        for d in reader:
            if int(d['qcovhsp']) >= cov_threshold and float(d['pident']) >= pident_threshold:
                G.add_edge(d['qseqid'], d['sseqid'], weight=float(d['pident']))
    nodes_to_remove = [n for n in nx.nodes(G) if n not in filtered_seq_ids]
    G.remove_nodes_from(nodes_to_remove)
    # Add siprint( f'{func.__name__!r}  {(te - ts):.4f}s')ngleton seq that does not have any line in blast result

    return G


def get_resolution_stat(preprocess_result, taxid_ranked, assembly_dict, seq_id_selection, cov_threshold=90, pident_threshold=90):
    """

    """
    capture_assemby_name = re.compile("^([A-Z]{3}_\d+.\d+)")
    outfmt = 'qseqid qstart qend qlen sseqid sstart send slen length pident qcovhsp qcovs evalue bitscore'.split(
        ' ')

    # all_possible_seq_ids = [record.id for record in SeqIO.parse(open(fasta_file), "fasta")]
    #
    # filtered_seq_ids = [seq_id for seq_id in all_possible_seq_ids if capture_assemby_name.search(
    #     seq_id).group(1) in assemblies_selection]

    G = get_graph_from_blast_result(
        preprocess_result, seq_id_selection, cov_threshold, pident_threshold, outfmt)

    # assert set(nx.nodes(G)) == set(filtered_seq_ids)
    node_to_lineage_dict = get_node_to_lineage_dict(
        seq_id_selection, taxid_ranked, assembly_dict)
    trees = get_trees_from_graph(G, node_to_lineage_dict)
    #phylodiv_list = [get_phylo_div(tree) for tree in trees]

    # mean_phylo_div = np.mean(phylodiv_list)
    # nb_genome_identified_at_species_lvl = sum([len(tree) for tree in trees if blast_analysis_fct.get_phylo_div(tree) == 1])

    seq_id_identified_at_species_lvl = []
    phylodiv_list = []
    for tree in trees:
        phylo_div = get_phylo_div(tree)
        phylodiv_list.append(phylo_div)
        if phylo_div == 1:
            seq_id_identified_at_species_lvl += [leaf.name for leaf in tree.get_leaves()]

    # tax_name_to_assembly = {assembly_info[0]['organism_name']
    #     : assembly for assembly, assembly_info in assembly_dict.items()}
    assemblies_identified_at_species_lvl = {capture_assemby_name.search(
        seq_id).group(1) for seq_id in seq_id_identified_at_species_lvl}
    if len(assemblies_identified_at_species_lvl) != len(seq_id_identified_at_species_lvl):
        print(len(set(seq_id_identified_at_species_lvl)))
        print(len(assemblies_identified_at_species_lvl))
        print()

        duplicated_a = []
        assemblies_identified_at_species_lvl_list = [capture_assemby_name.search(
            seq_id).group(1) for seq_id in seq_id_identified_at_species_lvl]
        for a in assemblies_identified_at_species_lvl_list:
            if assemblies_identified_at_species_lvl_list.count(a) > 1:
                # print(assemblies_identified_at_species_lvl_list.count(a), a)
                duplicated_a.append(a)
        for seq_id in seq_id_identified_at_species_lvl:
            if capture_assemby_name.search(seq_id).group(1) in duplicated_a:
                print(seq_id)
        print()
        logging.warning(
            'number of assembly and seq id identified at sp lvl is different in'+fasta_file)

    return G, trees, phylodiv_list, assemblies_identified_at_species_lvl, seq_id_identified_at_species_lvl


def get_taxid_ranked_lineage_old(file):
    """
    # Header from
    # ftp://ftp.ncbi.nlm.nih.gov/pub/taxonomy/new_taxdump/taxdump_readme.txt

    """
    ranks = ['tax_id', 'tax_name', 'species', 'genus', 'family', 'order',
             'class', 'phylum', 'kingdom', 'superkingdom']
    key_field = 'tax_id'
    taxid_ranked = {}
    with open(file) as fl:
        for l in fl:
            splited_line = l.rstrip().replace('\t|', '').split('\t')
            line_dict = {rank: name for rank, name in zip(ranks, splited_line)}
            taxid = line_dict.pop('tax_id')
            taxid_ranked[taxid] = line_dict

            name_ahead = taxid
            for rank, name in line_dict.items():
                # manage when taxonomy is empty at some rank
                if name == '':
                    name = name_ahead
                    if name_ahead == line_dict['tax_name']:
                        name = 'other'
                line_dict[rank] = f'{name} [{rank}]'
                name_ahead = name

    return taxid_ranked


def get_taxid_ranked_lineage(file):
    """
    # Header from
    # ftp://ftp.ncbi.nlm.nih.gov/pub/taxonomy/new_taxdump/taxdump_readme.txt

    """
    ranks = ['tax_id', 'tax_name', 'species', 'genus', 'family', 'order',
             'class', 'phylum', 'kingdom', 'superkingdom']
    key_field = 'tax_id'
    taxid_ranked = {}
    with open(file) as fl:
        for l in fl:
            splited_line = l.rstrip().replace('\t|', '').split('\t')
            line_dict = {rank: name for rank, name in zip(ranks, splited_line)}
            taxid = line_dict.pop('tax_id')
            taxid_ranked[taxid] = line_dict

            flag = False

            earlier_taxon = 'other'
            for rank in ranks[1:][:: -1]:
                taxon = line_dict[rank]
                # manage when taxonomy is empty at some rank
                if taxon == '':
                    taxon = earlier_taxon
                    if rank != 'kingdom':
                        flag = True
                    line_dict[rank] = f'{taxon} other [{rank}]'
                else:
                    line_dict[rank] = f'{taxon} [{rank}]'

                earlier_taxon = taxon
            if flag and 0:
                print(line_dict)
                input()

    return taxid_ranked


def get_tree_from_sub_graph_nodes(nodes, node_to_lineage_dict):
    parent_child_set = set()
    # already_processed_lineages = set()
    for seq in nodes:
        # print(seq)
        lineage = node_to_lineage_dict[seq]
        # lineage = [seq] + lineage

        # print(lineage)
        # if '-'.join(lineage) in already_processed_lineages:
        #     continue
        # already_processed_lineages.add('-'.join(lineage))
        if not lineage:
            logging.warning(f'taxid not found {seq_taxid}.')
            continue
        parent_child_set |= {(child, parent, 1)
                             for parent, child in zip(lineage[: -1], lineage[1:])}

    if len(parent_child_set) == 0:
        return None

    tree = Tree.from_parent_child_table(list(parent_child_set))

    return tree


def get_trees_from_graph(G, node_to_lineage_dict):
    sub_graphs_nodes = get_sorted_sub_graph_nodes(G)
    trees = [get_tree_from_sub_graph_nodes(nodes, node_to_lineage_dict)
             for nodes in sub_graphs_nodes]
    return trees


def iter_trees_from_graph(G, node_to_lineage_dict):
    sub_graphs_nodes = get_sorted_sub_graph_nodes(G)
    for nodes in sub_graphs_nodes:
        tree = get_tree_from_sub_graph_nodes(nodes, node_to_lineage_dict)
        if tree:
            yield tree


def get_sorted_sub_graph_nodes(G):
    return sorted(list(nx.connected_components(G)), key=len, reverse=True)


def get_lca_tree(tree):
    root = tree.get_tree_root()
    leaves = root.get_leaves()
    leaf = leaves[0]
    lca_tree = leaf.get_common_ancestor(leaves)
    return lca_tree


def get_phylo_div(tree):
    root = tree.get_tree_root()
    leaves = root.get_leaves()
    if len(leaves) == 1:
        return 1
    leaf = leaves[0]
    common = leaf.get_common_ancestor(leaves)
    edges = common.get_edges()
    """
    # leaves is sequence id
    # leaves -1 is tax_name
    # leaves -2 is spcies
    # To get phylo div at species lvl
    # multiply number of leaves by 2 and remove it to the number of edges
    To get PhyloD at taxname level : multiply by 1
    """
    pd = len(edges) - len(leaves)*2
    # pd /= len(leaves)
    return pd


def get_mean_phylo_div_from_tree_list(G, node_to_lineage_dict):
    phylo_div_time_seqs_list = []
    nb_seq = 0
    for tree in iter_trees_from_graph(G, node_to_lineage_dict):
        phylo_div = get_phylo_div(tree)
        # phylo_div_time_seqs = phylo_div * len(tree)
        phylo_div_list.append(phylo_div)
        nb_group += 1
    mean_phylo_div = sum(phylo_div_list)/nb_group
    return mean_phylo_div


def get_mean_phylo_div_from_trees(trees):
    phylo_div_time_seqs_list = []
    nb_seq = 0
    for tree in iter_trees_from_graph(G, node_to_lineage_dict):
        phylo_div = get_phylo_div(tree)
        # phylo_div_time_seqs = phylo_div * len(tree)
        phylo_div_list.append(phylo_div)
        nb_group += 1
    mean_phylo_div = sum(phylo_div_list)/nb_group
    return mean_phylo_div


def get_global_phylo_div(G, node_to_lineage_dict):
    phylo_div_time_seqs_list = []
    nb_seq = 0
    for tree in iter_trees_from_graph(G, node_to_lineage_dict):
        phylo_div = get_phylo_div(tree)
        phylo_div_time_seqs = phylo_div * len(tree)
        phylo_div_time_seqs_list.append(phylo_div_time_seqs)
        nb_seq += len(tree)
    global_phylo_div = sum(phylo_div_time_seqs_list)/nb_seq
    return global_phylo_div


def get_global_phylo_div_normal(G, node_to_lineage_dict):
    phylo_div_time_seqs_list = []
    nb_seq = 0
    for tree in iter_trees_from_graph(G, node_to_lineage_dict):
        phylo_div = get_phylo_div_normal(tree)
        phylo_div_time_seqs = phylo_div * len(tree)
        phylo_div_time_seqs_list.append(phylo_div_time_seqs)
        nb_seq += len(tree)
    global_phylo_div = sum(phylo_div_time_seqs_list)/nb_seq
    return global_phylo_div


def get_best_hsp(hsps):
    return sorted(hsps, key=lambda t: float(t['bitscore']), reverse=True)[0]


def iter_preprocessed_blast_result(blast_result_file, outfmt):
    """
    Preprocess of blast result
    """
    hsps = []
    tuple_ids = None

    with open(blast_result_file) as fl:
        reader = csv.DictReader(fl, delimiter='\t', fieldnames=outfmt)
        for d in reader:
            if d['qseqid'] == d['sseqid']:
                continue

            if int(d['qlen']) < int(d['slen']):
                continue
            elif int(d['qlen']) == int(d['slen']) and d['qseqid'] < d['sseqid']:
                continue

            if not tuple_ids:
                # first line: tuple_ids is not initialize
                tuple_ids = (d['qseqid'], d['sseqid'])

            # Current line is another couple of sequence
            # Best hsp of previous couple is determined and yield
            if tuple_ids and tuple_ids != (d['qseqid'], d['sseqid']):
                hsp = get_best_hsp(hsps)
                yield hsp

                # new tuple ids and hsp list
                tuple_ids = (d['qseqid'], d['sseqid'])
                hsps = []

            hsps.append(d)
    # yield last hsp
    hsp = get_best_hsp(hsps)
    yield hsp


def get_node_to_lineage_dict(nodes, taxid_ranked, assembly_dict):
    node_to_lineage = {}
    capture_assemby_name = re.compile("^([A-Z]{3}_\d+.\d+)")
    for node in nodes:
        # print(seq)
        re_result = capture_assemby_name.search(node)
        assembly_name = re_result.group(1)
        seq_taxid = assembly_dict[assembly_name][0]['taxid']
        try:
            lineage = [node] + list(taxid_ranked[seq_taxid].values())
        except KeyError:
            logging.warning(f'taxid not found {seq_taxid}.')
            continue

        node_to_lineage[node] = lineage
    return node_to_lineage


if __name__ == '__main__':
    import doctest
    doctest.testmod()
