## Analyse results with a graph startegy

Try python module networkx:
docs : https://networkx.github.io/documentation/networkx-1.10/overview.html

Add to conda env:

`conda install -n jupter_env -c anaconda networkx`

**Goal is to make plot: number of sub graph in function of identity threshold**

* Analysis of blast result from test run of COG0048-COG0049:

* Raw blast result are filtered by script: `scripts/pre_process_blast_results.py`

* Giving the folowing file: `descrimination_evaluation/preprocessed_COG0048-COG0049_blast_result.tsv`


Use jupyter notebook : `blast_graph_analysis.ipynb `




## Taxonomy rank and LCA of the sub graph

Retrieve taxonomy rank of the lineage?  

### With ete3  ?
Easy special Class to deal with ncbi and special method to retrieve the rank
but ete3 download by itself the taxonomy


### directly with ncbi taxdump ?
Better solution.
* coherent with the taxo used earlier in the workflow
* not download twice same things in different place
Rank is specified in Nodes of the taxdump

### Cosntruct taxonomic tree of the sub graph

Use ETE3 to construct taxonomic tree with taxonomic lineage

Use method: from_parent_child_table(parent_child_table)
http://etetoolkit.org/docs/latest/reference/reference_tree.html#ete3.TreeNode.from_parent_child_table

### Deal with no rank
Should we remove them?? or keep them but not count them in the Diversity ???

Alternative convert to ultrametric with method  convert_to_ultrametric(tree_length=None, strategy='balanced')
http://etetoolkit.org/docs/latest/reference/reference_tree.html#ete3.TreeNode.convert_to_ultrametric


ranked taxonomy can be found in rankedlineage.dmp
From the readme : ftp://ftp.ncbi.nlm.nih.gov/pub/taxonomy/new_taxdump/taxdump_readme.txt
```
-----------------
Select ancestor names for well-established taxonomic ranks (species, genus, family, order, class, phylum, kingdom, superkingdom) file fields:

  tax_id       -- node id
  tax_name     -- scientific name of the organism
  species      -- name of a species (coincide with organism name for species-level nodes)
	genus					-- genus name when available
	family					-- family name when available
	order					-- order name when available
	class					-- class name when available
	phylum					-- phylum name when available
	kingdom					-- kingdom name when available
	superkingdom				-- superkingdom (domain) name when available
```

Let's join this table with the taxids we selected for the study:

```
tar -zxvf new_taxdump.tar.gz rankedlineage.dmp

cat selected_assemblies.tsv | cut -f3 | grep -v taxid > selected_taxid.txt

join -t $'\t' <(sort selected_taxid.txt) <(sort -k1,1 rankedlineage.dmp)  > selected_taxids_ranked.txt

```

Let's do that again for all assemblies in refseq

```
cd descrimination_evaluation/all_refseq_genome_taxonomy
tar -zxvf new_taxdump.tar.gz rankedlineage.dmp
cp ../../nextflow_result_2/all_assemblies.tsv .

cat all_assemblies.tsv | cut -f3 | grep -v taxid > selected_taxid.txt
join -t $'\t' <(sort selected_taxid.txt) <(sort -k1,1 rankedlineage.dmp)  > selected_taxids_ranked.txt

```
### Compute simple Phylogenetic Diversity (PD)
Get Phylogenetic Diversity of the group using the tree:
very simple: number of edges of the tree starting at the LCA



## COG0092-COG0197 vs COG0087-COG0186

Preprocess blast result in dir descrimination_evaluation/COG0092-COG0197_vs_COG0087-COG0186

```

python ../../scripts/pre_process_blast_results.py COG0087-COG0186_blast_result.tsv "qseqid qstart qend qlen sseqid sstart send slen length pident qcovhsp qcovs evalue bitscore"

python ../../scripts/pre_process_blast_results.py COG0092-COG0197_blast_result.tsv  "qseqid qstart qend qlen sseqid sstart send slen length pident qcovhsp qcovs evalue bitscore"

```

Analyze the preprocess blast result with the jupyter: blast_graph_taxonomy_analysis.ipynb


## Analysis all regions


symb link of blast result folder in

```
mkdir ~/marker_identification/descrimination_evaluation/all_regions_analysis

ln -s ~/marker_identification/sample_test_nextflow_result_2/blast_results descrimination_evaluation/all_regions_analysis/

python blast_result_preprocessing.py all_regions_analysis/blast_results/ --output_dir all_regions_analysis/preprocessed_blast_results  

```

On genologin in all_regions_analysis dir:

`sbatch all_regions_analysis/preprocess_all.sbatch`

seff jobid : `seff 7315187`

```
Job ID: 7315187
Cluster: genobull
User/Group: jmainguy/BIOINFO
State: COMPLETED (exit code 0)
Cores: 1
CPU Utilized: 01:59:06
CPU Efficiency: 99.42% of 01:59:48 core-walltime
Job Wall-clock time: 01:59:48
Memory Utilized: 108.72 MB
Memory Efficiency: 5.31% of 2.00 GB
```


### PhyloDiv vs Pident threshold

script python : phylo_div_for_all.py compute phyloD Global for multiple pident threshold
result store in `cog_pairs_PD_G_cov90.json`

run on sbatch:
`sbatch phylo_div_for_all.py `

The script is not optimized at all.




## Visualization :
### GRAPH FOR ONE (or fery few region)
make a bar plot of PD and density
	density plot... + bar plot

PD vs size of the group

Be able to generate tree and dig into phylodiv of a region easily

Species of node in cytoscape ==> need to add node attribute in tsv created

## Graph for all regions


sorted PDG and couple of COG with legend and maybe color with a variable

PLOT result of the computation of GPD and different pident.


Filter the regions plotted with more stringent cutoff on different thing (std median etc... )

PDG vs median size of the regions

Give some info on the two regions that seems to be better..

## From blast to Blat
Blat faster programm but less accurate
When using all  refseq genome blast is too slow (>16h)

```
conda install -c bioconda -n axe3_local blat
mkdir descrimination_evaluation/blat_search

blat sample_test_nextflow_result_2/region_extracted/COG0200-COG0256.fna sample_test_nextflow_result_2/region_extracted/COG0200-COG0256.fna descrimination_evaluation/blat_search/output.tsv -t=dna -q=dna -out=blast8


```
BLAT Error
Same header in two seqs. Same pair is starting at same position in two contig
add contig id to fix that issue
```
ERROR ~ Error executing process > 'blat_all_vs_all (7)'

Caused by:
  Process `blat_all_vs_all (7)` terminated with an error exit status (255)

Command executed:

  wanted_outfmt="qseqid qstart qend qlen sseqid sstart send slen length pident qcovhsp qcovs evalue bitscore"
  blat_outfmt="qseqid sseqid pident length mismatches gap_openings qstart qend sstart send evalue bitscore"

  for fna_file in *.fna; do
    name="${fna_file%.*}"
    blat $fna_file $fna_file  blat_result_${name}.tsv               -t=dna -q=dna -out=blast8


    python /work2/project/seqoccin/metaG/marker_identification/scripts/pre_process_blat_results.py blat_result_${name}.tsv "$blat_outfmt" "$wanted_outfmt"


  done

Command exit status:
  255

Command output:
  Loaded 192389810 letters in 168356 sequences

Command error:
  Error: sequence name GCF_002811975.1|2721-3849 is repeated in the database, all names must be unique.

```

Write a parser to the blat output

Blat error: probably an error message
```
Caused by:
  Process `blat_all_vs_all (3)` terminated with an error exit status (135)

Command executed:

  wanted_outfmt="qseqid qstart qend qlen sseqid sstart send slen length pident qcovhsp qcovs evalue bitscore"
  blat_outfmt="qseqid sseqid pident length mismatches gap_openings qstart qend sstart send evalue bitscore"

  for fna_file in *.fna; do
    name="${fna_file%.*}"
    blat $fna_file $fna_file  blat_result_${name}.tsv               -t=dna -q=dna -out=blast8


    python /home/jmainguy/metaG/marker_identification/scripts/pre_process_blat_results.py blat_result_${name}.tsv "$blat_outfmt" "$wanted_outfmt"


  done

Command exit status:
  135

Command output:
  (empty)

Command error:
  .command.sh : ligne 5 : 117278 Erreur du bus           blat $fna_file $fna_file blat_result_${name}.tsv -t=dna -q=dna -out=blast8

Work dir:
  /work2/project/seqoccin/metaG/marker_identification/work/0f/f3fd0fa9e787488d49ace1836ffe0d


```
