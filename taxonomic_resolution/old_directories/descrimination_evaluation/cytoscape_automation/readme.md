


https://nbviewer.jupyter.org/github/idekerlab/py2cytoscape/blob/develop/examples/New_wrapper_api_sample.ipynb

http://manual.cytoscape.org/en/3.5.0/Programmatic_Access_to_Cytoscape_Features_Scripting.html


Need to have a cytoscape session open to render graph...
Seems problematic on genologin

Solution would be to do it on local at the end to open all graph in a cytoscape session.
Or to try on the server but not essential..

Does not work :

`conda install -c bioconda -n jupy cytoscap`


Another cool solution would be to use cytoscape.js : http://js.cytoscape.org/
But not essential....
