import sys
import os
import csv
import re
import numpy as np
import plotly
import pprint
import logging

import matplotlib.pyplot as plt
import plotly.figure_factory as ff
import plotly.tools as tls
import plotly.graph_objects as go


from collections import defaultdict
from Bio import SeqIO
from dna_features_viewer import GraphicFeature, GraphicRecord
from IPython.display import display, Markdown
from ete3 import Tree, SeqMotifFace, TreeStyle, add_face_to_node, TextFace, NodeStyle


module_path = os.path.abspath("scripts/")
sys.path.append(module_path)
module_path = os.path.abspath("descrimination_evaluation/")
sys.path.append(module_path)

import tools
import blast_analysis_fct
import regions_analysis_fct
import displaying_fct

pp = pprint.PrettyPrinter(indent=4)


def visualize_genomic_region(region_elements, ax=None):
    features = []
    current_po = 0
    for el in region_elements:
        el['start'] = current_po + 1
        current_po += el['length']
        el['end'] = current_po

    for el in region_elements:
        feat = GraphicFeature(start=el['start'], end=el['end'], strand=el['strand'],
                              color=el['color'], label=f'{el["name"]}\n(~{el["length"]}nt)')

        features.append(feat)

    feat = GraphicFeature(start=region_elements[0]['start'], end=region_elements[-1]['end'], strand=0,
                          color='w', label=f'region: {region_elements[0]["name"]}-{region_elements[-1]["name"]}\n(~{current_po}nt)')
    # features.append(feat)
    record = GraphicRecord(sequence_length=current_po+100, first_index=0, features=features)
    return record.plot(ax=ax, figure_width=10, annotate_inline=False, with_ruler=True)


def get_region_structural_info(pair_details):
    def get_length(start, end): return max(int(start), int(end)) - min(int(start), int(end)) + 1
    length_elements = defaultdict(list)

    for d in pair_details:
        cog1, cog2 = d['cog_pair'].split('|')
        cog1_coord, cog2_coord = [tuple(coord_string.split('-'))
                                  for coord_string in d['positions'].split('|')]

        cog1_length = get_length(cog1_coord[0], cog1_coord[1])
        cog2_length = get_length(cog2_coord[0], cog2_coord[1])
        intergenique_length = int(d['distance']) - (cog1_length + cog2_length)
        length_elements[cog1].append(cog1_length)
        length_elements[cog2].append(cog2_length)
        length_elements['intergenic_region'].append(intergenique_length)

    return length_elements


def display_region_schematic_organisation(length_elements, pair_example, cog_id_to_gene_name, colors, ax=None):
    cog1, cog2 = pair_example['cog_pair'].split('|')
    strand1, strand2 = pair_example['strands'].split('|')
    if strand1 == strand2:
        strand1 = '+'
        strand2 = '+'
    elif strand1 != strand2:
        strand1 = '+'
        strand2 = '-'

    gene1 = cog_id_to_gene_name[cog1]
    gene2 = cog_id_to_gene_name[cog2]
    sum_up_fct = np.median

    length1 = round(sum_up_fct(length_elements[cog1]))
    length2 = round(sum_up_fct(length_elements[cog2]))
    length_intergenic = round(sum_up_fct(length_elements['intergenic_region']))

    gene1 = {'length': length1, 'name': f'{gene1} ({cog1})', 'strand': int(strand1+'1'), 'color': colors[1]}
    gene2 = {'length': length2, 'name': f'{gene2} ({cog2})', 'strand': int(strand2+'1'), 'color': colors[2]}
    inter_G = {'length': length_intergenic,
               'name': 'intergenic_region', 'strand': 0, 'color': colors[3]}

    return visualize_genomic_region([gene1, inter_G, gene2], ax)


def get_extraction_stat_old(extraction_dir, cogs):

    cogs_extraction_stat = {c: {'cog_id': c,
                                'cog_protein_count': 0,
                                'cog_genome_count': 0,
                                'cog_single_copy_count': 0} for c in cogs}

    for result_file_name in os.listdir(extraction_dir):
        result_file = os.path.join(extraction_dir, result_file_name)
        protein_count, genome_count, single_copy_count = parse_extraction_result(result_file)
        for c in cogs:
            cogs_extraction_stat[c]['cog_protein_count'] += protein_count.pop(c, 0)
            cogs_extraction_stat[c]['cog_genome_count'] += genome_count.pop(c, 0)
            cogs_extraction_stat[c]['cog_single_copy_count'] += single_copy_count.pop(c, 0)

    # add stat in prct of all genomes
    total_genomes = len(os.listdir(extraction_dir))
    for cog, stat in cogs_extraction_stat.items():
        # stat['cog_protein_count_per_genome'] = stat['cog_protein_count']/total_genomes
        stat['cog_genome_prct'] = (stat['cog_genome_count']/total_genomes)*100
        stat['cog_single_copy_prct'] = (stat['cog_single_copy_count']/total_genomes)*100

    return cogs_extraction_stat


def plot_region_general_info(regions, cog_id_to_gene_name, region_infos, colors=["violet", "#341677", "#ff6363", 'grey']):
    nb_sub_plot = len(regions)
    mpl_fig, axes = plt.subplots(nb_sub_plot, 1, figsize=(8, 3*nb_sub_plot), sharex=True)
    plt.subplots_adjust(wspace=1, hspace=1)

    # display(Markdown(f'## Region length distribution'))

    # print(pair_example)

    for i, region in enumerate(regions):
        pair_summary = region_infos[region]
        pairs_main_orga = pair_summary['pairs_main_orga']

        pair_detail_example = pairs_main_orga[0]
        cog1 = pair_summary['cog1']
        cog2 = pair_summary['cog2']
        region_name = pair_summary['region_name']
        length_elements = pair_summary['length_elements']

        try:
            ax = axes[i]
        except TypeError:
            ax = axes
        ax.title.set_text(region_name)
        ax.title.set_fontsize(15)

        display_region_schematic_organisation(
            length_elements, pair_detail_example, cog_id_to_gene_name, colors, ax)


def plot_length_distribution(regions, region_infos, colors=["violet", "#341677", "#ff6363", 'grey']):
    hist_data_region = []
    group_labels_region = []
    for i, region in enumerate(regions):
        pair_summary = region_infos[region]
        pairs_main_orga_len_filter = pair_summary['pairs_main_orga_post_len_filtering']
        region_name = pair_summary['region_name']
        length_elements = pair_summary['length_elements']

        hist_data = []
        group_labels = []

        region_length = [int(p['distance']) for p in pairs_main_orga_len_filter]

        group_labels.append(region_name)
        hist_data.append(region_length)

        hist_data_region.append(region_length)
        group_labels_region.append(region_name)

        for element_name, lengths in length_elements.items():
            group_labels.append(f'{element_name}')
            hist_data.append(lengths)

        fig = ff.create_distplot(hist_data, group_labels, show_hist=False, colors=colors)
        fig.update_layout(title_text=region_name, xaxis=go.layout.XAxis(title=go.layout.xaxis.Title(
            text="Length (pb)")),)

        plotly.offline.iplot(fig)

    if len(regions) > 1:
        fig = ff.create_distplot(hist_data_region, group_labels_region, show_hist=False)
        fig.update_layout(title_text='Regions Length distribution',)
        plotly.offline.iplot(fig)


def display_region_stat(regions, region_infos):
    md_table_lines = ['|region|', "|---|"]

    # what info to include in table?
    k_to_write = ['cog1',
                  'cog2',
                  'nb_of_pairs',
                  'nb_represented_genomes',
                  'nb_pair_with_main_orga',
                  'nb_pairs_main_orga_length_exceeded',
                  'nb_genome_with_main_orga',
                  'mean',
                  'median',
                  'min',
                  'max',
                  'std',
                  'nb_genome_std_500',
                  'nb_alternative_pairs',
                  'nb_problematic_pairs',
                  'nb_alternative_sites',
                  'g_phylo_div'
                  ]

    for k in k_to_write:
        md_table_line = f"| **{k}** |"
        md_table_lines.append(md_table_line)

    for i, region in enumerate(regions):
        pair_summary = region_infos[region]
        gene_pair = pair_summary['gene_pair']
        md_table_lines[0] += f' {gene_pair} |'
        md_table_lines[1] += '---|'

        index_line = 1
        for k in k_to_write:
            index_line += 1
            v = pair_summary[k]
            if type(v) != int and type(v) != str:
                v = round(v, 1)

            md_table_lines[index_line] += f" {v} |"

        # md_str += f'| **pairs with a distance > {MAX_LENGTH}nt** | {len(pairs_main_orga_length_exceeded)} | \n'
    md_str = '\n'.join(md_table_lines)
    display(Markdown(md_str))
    # display(Markdown(f'* {len(pairs_main_orga_length_exceeded)} pairs with a distance > {MAX_LENGTH}nt'))


def get_missing_tree(rank_name_to_display, assemblies_to_display, missing_assemblies, assembly_to_lineage_dict, missing_assembly_reasons={}):
    color_gradient = ['darkgreen', 'green', '#208e00', '#459c00',
                      '#72aa00', '#a4b800', '#c7b000', '#d58e00',
                      '#e36500', '#f13600', 'red']

    possible_rank_names = ['seq', 'tax_name', 'species', 'genus', 'family', 'order',
                           'class', 'phylum', 'kingdom', 'superkingdom']

    ranks = [possible_rank_names.index(name) for name in rank_name_to_display]
    missing_rank_count = defaultdict(int)
    total_rank_count = defaultdict(int)

    missing_rank_count_by_cat = {cat: defaultdict(int) for cat in missing_assembly_reasons}

    extrem_rank = min(ranks)
    assembly_to_short_lineage = {a: l[extrem_rank:] for a, l in assembly_to_lineage_dict.items()}

    for rank in ranks:
        for assembly, lineage in assembly_to_lineage_dict.items():
            rank_taxon = lineage[rank]
            total_rank_count[rank_taxon] += 1
            if assembly in missing_assemblies:
                missing_rank_count[rank_taxon] += 1
            # detail by cat
            for cat, assemblies_of_cat in missing_assembly_reasons.items():
                if assembly in assemblies_of_cat:
                    missing_rank_count_by_cat[cat][rank_taxon] += 1

    rank_to_stat = {taxon: f"{missing_rank_count[taxon]}/{total_rank_count[taxon]}" for taxon in total_rank_count}
    # pp.pprint(rank_to_rank_stat)
    # pp.pprint(assembly_to_short_lineage)
    #rank_stat_to_short_lineage = {}

    taxon_to_missing_lvl = {taxon: int(
        round((missing_rank_count[taxon]/total_rank_count[taxon])*10)) for taxon in missing_rank_count}

    nodes_style_lvls = [NodeStyle(fgcolor=color_gradient[lvl]) for lvl in range(10)]
    #{print(f'{taxon}: {missing_rank_count[taxon]}/{total_rank_count[taxon]}') for taxon in  missing_rank_count}

    tree = blast_analysis_fct.get_tree_from_sub_graph_nodes(
        assemblies_to_display, assembly_to_short_lineage)

    for node in tree.traverse():
        if node.name in rank_to_stat:
            stat = rank_to_stat[node.name]

            missing_level = taxon_to_missing_lvl[node.name]

            tface = TextFace(node.name)
            tface.margin_right = 5
            node.add_face(tface, column=-1)

            tface = TextFace(stat, fgcolor=color_gradient[missing_level])
            tface.margin_right = 5
            node.add_face(tface, column=1)

            if node.is_leaf():
                stat = ''
                for i, cat in enumerate(missing_assembly_reasons):
                    nb_assembly_in_cat = missing_rank_count_by_cat[cat][node.name]
                    if nb_assembly_in_cat > 0:
                        stat = f'{cat}:{nb_assembly_in_cat}'

                        tface = TextFace(f'{stat}', fgcolor="black")
                        if i == 0:
                            tface.margin_top = 5

                        node.add_face(tface, column=2)

    return tree


def display_missing_genomes_tree(regions, rank_name_to_display, region_infos, assembly_to_lineage_dict, taxid_ranked, assembly_dict, display_only_missing=True, display_missing_details=False, taxon_to_display=[]):
    display(Markdown(f'# Missing genomes investigation'))
    display(Markdown(f'{len(assembly_to_lineage_dict)} genomes in total'))
    capture_assemby_name = re.compile("^([A-Z]{3}_\d+.\d+)")

    all_assemblies = set(assembly_to_lineage_dict)

    for region in regions:
        summary_region = region_infos[region]
        pairs_main_orga_len_filter = summary_region['pairs_main_orga_post_len_filtering']

        genome_names_region = [p['genome'] for p in pairs_main_orga_len_filter]
        region_name = summary_region['region_name']
        missing_assemblies, missing_assembly_reasons, unambiguous_assembly_identified = displaying_fct.get_missing_assemblies_reasons(
            summary_region, all_assemblies)
        displaying_fct.display_genomes_stat(
            region_name,  missing_assemblies, missing_assembly_reasons, unambiguous_assembly_identified, all_assemblies)

        # assembly where the region has been found properly
        assembly_names_region = [capture_assemby_name.search(
            genome_name).group(1) for genome_name in genome_names_region]
        #assembly_to_lineage_dict_region = blast_analysis_fct.get_node_to_lineage_dict(assembly_names_region,taxid_ranked, assembly_dict)

        #print('total nb of assemblies', len(assembly_to_lineage_dict))

        # display(Markdown(f'## {region_name}'))
        # display(Markdown(f'In **{len(assembly_names_region)}** genomes ({round(len(assembly_names_region)/len(assembly_to_lineage_dict)*100)}%)  the region has been found properly '))
        missing_assemblies = all_assemblies - set(assembly_names_region)

        # missing assembly because of missing cog
        cog1 = summary_region["cog1"]
        cog2 = summary_region["cog2"]

        cog1_assemblies = set(summary_region[cog1]['genome_set'])
        # cog1_assemblies = {capture_assemby_name.search(
        #    genome_name).group(1) for genome_name in cog1_genomes}

        cog2_assemblies = set(summary_region[cog2]['genome_set'])
        # print('nb genome where both of gene is found', len(cog2_assemblies & cog1_assemblies))
        # cog2_assemblies = {capture_assemby_name.search(
        #    genome_name).group(1) for genome_name in cog2_genomes}

        cog_missing_assemblies = all_assemblies - (cog2_assemblies & cog1_assemblies)

        # missing assemblies different organisation
        pairs_main_orga = summary_region['pairs_main_orga']
        # print('pairs_main_orga', len(pairs_main_orga))
        genome_with_main_orga = [p['genome'] for p in pairs_main_orga]
        assemblies_with_main_orga = {capture_assemby_name.search(
            genome_name).group(1) for genome_name in genome_with_main_orga}
        #print('genome_with_main_orga', len(genome_with_main_orga))
        assemblies_with_alternative_orga = all_assemblies - \
            (assemblies_with_main_orga | cog_missing_assemblies)
        #print('assemblies_with_alternative_orga',len(assemblies_with_alternative_orga) )

        # missing assembly due to exceeding length
        pairs_main_orga_length_filtered = summary_region['pairs_main_orga_post_len_filtering']
        genome_with_length_filtered = [p['genome'] for p in pairs_main_orga_length_filtered]
        assemblies_with_length_filtered = {capture_assemby_name.search(
            genome_name).group(1) for genome_name in genome_with_length_filtered}

        assemblies_with_length_exceeded = all_assemblies - \
            (assemblies_with_alternative_orga | cog_missing_assemblies | assemblies_with_length_filtered)

        # missing assemblies because can't get the distance between the two cogs (fragmented genomes)
        missing_assemblies_fragmented = missing_assemblies - \
            set(cog_missing_assemblies | assemblies_with_length_exceeded |
                assemblies_with_alternative_orga)

        unambigous_assembly_identification = summary_region['assemblies_identified_at_species_lvl']
        # print('unambigous_assembly_identification', len(unambigous_assembly_identification))
        ambigous_assembly_identification = set(
            assembly_names_region) - unambigous_assembly_identification
        # print('ambigous_assembly_identificationlen', len(ambigous_assembly_identification))
        #
        # print('inteersection ambigous_assembly_identification',
        #       ambigous_assembly_identification & cog_missing_assemblies)

        max_length_cutoff = summary_region['max_length_cutoff']
        # missing_assemblies_to_lineage_dict = blast_analysis_fct.get_node_to_lineage_dict(missing_assemblies,taxid_ranked, assembly_dict)
        # display(Markdown(f'number of missing genomes {len(missing_assemblies)} ({round(len(missing_assemblies)/len(assembly_to_lineage_dict)*100,1)}%):'))
        # display(Markdown(f' * {len(cog_missing_assemblies)}  ({round(len(cog_missing_assemblies)/len(assembly_to_lineage_dict)*100,1)}%) because at least one of the gene was not found'))
        # display(Markdown(f' * {len(missing_assemblies_fragmented)} ({round(len(missing_assemblies_fragmented)/len(assembly_to_lineage_dict)*100,1)}%) because gene pair distance information is not avalaible (fragmented genomes)'))
        # display(Markdown(f' * {len(assemblies_with_alternative_orga)} ({round(len(assemblies_with_alternative_orga)/len(assembly_to_lineage_dict)*100,1)}%) because not in the main organisation'))
        # display(Markdown(f' * {len(assemblies_with_length_exceeded)} ({round(len(assemblies_with_length_exceeded)/len(assembly_to_lineage_dict)*100,1)}%) because region length > {max_length_cutoff}nt'))

        if display_missing_details:
            pass
            # missing_assembly_reasons = {"missing cog": cog_missing_assemblies,
            #                             'alternative orientation': assemblies_with_alternative_orga,
            #                             'region length': assemblies_with_length_exceeded,
            #                             "missing info": missing_assemblies_fragmented,
            #                             'ambigous identification': ambigous_assembly_identification}
            # missing_assembly_reasons['ambigous identification'] = {}
        else:
            missing_assembly_reasons = {}

        #missing_assemblies |= ambigous_assembly_identification

        # Filter missing assemblies to display only the required taxon
        if taxon_to_display:
            print('Display only', taxon_to_display)
            missing_assemblies_to_lineage = blast_analysis_fct.get_node_to_lineage_dict(
                missing_assemblies, taxid_ranked, assembly_dict)

            all_assemblies_to_lineage = blast_analysis_fct.get_node_to_lineage_dict(
                all_assemblies, taxid_ranked, assembly_dict)

            missing_assemblies_filter = []
            all_assemblies_filter = []
            print(len(missing_assemblies))
            for taxon in taxon_to_display:
                missing_assemblies_filter += [assembly for assembly,
                                              lineage in missing_assemblies_to_lineage.items() if taxon in lineage]

                all_assemblies_filter += [assembly for assembly,
                                          lineage in all_assemblies_to_lineage.items() if taxon in lineage]
                missing_assemblies = set(missing_assemblies_filter)

                all_assemblies = set(all_assemblies_filter)
                print(len(missing_assemblies))

            if len(all_assemblies) == 0:
                logging.warning(f'no missing assembly belonging to taxon {taxon_to_display}')
                return

         # print(len(missing_assemblies))
        if display_only_missing:
            assemblies_to_display = missing_assemblies
        else:
            assemblies_to_display = all_assemblies

        tree = get_missing_tree(rank_name_to_display, assemblies_to_display, missing_assemblies,
                                assembly_to_lineage_dict, missing_assembly_reasons)
        ts = TreeStyle()
        ts.show_leaf_name = False

        ts.show_scale = False
        ts.orientation = 0
        ts.branch_vertical_margin = 5
        display(tree.render("%%inline", tree_style=ts))


def plot_pdg_across_regions(regions, regions_to_plot, pdg_dict, region_infos):
    pdgs = [pdg_dict[r] for r in regions_to_plot]

    color = ["cadetblue"]*len(regions_to_plot)
    # color[regions_to_plot.index('rrn')] = "crimson"
    # color[]
    fig = go.Figure()
    fig.add_trace(go.Scatter(
        x=regions_to_plot,
        y=pdgs,
        marker=dict(color=color, size=8),
        mode="markers",
        name="All filtered regions",
        # text=regions_to_plot
    ))

    for region in regions:
        region_name = region_infos[region]['region_name']
        # color[regions_to_plot.index('rrn')] = "crimson"
        fig.add_trace(go.Scatter(
            x=[region],
            y=[pdg_dict[region]],
            # marker=dict(color="crimson", size=8),
            marker=dict(size=8),
            mode="markers",
            name=region_name,
            # text=regions_to_plot
        ))

    fig.update_layout(title="Taxonomic resolution",
                      yaxis_title="Taxonomic resolution")
    fig.update_xaxes(showticklabels=False)
    # fig.update_xaxes(tickfont=dict(size=10))

    fig.show()


def get_assembly_name(string):
    capture_assemby_name = re.compile("^([A-Z]{3}_\d+.\d+)")
    return capture_assemby_name.search(string).group(1)


def human_format(num):
    if type(num) == str:
        num = round(float(num))

    magnitude = 0
    while abs(num) >= 1000:
        magnitude += 1
        num /= 1000.0
    # add more suffixes if you need them
    return f"{num:.0f}{['', 'K', 'M', 'G', 'T', 'P'][magnitude]}"


def taxon_filtering_assemblies(taxon_to_display, assemblies_to_lineage):

    # assemblies_to_lineage = blast_analysis_fct.get_node_to_lineage_dict(
    #                                        assemblies, taxid_ranked, assembly_dict)

    assemblies_filter = []
    for taxon in taxon_to_display:
        print('taxon to display', taxon)
        assemblies_filter += [assembly for assembly,
                              lineage in assemblies_to_lineage.items() if taxon in lineage]

    #[print(assembly, lineage[6]) for assembly, lineage in assemblies_to_lineage.items() if assembly in assemblies_filter]
    assemblies_filter = set(assemblies_filter)

    if len(assemblies_filter) == 0:
        logging.warning(f'no assembly found belonging to taxon {taxon_to_display}')
        return

    return assemblies_filter


def display_genomes_stat(region_name, missing_assemblies, missing_assembly_reasons, unambiguous_assembly_identified, all_assemblies):

    nb_retrieved_genome = len(all_assemblies) - len(missing_assemblies)

    string = f'## {region_name}\n'
    string += f'In **{nb_retrieved_genome}** genomes ({round(nb_retrieved_genome/len(all_assemblies)*100)}%) the region has been found properly\n'
    # missing_assemblies_to_lineage_dict = blast_analysis_fct.get_node_to_lineage_dict(missing_assemblies,taxid_ranked, assembly_dict)
    string += '### Missing genomes\n'
    string += f'Number of missing genomes {len(missing_assemblies)} ({round(len(missing_assemblies)/len(all_assemblies)*100,1)}%):\n'

    for reason, missing_ass in missing_assembly_reasons.items():
        nb_missing_ass = len(missing_ass)
        if nb_missing_ass > 0:
            string += f' * {nb_missing_ass}  ({round(nb_missing_ass/len(all_assemblies)*100,1)}%) {reason}\n'

    string += f'\n### Species identification:\n'

    string += f' * {len(unambiguous_assembly_identified)} ({round(len(unambiguous_assembly_identified)/len(all_assemblies)*100,1)}%) genomes identified at the species level\n'

    display(Markdown(string))


def get_missing_assemblies_reasons(summary_region, all_assemblies):
    pairs_main_orga_len_filter = summary_region['pairs_main_orga_post_len_filtering']
    all_pairs = summary_region['all_pairs']
    # assembly where the region has been found properly
    assemblies_retrieved = {get_assembly_name(p['genome']) for p in pairs_main_orga_len_filter}
    all_assembly_with_pair = {get_assembly_name(p['genome']) for p in all_pairs}

    missing_assemblies = all_assemblies - set(assemblies_retrieved)

    # missing assembly because of missing cog
    cog1 = summary_region["cog1"]
    cog2 = summary_region["cog2"]

    cog1_assemblies = set(summary_region[cog1]['genome_set'])
    # cog1_assemblies = {capture_assemby_name.search(
    #    genome_name).group(1) for genome_name in cog1_genomes}

    cog2_assemblies = set(summary_region[cog2]['genome_set'])
    # cog2_assemblies = {capture_assemby_name.search(
    #    genome_name).group(1) for genome_name in cog2_genomes}

    cog_missing_assemblies = all_assemblies - (cog2_assemblies & cog1_assemblies)
    missing_assemblies_fragmented = all_assemblies - \
        (all_assembly_with_pair | cog_missing_assemblies)
    #print("info_missing_assemblies", len(missing_assemblies_fragmented))
    # missing assemblies different organisation
    pairs_main_orga = summary_region['pairs_main_orga']
    assemblies_with_main_orga = {get_assembly_name(p['genome']) for p in pairs_main_orga}
    # print('genome_with_main_orga', len(genome_with_main_orga))
    # missing assemblies because can't get the distance between the two cogs (fragmented genomes)
    assemblies_with_alternative_orga = all_assemblies - \
        (assemblies_with_main_orga | cog_missing_assemblies | missing_assemblies_fragmented)
    # print('assemblies_with_alternative_orga',len(assemblies_with_alternative_orga) )

    # missing assembly due to exceeding length
    pairs_main_orga_length_filtered = summary_region['pairs_main_orga_post_len_filtering']
    assemblies_with_length_filtered = {get_assembly_name(
        p['genome']) for p in pairs_main_orga_length_filtered}

    assemblies_with_length_exceeded = all_assemblies - \
        (assemblies_with_alternative_orga | cog_missing_assemblies |
         assemblies_with_length_filtered | missing_assemblies_fragmented)

    unambiguous_assembly_identified = summary_region['assemblies_identified_at_species_lvl']

    ambigous_assembly_identification = set(assemblies_retrieved) - unambiguous_assembly_identified

    max_length_cutoff = summary_region['max_length_cutoff']
    length_reason = f'region length > {human_format(max_length_cutoff)}B'

    missing_assembly_reasons = {"missing gene": cog_missing_assemblies,
                                "missing info": missing_assemblies_fragmented,
                                'alternative orientation': assemblies_with_alternative_orga,
                                length_reason: assemblies_with_length_exceeded}

    return missing_assemblies, missing_assembly_reasons, unambiguous_assembly_identified


def adjust_size(value):
    return 1
    return int(np.log2(value))


def annotate_tree(tree, rank_name_to_display, assemblies_to_display, unambiguous_assemblies, assembly_to_lineage_dict, column):
    color_gradient = ['#ceffb3',
                      '#e0ffb3',
                      "#f2ffb3",
                      '#fffbb3',
                      "#ffe9b3",
                      '#ffd7b3',
                      "#ffc5b3"
                      '#ffc5b3', ][::-1]

    color_gradient = ["#ffe9b3"]
    maxi_color = '#9ff8ae'
    mini_color = '#ffb4b4'

    possible_rank_names = ["seq", 'tax_name', 'species', 'genus', 'family', 'order',
                           'class', 'phylum', 'kingdom', 'superkingdom']
    if not rank_name_to_display:
        rank_name_to_display = possible_rank_names
    ranks = [possible_rank_names.index(name) for name in rank_name_to_display]
    unambiguous_rank_count = defaultdict(int)
    total_rank_count = defaultdict(int)

    extrem_rank = min(ranks)
    print("extrem_rank", extrem_rank)
    for rank in ranks:
        for assembly, lineage in assembly_to_lineage_dict.items():
            rank_taxon = lineage[rank]
            total_rank_count[rank_taxon] += 1
            if assembly in unambiguous_assemblies:
                unambiguous_rank_count[rank_taxon] += 1

    rank_to_stat = {taxon: f"{unambiguous_rank_count[taxon]}/{total_rank_count[taxon]} ({unambiguous_rank_count[taxon]/total_rank_count[taxon]*100:.0f}%)" for taxon in total_rank_count}

    # pp.pprint(rank_to_rank_stat)
    # pp.pprint(assembly_to_short_lineage)
    # rank_stat_to_short_lineage = {}

    taxon_lvl = {taxon: int(
        round((unambiguous_rank_count[taxon]/total_rank_count[taxon])*(len(color_gradient)-1))) for taxon in unambiguous_rank_count}

    # nodes_style_lvls = [NodeStyle(fgcolor=color_gradient[lvl]) for lvl in range(10)]

    #{print(f'{taxon}: {missing_rank_count[taxon]}/{total_rank_count[taxon]}') for taxon in  missing_rank_count}

    for leave in tree.get_leaves():
        if leave.name in rank_to_stat:
            stat = rank_to_stat[leave.name]
            resol_tax = unambiguous_rank_count[leave.name]/total_rank_count[leave.name]
            if resol_tax == 0:
                color = mini_color
            elif resol_tax == 1:
                color = maxi_color
            else:
                missing_level = round((resol_tax)*(len(color_gradient)-1))
                color = color_gradient[missing_level]

            tface = TextFace(stat, fgcolor='black', fsize=10)  # +
            # adjust_size(total_rank_count[leave.name]))
            tface.background.color = color
            tface.border.width = 0
            tface.hz_align = 1
            tface.margin_right = 4
            tface.margin_left = 4

            leave.add_face(tface, column=column, position="aligned")  # ,  position="aligned")


def tronc_lineage(rank_name_to_display, assembly_to_lineage_dict):
    possible_rank_names = ['seq', 'tax_name', 'species', 'genus', 'family', 'order',
                           'class', 'phylum', 'kingdom', 'superkingdom']
    if not rank_name_to_display:
        return assembly_to_lineage_dict
    ranks = [possible_rank_names.index(name) for name in rank_name_to_display]

    extrem_rank = min(ranks)
    assembly_to_short_lineage = {a: l[extrem_rank:] for a, l in assembly_to_lineage_dict.items()}
    return assembly_to_short_lineage


def get_taxo_tree(assemblies_to_display, assembly_to_short_lineage, taxon_to_display, rank_name_to_display, assembly_to_lineage_dict, adjust_size):

    possible_rank_names = ['seq', 'tax_name', 'species', 'genus', 'family', 'order',
                           'class', 'phylum', 'kingdom', 'superkingdom']

    tree = blast_analysis_fct.get_tree_from_sub_graph_nodes(
        assemblies_to_display, assembly_to_short_lineage)

    ranks = [possible_rank_names.index(name) for name in rank_name_to_display]
    rank_count = defaultdict(int)
    for rank in ranks:
        for assembly, lineage in assembly_to_lineage_dict.items():
            rank_taxon = lineage[rank]

            if assembly in assemblies_to_display:
                rank_count[rank_taxon] += 1

    for node in tree.traverse():
        if node.name in rank_count:
            stat = rank_count[node.name]

            tface = TextFace(text=f"{node.name} ({stat})", fsize=10 + adjust_size(stat))
            tface.margin_right = 5
            node.add_face(tface, column=1)
    return tree


def get_sub_graph_location(sub_graph_size, remaining_spaces, edgings, min_ratio):
    """
    Equations:

    min_length * max_length = sub_graph_size

    min_length/max_length >= min_ratio

    Finally:
    min_len >= srqt(sub_graph_size*min_ratio)
    max_len <= srqt(sub_graph_size/min_ratio)
    """
    # print( '    ' ,'<-->'*10)
    for s in sorted(remaining_spaces, key=lambda k: k['row']):
        pass
        # print('   ', s)
    # print( '    ' ,'<-->'*10)

    remove_last_space = False
    min_side_ss_graph = ceil(sqrt(sub_graph_size * min_ratio))
    max_side_ss_graph = ceil(sub_graph_size/min_side_ss_graph)

    # print(f'  Graph size={ sub_graph_size}')

    for space in sorted(remaining_spaces, key=lambda k: k['row']):
        h = space['h']
        w = space['w']
        # print(f'    ({space["row"]}, {space["col"]}), h={h}, w={w}, w*h={w*h}')
        # print(f'    min space side ({min([h,w])}) should be bigger than sqrt(sub_graph_size * min_ratio) ({sqrt(sub_graph_size * min_ratio)})')

        # min space side >= min allowed ss graph side
        if h*w >= sub_graph_size and min([h, w]) >= min_side_ss_graph:
            # The space is big enough for the sub_graph
            # print('    SPACE ENOUGH')
            remove_last_space = True
            break
        else:
            # check next space
            pass

    if remove_last_space:
        compatible_space = space
        remaining_spaces.remove(space)

    else:
        # create a space of height of the sub graph and with maximum width
        # This space will be used by the sub graph
        # and what remain will be added to remaining space list
        compatible_space = {'row': edgings['row'],
                            "col": 0,
                            'w': edgings['col'],
                            'h': edgings['row'] + min_side_ss_graph}

        # new edging row is defined
        edgings['row'] += edgings['row'] + min_side_ss_graph

    # Let's define location of the subgraph
    graph_row = compatible_space['row']
    graph_col = compatible_space['col']

    # meaning if space width > max_length
    if compatible_space['w'] > ceil(sqrt(sub_graph_size/min_ratio)):
        # prioritize big w over big h but still in respect of the ratio
        graph_w = max_side_ss_graph
    else:
        # side w is equal or lower than max len
        # then of side w is allocated to the subgraph
        graph_w = compatible_space['w']

    # we know the width now let's calculate the corresponding h
    graph_h = ceil(sub_graph_size/graph_w)

    graph_location = {'row': graph_row, "col": graph_col, 'w': graph_w, 'h': graph_h}

    # define the new remaining spaces and add it to the remaining space list.
    # print("  graph_row", graph_row, "graph_h", graph_h, "edgings[row]", edgings['row'])
    if graph_h < compatible_space['h']:
        # An empty space exists between the created subgraph and the edging row of the space
        # Let's characterize it
        # print('     new space on the edging row ')
        new_space_row = graph_row + graph_h
        new_space_col = graph_col
        # redefinded
        new_space_h = compatible_space['row'] + compatible_space['h'] - new_space_row
        new_space_w = compatible_space['col'] + compatible_space['w'] - new_space_col

        new_space = {'row': new_space_row, "col": new_space_col, 'w': new_space_w, 'h': new_space_h}

        if new_space not in remaining_spaces:

            remaining_spaces.append(new_space)
        else:
            # print('AAAA, SPACE ALREADY FOUND ! ')
            raise
        # print(f"    {new_space}")

    # print("  graph_col", graph_col, "graph_w", graph_w, "edgings['col']", edgings['col'])
    if graph_w < compatible_space['w']:
        # An empty space exists between the created subgraph and the edging col of the compatible space
        # Let's characterize it
        # print('    new space on the edging col ')
        new_space_row = graph_row
        new_space_col = graph_col + graph_w
        new_space_h = compatible_space['row'] + graph_h - new_space_row
        new_space_w = compatible_space['col'] + compatible_space['w'] - new_space_col

        new_space = {'row': new_space_row, "col": new_space_col, 'w': new_space_w, 'h': new_space_h}
        # print(f"    {new_space}")
        if new_space not in remaining_spaces:
            remaining_spaces.append(new_space)
        else:
            # print('AAAA, SPACE ALREADY FOUND ! ')
            raise

    return graph_row, graph_col, graph_w, graph_h


def display_sub_graphs_proportionally(G):

    sub_graphs = sorted(list(nx.connected_components(G)), key=len, reverse=True)

    nb_sub_graph = len(sub_graphs)

    max_nb_nodes = max((len(sub_graph) for sub_graph in sub_graphs))

    # sub graph size go from 1 to max_square_size
    max_square_size = 100
    max_length_side = int(sqrt(max_square_size))
    # print("max_length_side", max_length_side)

    # divide graph size by the scalling value to find their square size
    scalling_value = max_nb_nodes/max_square_size

    logging.info(f"scalling_value ({scalling_value}) = max_nb_nodes ({max_nb_nodes})/ max_square_size ({max_square_size})")

    # minimaw nb of sub graph by row = at least this number of sub graph by row
    min_sub_graphs_by_row = 4

    # Cols size is the max length size possible for a sub graph multiply by the minimaw number of subgraph
    nb_cols = min_sub_graphs_by_row * max_length_side

    # row size is the number of subgraph divide by the minimaw number of subgraph by row multiply the max side length
    # row size is overestimated
    nb_rows = int((nb_sub_graph / min_sub_graphs_by_row) * max_length_side) + 1

    # minimaw ratio of smallest_side/biggest_side. 4/5 = 0.8
    min_sub_graph_ratio = 1

    # print('nb sub graph',nb_sub_graph )
    # print('rows', nb_rows, 'cols', nb_cols)

    fig = plt.figure(figsize=(20, 100), dpi=80, facecolor='w', edgecolor='k')
    gridspec.GridSpec(nb_rows, nb_cols)
    row_i = 0
    col_i = 0
    remaining_spaces = []
    edgings = {"row": 0, "col": nb_cols}
    ss_pos = []
    for sub_graph_nodes in sub_graphs:
        # print("="*10)

        nb_nodes = len(sub_graph_nodes)
        sub_graph_scalled_size = ceil(nb_nodes/scalling_value)

        # print('nb nodes',nb_nodes )
        # print('size graph scalled', sub_graph_scalled_size)
        # row_i = row_i + sub_graph_len_side

        graph_row, graph_col, graph_w, graph_h = get_sub_graph_location(
            sub_graph_scalled_size, remaining_spaces, edgings, min_sub_graph_ratio)
        # print("graph_row",graph_row, "graph_col", graph_col, "graph_w",graph_w, "graph_h" , graph_h)

        # input()
        # logging.warning(f'{graph_row},{graph_col}')
        sub_graph = nx.subgraph(G, sub_graph_nodes)
        ss_pos.append((graph_row, graph_col))

        plt.subplot2grid((nb_rows, nb_cols), (graph_row, graph_col),
                         colspan=graph_w, rowspan=graph_h)

        nx.draw(sub_graph, node_size=100)


def count_sub_graph(G):
    return len(list(nx.connected_components(G)))


def display_sub_graphs(G):
    fig = plt.figure(figsize=(18, 16), dpi=80, facecolor='w', edgecolor='k')
    sub_graphs = sorted(list(nx.connected_components(G)), key=len, reverse=True)

    nb_sub_graph = len(sub_graphs)
    nb_rows = int(sqrt(nb_sub_graph))
    nb_cols = int(nb_sub_graph/nb_rows)
    print('nb sub graph', nb_sub_graph)
    print('rows', nb_rows, 'cols', nb_cols)

    gridspec.GridSpec(nb_rows, nb_cols)
    for i, sub_graph_nodes in enumerate(sub_graphs):
        sub_graph = nx.subgraph(G, sub_graph_nodes)

        row_i = int(i/nb_cols)
        col_i = i % nb_cols
        plt.subplot2grid((nb_rows, nb_cols), (row_i, col_i))

        nx.draw_spring(sub_graph, node_size=150)
