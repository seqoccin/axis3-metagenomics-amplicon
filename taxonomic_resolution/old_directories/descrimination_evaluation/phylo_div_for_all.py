#!/usr/bin/env python3


import sys
import os
import blast_analysis_fct as analysis
from collections import defaultdict
module_path = os.path.abspath("./../scripts/")
sys.path.append(module_path)
import tools
import re
import networkx as nx

cov_threshold = 90
pident_thresholds = [60, 70, 75, 80, 85, 90, 95, 98, 100]
outfmt = 'qseqid qstart qend qlen sseqid sstart send slen length pident qcovhsp qcovs evalue bitscore'.split(
    ' ')
preprocess_dir = 'all_regions_analysis/preprocessed_blast_results/'
tsv_graph_out = "all_regions_analysis/graph_in_tsv/"
capture_cog_pair = re.compile("(COG\d+-COG\d+)")

# Get info from genome of the sequences in sub_graph
selected_assemblies_fl = 'genome_taxonomy/selected_assemblies.tsv'
assembly_dict = tools.tsv_to_dict_of_dicts(selected_assemblies_fl, "assembly_accession")

# preprocess file with only taxid under study
ranked_linage_file = "genome_taxonomy/selected_taxids_ranked.txt"
taxid_ranked = analysis.get_taxid_ranked_lineage(ranked_linage_file)

pd_g_dict = {}
pre_result_iter = [os.path.join(preprocess_dir, f) for f in os.listdir(preprocess_dir)]
print('COV', cov_threshold)

result_by_pident = defaultdict(dict)
for result_file in pre_result_iter:

    cog_pair = capture_cog_pair.search(os.path.basename(result_file)).group(1)
    print(cog_pair)
    for pident_threshold in pident_thresholds:
        G = analysis.get_graph_from_blast_result(
            result_file, cov_threshold, pident_threshold, outfmt)
        #analysis.nx_graph_to_tsv(G, os.path.join(tsv_graph_out, 'graph'+cog_pair+'.tsv'))
        node_to_lineage = analysis.get_node_to_lineage_dict(
            nx.nodes(G), taxid_ranked, assembly_dict)
        pd_g = analysis.get_global_phylo_div(G, node_to_lineage)
        print(' ', pident_threshold, pd_g)
        result_by_pident[pident_threshold][cog_pair] = pd_g


file_out = f"all_regions_analysis/cog_pairs_PD_G_cov{90}.json"

tools.encoder(file_out, result_by_pident)
# for pident, pd_g_dict in result_by_pident.items():
#     file_out = f"all_regions_analysis/cog_pairs_PD_G_cov{cov_threshold}_pident{pident}.json"
#     print(file_out)
#
