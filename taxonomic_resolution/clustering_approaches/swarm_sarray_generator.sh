
seqdir=$1
seq_type=$2
t=2
dir_name=$(basename -- "$seqdir")
result_dir="swarm/$seq_type/$dir_name/"
mkdir -p $result_dir

for f in $seqdir/no_ambiguous_seq_*;
do  
    
    filename=$(basename -- "$f")
    filesimplename="${filename%.*}"
    
    region=${filesimplename#no_ambiguous_seq_}
    
    echo "module load bioinfo/swarm-2.2.2;swarm -t $t -a 1  $f >  $result_dir/${filesimplename}.cluster 2> $result_dir/${filesimplename}.log"

done
