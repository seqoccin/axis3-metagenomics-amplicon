#!/usr/bin/env python3

"""
Process swarm clusters result to compute taxonomic resolution.

:Example:

"""

# Metadata
__author__ = 'Mainguy Jean - Plateforme bioinformatique Toulouse'
__copyright__ = 'Copyright (C) 2019 INRA'
__license__ = 'GNU General Public License'
__version__ = '0.1'
__email__ = 'support.bioinfo.genotoul@inra.fr'
__status__ = 'dev'


from argparse import ArgumentParser, ArgumentDefaultsHelpFormatter
import logging
import csv
import os

def parse_swarm_result(cluster_file, assembly_to_rank):
    """Parse swarm clustering."""
    cluster = 0
    cluster_homogeneous_assembly = 0
    cluster_homogeneous_sp = 0
    cluster_homogeneous_genus = 0
    cluster_homogeneous_family = 0
    singleton = 0
    seq = 0
    ambiguous_sp_set = set()
    all_sp_set = set()
    ambiguous_genus_set = set()
    all_genus_set = set()

    ambiguous_family_set = set()
    all_family_set = set()

    with open(cluster_file) as fl:
        for cluster_line in fl:
            seqs = cluster_line.split(' ')
            seq += len(seqs)
            assemblies = {seq.split('|')[0] for seq in seqs}

            species = {assembly_to_rank['species'][a] for a in assemblies}
            genus = {assembly_to_rank['genus'][a] for a in assemblies}
            family = {assembly_to_rank['family'][a] for a in assemblies}

            cluster += 1
            singleton += 1 if len(seqs) == 1 else 0
            cluster_homogeneous_assembly += 1 if len(assemblies) == 1 else 0
            cluster_homogeneous_sp += 1 if len(species) == 1 else 0
            cluster_homogeneous_genus += 1 if len(genus) == 1 else 0
            cluster_homogeneous_family += 1 if len(family) == 1 else 0

            if len(species) != 1:
                ambiguous_sp_set |= species
            all_sp_set |= species

            if len(genus) != 1:
                ambiguous_genus_set |= genus
            all_genus_set |= genus

            if len(family) != 1:
                ambiguous_family_set |= family
            all_family_set |= family

    info = {
        "singleton": singleton,
        "cluster": cluster,
        'cluster_assembly_homogeneous': cluster_homogeneous_assembly,
        "cluster_sp_homogeneous": cluster_homogeneous_sp,
        "cluster_genus_homogeneous": cluster_homogeneous_genus,
        "cluster_family_homogeneous": cluster_homogeneous_family,
        "ambiguous_species": len(ambiguous_sp_set),
        "unambiguous_species": len(all_sp_set) - len(ambiguous_sp_set),
        "species": len(all_sp_set),
        "sequence": seq
    }
    return info


def count_seq_faste(fasta_file):
    with open(fasta_file) as fl:
        return len([l for l in fl if l.startswith('>')])


def list_of_dict_to_tsv(list_of_dict, output_file, mode='w', writeheader=True):
    """Encode a list of dictionnary into a tsv file.

    The mode of writing can be specified.
    By default the mode is set to "w"

    """
    if len(list_of_dict) == 0:
        raise ValueError('The given list is empty: we cant write it in a file')

    with open(output_file, mode, newline='') as csvfile:
        writer = csv.DictWriter(csvfile, fieldnames=list(list_of_dict[0].keys()), delimiter='\t')
        if mode == 'w' and writeheader is True:
            writer.writeheader()
        for dict_info in list_of_dict:
            writer.writerow(dict_info)


def parse_arguments():
    parser = ArgumentParser(description="...",
                            formatter_class=ArgumentDefaultsHelpFormatter)
    parser.add_argument(
        "-o", "--output", help="Stat file containing information on clusters..", required=True)
    parser.add_argument("--assembly_selection", help="Assembly selection file.", required=True)
    parser.add_argument("--results",  nargs='+', help="", required=True)
    parser.add_argument("--sequences",  nargs='+',
                        help="sequences used for clustering.. with swarm ambiguous seq and no ambiguous seq are required", required=True)
    parser.add_argument("-v", "--verbose", help="increase output verbosity",
                        action="store_true")

    args = parser.parse_args()
    return args


def main():

    args = parse_arguments()

    if args.verbose:
        logging.basicConfig(format="%(levelname)s: %(message)s", level=logging.DEBUG)
        logging.info('Mode verbose ON')

    else:
        logging.basicConfig(format="%(levelname)s: %(message)s")

    output = args.output

    results = args.results
    cluster_infos = []

    assembly_summary = args.assembly_selection
    sequences_files = args.sequences

    with open(assembly_summary) as fl:
        reader = csv.DictReader(fl, delimiter='\t')
        assembly_infos = {d['assembly_accession']: d for d in reader}

    # Example of taxonomy field:
    # Campylobacter coli [tax_name];Campylobacter coli [species];Campylobacter [genus];Campylobacteraceae [family];Campylobacterales [order];Epsilonproteobacteria [class];Proteobacteria [phylum];Bacteria other

    assembly_to_rank = {}
    assembly_to_rank['species'] = {a: info['taxonomy'].split(
        ';')[1] for a, info in assembly_infos.items()}
    assembly_to_rank['genus'] = {a: info['taxonomy'].split(
        ';')[2] for a, info in assembly_infos.items()}
    assembly_to_rank['family'] = {a: info['taxonomy'].split(
        ';')[3] for a, info in assembly_infos.items()}

    total_sp = len({info['species_taxid'] for info in assembly_infos.values()})
    logging.info(f'{total_sp} species found in assembly summary {assembly_summary}')
    # logging.info(sequences_files)

    for result in results:

        clusters_info = parse_swarm_result(result, assembly_to_rank)
        name = ''.join(os.path.basename(result).split('.')[:-1])
        region = name[len('no_ambiguous_seq_'):]
        logging.info(region)
        ambiguous_seq = [seq for seq in sequences_files if os.path.basename(seq) == f'ambiguous_seq_{region}.fna'][0]
        no_ambiguous_seq = [seq for seq in sequences_files if os.path.basename(seq) == f'no_ambiguous_seq_{region}.fna'][0]
        ambiguous_seq_count = count_seq_faste(ambiguous_seq)
        no_ambiguous_seq_count = count_seq_faste(no_ambiguous_seq)

        clusters_info['ambiguous_seq'] = ambiguous_seq_count
        clusters_info['no_ambiguous_seq'] = no_ambiguous_seq_count
        clusters_info['region'] = region
        err = f'number of seq found in clusters {result} is different with seq found in seq file {no_ambiguous_seq}'
        assert no_ambiguous_seq_count == clusters_info['sequence'], err
        cluster_infos.append(clusters_info)

        # logging.info('==='*10)
        logging.info('==='*5 + f'region {region}' + '==='*5)
        [logging.info(f'{k}: {v}') for k, v in  clusters_info.items()]

    logging.info(f'write cluster_infos ({len(cluster_infos)} lines) into {output}')
    list_of_dict_to_tsv(cluster_infos, output)


if __name__ == '__main__':
    main()
