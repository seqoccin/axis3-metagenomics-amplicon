
seqdir=$1
seq_type=$2
memory=4000
threads=4
dir_name=$(basename -- "$seqdir")
result_dir=cd-hit/$seq_type/$dir_name/
mkdir -p $result_dir

for identity_cutoff in 0.95 0.96 0.97 0.98 0.99 1;
do

    #echo ">> $f"
    command='module load bioinfo/cdhit-4.6.8'
        for f in $seqdir/*;
        do
        #echo "  $identity_cutoff"
        mkdir -p $result_dir/$identity_cutoff
        
        filename=$(basename -- "$f")
        filesimplename="${filename%.*}"
        single_command="cd-hit-est -i $f -o $result_dir/$identity_cutoff/$filesimplename  -c $identity_cutoff -n 10 -d 0 -M $memory -T $threads > $result_dir/$identity_cutoff/$filesimplename.out"
       command="${command};$single_command"
       #echo $single_command
       #sleep 1
  done
  echo $command
done
