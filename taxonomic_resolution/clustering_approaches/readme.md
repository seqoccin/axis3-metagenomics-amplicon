# Clustering approaches

## Sequence to cluster
Use sequences extracted by the region identification piepline based on assembly selection `rep_ref_and_10_assemblies_per_sp.tsv`

### Selection of sequence of interest

Retrieve sequence from pipeline with symb link. Remove 16S-16S region and add 16S single

```
mkdir sequences/rep_ref_and_10_assemblies_per_sp/
ln -s /home/jmainguy/metaG/marker_identification/region_identification_workflow/bacteria_investigation/results_extended_with_16S/region_extracted/* sequences/rep_ref_and_10_assemblies_per_sp/
rm sequences/rep_ref_and_10_assemblies_per_sp/16S-16S.fna
ln -s /home/jmainguy/metaG/marker_identification/region_identification_workflow/bacteria_investigation/results_extended_with_16S/single_gene_extracted/16S.fna sequences/rep_ref_and_10_assemblies_per_sp/

```

Sequences retrieved have been extracted based on selection `rep_ref_and_10_assemblies_per_sp.tsv`

Select sequences from selection `one_assembly_per_sp.tsv`

Use script `filter_seq_based_on_assembly_list.py` to sub select all sequences that belong to an assembly selection of interest

```
mkdir sequences/one_assembly_per_sp

 python ../tax_resolution_test/filter_seq_based_on_assembly_list.py --assembly_summary assembly_selections/one_assembly_per_sp.tsv -s sequences/rep_ref_and_10_assemblies_per_sp/* -o sequences/one_assembly_per_sp/ -v

```
Select assembly where 16S is found:
Create assembly summary according this new selection :
```
grep ^'>' sequences/one_assembly_per_sp/16S.fna | cut -f2 -d'>' | cut -f1 -d'|' | sort | uniq > one_seq_per_assembly_with_16S.assembly_list

python ../16S_investigation/assembly_list_to_assembly_summary_file.py --assembly_summary assembly_selections/ref_and_rep_assemblies.tsv --assembly_list one_seq_per_assembly_with_16S.assembly_list --out_assembly_summary ref_and_rep_assemblies_with_16S.tsv

python ../16S_investigation/assembly_list_to_assembly_summary_file.py --assembly_summary assembly_selections/one_assembly_per_sp.tsv --assembly_list one_seq_per_assembly_with_16S.assembly_list --out_assembly_summary assembly_selections/one_assembly_per_sp_with_16S.fna

```

Reuse script `filter_seq_based_on_assembly_list.py` to select sequences of the new assembly selection of the 16S..

```
python ../tax_resolution_test/filter_seq_based_on_assembly_list.py --assembly_summary assembly_selections/one_assembly_per_sp_with_16S.fna -s sequences/one_assembly_per_sp/* -o sequences/ -v

```

TODO :
Keep only one seq per assembly:
```
$ python ../../16S_investigation/keep_one_sequence_per_assembly.py -h

```

command
```
python ../16S_investigation/keep_one_sequence_per_assembly.py -s sequences/one_assembly_per_sp_with_16S/* -o sequences/one_assembly_per_sp_with_16S_one_seq_per_assembly -v 2> log_one_seq_per_assembly.log

```

## cd hit
`module load bioinfo/cdhit-4.6.8`

Use of CD-HIT-EST
Git hub of the tool  https://github.com/weizhongli/cdhit/wiki/3.-User's-Guide#cd-hit-est

manual http://weizhongli-lab.org/lab-wiki/doku.php?id=cd-hit-user-guide

Test command :
```
cd-hit-est -i ref_and_rep_assemblies_and_16S_complete_sequence/COG0052-COG0264.fna -o COG0052-COG0264_95 -c 0.95 -n 10 -d 0 -M 2000 -T 1
```

```bash
module load bioinfo/cdhit-4.6.8
memory=4000
threads=4
identity_cutoff=95
```

```bash
seqdir="../sequences/ref_and_rep_one_assembly_per_sp_and_16S_complete"
# OR
seqdir="../sequences/one_assembly_per_sp_and_16S_complete/"

seqdir="../sequences/one_assembly_per_sp_with_16S_one_seq_per_assembly/"

```

```bash
dir_name=$(basename -- "$seqdir")
result_dir=$dir_name/
mkdir -p $result_dir

for identity_cutoff in 0.95 0.96 0.97 0.98 0.99 1;
do
  echo $identity_cutoff
  mkdir $result_dir/$identity_cutoff
  result_file=$result_dir/$identity_cutoff/cluster_stat.tsv
  echo region$'\t'number_of_seq$'\t'number_of_cluster > $result_file

  for f in $seqdir/*;
  do
    filename=$(basename -- "$f")
    filesimplename="${filename%.*}"
    cd-hit-est -i $f -o $result_dir/$identity_cutoff/$filesimplename  -c $identity_cutoff -n 10 -d 0 -M $memory -T $threads > $result_dir/$identity_cutoff/$filesimplename.out
    nb_cluster=`grep ^'>' -c $result_dir/$identity_cutoff/$filesimplename`
    nb_initial_seq=`grep ^'>' -c $f`
    echo $filesimplename$'\t'$nb_initial_seq$'\t'$nb_cluster >> $result_file
    tail -n1  $result_file
  done
done
```
Create a sarray with the command
one line per sequence with the different identity ...
Create only cd hit cluster... the file are going to be process later on with a python script;..

Sarray generator : `sarray cdhit_one_assembly_per_sp_with_16S.sarray `
```
bash cd_hit_sarray_generator.sh sequences/one_assembly_per_sp_with_16S/ > cdhit_one_assembly_per_sp_with_16S.sarray

```

## Swarm

`module load bioinfo/swarm-2.2.2`

Git hub of the tool https://github.com/torognes/swarm

Swarm fail when ambiguous nt are found in sequences:

Remove those sequences with script : `remove_ambiguous_sequences.py`

Example :
```
 python ../remove_ambiguous_sequences.py -f ../sequences/one_assembly_per_sp_with_16S_one_seq_per_assembly/* -o ../sequences/ref_and_rep_assemblies_with_16S_one_seq_per_assembly_ambiguous_filtering/ -v

```
Check correctness of the script with simple grep command that grep ambiguous sequences..
```bash
grep -E '^[^>].*(S|M|K|R|Y|B|D|H|W|V|N|Z).*' -B1 ../sequences/ref_and_rep_assemblies_and_16S_complete/COG0089-COG0185.fna

# Count ambiguous sequences in all file of dir sequenes
grep -E '^[^>].*(S|M|K|R|Y|B|D|H|V|W|N|Z).*' sequences/ref_and_rep_assemblies_and_16S_complete/* -c


```
```
module load bioinfo/swarm-2.2.2

seqdir=../sequences/ref_and_rep_assemblies_with_16S_one_seq_per_assembly_ambiguous_filtering/
t=4
dir_name=$(basename -- "$seqdir")
result_dir=$dir_name/
mkdir -p $result_dir
```

For loop to generate swarm result...
```bash
module load bioinfo/swarm-2.2.2

seqdir=../sequences/ref_and_rep_assemblies_with_16S_one_seq_per_assembly_ambiguous_filtering/
t=4
dir_name=$(basename -- "$seqdir")
result_dir=$dir_name/
mkdir -p $result_dir

result_file=$result_dir/cluster_stat.tsv
result_file=$result_dir/$identity_cutoff/cluster_stat.tsv
echo region$'\t'number_of_seq$'\t'number_of_cluster$'\t'nb_ambiguous_seq > $result_file

for f in $seqdir/no_ambiguous_seq_*;
do  
    echo $f
    echo
    filename=$(basename -- "$f")
    filesimplename="${filename%.*}"

    region=${filesimplename#no_ambiguous_seq_}

    nb_ambiguous_seq=`grep -c ^'>' $seqdir/ambiguous_seq_${region}.fna`

    swarm -t 4 -a 1  $f >  $result_dir/${filesimplename}.cluster 2> $result_dir/${filesimplename}.log
    cat $result_dir/${filesimplename}.log

    nb_cluster=`wc -l $result_dir/${filesimplename}.cluster | cut -f1 -d' '`
    nb_seq=`grep -c ^'>' $f`

    echo $filesimplename$'\t'$nb_seq$'\t'$nb_cluster$'\t'$nb_ambiguous_seq >> $result_file


    tail -n1 $result_file
    echo ">>>>>>>>>>>>>><<<<<<<<<<<<<<<<<"
done
```
sarray version
```
module load bioinfo/swarm-2.2.2

seqdir=../sequences/ref_and_rep_assemblies_with_16S_one_seq_per_assembly_ambiguous_filtering/
t=4
dir_name=$(basename -- "$seqdir")
result_dir=$dir_name/
mkdir -p $result_dir

result_file=$result_dir/cluster_stat.tsv
result_file=$result_dir/$identity_cutoff/cluster_stat.tsv
echo region$'\t'number_of_seq$'\t'number_of_cluster$'\t'nb_ambiguous_seq > $result_file

for f in $seqdir/no_ambiguous_seq_*;
do  
    echo $f
    echo
    filename=$(basename -- "$f")
    filesimplename="${filename%.*}"

    region=${filesimplename#no_ambiguous_seq_}

    nb_ambiguous_seq=`grep -c ^'>' $seqdir/ambiguous_seq_${region}.fna`

    swarm -t 4 -a 1  $f >  $result_dir/${filesimplename}.cluster 2> $result_dir/${filesimplename}.log
    cat $result_dir/${filesimplename}.log

    nb_cluster=`wc -l $result_dir/${filesimplename}.cluster | cut -f1 -d' '`
    nb_seq=`grep -c ^'>' $f`

    echo $filesimplename$'\t'$nb_seq$'\t'$nb_cluster$'\t'$nb_ambiguous_seq >> $result_file


    tail -n1 $result_file
    echo ">>>>>>>>>>>>>><<<<<<<<<<<<<<<<<"
done
```
## Usearch
Load module usearch `module load bioinfo/usearch11.0.667`

From Method of the article Evaluation of 16S rRNA gene sequencing for species and strain-level microbiome analysis
https://www.nature.com/articles/s41467-019-13036-1#Sec21

>To create OTUs, in-silico amplicon datasets generated for each sub-region were filtered to remove non-unique sequences and re-ordered to correspond with the sequence order in the V1–V9 dataset. Each amplicon was assigned a unitary abundance and OTUs were generated at a variety of similarity thresholds (97%, 98%, and 99%) using the USEARCH command cluster_otus, with chimera detection disabled using the option -uparse_break −999.

Generating OTUs https://www.drive5.com/usearch/manual/pipe_otus.html

```
usearch -fastx_uniques test/COG0052-COG0264.fna -fastaout uniques.fa -sizeout -relabel Uniq

```

Seems complicated to perform a clustering different from 97%.
Solution here with https://www.drive5.com/usearch/manual/uparse_otu_radius.html

```

usearch -unoise3 uniques.fa -zotus otus100.fa

for id in 99 97 95 90
do
   usearch -cluster_smallmem otus100.fa -id 0.$id -centroids otus$id.fa
done
```

Using uclust algorithm.
With cluster_smallmem:
`usearch -cluster_smallmem uniques.fa -id 0.99 -centroids otus99.fa`
Error with cluster_smallmem:
```
---Fatal error---
Not sorted by length, see -sortedby option in manual
```
Need to sort sequence..

With cluster_fast sort can be specified in the command..

`usearch -cluster_fast uniques.fa -id 0.99 -centroids otus99.fa -sort length`

## Analyse clustering results
using the script `process_clustering_results.py` to analyse cluster results

With swarm
```
python process_clustering_results.py --assembly_selection assembly_selections/rep_ref_and_10_assemblies_per_sp.tsv --clustering_tool swarm --results swarm/test/no_ambiguous_seq_16S-23S.cluster --sequences swarm/test/*fna -o t
```

```
#Swarm
python process_swarm_results.py --assembly_selection assembly_selections/ref_and_rep_assemblies_with_16S.tsv --results swarm/ref_and_rep_assemblies_with_16S/no_ambiguous_seq_*.cluster --sequences swarm/clean_sequences/ref_and_rep_assemblies_with_16S/* --clustering_tool swarm -v -o t
#cd-hit
python process_cdhit_results.py --assembly_selection assembly_selections/one_assembly_per_sp_with_16S.tsv --results cd-hit/one_assembly_per_sp_with_16S/*/*clstr --sequences_dir sequences/one_assembly_per_sp_with_16S/ -o cdhit_stat_one_assembly_per_sp_with_16S.tsv -v

```
