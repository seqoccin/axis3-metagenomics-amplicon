#!/usr/bin/env python3

"""
Description

:Example:

"""

# Metadata
__author__ = 'Mainguy Jean - Plateforme bioinformatique Toulouse'
__copyright__ = 'Copyright (C) 2019 INRA'
__license__ = 'GNU General Public License'
__version__ = '0.1'
__email__ = 'support.bioinfo.genotoul@inra.fr'
__status__ = 'dev'


from argparse import ArgumentParser, ArgumentDefaultsHelpFormatter
import logging
import os
import pandas as pd
import csv
import re


def parse_cdhit_result(cluster_file, assembly_to_rank):

    cluster_id = None
    line_pattern =re.compile('[\d]+\t[\d]+nt, >([^|]+).+')
    clusters = {}

    with open(cluster_file) as fl:
        for l in fl:
            if l.startswith('>Cluster'):
                cluster_id = int(l.rstrip()[len('>Cluster '):])
                clusters[cluster_id] = []
            elif line_pattern.match(l):
                clusters[cluster_id].append(l)
                #print(cluster_id, l)
            else:
                raise ValueError(f'line does not match pattern: {l}')

    cluster = 0
    cluster_homogeneous_assembly = 0
    cluster_homogeneous_sp = 0
    cluster_homogeneous_genus = 0
    cluster_homogeneous_family = 0
    singleton = 0
    seq = 0
    ambiguous_sp_set = set()
    all_sp_set = set()
    ambiguous_genus_set= set()
    all_genus_set= set()

    ambiguous_family_set= set()
    all_family_set= set()

    for clstr, cluster_lines in clusters.items():
        seq += len(cluster_lines)
        assemblies = {line_pattern.match(line).group(1) for line in cluster_lines}


        species = {assembly_to_rank['species'][a] for a in assemblies}
        genus = {assembly_to_rank['genus'][a] for a in assemblies}
        family = {assembly_to_rank['family'][a] for a in assemblies}

        cluster += 1
        singleton += 1 if len(cluster_lines) == 1 else 0
        cluster_homogeneous_assembly += 1 if len(assemblies) == 1 else 0
        cluster_homogeneous_sp += 1 if len(species) == 1 else 0
        cluster_homogeneous_genus += 1 if len(genus) == 1 else 0
        cluster_homogeneous_family += 1 if len(family) == 1 else 0

        if len(species) != 1:
            ambiguous_sp_set |= species
        all_sp_set |= species

        if len(genus) != 1:
            ambiguous_genus_set |= genus
        all_genus_set |= genus

        if len(family) != 1:
            ambiguous_family_set |= family
        all_family_set |= family


    info = {
        "singleton":singleton ,
        "cluster":cluster ,
        'cluster_assembly_homogeneous':cluster_homogeneous_assembly ,
        "cluster_sp_homogeneous":cluster_homogeneous_sp ,
        "cluster_genus_homogeneous":cluster_homogeneous_genus ,
        "cluster_family_homogeneous":cluster_homogeneous_family ,
        "ambiguous_species":len(ambiguous_sp_set),
        "unambiguous_species":len(all_sp_set) - len(ambiguous_sp_set),
        "species":len(all_sp_set),
        "ambiguous_genus":len(ambiguous_genus_set),
        "unambiguous_genus":len(all_genus_set) - len(ambiguous_genus_set),
        "genus":len(all_genus_set),
        "ambiguous_family":len(ambiguous_family_set),
        "unambiguous_family":len(all_family_set) - len(ambiguous_family_set),
        "family":len(all_family_set),
        "sequence":seq,
    }

    return info

def count_seq_faste(fasta_file):
    with open(fasta_file) as fl:
        return len([l for l in fl if l.startswith('>')])

def list_of_dict_to_tsv(list_of_dict, output_file, mode='w', writeheader=True):
    """Encode a list of dictionnary into a tsv file.

    The mode of writing can be specified.
    By default the mode is set to "w"

    """
    if len(list_of_dict) == 0:
        raise ValueError('The given list is empty: we cant write it in a file')

    with open(output_file, mode, newline='') as csvfile:
        writer = csv.DictWriter(csvfile, fieldnames=list(list_of_dict[0].keys()), delimiter='\t')
        if mode == 'w' and writeheader is True:
            writer.writeheader()
        for dict_info in list_of_dict:
            writer.writerow(dict_info)

def parse_arguments():
    parser = ArgumentParser(description="...",
                            formatter_class=ArgumentDefaultsHelpFormatter)
    parser.add_argument("-o", "--output", help="Stat file containing information on clusters..", required=True)
    parser.add_argument("--assembly_selection", help="Assembly selection file.", required=True)
    parser.add_argument("--results",  nargs='+', help="Result dirs with name = identity threshold", required=True)
    parser.add_argument("--sequences_dir",  help="sequences used for clustering.. with swarm ambiguous seq and no ambiguous seq are required", required=True)
    parser.add_argument("-v", "--verbose", help="increase output verbosity",
                        action="store_true")

    args = parser.parse_args()
    return args


def main():

    args = parse_arguments()

    if args.verbose:
        logging.basicConfig(format="%(levelname)s: %(message)s", level=logging.DEBUG)
        logging.info('Mode verbose ON')

    else:
        logging.basicConfig(format="%(levelname)s: %(message)s")


    output = args.output
    assembly_selection = args.assembly_selection

    results = args.results
    cluster_infos = []

    assembly_summary = args.assembly_selection
    sequences_dir = args.sequences_dir

    lines_info = []
    with open(assembly_summary) as fl:
        reader = csv.DictReader(fl, delimiter='\t')
        assembly_infos = {d['assembly_accession']: d for d in reader}

    # Example of taxonomy field:
    # Campylobacter coli [tax_name];Campylobacter coli [species];Campylobacter [genus];Campylobacteraceae [family];Campylobacterales [order];Epsilonproteobacteria [class];Proteobacteria [phylum];Bacteria other

    assembly_to_rank = {}
    assembly_to_rank['species'] = {a: info['taxonomy'].split(';')[1] for a, info in assembly_infos.items()}
    assembly_to_rank['genus'] = {a: info['taxonomy'].split(';')[2] for a, info in assembly_infos.items()}
    assembly_to_rank['family'] = {a: info['taxonomy'].split(';')[3] for a, info in assembly_infos.items()}

    total_sp = len({info['species_taxid'] for info in assembly_infos.values()})
    logging.info(f'{total_sp} species found in assembly summary {assembly_summary}')
    #logging.info(sequences_files)
    expected_dirs = ['0.95','0.96','0.97', '0.98', '0.99', '1']
    for result_fl in results:
        #logging.info(f'Parsing {result_fl}')
        result_directory = os.path.dirname(result_fl)

        if os.path.basename(result_directory) not in expected_dirs:
            logging.warning(f'Result dir {os.path.basename(result_directory)} does not match expected dir name: {expected_dirs} ')
            continue

        if not result_fl.endswith('.clstr'):
            logging.warning(f'result_fl does not end with the proper extension ".clstr" .. {result_fl}')
            continue

        identity = os.path.basename(result_directory)
        #logging.info(f'Analyse file with identity {identity}')

        clusters_info = parse_cdhit_result(result_fl, assembly_to_rank)
        #logging.info(clusters_info)
        region = ''.join(os.path.basename(result_fl).split('.')[:-1])
        #logging.info(region)

        sequence_fl = os.path.join(sequences_dir, f'{region}.fna')


        if not os.path.isfile(sequence_fl):
            raise FileNotFoundError(f'Sequence file does not exist: {sequence_fl}, is the seq directory valid?')


        seq_count =  count_seq_faste(sequence_fl)

        assert seq_count == clusters_info['sequence'], f'number of sequences found in clusters {result_fl} is different with seq found in seq file {seq_count}'
        clusters_info['identity'] = identity
        clusters_info['region'] = region
        cluster_infos.append(clusters_info)

        logging.info(f"{region} {identity}  unambiguous sp: {clusters_info['unambiguous_species']}/{clusters_info['species']}")
    logging.info(f'write cluster_infos ({len(cluster_infos)} lines) into {output}')
    list_of_dict_to_tsv(cluster_infos, output)


if __name__ == '__main__':
    main()                 
