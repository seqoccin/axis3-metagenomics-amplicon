#!/usr/bin/env python3

"""
Description

:Example:
python template.py -v
"""

# Metadata
__author__ = 'Mainguy Jean - Plateforme bioinformatique Toulouse'
__copyright__ = 'Copyright (C) 2019 INRA'
__license__ = 'GNU General Public License'
__version__ = '0.1'
__email__ = 'support.bioinfo.genotoul@inra.fr'
__status__ = 'dev'


from argparse import ArgumentParser, ArgumentDefaultsHelpFormatter
import logging
import pandas as pd
#import plotly.express as px
import numpy as np
import csv
import os
from Bio import SeqIO


def parse_arguments():
    """Parse script arguments."""
    parser = ArgumentParser(description="...",
                            formatter_class=ArgumentDefaultsHelpFormatter)
    parser.add_argument('-f', "--fasta_files", nargs='+', required=True)
    parser.add_argument('-o', "--outdir", default='./')
    parser.add_argument("-v", "--verbose", help="increase output verbosity",
                        action="store_true")

    args = parser.parse_args()
    return args


def main():

    args = parse_arguments()

    if args.verbose:
        logging.basicConfig(format="%(levelname)s: %(message)s", level=logging.DEBUG)
        logging.info('Mode verbose ON')

    else:
        logging.basicConfig(format="%(levelname)s: %(message)s")

    fasta_files = args.fasta_files

    logging.info(f'Remove ambiguous sequences')


    assembly_already_treated = set()
    # output file
    outdir = args.outdir
    
    for fasta_file in fasta_files:
        s_ambiguous = 0
        s_kept = 0
        filtered_fasta_file = os.path.join(outdir, 'no_ambiguous_seq_' + os.path.basename(fasta_file))
        ambiguous_fasta_file = os.path.join(outdir, 'ambiguous_seq_' + os.path.basename(fasta_file))
        ambiguous_letters = ["S", "M", "K", "R", "Y", "B", "D", "H", "V", "N", "Z", 'W']
        with open(fasta_file) as fl_r, open(filtered_fasta_file, 'w') as fl_keep, open(ambiguous_fasta_file, 'w') as fl_ambig:
            for record in SeqIO.parse(fl_r, "fasta"):
                header = record.description
                seq = str(record.seq)
                if any([True for l in ambiguous_letters if l in seq]):
                    s_ambiguous += 1
                    fl_ambig.write('>'+header+'\n')
                    fl_ambig.write(seq+'\n')
                else:
                    fl_keep.write('>'+header+'\n')
                    fl_keep.write(seq+'\n')
                    s_kept += 1

        logging.info(f'{fasta_file}: sequences {s_kept + s_ambiguous}')
        logging.info(f'{fasta_file}: ambiguous {s_ambiguous}')
        logging.info(f'{fasta_file}: unambiguous {s_kept}')
        
if __name__ == '__main__':
    main()
