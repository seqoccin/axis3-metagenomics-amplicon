#!/usr/bin/env nextflow


/*
 * Define helpMessage
 */

 def helpMessage() {

   log.info"""

   Usage:

     The typical command for running the pipeline is as follows:

       nextflow run [...]

     Mandatory arguments:
       --tax_level

     Other options:
       --outdir                      The output directory where the results will be saved.
       --help                        Show this message and exit.


     Options:
     [...]
   """.stripIndent()
 }

 // Show help message.
 params.help = false
if (params.help){
  helpMessage()
  exit 0
}


// directory where result files are going to be pubblished
params.results_dir = "results"
results_dir ="${params.results_dir}"

params.seq_dir = "./"
sequences = Channel.fromPath( "${params.seq_dir}/*.fna" )

sequences.into {makedb_seq ; seq}

makedb_seq.map { f -> [f.getSimpleName(),  f] }.into {makedb_seq_with_file_name; global_align_seq_with_file_name }
params.nb_thread = 2

params.blast_chunkSize = 10100
params.glob_aln_chunkSize = 150000
params.assembly_summary = false

assembly_summary = Channel.fromPath(params.assembly_summary)
process make_blast_db {
   cache 'deep'

   publishDir "${results_dir}/${name}", mode:'copy', pattern: 'all_seq_ids.txt'

   input:
     set val(name), file(seq_file) from makedb_seq_with_file_name
   output:
     set val(name), file(seq_file), file("*fna*") into blast_db_files
     tuple val(name), file("all_seq_ids.txt") into seq_ids

   """
   makeblastdb -in $seq_file -dbtype nucl

   # Get all sequence ids
   grep ^'>' $seq_file | cut -d'>' -f2 | cut -d' ' -f1 > all_seq_ids.txt


   """
 }

seq_chunks = seq.splitFasta(by: params.blast_chunkSize, file:true).map { f -> [f.getSimpleName(),  f] }



seq_db_and_chunk = blast_db_files.cross(seq_chunks).map {l -> [l[0][0], l[0][1], l[0][2], l[1][1] ] }
// seq_db_and_chunk.subscribe { println it}

//
process blast_all_vs_all {
   cache 'deep'
   cpus params.nb_thread
   
   input:
     set name, file(db), file("*"), file(seq_chunk) from seq_db_and_chunk
   output:
     set name, file("blast_result.tsv") into chunk_blast_results

   """

   outfmt="qseqid qstart qend qlen sseqid sstart send slen length pident qcovhsp qcovs evalue bitscore"


   # name="\${fna_file%.*}"
   blastn -db $db -query $seq_chunk   \
                  -outfmt "6 \$outfmt" \
                  -evalue 0.0005 \
                  -out blast_result.tsv \
                  -strand plus \
                  -num_threads ${params.nb_thread}

   """
 }

chunk_blast_results_grouped = chunk_blast_results.groupTuple()

params.pident_threshold = 90

params.cov_threshold = 90

 process merge_and_preprocess_blast_result {
   cache 'deep'
   tag "$name"

   publishDir "${results_dir}/${name}/", pattern: "blast_result_*.tsv"
   publishDir "${results_dir}/${name}/", mode:'copy',  pattern: "preprocessed_blast_result_*.tsv"
   publishDir "${results_dir}/${name}/", mode:'copy',  pattern: "seq_pairs_to_align_globally_*.tsv"

   input:
      set name, file("chunck_blast_result*") from chunk_blast_results_grouped
   output:
      file "blast_result_${name}.tsv" into blast_results
      file "preprocessed_blast_result_${name}.tsv" into preprocessed_blast_result
      tuple val(name),  file("seq_pairs_to_align_globally_${name}.tsv") into seq_pairs_to_align

    """
    outfmt="qseqid qstart qend qlen sseqid sstart send slen length pident qcovhsp qcovs evalue bitscore"
    # CONCAT BLAST RESULT

    for f in chunck_blast_result*; do
      cat \$f >> blast_result_${name}.tsv
    done

    pre_process_blast_results.py blast_result_${name}.tsv "\$outfmt" -o preprocessed_blast_result_${name}.tsv


    selected_seq_pairs_to_align_globally.py  preprocessed_blast_result_${name}.tsv \
                                            --pident_threshold ${params.pident_threshold} \
                                            --cov_threshold ${params.cov_threshold} \
                                            --outfmt "\$outfmt" \
                                            -o seq_pairs_to_align_globally_${name}.tsv -v

    """
 }

// Split seq to align to paral global aln
seq_and_ids_to_align = global_align_seq_with_file_name
                         .join(seq_pairs_to_align)
                         .splitText(by: params.glob_aln_chunkSize, elem:2,  file:true)




 process global_alignment {
   cache 'deep'
   tag "$name"

   //publishDir "${results_dir}/${name}/", mode:'copy',  pattern: "alignment_result*.tsv"

   input:
    tuple val(name), file(sequences), file(ids_to_align) from seq_and_ids_to_align

  output:
    file "${name}.alignment_result.tsv" into aln_results

    """
    echo $name
    global_alignment.py $sequences $ids_to_align -v -o ${name}.alignment_result.tsv



    """

 }

 // alignment_results = aln_results.collectFile(keepHeader:true) { f ->
 //        [ file(f).getName(), f.text ]}.map{f -> [f.getSimpleName(), f] } //.subscribe{ println "alignment_results_and_seq_ids >>$it"}
 //

alignment_results = aln_results.map{f -> [f.getSimpleName(), f] }
                                .groupTuple()
process concat_aln_result {
  publishDir "${results_dir}/$name/"
  input:
    tuple val(name), file("alignment_result/chunk*") from alignment_results
  output:
    tuple val(name), file("alignment_result.tsv") into alignment_results_concat
  """
  # Concatenation of alignment results
  alignment_result=alignment_result.tsv
  for f in alignment_result/chunk*;
  do
    if [ ! -f "alignment_result.tsv" ]; then
      echo "alignment_result.tsv does not exist. lets create it"
      head -n1 \$f > alignment_result.tsv
    fi
    tail --line=+2 \$f >> alignment_result.tsv
  done
  """
}

alignment_results_and_seq_ids = alignment_results_concat.join(seq_ids)
                                  // .subscribe{ println "alignment_results >>$it"}

assembly_summary.into{assembly_summary; assembly_summary_for_plot; test}
test.subscribe{ println "$it"}


alignment_results_seq_ids_and_selection = alignment_results_and_seq_ids.combine(assembly_summary) //.subscribe{ println "combine >>$it<<"}
// alignment_results_and_seq_ids .into{alignment_results_and_seq_ids; test}

// test.subscribe{ println "alignment_results_and_seq_ids >>$it"}
params.metric = "prct_ident"
 //
 process compute_taxonomic_resolution {
   cache 'deep'
   publishDir "${results_dir}/tax_resolution/${assembly_selection.getSimpleName()}", mode:'copy', pattern: "tax_resolution.tsv", saveAs: { f -> "${name}_tax_resolution.tsv"}
   publishDir "${results_dir}/$name/${assembly_selection.getSimpleName()}/", mode:'copy', pattern: "pairs_${params.metric}.tsv"
   publishDir "${results_dir}/$name/${assembly_selection.getSimpleName()}/", mode:'copy', pattern: "seq_ids_filtered.txt"
   publishDir "${results_dir}/$name/${assembly_selection.getSimpleName()}/", mode:'copy', pattern: "tax_resolution.tsv"
   input:
     tuple val(name), file(alignment_result), file(all_seq_ids), file(assembly_selection) from alignment_results_seq_ids_and_selection
  output:
    file "tax_resolution.tsv" into taxonomic_resolutions
    file "pairs_${params.metric}.tsv" into edges_list
    file "seq_ids_filtered.txt" into seq_id_selection_specific

    """
    selection_name="${assembly_selection.getSimpleName()}"

    filter_seq_pairs_ident.py --seq_pair_file $alignment_result \
                        --assembly_info $assembly_selection \
                        --seq_ids_list $all_seq_ids \
                        --out_pair_fl alignment_result_filtered.tsv\
                        --out_ids_list  seq_ids_filtered.txt


    compute_taxonomic_resolution.py --seq_pair_identity alignment_result_filtered.tsv \
                                    --assembly_info $assembly_selection \
                                    --seq_ids_list seq_ids_filtered.txt\
                                    --metric ${params.metric} -v \
                                    -o tax_resolution.tsv\
                                    -e pairs_${params.metric}.tsv

    """

 }

 taxonomic_resolutions_collect = taxonomic_resolutions.collect()

 params.resolution_pident = 95


 process plot_resolution {
   cache 'deep'

  publishDir "${results_dir}/", mode:'copy', pattern: "tax_resolution*.html"
  publishDir "${results_dir}/", mode:'copy', pattern: "*.krona_resolution_tax.html", saveAs: { f -> file(f).getSimpleName()+"/krona_resolution_tax.html"}

   input:
     file 'tax_resolution_results/*_tax_resolution.tsv' from taxonomic_resolutions_collect
     file "assemblies_selections/*" from assembly_summary_for_plot
   output:
     file "*.html" into taxresolution_plot

    """

    for assembly_selection in assemblies_selections/*;
    do
      plot_tax_resolution.py --prct_ident_threshold  ${params.resolution_pident} \
                              --tax_resolution_dir 'tax_resolution_results/' --assembly_summary \$assembly_selection
    done

    # PERL5LIB="" # needed to let krona works in the conda env https://github.com/bioconda/bioconda-recipes/issues/4390

    for f in *tax_resolution.tsv; do
      region_name=`basename \$f _tax_resolution.tsv`
      # mkdir \$region_name
      # assembly_summary_to_resolution_krona.py --assemblies_summary_taxonomy $assembly_summary  \
      #                                        --resolution_tax_result \$f -o 'formatted_assembly_summary.tsv' \
      #                                        --pident_threshold ${params.resolution_pident}

      # make_krona_xml.py formatted_assembly_summary.tsv --color_label unambiguously_identified --color_by_default > resolution_tax_krona.xml
      # ktImportXML resolution_tax_krona.xml -o \${region_name}.krona_resolution_tax.html
    done
    """
 }
