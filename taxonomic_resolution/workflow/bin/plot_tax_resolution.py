#!/usr/bin/env python3

"""


:Example:
$ python
"""

# Metadata
__author__ = 'Mainguy Jean - Plateforme bioinformatique Toulouse'
__copyright__ = 'Copyright (C) 2019 INRA'
__license__ = 'GNU General Public License'
__version__ = '0.1'
__email__ = 'support.bioinfo.genotoul@inra.fr'
__status__ = 'dev'


import plotly.graph_objects as go
import pandas as pd
from argparse import ArgumentParser, ArgumentDefaultsHelpFormatter
import logging
import os
import csv


def parse_arguments():
    parser = ArgumentParser(description="...",
                            formatter_class=ArgumentDefaultsHelpFormatter)
    parser.add_argument("--prct_ident_threshold", type=float, default=95,
                        help="Threshold of taxonomic resolution.")
    parser.add_argument("--tax_resolution_dir", type=str, default='./',
                        help="directory where taxonomic resolution results are stored")
    parser.add_argument("--assembly_summary", type=str, required=True,
                        help="Assembly summary to get the total number of species")

    parser.add_argument("-v", "--verbose", help="increase output verbosity",
                        action="store_true")

    parser.add_argument("-o", "--output", help="output file name",
                        default='resolution_taxonomic.html')

    args = parser.parse_args()
    return args


def main():
    args = parse_arguments()

    # Set the logging level. When the flag verbose is defined to true
    # logging are displayed from level info
    if args.verbose:
        logging.basicConfig(format="%(levelname)s: %(message)s", level=logging.DEBUG)
        logging.info("Verbose output.")
    else:
        logging.basicConfig(format="%(levelname)s: %(message)s")

    result_dir = args.tax_resolution_dir
    prct_ident_threshold = args.prct_ident_threshold
    assembly_summary = args.assembly_summary

    with open(assembly_summary) as fl:
        reader = csv.DictReader(fl, delimiter='\t')
        species_taxid = {d['species_taxid'] for d in reader}
    total_nb_sp = len(species_taxid)
    logging.info(f'Analysis is based on {total_nb_sp} species')

    result_files = [f
                    for f in os.listdir(result_dir) if f.endswith('.tsv')]

    if not result_files:
        raise NameError(f'No tsv file found in tax_resolution_dir {tax_resolution_dir}')

    # Parse resolution tax result files
    result_by_region = {}
    for f in result_files:
        fl_path = os.path.join(result_dir, f)
        region = f.replace('_tax_resolution.tsv', '')
        data_tb = pd.read_csv(fl_path, sep='\t')

        data_tb["tax_resolution_over_totalsp"] = 100*data_tb['nb_taxid_ok']/total_nb_sp

        result_by_region[region] = data_tb

    # Tax resolution CURVE
    fig = go.Figure()

    for region, df in result_by_region.items():
        logging.info(region)
        fig.add_trace(go.Scatter(x=df['prct_ident_threshold'], y=df['nb_taxid_ok'],
                                 mode='lines',
                                 name=region,
                                 text=df['tax_resolution_over_totalsp'],))

    fig.add_trace(go.Scatter(x=[90, 100], y=[total_nb_sp, total_nb_sp],
                             mode='lines',
                             name='total sp'))

    fig.update_layout(title="Taxonomic resolution: Misleading result as global aln have been performed on seq pairs with blast cov and ident > 90%",
                      yaxis_title="# of unambiguously identified species", xaxis_title="Percentage of identity",)

    fig.update_xaxes(range=[90, 100])
    fig.write_html('tax_resolution_curve.html')

    # Tax resolution at 95%
    tax_identified_dict = {}
    tax_not_identified_dict = {}
    tax_cov_dict = {}
    for region, df in result_by_region.items():

        nb_taxid_identified = df[df["prct_ident_threshold"] == prct_ident_threshold]['nb_taxid_ok']
        tax_identified_dict[region] = float(nb_taxid_identified)

        nb_taxid_not_identified = df[df["prct_ident_threshold"]
                                     == prct_ident_threshold]['nb_taxid_not_ok']

        tax_not_identified_dict[region] = float(nb_taxid_not_identified)

        tax_cov_dict[region] = float(nb_taxid_identified) + float(nb_taxid_not_identified)

    regions = sorted(tax_identified_dict, key=tax_identified_dict.get, reverse=False)

    logging.warning(regions)

    showlegend = True if len(regions) < 20 else False

    color = "cadetblue"
    traces = []
    for region in regions:
        df = result_by_region[region]
        # df[df["prct_ident_threshold"] == prct_ident_threshold]['nb_taxid_ok']
        nb_taxid_ok = tax_identified_dict[region]
        nb_taxid_not_ok = tax_not_identified_dict[region]

        taxr = float(nb_taxid_ok) / total_nb_sp * 100
        logging.info(region)
        logging.info(f'{taxr}')

        trace = go.Bar(
            x=[region],
            y=[nb_taxid_ok],
            showlegend=False,
            marker_color=color, opacity=0.9,
            name="All filtered regions",
            text=f'{region}<br>resolution:{round(taxr,2)}% ({int(nb_taxid_ok)}/{total_nb_sp})',
            # text=regions_to_plot
        )
        traces.append(trace)
        coverage = ((nb_taxid_not_ok + nb_taxid_ok)/total_nb_sp)*100
        trace = go.Bar(
            x=[region],
            y=[nb_taxid_not_ok],
            showlegend=False,
            marker_color=color, opacity=0.5,
            name="All filtered regions",
            text=f'{region}<br>coverage:{round(coverage,2)} ({int(nb_taxid_not_ok +nb_taxid_ok)}/{total_nb_sp})',
            # text=regions_to_plot
        )
        traces.append(trace)

    trace = go.Scatter(x=[regions[0], regions[-1]], y=[total_nb_sp, total_nb_sp],
                       mode='lines',
                       name='total sp')
    traces.append(trace)

    fig = go.Figure(data=traces)

    fig.update_layout(title="Taxonomic resolution",
                      yaxis_title="Number of species")
    fig.update_xaxes(showticklabels=showlegend)
    # fig.update_xaxes(tickfont=dict(size=10))
    fig.update_yaxes(automargin=True)
    fig.update_yaxes(range=[-total_nb_sp/30, total_nb_sp+total_nb_sp/30])
    fig.update_layout(barmode='stack')

    # fig.update_layout(showlegend=False)
    html_file = f'tax_resolution_{round(prct_ident_threshold)}.html'
    html_file = f'tax_resolution.html'
    logging.info(f'Write {html_file}')
    fig.write_html(html_file)


if __name__ == '__main__':
    main()
