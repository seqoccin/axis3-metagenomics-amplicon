#!/usr/bin/env python3

"""
Description

:Example:
filter_seq_pairs_ident.py --seq_pair_file $alignment_result \
                  --assembly_info \$assembly_selection \
                  --seq_ids_list $all_seq_ids \
                  --out_pair_fl \${selection_name}.${name}.$all_seq_ids\
                  --out_ids_list  \${selection_name}.${name}.$all_seq_ids
"""

# Metadata
__author__ = 'Mainguy Jean - Plateforme bioinformatique Toulouse'
__copyright__ = 'Copyright (C) 2019 INRA'
__license__ = 'GNU General Public License'
__version__ = '0.1'
__email__ = 'support.bioinfo.genotoul@inra.fr'
__status__ = 'dev'


from argparse import ArgumentParser, ArgumentDefaultsHelpFormatter
import logging
import csv


def parse_arguments():
    """Parse script arguments."""
    parser = ArgumentParser(description="...",
                            formatter_class=ArgumentDefaultsHelpFormatter)
    parser.add_argument("--seq_pair_file", type=str, required=True,
                        help="")
    parser.add_argument("--assembly_info", type=str, required=True,
                        help="")
    parser.add_argument("--seq_ids_list", type=str, required=True,
                        help="")
    parser.add_argument("--out_pair_fl", type=str, required=True,
                        help="")
    parser.add_argument("--out_ids_list", type=str, required=True,
                        help="")

    parser.add_argument("-v", "--verbose", help="increase output verbosity",
                        action="store_true")

    args = parser.parse_args()
    return args


def main():

    args = parse_arguments()

    if args.verbose:
        logging.basicConfig(format="%(levelname)s: %(message)s", level=logging.DEBUG)
        logging.info('Mode verbose ON')

    else:
        logging.basicConfig(format="%(levelname)s: %(message)s")

    seq_pair_file = args.seq_pair_file
    print(seq_pair_file)
    assembly_info = args.assembly_info
    seq_ids_list = args.seq_ids_list
    out_pair_fl = args.out_pair_fl
    out_ids_list = args.out_ids_list

    with open(assembly_info) as fl:
        reader = csv.DictReader(fl, delimiter='\t')
        assemblies_to_keep = [d['assembly_accession'] for d in reader]

    with open(seq_ids_list, 'r') as fl, open(out_ids_list, 'w') as out:
        for l in fl:
            assembly = l.split('|')[0]
            if assembly in assemblies_to_keep:
                out.write(l)

    with open(seq_pair_file) as fl, open(out_pair_fl, 'w') as out:
        reader = csv.DictReader(fl, delimiter='\t')
        writer = csv.DictWriter(out, fieldnames=reader.fieldnames, delimiter='\t')
        writer.writeheader()
        for d in reader:
            assembly1 = d['seq1'].split('|')[0]
            assembly2 = d['seq2'].split('|')[0]
            if assembly1 in assemblies_to_keep and assembly2 in assemblies_to_keep:
                writer.writerow(d)


if __name__ == '__main__':
    main()
