#!/usr/bin/env python3

"""
Pairwise global alignment of sequences

:Example:
python
"""

# Metadata
__author__ = 'Mainguy Jean - Plateforme bioinformatique Toulouse'
__copyright__ = 'Copyright (C) 2019 INRA'
__license__ = 'GNU General Public License'
__version__ = '0.1'
__email__ = 'support.bioinfo.genotoul@inra.fr'
__status__ = 'dev'


from argparse import ArgumentParser, ArgumentDefaultsHelpFormatter
import logging
# from Bio import pairwise2
from Bio import SeqIO
from edlib import align


def parse_arguments():
    """Parse script arguments."""
    parser = ArgumentParser(description="...",
                            formatter_class=ArgumentDefaultsHelpFormatter)
    parser.add_argument("sequences", type=str,  help="sequence file in fasta format")
    parser.add_argument("seq_pair_ids", type=str,  help="pair of sequence ids to align")
    parser.add_argument("-o", '--outfile', type=str,
                        default='global_aln_result.tsv',  help="result file")
    parser.add_argument("-v", "--verbose", help="increase output verbosity",
                        action="store_true")

    args = parser.parse_args()
    return args


def main():

    args = parse_arguments()
    
    if args.verbose:
        logging.basicConfig(format="%(levelname)s: %(message)s", level=logging.DEBUG)
        logging.info('Mode verbose ON')

    else:
        logging.basicConfig(format="%(levelname)s: %(message)s")

    fasta_file = args.sequences
    outfile = args.outfile
    seq_pair_ids_file = args.seq_pair_ids

    with open(seq_pair_ids_file) as fl:
        pairs_to_align = [tuple(l.strip().split('\t')) for l in fl]

    logging.info(f'{len(pairs_to_align)} alignments to make')

    #record_dict = SeqIO.index(fasta_file, "fasta")
    record_dict = SeqIO.to_dict(SeqIO.parse(fasta_file, "fasta"))

    alignment_results = []
    for id1, id2 in pairs_to_align:
        # aln = pairwise2.align.globalxx(record_dict[id1].seq,
        #                                record_dict[id2].seq,
        #                                one_alignment_only=True, score_only=False)

        # Use of module edible
        aln_result = align(record_dict[id1].seq, record_dict[id2].seq, task='path')
        aln_len = sum([int(i) for i in aln_result['cigar'].replace('=', ',').replace(
            'D', ',').replace('X', ',').replace('I', ',').split(',')[:-1]])
        distance = aln_result['editDistance']

        alignment_results.append((id1, id2, str(distance), str(aln_len)))

    with open(outfile, 'w') as fl:
        fl.write('seq1\tseq2\tdistance\taln_length\n')
        for l in alignment_results:

            fl.write('\t'.join(l)+'\n')


if __name__ == '__main__':
    main()
