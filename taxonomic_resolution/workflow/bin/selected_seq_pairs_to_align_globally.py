#!/usr/bin/env python3

"""
Description

:Example:
python template.py -v
"""

# Metadata
__author__ = 'Mainguy Jean - Plateforme bioinformatique Toulouse'
__copyright__ = 'Copyright (C) 2019 INRA'
__license__ = 'GNU General Public License'
__version__ = '0.1'
__email__ = 'support.bioinfo.genotoul@inra.fr'
__status__ = 'dev'


from argparse import ArgumentParser, ArgumentDefaultsHelpFormatter
import logging
import csv


def parse_arguments():
    """Parse script arguments."""
    parser = ArgumentParser(description="...",
                            formatter_class=ArgumentDefaultsHelpFormatter)
    parser.add_argument("preprocessed_blast_result", help="preprocessed blast result", type=str)
    parser.add_argument("-p", "--pident_threshold",
                        help="Pair of sequence have to have a prct identity greater than this threshold to be selected",
                        type=float, default=90)
    parser.add_argument("-c", "--cov_threshold",
                        help="Pair of sequence have to have a coverage greater than this threshold to be selected",
                        type=float, default=90)
    parser.add_argument("--outfmt", help="Column names seperated by space of the preprocessed_blast_result",
                        default='qseqid qstart qend qlen sseqid sstart send slen length pident qcovhsp qcovs evalue bitscore',
                        type=str)
    parser.add_argument("-o", "--outfile",
                        help="Column names seperated by space of the preprocessed_blast_result",
                        default='selected_pair_of_sequence.txt',
                        type=str)
    parser.add_argument("-v", "--verbose", help="increase output verbosity",
                        action="store_true")

    args = parser.parse_args()
    return args


def main():

    args = parse_arguments()

    if args.verbose:
        logging.basicConfig(format="%(levelname)s: %(message)s", level=logging.DEBUG)
        logging.info('Mode verbose ON')

    else:
        logging.basicConfig(format="%(levelname)s: %(message)s")

    preprocessed_blast_result = args.preprocessed_blast_result
    selected_pairs_file = args.outfile

    pident_threshold = args.pident_threshold
    cov_threshold = args.cov_threshold

    outfmt = args.outfmt.split(' ')
    cpt_selec = 0
    cpt_all = 0
    with open(preprocessed_blast_result) as tsvfile, open(selected_pairs_file, 'w') as select_fh:
        reader = csv.DictReader(tsvfile, delimiter='\t', fieldnames=outfmt)
        for l in reader:
            cpt_all += 1
            if float(l['pident']) >= pident_threshold and float(l['qcovs']) >= cov_threshold:
                ids = [l['qseqid'], l['sseqid']]
                select_fh.write('\t'.join(ids)+'\n')
                cpt_selec += 1

    logging.info(f'Selected sequences pairs {cpt_selec}/{cpt_all}')


if __name__ == '__main__':
    main()
