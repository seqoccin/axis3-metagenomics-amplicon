#!/usr/bin/env python3

"""
Description

:Example:
python
"""

# Metadata
__author__ = 'Mainguy Jean - Plateforme bioinformatique Toulouse'
__copyright__ = 'Copyright (C) 2019 INRA'
__license__ = 'GNU General Public License'
__version__ = '0.1'
__email__ = 'support.bioinfo.genotoul@inra.fr'
__status__ = 'dev'

import networkx as nx
import csv
import os
from collections import defaultdict
from argparse import ArgumentParser, ArgumentDefaultsHelpFormatter
import logging


def get_ambiguous_taxids(G, assembly_to_sptaxid):
    taxid_not_ok = set()
    for sub_graph in nx.connected_components(G):
        sub_graph_taxid = {assembly_to_sptaxid[n.split('|')[0]] for n in sub_graph if n.split('|')[
            0] in assembly_to_sptaxid}
        if len(sub_graph_taxid) > 1:
            taxid_not_ok |= sub_graph_taxid
    return taxid_not_ok


def get_edges_old(seq_pair_ident_fl, metric):
    """"Functions used to parse edges from obitools scripts"""
    egdes = defaultdict(list)
    with open(seq_pair_ident_fl, encoding="latin-1") as fl:
        for l in fl:
            seq1, seq2, dist, ali_len = l.strip().split()
            identity = float(ali_len) - float(dist)

            # distance_egdes_w[int(dist)].append((seq1, seq2, int(dist)))
            if metric == 'distance':
                egdes[int(float(dist))].append((seq1, seq2))
            else:  # metric == 'prct_ident':
                egdes[round(float(identity)/float(ali_len)*100)].append((seq1, seq2))
    return egdes


def get_edges(seq_pair_ident_fl, metric):
    """Parse alignment result and build the edge dict."""
    egdes = defaultdict(list)
    with open(seq_pair_ident_fl) as fl:
        reader = csv.DictReader(fl, delimiter='\t')
        for d in reader:
            seq1 = d['seq1']
            seq2 = d['seq2']
            dist = d['distance']
            ali_len = d['aln_length']

            identity = float(ali_len) - float(dist)

            # distance_egdes_w[int(dist)].append((seq1, seq2, int(dist)))
            if metric == 'distance':
                egdes[int(float(dist))].append((seq1, seq2))
            else:  # metric == 'prct_ident':
                egdes[round(float(identity)/float(ali_len)*100)].append((seq1, seq2))
    return egdes


def get_assembly_to_sp_taxid_dict(assembly_file):
    assembly_to_sptaxid = {}
    with open(assembly_file) as fl:
        reader = csv.DictReader(fl, delimiter='\t')
        for d in reader:
            assembly_to_sptaxid[d['assembly_accession']] = d['species_taxid']
    return assembly_to_sptaxid


def get_sequence_names_from_ecopcr(ecopcr_result):

    with open(ecopcr_result, encoding="latin-1") as fl:
        seqnames = [l.split(' | ')[0].strip() for l in fl if not l.startswith('#')]
    return seqnames


def get_sequence_names_from_ecopcr_old(ecopcr_result,  add_seq_length=False):
    uniq_seq_names = []
    if add_seq_length:
        uniq_seq_names = {}
    previous_seqname = ''
    i = 1
    with open(ecopcr_result, encoding="latin-1") as fl:
        for l in fl:
            if l.startswith('#'):
                continue
            seqname = l.split(' | ')[0].strip()

            if seqname == previous_seqname:
                i += 1
                uniq_seqname = f'{seqname}.{i}'
            else:
                previous_seqname = seqname
                i = 1
                uniq_seqname = f'{seqname}.{i}'
            if add_seq_length:
                seq_length = l.split(' | ')[19].strip()
                uniq_seq_names[uniq_seqname] = int(seq_length)
            else:
                uniq_seq_names.append(uniq_seqname)

    # assert len(set(uniq_seq_names)) == len(uniq_seq_names)
    return uniq_seq_names


def get_prct_from_distance(distance_egdes, sequence_length_dict):
    prct_id_edges = defaultdict(list)
    for dist, seq_tuples in distance_egdes.items():
        for seq1, seq2 in seq_tuples:
            longuest_length = max(sequence_length_dict[seq1], sequence_length_dict[seq2])
            # prct_id = nb identical bases / longuest_length
            prct_id = (longuest_length - dist) / longuest_length
            prct_id = round(prct_id)
            prct_id_edges[int(prct_id)].append((seq1, seq2))

    return prct_id_edges


def parse_arguments():
    parser = ArgumentParser(description="...",
                            formatter_class=ArgumentDefaultsHelpFormatter)

    parser.add_argument("--seq_pair_identity", type=str, required=True,
                        help='Result of the global alignment. TSV file with not header and column: seqid1 seqid2 distance alignment_length')
    parser.add_argument("--assembly_info", type=str, required=True,
                        help='')

    parser.add_argument("--seq_ids_list", type=str, required=True,
                        help='All sequence ids use to construct the full tree. ')
    parser.add_argument('--metric', default='prct_ident',
                        choices=['distance', 'prct_ident'],
                        help='Resolution based on distance or identity percentage')
    parser.add_argument("-v", "--verbose", help="increase output verbosity",
                        action="store_true")
    parser.add_argument("-o", "--outfile", help="output file", default="tax_resolution.tsv")
    parser.add_argument("-e", "--edges_list_file", help="edges list", default=None)

    args = parser.parse_args()
    return args


def main():

    args = parse_arguments()
    if args.verbose:
        logging.basicConfig(format="%(levelname)s: %(message)s", level=logging.DEBUG)
        logging.info('Mode verbose ON')

    else:
        logging.basicConfig(format="%(levelname)s: %(message)s")

    assembly_info = args.assembly_info
    seq_pair_ident_fl = args.seq_pair_identity
    metric = args.metric
    out_file = args.outfile
    seq_ids_list = args.seq_ids_list
    edges_list_file = args.edges_list_file

    # if metric == 'distance':
    #     out_file = os.path.join(outdir, 'distance_resolution_tax.tsv')
    # else:
    #     out_file = os.path.join(outdir, 'prct_id_resolution_tax.tsv')

    assembly_to_sptaxid = get_assembly_to_sp_taxid_dict(assembly_info)

    edges = get_edges(seq_pair_ident_fl, metric)
    logging.info(f"{len(edges)} edges have been retrieved.")
    if edges_list_file:
        with open(edges_list_file, 'w') as fl:
            for ident, pairs in edges.items():
                for p in pairs:
                    pairs = '\t'.join(p)
                    fl.write(f"{pairs}\t{ident}\n")

    nb_taxid_not_ok_by_dist = {}
    taxid_not_ok_by_dist = {}
    # TODO get sequence name
    with open(seq_ids_list) as fl:
        sequences_names = [l.strip() for l in fl if l.strip()]
        logging.info(f'{len(sequences_names)} sequences ids.')

    G = nx.Graph()
    G.add_nodes_from(sequences_names)
    all_taxid = {assembly_to_sptaxid[n.split('|')[0]]
                 for n in G if n.split('|')[0] in assembly_to_sptaxid}

    taxid_not_ok = []
    logging.info(edges.keys())

    if metric == 'distance':
        range_threshold = range(0, max(edges)+1)
    else:
        range_threshold = range(100, 0, -1)

    for i in range_threshold:
        # G.add_weighted_edges_from(distance_egdes_w[i])
        if len(edges[i]) == 0:
            nb_taxid_not_ok_by_dist[i] = len(taxid_not_ok)
            taxid_not_ok_by_dist[i] = taxid_not_ok
            continue
        G.add_edges_from(edges[i])

        taxid_not_ok = get_ambiguous_taxids(G, assembly_to_sptaxid)
        taxid_not_ok_by_dist[i] = taxid_not_ok

        nb_taxid_not_ok_by_dist[i] = len(taxid_not_ok)

    # All nodes have been added at the beginning.
    # Size of G should not be bigger than the initial nodes
    assert len(G) == len(sequences_names), f"Graph is bigger than the total number of sequences len(G) {len(G)} != len(sequences_names) {len(sequences_names)}"

    with open(out_file, 'w') as fl:
        headers = [f'{metric}_threshold',
                   'nb_taxid_ok',
                   'nb_taxid_not_ok',
                   'resolution_tax',
                   'taxids_ok']
        fl.write('\t'.join(headers)+"\n")
        for i in range_threshold:
            taxid_not_ok = taxid_not_ok_by_dist[i]
            taxid_ok = all_taxid - set(taxid_not_ok)
            nb_taxid_not_ok = nb_taxid_not_ok_by_dist[i]
            nb_taxid_ok = len(all_taxid) - nb_taxid_not_ok
            resolution_tax = round(nb_taxid_ok / len(all_taxid) * 100, 2)
            result_list = [i, nb_taxid_ok, nb_taxid_not_ok,
                           resolution_tax, ','.join(taxid_ok)]
            fl.write('\t'.join([str(i) for i in result_list]) + '\n')


if __name__ == '__main__':
    main()
