#!/usr/bin/env python3

"""
Pre process blast results.

:Example:
python
"""

# Metadata
__author__ = 'Mainguy Jean - Plateforme bioinformatique Toulouse'
__copyright__ = 'Copyright (C) 2020 INRAE'
__license__ = 'GNU General Public License'
__version__ = '0.1'
__email__ = 'support.bioinfo.genotoul@inra.fr'
__status__ = 'dev'



from argparse import ArgumentParser, ArgumentDefaultsHelpFormatter
import csv
import os
import logging


def get_best_hsp(hsps):
    return sorted(hsps, key=lambda t: float(t['bitscore']), reverse=True)[0]


def parse_arguments():
    parser = ArgumentParser(description="...",
                            formatter_class=ArgumentDefaultsHelpFormatter)

    parser.add_argument("blast_result_file", type=str,
                        help="")
    parser.add_argument("bast_outfmt", type=str,
                        help="")
    parser.add_argument("-o", '--outfile', type=str, default="preprocessed_blast_result.tsv",
                        help="")
    parser.add_argument("-v", "--verbose", help="increase output verbosity",
                        action="store_true")
    args = parser.parse_args()
    return args


def main():

    args = parse_arguments()
    if args.verbose:
        logging.basicConfig(format="%(levelname)s: %(message)s", level=logging.DEBUG)
        logging.info('Mode verbose ON')

    else:
        logging.basicConfig(format="%(levelname)s: %(message)s")

    blast_result_file = args.blast_result_file
    outfmt_list = args.bast_outfmt.split(' ')
    preprocess_result_file = args.outfile

    with open(blast_result_file) as fl, open(preprocess_result_file, 'w') as out:

        reader = csv.DictReader(fl, delimiter='\t', fieldnames=outfmt_list)
        writer = csv.DictWriter(out, fieldnames=outfmt_list, delimiter='\t')

        tuple_ids = None
        hsps = []
        for d in reader:
            logging.debug('\n'.join([f"{k}\t{v}"for k, v in d.items()]))
            if d['qseqid'] == d['sseqid']:
                logging.debug(f"{d['qseqid']} == {d['sseqid']}")
                continue
            if int(d['qlen']) < int(d['slen']):
                logging.debug(f" qlen {d['qlen']} <  slen {d['slen']}")
                continue
            elif int(d['qlen']) == int(d['slen']) and d['qseqid'] < d['sseqid']:
                logging.debug(f" qlen {d['qlen']} ==  slen {d['slen']} but {d['qseqid']} < {d['sseqid']}")
                continue

            if not tuple_ids:
                tuple_ids = (d['qseqid'], d['sseqid'])

            if tuple_ids and tuple_ids != (d['qseqid'], d['sseqid']):
                hsp = get_best_hsp(hsps)
                writer.writerow(hsp)
                tuple_ids = (d['qseqid'], d['sseqid'])
                hsps = []

            hsps.append(d)

        if tuple_ids:
            hsp = get_best_hsp(hsps)
            writer.writerow(hsp)
        hsps.append(d)


if __name__ == '__main__':
    main()
