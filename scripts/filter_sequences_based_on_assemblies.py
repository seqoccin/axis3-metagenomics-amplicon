#!/usr/bin/env python3

"""
Filter sequences based on assembly selections.
"""

# Metadata
__author__ = 'Mainguy Jean - Plateforme bioinformatique Toulouse'
__copyright__ = 'Copyright (C) 2019 INRA'
__license__ = 'GNU General Public License'
__version__ = '0.1'
__email__ = 'support.bioinfo.genotoul@inra.fr'
__status__ = 'dev'


from argparse import ArgumentParser, ArgumentDefaultsHelpFormatter
import logging
import csv
import os
from Bio import SeqIO


def parse_arguments():
    """Parse script arguments."""
    parser = ArgumentParser(description="...",
                            formatter_class=ArgumentDefaultsHelpFormatter)
    parser.add_argument('--assembly_selection', help="Assembly selection: tsv file with at least a column 'assembly_accession' that we want to keep")
    parser.add_argument('-s', "--fasta_files", nargs='+', required=True)
    parser.add_argument('-o', "--outdir", default='./')
    parser.add_argument("-v", "--verbose", help="increase output verbosity",
                        action="store_true")

    args = parser.parse_args()
    return args


def main():

    args = parse_arguments()

    if args.verbose:
        logging.basicConfig(format="%(levelname)s: %(message)s", level=logging.DEBUG)
        logging.info('Mode verbose ON')

    else:
        logging.basicConfig(format="%(levelname)s: %(message)s")

    fasta_files = args.fasta_files
    assembly_summary = args.assembly_selection
    outdir = args.outdir

    logging.info(f'{len(fasta_files)} fasta files provided')

    with open(assembly_summary) as fl:
        reader = csv.DictReader(fl, delimiter='\t')
        assemblies_to_keep = [d['assembly_accession'] for d in reader]

    logging.info(f'keep sequences from {len(assemblies_to_keep)} assemblies')

    # output file
    output_dir = os.path.join(outdir, os.path.basename(assembly_summary).split('.')[
        0])
    os.makedirs(output_dir, exist_ok=True)
    logging.info(f'Filtered sequences will be stored in {output_dir}')

    for fasta_file in fasta_files:
        logging.info(f'Process of {fasta_file}')
        s_removed = 0
        s_kept = 0
        if fasta_file.endswith('fna') or fasta_file.endswith('fatsa'):
            filtered_fasta_file = os.path.join(output_dir, os.path.basename(fasta_file))
            logging.info(f'Write filtered seq file in {filtered_fasta_file}')
            with open(fasta_file) as fl_r, open(filtered_fasta_file, 'w') as fl_w:
                for record in SeqIO.parse(fl_r, "fasta"):
                    header = record.id
                    assembly = header.split('|')[0]

                    if assembly in assemblies_to_keep:
                        fl_w.write(f">{record.description}\n")
                        fl_w.write(f"{record.seq}\n")
                        s_kept += 1
                    else:
                        s_removed += 1

            logging.info(f'sequences processed {s_kept + s_removed} : remove {s_removed} kept {s_kept}')
        else:
            logging.warning(f'{fasta_file} does not ends with fasta extension')


if __name__ == '__main__':
    main()
